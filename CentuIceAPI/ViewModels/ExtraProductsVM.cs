﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class ExtraProductsVM
    {
        public Guid ExtraProductsTakenID { get; set; }
        public Guid ProductID { get; set; }
        public string ProductName {  get; set; }
        public Guid DeliveryTripSheetID { get; set; }
        public int QuantityTaken { get; set; }
        public int QuantityRemaining { get; set; }
    }
}
