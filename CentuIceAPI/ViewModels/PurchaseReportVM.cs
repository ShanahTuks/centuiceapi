﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class PurchaseReportVM
    {
        public DateTime DateOrdered { get; set; }
        public DateTime? DateReceived { get; set; }
        public string SupplierName { get; set;}
        public decimal TotalCost { get; set; }
    }
}
