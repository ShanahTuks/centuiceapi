﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class ProductPopularityReportVM
    {
        public string ProductName { get; set; }
        public int BagsSold { get; set; }
    }
}
