﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class CreateInventoryVM
    {
        public Guid InventoryID { get; set; }
        public Guid SupplierID { get; set; }
        public Guid InventoryTypeID { get; set; }
        public Guid TableStatusID { get; set; }
        public Decimal ItemPrice { get; set; }
        public string ItemName { get; set; }

        public int Quantity { get; set; }
    }
}
