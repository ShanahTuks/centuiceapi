﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class TripSheetGroupVM
    {
        public Guid Key {  get; set; }
        public List<UpdateDeliveryGroupVM> DeliveryGroups { get; set; }
    }
}
