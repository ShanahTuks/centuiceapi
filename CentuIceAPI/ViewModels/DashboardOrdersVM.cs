﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class DashboardOrdersVM
    {
       public Guid OrderID { get; set; }
       public string ClientName { get; set; }
       public string Address { get; set; }
        public string OrderStatus { get; set; }
        public decimal OrderTotal { get; set; }
    }
}
