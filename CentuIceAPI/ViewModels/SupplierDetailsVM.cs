﻿using CentuIceAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class SupplierDetailsVM
    {
        public Guid SupplierID { get; set; }
        public string SupplierName { get; set; }
        public string GoodsSupplied { get; set; }

        public List<SupplierContactPerson> SupplierContactPeople { get; set; }
    }
}
