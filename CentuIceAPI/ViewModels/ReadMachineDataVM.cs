﻿using System;
using CsvHelper.Configuration.Attributes;

namespace CentuIceAPI.ViewModels
{
    public class ReadMachineDataVM
    {
        [Name("Time")]
        public string Date { get; set; }
        [Name("Cycles")]
        public string Cycles { get; set; }
        [Name("Weight")]
        public string Weight { get; set; }
        [Name("WTR Use")]
        public string WaterUse { get; set; }
        [Name("Full Bin")]
        public string FullBin { get; set; }
        [Name("Run Time")]
        public string RunTime { get; set; }
        [Name("Avg Frz")]
        public string AVGCycleMin { get; set; }
    }
}
