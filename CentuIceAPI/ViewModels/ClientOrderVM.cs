﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class ClientOrderVM
    {
        public Guid OrderID { get; set; }
        public string ClientName {  get; set; }
        public DateTime OrderDate {  get; set; }
        public decimal OrderTotal {  get; set; }
        
        public AddressVM ClientAddress {  get; set; }
        public List<ClientOrderLineVM> OrderLines {  get; set; }
        public List<OrderExtraProductVM> OrderExtraProducts {  get; set; }
    }
}
