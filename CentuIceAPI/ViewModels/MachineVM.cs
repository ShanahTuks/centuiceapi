﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.ViewModels
{
    public class MachineVM
    {
        public Guid MachineID { get; set; }
        public Guid MachineTypeID { get; set; }
        public Guid TableStatusID { get; set; }
        [StringLength(100)]
        public string Make { get; set; }
        [StringLength(100)]
        public string Model { get; set; }
        public DateTime Last_Service_Date { get; set; }
        public DateTime Year_Made { get; set; }
        public string MachineType { get; set; }
        public string MachineStatus { get; set; }
    }
}
