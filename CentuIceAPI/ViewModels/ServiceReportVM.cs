﻿using System;
using System.Collections.Generic;
namespace CentuIceAPI.ViewModels
{
    public class ServiceReportVM
    {
        public string VehicleReg { get; set; }
        public string MachineModel { get; set; }
        public List<JobsServiceReportVM> JobList { get; set; }
    }
}
