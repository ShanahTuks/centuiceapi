﻿using System;
using System.Collections.Generic;
using CentuIceAPI.Models;
namespace CentuIceAPI.ViewModels

{
    public class ServiceFrequencyVM
    {
	
		public string VehicleReg { get; set; }
		public string MachineModel { get; set; }
		public List<JobsFrequencyVM> JobList { get; set; }
	}
}
