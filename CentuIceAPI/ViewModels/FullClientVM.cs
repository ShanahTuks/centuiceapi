﻿using System;
using System.Collections.Generic;
using CentuIceAPI.Models;
namespace CentuIceAPI.ViewModels
{
    public class FullClientVM
    {
        public Client Client { get; set; }
        public List<Address> Addresses { get; set; }
        public List<ClientContactPerson> Contacts { get; set; }

    }
}
