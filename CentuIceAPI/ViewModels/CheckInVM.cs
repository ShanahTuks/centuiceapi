﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class CheckInVM
    {
        public Guid DeliveryTripSheetID { get; set; }
        public string Alias { get; set; }
        public DateTime Created { get; set; }
        public DateTime? DepatureTime { get; set; }
        public string DriverName { get; set; }
        public string AssistantName { get; set; }
        public string SupervisorName { get; set; }
        public string VehicleRegNumber { get; set; }
        public Guid VehicleID {  get; set; }
        public int Mileage { get; set; }
        public List<ExtraProductsVM> ExtraProducts { get; set; }
    }
}
