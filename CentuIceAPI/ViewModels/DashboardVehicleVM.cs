﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class DashboardVehicleVM
    {
        public int TotalVehicles { get; set; }
        public int OutForDelivery { get; set; }
        public int Deactivated { get; set; }
        public int BeingServiced { get; set; }
        public int Pending { get; set; }
        public int Available { get; set; }
    }
}
