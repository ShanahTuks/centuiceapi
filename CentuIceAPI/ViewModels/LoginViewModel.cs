﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class LoginViewModel
    {
        public string SystemUsername { get; set; }
        public string SystemPassword { get; set; }
    }
}
