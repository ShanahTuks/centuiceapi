﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class OrderTotalVM
    {
        public DateTime OrderDate { get; set; }
        public Decimal TotalOrders { get; set; }
    }
}
