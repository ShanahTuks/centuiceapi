﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CentuIceAPI.Models;

namespace CentuIceAPI.ViewModels
{
    public class ClientDetailsVM
    {
        public Guid ClientID { get; set; }
        [StringLength(50)]
        public string ClientName { get; set; }
        [StringLength(200)]
        public string Note { get; set; }
       
       
        public List<Addresses> Addresses { get; set; }
        public List<ClientContactPerson> ContactPeople { get; set; }

       
    }

    public class Addresses
    {
        [StringLength(50)]
        public string CityName { get; set; }
        public string CountryName { get; set; }
        [StringLength(50)]
        public string SuburbName { get; set; }
        public int UnitNumber { get; set; }
        [StringLength(100)]
        public string ComplexName { get; set; }
        public int StreetNumber { get; set; }
        [StringLength(100)]
        public string StreetName { get; set; }
        [StringLength(10)]
        public string PostalCode { get; set; }
        public string Province { get; set; }
    }
}
