﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class InventoryExpensesVM
    {
        public string InventoryName { get; set; }
        public Decimal TotalExpense { get; set; }
    }
}
