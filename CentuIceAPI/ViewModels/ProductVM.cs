﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class ProductVM
    {
        public Guid ProductID { get; set; }
        public Guid InventoryID { get; set; }

        public bool isVatApplicable { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }

   
    }
}
