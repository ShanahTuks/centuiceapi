﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.ViewModels
{
    public class ClientAddressVM
    {
        public Guid AddressID { get; set; }
        public Guid AddressTypeID { get; set; }
        public int StreetNumber { get; set; }
        [StringLength(100)]
        public string StreetName { get; set; }
        [StringLength(10)]
        public string PostalCode { get; set; }
        [StringLength(50)]
        public string CountryName { get; set; }
        [StringLength(50)]
        public string SuburbName { get; set; }
        [StringLength(50)]
        public string CityName { get; set; }
        public int UnitNumber { get; set; }
        public string Province { get; set; }
        public string ComplexName { get; set; }
        public bool isActive { get; set; }




    }

}
