﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.ViewModels
{
    public class ClientVM
    {
        public Guid ClientID { get; set; }
        [StringLength(50)]
        public string ClientName { get; set; }
        [StringLength(50)]
        public string ContactPerson { get; set; }
        [StringLength(50)]
        public string PhoneNumber { get; set; }
        [StringLength(50)]
        public string EmailAddress { get; set; }



    }

}
