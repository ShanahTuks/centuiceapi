﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class DeliveryTriphsheetVM
    {
        public string VehicleReg { get; set; }
        public DateTime? DepartureTime { get; set; }
        public DateTime? ReturnTime { get; set; }
        public int StartKM { get; set; }
        public int? EndKM { get; set; }
        public string Status { get; set; }
        public string AssistantName { get; set; }
        public string DriverName { get; set; }
        public string SupervisorName { get; set; }
        public string Alias { get; set; }
        public DateTime Created { get; set; }
        public string VehicleValidationImage { get; set; }



    }
}
