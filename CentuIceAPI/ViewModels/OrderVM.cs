﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class OrderVM
    {
        public Guid OrderID { get; set; }
        public string OrderStatus { get; set; }
        public string ClientName { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
    }
}
