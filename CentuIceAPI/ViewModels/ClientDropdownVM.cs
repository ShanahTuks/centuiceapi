﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class ClientDropdownVM
    {
        public Guid ClientID { get; set; }
        public string ClientName { get; set; }
        
    }
}
