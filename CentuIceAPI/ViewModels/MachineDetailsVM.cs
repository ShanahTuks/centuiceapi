﻿using System;
using System.Collections.Generic;
using CentuIceAPI.Models;

namespace CentuIceAPI.ViewModels
{
    public class MachineDetailsVM
    {
        public MachineVM Machine { get; set; }
        public List<MachineData> MachineData { get; set; }
    }
}
