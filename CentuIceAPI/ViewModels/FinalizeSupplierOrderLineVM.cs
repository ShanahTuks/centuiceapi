﻿using System;

namespace CentuIceAPI.ViewModels
{
    public class FinalizeSupplierOrderLineVM
    {
        public Guid SupplierOrderLineID { get; set; }
        public Guid InventoryID { get; set; }
        public Guid SupplierOrderID { get; set; }
        public Guid InventoryPriceID { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
