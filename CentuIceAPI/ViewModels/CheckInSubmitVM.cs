﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class CheckInSubmitVM
    {
        public Guid DeliveryTripSheetID { get; set; }
        public DateTime ReturnTime { get; set; }
        public int EndKM { get; set; }
    }
}
