﻿using CentuIceAPI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class ClientDeliveryVM
    {
        public Guid ClientID { get; set; }
        public Guid OrderID {  get; set; }
        public string ClientName { get; set; }

        public AddressDTO ClientAddress {  get; set; }
        public List<ClientOrderProductVM> ClientOrderProducts { get; set; }
    }
}
