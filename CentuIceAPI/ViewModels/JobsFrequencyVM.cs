﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class JobsFrequencyVM
    {
        public string ServiceJobName { get; set; }
        public string SupplierName { get; set; }
        public DateTime DateOfService { get; set; }
    }
}
