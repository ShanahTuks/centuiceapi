﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class SupplierOrderLineVM
    {
        public string ItemName {  get; set; }
        public decimal ItemPrice { get; set; }
        public int Quantity {  get; set; }
    }
}
