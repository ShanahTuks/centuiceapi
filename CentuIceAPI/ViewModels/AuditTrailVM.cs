﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class AuditTrailVM
    {
        public Guid AuditTrailID { get; set; }
        public string EmployeeName { get; set; }
        public DateTime ActionDate { get; set; }
        public string EmployeeAction { get; set; }
        public string ActionDescription { get; set; }
        public string ActionData { get; set; }
    }
}
