﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.ViewModels
{
    public class VehicleVM
    {
        public Guid VehicleID { get; set; }
        public Guid VehicleStatusID { get; set; }
        [StringLength(50)]
        public string Make { get; set; }
        [StringLength(50)]
        public string Model { get; set; }
        [StringLength(50)]
        public string Reg_Number { get; set; }
        public int Mileage { get; set; }
        [StringLength(50)]
        public string Tracker_Number { get; set; }
        public DateTime Last_Service_Date { get; set; }
        public DateTime Year_Made { get; set; }
        public int Capacity { get; set; }
        public string VehicleStatus { get; set; }
        public DateTime License_Expiry_Date { get; set; }
    }
}
