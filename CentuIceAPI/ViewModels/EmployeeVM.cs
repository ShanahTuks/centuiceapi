﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class EmployeeVM
    {
        public Guid EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Initials { get; set; }
        public string CellNumber { get; set; }
        public string WorkNumber { get; set; }
        public string EmployeeType { get; set; }
        public string EmployeeStatus { get; set; }
    }
}
