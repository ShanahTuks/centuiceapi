﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class SalesTotalVM
    {
       public DateTime SalesDate { get; set; }
       public Decimal TotalSales { get; set; }
       
    }
}
