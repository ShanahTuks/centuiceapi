﻿using CentuIceAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class FullSupplierOrderVM
    {
        public SupplierOrder SupplierOrders { get; set; }
        public List<SupplierOrderLine> SupplierOrderLines { get; set; }
    }
}
