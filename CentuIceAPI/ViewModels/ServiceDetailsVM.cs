﻿using System;
using System.Collections.Generic;
using CentuIceAPI.Models;

namespace CentuIceAPI.ViewModels
{
    public class ServiceDetailsVM
    {
        public Service Service { get; set; }
        public List<ServiceJob> ServiceJobs { get; set; }
        public string AssetID { get; set; }
        public string AssetType { get; set; }
        public string SupplierName { get; set; }
        public string MachineModel { get; set; }
        public string VehicleReg { get; set; }
    }
}
