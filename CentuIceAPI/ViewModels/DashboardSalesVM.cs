﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class DashboardSalesVM
    {
       public string Month { get; set; }
       public decimal Total { get; set; }
    }
}
