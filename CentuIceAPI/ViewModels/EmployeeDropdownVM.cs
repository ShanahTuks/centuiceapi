﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class EmployeeDropdownVM
    {
        public Guid EmployeeID { get; set; }
        public string FullName {  get; set; }
        public bool isSelected { get; set; }
    }
}
