﻿using CentuIceAPI.DTO;
using CentuIceAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class FullEmployeeVM
    {
        public EmployeeDTO Employee { get; set; }
        public List<AddressDTO> Addresses { get; set; }
        public List<EmergencyContactDTO> EmergencyContacts { get; set; }
    }
}
