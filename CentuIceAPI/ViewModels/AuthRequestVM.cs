﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class AuthRequestVM
    {
        public Guid SenderID { get; set; }
        public Guid OwnerID { get; set; }
        public Guid AuthNotificationTypeID { get; set; }
        public string Identifier { get; set; }
        public Guid IdentifierID {  get; set; }
    }
}
