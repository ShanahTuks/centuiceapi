﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class DashboardNotificationsVM
    {
        public Guid NotificationID { get; set; }
        public string EmployeeName { get; set; }
        public string NotificationTitle { get; set; }
        public string NotificationDescription { get; set; }
        public Boolean isRead { get; set; }
        public DateTime Created { get; set; }
  
    }
}
