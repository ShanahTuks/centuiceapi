﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class InvoiceVM
    {
        public Guid InvoiceID { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string ClientName { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceType { get; set; }
        public string InvoiceStatus { get; set; }
        public string InvoicePaymentMethod { get; set; }
    }
}
