﻿using System;
using System.Collections.Generic;
using CentuIceAPI.DTO;
using CentuIceAPI.Models;

namespace CentuIceAPI.ViewModels
{
    public class OrderInvoiceVM
    {
        public InvoiceVM Invoice { get; set; }
        public List<OrderLineDTO> OrderLines { get; set; }
        public List<OrderLineDTO> Extras { get; set; }
        public string SuburbName { get; set; }
        public string CityName { get; set; }
        public string StreetName { get; set; }
        public int StreetNumber { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
       
    }
}
