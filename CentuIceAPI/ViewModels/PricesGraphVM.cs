﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace CentuIceAPI.ViewModels
{
    public class PricesGraphVM
    {
        [Column(TypeName = "decimal(10, 2)")]
        public decimal Price { get; set; }
        public string Date { get; set; }
    }
}
