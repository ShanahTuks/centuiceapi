﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class JobsServiceReportVM
    {
        public string ServiceJobName { get; set; }
        public string SupplierName { get; set; }
        public DateTime DateOfService { get; set; }
        public decimal ServiceJobCost { get; set; }
    }
}
