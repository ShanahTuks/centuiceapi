﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class DateSelectorVM
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate {  get; set; }
    }
}
