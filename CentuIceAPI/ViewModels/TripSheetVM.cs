﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class TripSheetVM
    {
        public Guid DeliveryTripSheetID { get; set; }
        public Guid TripSheetStatusID { get; set; }
        public string Alias { get; set; }
        public DateTime Created { get; set; }
        //public DateTime OrderDate { get; set; }
        public string InvoiceBookNumber {  get; set; }
        public string DriverName { get; set; }
        public string AssistantName {  get; set; }
        public string SupervisorName {  get; set; }
        //public int NumOfOrders {  get; set; }
        public string VehicleRegNumber {  get; set; }
        public DateTime? DepartureTime {  get; set; }
        public bool FinalLoaded { get; set; }
        public bool FinalDelivered { get; set; }
        public string VehicleValidationImage { get; set; }

        public List<TripSheetOrderVM> TripSheetOrders { get; set; }
        public List<ExtraProductsVM> ExtraProducts {  get; set; }

    }
}
