﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class InventoryReportVM
    {
        public DateTime StockTakeDate { get; set; }
        public string InventoryName { get; set; }
        public int Quantity { get; set; }
        public int DiscrepancyAmount { get; set; }
       
  
    }
}
