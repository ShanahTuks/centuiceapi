﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class VehicleDropdownVM
    {
        public Guid VehicleID { get; set; }
        public string Reg_Number { get; set; }
        public bool isSelected { get; set; }
    }
}
