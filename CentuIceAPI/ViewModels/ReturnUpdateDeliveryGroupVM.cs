﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class ReturnUpdateDeliveryGroupVM
    {
        public List<UpdateDeliveryGroupVM> DeliveryGroups { get; set; }
        public List<ClientDeliveryVM> ClientOrders { get; set; }

    }
}
