﻿using System;

namespace CentuIceAPI.ViewModels
{
    public class ClientOrderProductVM
    {
        public Guid ProductID { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
    }
}
