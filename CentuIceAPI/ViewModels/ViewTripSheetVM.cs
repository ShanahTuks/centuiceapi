﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class ViewTripSheetVM
    {
        public Guid DeliveryTripSheetID { get; set; }
        public string Alias { get; set; }
        public string DriverName { get; set; }
        public string AssistantName { get; set; }
        public string SupervisorName { get; set; }
        public string VehicleRegNumber { get; set; }
        public string TripSheetStatus { get; set; }
        public DateTime CreationDate {  get; set; }
    }
}
