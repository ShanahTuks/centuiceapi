﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class OrderExtraProductVM
    {
        public Guid ExtraProductTakenID { get; set; }
        public Guid OrderID { get; set; }
        public int Quantity {  get; set; }
        public string ProductName {  get; set; }
    }
}
