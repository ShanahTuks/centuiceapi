﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class SupplierOrderDetailsVM
    {
        public string SupplierName {  get; set; }
        public string OrderStatus {  get; set; }
        public DateTime? DateReceived { get; set; }
        public DateTime DatePlaced { get; set; }
        public List<SupplierOrderLineVM> SupplierOrderLines { get; set; }
    }
}
