﻿using System;
using System.Collections.Generic;
using CentuIceAPI.DTO;

namespace CentuIceAPI.ViewModels
{
    public class SaleInvoiceVM
    {
        public InvoiceVM Invoice { get; set; }
        public List<SaleLineDTO> SaleLines { get; set; }
    }
}
