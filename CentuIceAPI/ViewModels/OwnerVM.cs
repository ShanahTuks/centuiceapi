﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class OwnerVM
    {
        public Guid EmployeeID { get; set; }
        public string FirstName { get; set; }
    }
}
