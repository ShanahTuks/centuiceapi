﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class SupplierDropdownVM
    {
        public Guid SupplierID { get; set; }
        public string SupplierName { get; set; }
    }
}
