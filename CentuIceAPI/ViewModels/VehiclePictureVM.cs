﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class VehiclePictureVM
    {
        public Guid DeliveryTripSheetID { get; set; }
        public string Picture { get; set; }
    }
}
