﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class ServiceVM
    {
       public Guid ServiceID { get; set; }
       public string SupplierName { get; set; }
       public Guid AssetID { get; set; }
       public string AssetType { get; set; }
       public DateTime ServiceDate { get; set; }
    }
}
