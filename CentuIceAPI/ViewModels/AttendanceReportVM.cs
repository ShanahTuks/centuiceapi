﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class AttendanceReportVM
    {
        public string EmployeeName { get; set; }
        public int HoursWorked { get; set; }
    }
}
