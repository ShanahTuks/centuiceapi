﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.ViewModels
{
    public class EmployeeErrorVM
    { 
        public Guid EmployeeErrorID { get; set; }
        public string EmployeeFullName { get; set; }
        [StringLength(15)]
        public string ErrorCode { get; set; }
        [StringLength(100)]
        public string DescriptionOfError { get; set; }
        public DateTime DateOfError { get; set; }
    }
}
