﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class SaleVM
    {
        public Guid SaleID { get; set; }
        public DateTime SaleDate { get; set; }
        public Guid InvoiceID { get; set; }
        public string InvoiceNumber { get; set; }
        public string SaleStatus { get; set; }
    }
}
