﻿using System;

namespace CentuIceAPI.ViewModels
{
    public class EmployeeProfileVM
    {
        public Guid EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string SystemUsername { get; set; }
        public string KnownAsName { get; set; }
        public string Email { get; set; }
        public string CellNumber { get; set; }
    }
}
