﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class TripSheetOrderVM
    {
        public Guid DeliveryTripSheetID { get; set; }
        public Guid OrderID { get; set; }
        public string ClientName {  get; set; }
        public bool Delivered { get; set; }
        public bool FinalDelivered { get; set; }
        public bool Loaded { get; set; }
        public bool FinalLoaded { get; set; }
    }
}
