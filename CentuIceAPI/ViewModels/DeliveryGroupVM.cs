﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class DeliveryGroupVM
    {
        public Guid DriverID {  get; set; }
        public Guid AssistantID {  get; set; }
        public Guid SupervisorID {  get; set; }
        public Guid VehicleID {  get; set; }
        public string Alias { get; set; }
        public List<ClientDeliveryVM> Deliveries {  get; set; }
    }
}
