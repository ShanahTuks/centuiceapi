﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class EmergencyContactVM
    {
        public string ContactPerson { get; set; }
        public string ContactTelephoneNumber { get; set; }
        public string RelationshipToEmployee { get; set; }
    }
}
