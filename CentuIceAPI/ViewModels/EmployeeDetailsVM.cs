﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class EmployeeDetailsVM
    {
        public string Bank { get; set; }
        public string AccountType { get; set; }
        public string EmployeeQualification { get; set; }
        public string EmployeeType { get; set; }
        public string EmployeeStatus { get; set; }
        public string EmployeeTitle { get; set; }
        public string TableStatus { get; set; }

        public string KnownAsName { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string IDNumber { get; set; }
        public string HomeNumber { get; set; }
        public string PassportNumber { get; set; }
        public string CellNumber { get; set; }
        public string WorkNumber { get; set; }
        public string Initials { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string SystemUsername { get; set; }
        public string SystemPassword { get; set; }
        public string BranchCode { get; set; }
        public string AccountNumber { get; set; }
        public string TaxNumber { get; set; }
        public string PassportCountry { get; set; }
        public DateTime YearCompleted { get; set; }
        public string InstitutionName { get; set; }
        public bool isLoggedIn { get; set; }

        public List<AddressVM> Addresses { get; set; }
        public List<EmergencyContactVM> EmergencyContacts { get; set; }

    }
}
