﻿using CentuIceAPI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class OwnerAuthRequestVM
    {
        public NotificationDTO Notification { get; set; }
        public AuthNotificationDTO AuthNotification { get; set; }
    }
}
