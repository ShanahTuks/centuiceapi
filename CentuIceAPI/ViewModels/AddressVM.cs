﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.ViewModels
{
    public class AddressVM
    {
        public string AddressType { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Suburb { get; set; }
        public int UnitNumber { get; set; }
        public string ComplexName { get; set; }
        public int StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string PostalCode { get; set; }
        public string Province { get; set; }
        public bool isActive { get; set; }

    }
}
