﻿using System;
namespace CentuIceAPI.ViewModels
{
    public class SaleProductReportVM
    {
        public string ProductName {get;set;}
        public Decimal TotalSalesPerProduct { get; set; }
        
    }
}
