﻿using CentuIceAPI.Models;
using System.Collections.Generic;

namespace CentuIceAPI.ViewModels
{
    public class FinalizeSupplierOrderVM
    {
        public SupplierOrder SupplierOrders { get; set; }
        public List<FinalizeSupplierOrderLineVM> FinalizeSupplierOrderLineVM {  get; set; }
    }
}
