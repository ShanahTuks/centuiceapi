﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class MachineData
    {
      [Key]
      public Guid MachineDataID { get; set; }

       public DateTime Date { get; set; }
      [StringLength(100)]
      public string Cycles { get; set; }
      [StringLength(100)]
      public string Weight { get; set; }
        [StringLength(100)]
        public string WaterUse { get; set; }
        [StringLength(100)]
        public string FullBin { get; set; }
        [StringLength(100)]
        public string RunTime { get; set; }
        [StringLength(100)]
        public string AVGCycleMin { get; set; }
        public ICollection<MachineMachineData> MachineMachineData { get; set; }
    }
}
