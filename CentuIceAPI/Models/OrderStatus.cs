﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class OrderStatus
    {
        [Key]
        public Guid OrderStatusID { get; set; }
        [StringLength(50)]
        public string Description { get; set; }
    }
}
