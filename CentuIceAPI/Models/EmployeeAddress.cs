﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class EmployeeAddress
    {

        public Guid EmployeeID { get; set; }
        public Guid AddressID { get; set; }

        public Employee Employee { get; set; }
        public Address Address { get; set; }
    }
}
