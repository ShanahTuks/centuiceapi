﻿using System;
namespace CentuIceAPI.Models
{
    public class InvoiceNumber
    {
        public Guid InvoiceNumberID { get; set; }
        public int Number { get; set; }
    }
}
