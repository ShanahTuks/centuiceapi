﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class MachineType
    {
      [Key]
      public Guid MachineTypeID { get; set; }
      [StringLength(50)]
      public string Description { get; set; }
    }
}
