﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class EmployeeTripSheet
    {
        [Key]
        public Guid EmployeeTripSheetID { get; set; }
        public Guid DeliveryTripSheetID { get; set; }
        public Guid EmployeeID { get; set; }
        public Guid EmployeeTripSheetTypeID { get; set; }

        public DeliveryTripSheet DeliveryTripSheet { get; set; }
        public Employee Employee { get; set; }
        public EmployeeTripSheetType EmployeeTripSheetType { get; set; }
    }
}
