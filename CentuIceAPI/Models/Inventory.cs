﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CentuIceAPI.Models
{
    public class Inventory
    {
        [Key]
        public Guid InventoryID { get; set; }
        public Guid SupplierID { get; set; }
        public Guid InventoryTypeID { get; set; }
        public Guid TableStatusID { get; set; }
        [StringLength(50)]
        public string ItemName { get; set; }
        
        public int Quantity { get; set; }
        public ICollection<SupplierOrderLine> SupplierOrderLine { get; set; }
        public ICollection<StockTakeLine> StockTakeLines { get; set; }
        public ICollection<InventoryPrice> InventoryPrices { get; set; }

        public Supplier Supplier { get; set; }
        public InventoryType InventoryType { get; set; }
        public TableStatus TableStatus { get; set; }

    }
}
