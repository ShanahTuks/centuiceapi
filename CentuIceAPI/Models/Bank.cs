﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class Bank
    {
        [Key]
        public Guid BankID { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
    }
}
