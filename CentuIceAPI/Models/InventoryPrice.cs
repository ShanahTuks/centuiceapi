﻿using System;
namespace CentuIceAPI.Models
{
    public class InventoryPrice
    {
        public Guid InventoryPriceID { get; set; }
        public Guid PriceID { get; set; }
        public Guid InventoryID { get; set; }
        public DateTime Date { get; set; }

        public Price Price {  get; set; }
        public Inventory Inventory { get; set; }
    }
}
