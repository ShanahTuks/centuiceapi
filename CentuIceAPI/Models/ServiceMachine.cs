﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CentuIceAPI.Models
{
    public class ServiceMachine
    {
        
        
        public Guid ServiceID { get; set; }
        public Guid MachineID { get; set; }

        public Service Service { get; set; }
        public Machine Machine { get; set; }
    }
}
