﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class Notification
    {
        public Guid NotificationID { get; set; }
        public Guid NotificationTypeID { get; set; }
        public Guid EmployeeID {  get; set; }
        public string NotificationTitle {  get; set; }
        public string NotificationDescription {  get; set; }
        public bool IsRead {  get; set; }
        public bool Show { get; set; }
        public DateTime Created {  get; set; }

        public NotificationType NotificationType {  get; set; }
        public Employee Employee {  get; set; }

    }
}
