﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CentuIceAPI.Models
{
    public class ServiceJob
    {

        [Key]
        public Guid ServiceJobID { get; set; }
        [StringLength(50)]
        public string ServiceJobName { get; set; }
        [Column(TypeName = "decimal(10,2)")]
        public decimal CostOfJob { get; set; }
        public ICollection<ServiceServiceJob> ServiceServiceJob { get; set; }
    }
}
