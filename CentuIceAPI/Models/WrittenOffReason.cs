﻿using System;
namespace CentuIceAPI.Models
{
    public class WrittenOffReason
    {
      public Guid WrittenOffReasonID { get; set; }
      public string Description { get; set; }
    }
}
