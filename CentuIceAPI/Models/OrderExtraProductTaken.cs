﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class OrderExtraProductTaken
    {
        public Guid OrderID { get; set; }
        public Guid ExtraProductTakenID { get; set; }
        public int Quantity {  get; set; }

        public Order Order {  get; set; }
        public ExtraProductTaken ExtraProductTaken {  get; set; }
    }
}
