﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class EmergencyContact
    {
        [Key]
        public Guid EmergencyContactID { get; set; }
        public Guid EmployeeID { get; set; }
        [StringLength(50)]
        public string ContactPerson { get; set; }
        [StringLength(10)]
        public string ContactTelephoneNumber { get; set; }
        [StringLength(10)]
        public string RelationshipToEmployee { get; set; }

        public Employee Employee { get; set; }
    }
}
