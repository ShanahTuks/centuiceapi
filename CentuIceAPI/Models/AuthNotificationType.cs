﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class AuthNotificationType
    {
        [Key]
        public Guid AuthNotificationTypeID { get; set; }
        public string Description { get; set; }
    }
}
