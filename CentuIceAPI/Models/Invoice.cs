﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class Invoice
    {
        [Key]
        public Guid InvoiceID { get; set; }
        public Guid? InvoiceStatusID { get; set; }
        public Guid? PaymentMethodID { get; set; }
        public DateTime InvoiceDate { get; set; }
        
        
        [StringLength(50)]
        public string InvoiceNumber { get; set; }

        public PaymentMethod PaymentMethod { get; set; }
        public InvoiceStatus InvoiceStatus { get; set; }
        public Sale Sale { get; set; }
        public Order Order { get; set; }

    }
}
 