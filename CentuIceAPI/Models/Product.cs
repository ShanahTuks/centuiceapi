﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CentuIceAPI.Models
{
    public class Product
    {
        [Key]
        public Guid ProductID { get; set; }
        public Guid InventoryID { get; set; }

        public bool isVatApplicable { get; set; }
        public Guid TableStatusID { get; set; }
        [StringLength(100)]
        public string ProductName { get; set; }
        
        public ICollection<SaleLine> SaleLine { get; set; }
        public ICollection<OrderLine> OrderLine { get; set; }
        public ICollection<ProductPrice> ProductPrices { get; set; }

        public Inventory Inventory { get; set; }
        public TableStatus TableStatus { get; set; }

    }
}
