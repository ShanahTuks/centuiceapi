﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CentuIceAPI.Models
{
    public class MachineMachineData
    {
       
        
        public Guid MachineID { get; set; }
        public Guid MachineDataID { get; set; }

      public Machine Machine { get; set; }
      public MachineData MachineData { get; set; }
    }
}
