﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CentuIceAPI.Models
{
    public class SupplierOrder
    {
        [Key]
        public Guid SupplierOrderID { get; set; }
        public Guid SupplierID { get; set; }
        public Guid SupplierOrderStatusID { get; set; }
        [StringLength(50)]
        public string Details { get; set; }
        public DateTime? DateReceived { get; set; }
        public DateTime DatePlaced { get; set; }
        public ICollection<SupplierOrderLine> SupplierOrderLine { get; set; }

        public Supplier Supplier { get; set; }
        public SupplierOrderStatus SupplierOrderStatus { get; set; }
    }
}
