﻿using System;
namespace CentuIceAPI.Models
{
    public class ProductPrice
    {
        public Guid ProductPriceID { get; set; }
        public Guid PriceID { get; set; }
        public Guid ProductID { get; set; }
        public DateTime Date { get; set; }

        public Price Price { get; set; }
        public Product Product { get; set; }
    }
}
