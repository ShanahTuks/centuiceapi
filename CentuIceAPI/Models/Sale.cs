﻿using System;
using System.Collections.Generic;

namespace CentuIceAPI.Models
{
    public class Sale
    {
        public Guid SaleID { get; set; }
        public DateTime SaleDate { get; set; }
        public Guid SaleStatusID { get; set; }
        public Guid VatConfigurationID { get; set; }
        public bool VatActivated { get; set; }
        public ICollection<SaleLine> SaleLine { get; set; }

        public Guid InvoiceID { get; set; }
        public Invoice Invoice { get; set; }
        public SaleStatus SaleStatus { get; set; }

        public VatConfiguration VatConfiguration { get; set; }  
    }
}
