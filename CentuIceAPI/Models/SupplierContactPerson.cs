﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class SupplierContactPerson
    {
        [Key]
        public Guid SupplierContactPersonID { get; set; }
        public Guid SupplierID { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(50)]
        public string PhoneNumber {get;set; }
        [StringLength(50)]
        public string EmailAddress { get; set; }

        public Supplier Supplier { get; set; }

    }
}
