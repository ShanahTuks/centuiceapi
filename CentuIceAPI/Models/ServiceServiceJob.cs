﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CentuIceAPI.Models
{
    public class ServiceServiceJob
    {
       
        
        public Guid ServiceID { get; set; }
        public Guid ServiceJobID { get; set; }

        public Service Service { get; set; }
        public ServiceJob ServiceJob { get; set; }
    }
}
