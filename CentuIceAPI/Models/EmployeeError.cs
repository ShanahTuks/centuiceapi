﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class EmployeeError
    {
        [Key]
        public Guid EmployeeErrorID { get; set; }
        public Guid EmployeeID { get; set; }
        [StringLength(15)]
        public string ErrorCode { get; set; }
        [StringLength(100)]
        public string DescriptionOfError { get; set; }
        public DateTime DateOfError { get; set; }

        public Employee Employee { get; set; }

    }
}
