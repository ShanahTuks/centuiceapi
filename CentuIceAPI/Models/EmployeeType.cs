﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class EmployeeType
    {
        [Key]
        public Guid EmployeeTypeID { get; set; }
        [StringLength(20)]
        public string Description { get; set; }
    }
}
