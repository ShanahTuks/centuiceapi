﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class EmployeeQualification
    {
        [Key]
        public Guid EmployeeQualificationID { get; set; }
        [StringLength(50)]
        public string Description { get; set; }
    }
}
