﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class AuditTrail
    {
        [Key]
        public Guid AuditTrailID { get; set; }
        public Guid EmployeeID {  get; set; }
        public DateTime ActionDate {  get; set; }
        public string EmployeeAction {  get; set; }
        public string ActionDescription {  get; set; }
        public string ActionData {  get; set; }

        public Employee Employee {  get; set; }
    }
}
