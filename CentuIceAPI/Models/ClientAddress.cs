﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class ClientAddress
    {

        public Guid ClientID { get; set; }
        public Guid AddressID { get; set; }

        public Client Client { get; set; }
        public Address Address { get; set; }
    }
}
