﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using CentuIceAPI.Data;

namespace CentuIceAPI.Models
{
    public partial class CentuIceDBContext : DbContext
    {
        public CentuIceDBContext()
        {
        }

        public CentuIceDBContext(DbContextOptions<CentuIceDBContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            //modelBuilder.Entity<SupplierOrderLine>()
            //    .HasKey(bc => new { bc.SupplierOrderID, bc.InventoryID });
            //modelBuilder.Entity<SupplierOrderLine>()
            //    .HasOne(bc => bc.Inventory)
            //    .WithMany(bc => bc.SupplierOrderLine)
            //    .HasForeignKey(bc => bc.InventoryID);
            //modelBuilder.Entity<SupplierOrderLine>()
            //    .HasOne(bc => bc.SupplierOrder)
            //    .WithMany(bc => bc.SupplierOrderLine)
            //    .HasForeignKey(bc => bc.SupplierOrderID);

            modelBuilder.Entity<ServiceMachine>()
               .HasKey(bc => new { bc.ServiceID, bc.MachineID });
            modelBuilder.Entity<ServiceMachine>()
                .HasOne(bc => bc.Service)
                .WithMany(bc => bc.ServiceMachine)
                .HasForeignKey(bc => bc.ServiceID);
            modelBuilder.Entity<ServiceMachine>()
                .HasOne(bc => bc.Machine)
                .WithMany(bc => bc.ServiceMachine)
                .HasForeignKey(bc => bc.MachineID);

            modelBuilder.Entity<ServiceVehicle>()
               .HasKey(bc => new { bc.ServiceID, bc.VehicleID });
            modelBuilder.Entity<ServiceVehicle>()
                .HasOne(bc => bc.Service)
                .WithMany(bc => bc.ServiceVehicle)
                .HasForeignKey(bc => bc.ServiceID);
            modelBuilder.Entity<ServiceVehicle>()
                .HasOne(bc => bc.Vehicle)
                .WithMany(bc => bc.ServiceVehicle)
                .HasForeignKey(bc => bc.VehicleID);

            modelBuilder.Entity<ServiceServiceJob>()
               .HasKey(bc => new { bc.ServiceID, bc.ServiceJobID});
            modelBuilder.Entity<ServiceServiceJob>()
                .HasOne(bc => bc.Service)
                .WithMany(bc => bc.ServiceServiceJob)
                .HasForeignKey(bc => bc.ServiceID);
            modelBuilder.Entity<ServiceServiceJob>()
                .HasOne(bc => bc.ServiceJob)
                .WithMany(bc => bc.ServiceServiceJob)
                .HasForeignKey(bc => bc.ServiceJobID);


            modelBuilder.Entity<MachineMachineData>()
              .HasKey(bc => new { bc.MachineID, bc.MachineDataID });
            modelBuilder.Entity<MachineMachineData>()
                .HasOne(bc => bc.Machine)
                .WithMany(bc => bc.MachineMachineData)
                .HasForeignKey(bc => bc.MachineID);
            modelBuilder.Entity<MachineMachineData>()
                .HasOne(bc => bc.MachineData)
                .WithMany(bc => bc.MachineMachineData)
                .HasForeignKey(bc => bc.MachineDataID);

            modelBuilder.Entity<SaleLine>()
              .HasKey(bc => new { bc.SaleID, bc.ProductID });
            modelBuilder.Entity<SaleLine>()
                .HasOne(bc => bc.Sale)
                .WithMany(bc => bc.SaleLine)
                .HasForeignKey(bc => bc.SaleID);
            modelBuilder.Entity<SaleLine>()
                .HasOne(bc => bc.Product)
                .WithMany(bc => bc.SaleLine)
                .HasForeignKey(bc => bc.ProductID);

            modelBuilder.Entity<OrderLine>()
              .HasKey(bc => new { bc.OrderID, bc.ProductID });
            modelBuilder.Entity<OrderLine>()
                .HasOne(bc => bc.Order)
                .WithMany(bc => bc.OrderLine)
                .HasForeignKey(bc => bc.OrderID);
            modelBuilder.Entity<OrderLine>()
                .HasOne(bc => bc.Product)
                .WithMany(bc => bc.OrderLine)
                .HasForeignKey(bc => bc.ProductID);

            modelBuilder.Entity<ClientTrend>()
             .HasKey(bc => new { bc.DayID, bc.ClientID });
            modelBuilder.Entity<ClientTrend>()
                .HasOne(bc => bc.Day)
                .WithMany(bc => bc.ClientTrend)
                .HasForeignKey(bc => bc.DayID);
            modelBuilder.Entity<ClientTrend>()
                .HasOne(bc => bc.Client)
                .WithMany(bc => bc.ClientTrend)
                .HasForeignKey(bc => bc.ClientID);

            modelBuilder.Entity<ClientAddress>()
             .HasKey(bc => new { bc.AddressID, bc.ClientID });
            modelBuilder.Entity<ClientAddress>()
                .HasOne(bc => bc.Address)
                .WithMany(bc => bc.ClientAddresses)
                .HasForeignKey(bc => bc.AddressID);
            modelBuilder.Entity<ClientAddress>()
                .HasOne(bc => bc.Client)
                .WithMany(bc => bc.ClientAddresses)
                .HasForeignKey(bc => bc.ClientID);

            modelBuilder.Entity<EmployeeAddress>()
            .HasKey(bc => new { bc.AddressID, bc.EmployeeID });
            modelBuilder.Entity<EmployeeAddress>()
                .HasOne(bc => bc.Address)
                .WithMany(bc => bc.EmployeeAddresses)
                .HasForeignKey(bc => bc.AddressID);
            modelBuilder.Entity<EmployeeAddress>()
                .HasOne(bc => bc.Employee)
                .WithMany(bc => bc.EmployeeAddresses)
                .HasForeignKey(bc => bc.EmployeeID);

            modelBuilder.Entity<InventoryPrice>()
                .HasKey(bc => new {/* bc.PriceID, bc.InventoryID,*/ bc.InventoryPriceID });
            modelBuilder.Entity<InventoryPrice>()
                .HasOne(bc => bc.Price)
                .WithMany(bc => bc.InventoryPrices)
                .HasForeignKey(bc => bc.PriceID);
            modelBuilder.Entity<InventoryPrice>()
                .HasOne(bc => bc.Inventory)
                .WithMany(bc => bc.InventoryPrices)
                .HasForeignKey(bc => bc.InventoryID);

            modelBuilder.Entity<ProductPrice>()
               .HasKey(bc => new { /*bc.PriceID, bc.ProductID,*/ bc.ProductPriceID });
            modelBuilder.Entity<ProductPrice>()
                .HasOne(bc => bc.Price)
                .WithMany(bc => bc.ProductPrices)
                .HasForeignKey(bc => bc.PriceID);
            modelBuilder.Entity<ProductPrice>()
                .HasOne(bc => bc.Product)
                .WithMany(bc => bc.ProductPrices)
                .HasForeignKey(bc => bc.ProductID);

            modelBuilder.Entity<OrderExtraProductTaken>().HasKey(oep => new { oep.ExtraProductTakenID, oep.OrderID});

            //modelBuilder.Entity<Sale>()
            //    .HasOne(a => a.Invoice)
            //    .WithOne(b => b.Sale)
            //    .HasForeignKey<Invoice>(b => b.InvoiceID);

            //modelBuilder.Entity<Invoice>()
            //    .HasOne(a => a.Sale)
            //    .WithOne(b => b.Invoice)
            //    .HasForeignKey<Sale>(b => b.SaleID);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            
           
        
           
            
      
   
   

            modelBuilder.ApplyConfiguration(new AccountTypeData());
            modelBuilder.ApplyConfiguration(new AddressData());
            modelBuilder.ApplyConfiguration(new AddressTypeData());
            modelBuilder.ApplyConfiguration(new AuditTrailData());
            modelBuilder.ApplyConfiguration(new AuthNotificationTypeData());
            modelBuilder.ApplyConfiguration(new BankData());
            modelBuilder.ApplyConfiguration(new ClientAddressData());
            modelBuilder.ApplyConfiguration(new ClientContactPersonData());
            modelBuilder.ApplyConfiguration(new ClientData());
            modelBuilder.ApplyConfiguration(new DailyAttendanceData());
            modelBuilder.ApplyConfiguration(new DeliveryTripSheetData());
            modelBuilder.ApplyConfiguration(new EmployeeAddressData());
            modelBuilder.ApplyConfiguration(new EmployeeData());
            modelBuilder.ApplyConfiguration(new EmergencyContactData());
            modelBuilder.ApplyConfiguration(new EmployeeQualificationData());
            modelBuilder.ApplyConfiguration(new EmployeeStatusData());
            modelBuilder.ApplyConfiguration(new EmployeeTitleData());
            modelBuilder.ApplyConfiguration(new EmployeeTripSheetTypeData());
            modelBuilder.ApplyConfiguration(new EmployeeTypeData());
            modelBuilder.ApplyConfiguration(new InventoryData());
            modelBuilder.ApplyConfiguration(new InventoryPriceData());
            modelBuilder.ApplyConfiguration(new InventoryTypeData());
            modelBuilder.ApplyConfiguration(new InvoiceData());
            modelBuilder.ApplyConfiguration(new InvoiceNumberData());
            modelBuilder.ApplyConfiguration(new InvoiceStatusData());
            modelBuilder.ApplyConfiguration(new MachineTypeData());
            modelBuilder.ApplyConfiguration(new NotificationTimerData());
            modelBuilder.ApplyConfiguration(new NotificationTypeData());
            modelBuilder.ApplyConfiguration(new OrderData());
            modelBuilder.ApplyConfiguration(new OrderLineData());
            modelBuilder.ApplyConfiguration(new OrderStatusData());
            modelBuilder.ApplyConfiguration(new PaymentMethodData());
            modelBuilder.ApplyConfiguration(new PriceData());
            modelBuilder.ApplyConfiguration(new ProductData());
            modelBuilder.ApplyConfiguration(new ProductPriceData());
            modelBuilder.ApplyConfiguration(new SaleData());
            modelBuilder.ApplyConfiguration(new SaleLineData());
            modelBuilder.ApplyConfiguration(new SupplierContactPersonData());
            modelBuilder.ApplyConfiguration(new SupplierData());
            modelBuilder.ApplyConfiguration(new SupplierOrderData());
            modelBuilder.ApplyConfiguration(new SupplierOrderLineData());
            modelBuilder.ApplyConfiguration(new SupplierOrderStatusData());
            modelBuilder.ApplyConfiguration(new TableStatusData());
            modelBuilder.ApplyConfiguration(new TripSheetStatusData());
            modelBuilder.ApplyConfiguration(new VehicleData());
            modelBuilder.ApplyConfiguration(new SaleStatusData());
            modelBuilder.ApplyConfiguration(new VatConfigurationData());
            modelBuilder.ApplyConfiguration(new VehicleStatusData());
            modelBuilder.ApplyConfiguration(new WrittenOffReasonData());
        }


        public DbSet<AccountType> AccountTypes { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<AuthNotification> AuthNotifications { get; set; }
        public DbSet<AuthNotificationType> AuthNotificationTypes { get; set; }
        public DbSet<AddressType> AddressTypes { get; set; }
        public DbSet<AuditTrail> AuditTrails { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<AuthWriteOff> AuthWriteOffs { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientTrend> ClientTrends { get; set; }
        public DbSet<WrittenOffReason> WrittenOffReasons { get; set; }
        public DbSet<DailyAttendance> DailyAttendances { get; set; }
        
        public DbSet<Day> Days { get; set; }
        public DbSet<DeliveryTripSheet> DeliveryTripSheets { get; set; }
        public DbSet<EmergencyContact> EmergencyContacts { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeError> EmployeeErrors { get; set; }
        public DbSet<EmployeeQualification> EmployeeQualifications { get; set; }
        public DbSet<EmployeeStatus> EmployeeStatuses { get; set; }
        public DbSet<EmployeeTitle> EmployeeTitles { get; set; }
        public DbSet<EmployeeTripSheet> EmployeeTripSheets { get; set; }
        public DbSet<EmployeeTripSheetType> EmployeeTripSheetTypes { get; set; }
        public DbSet<EmployeeType> EmployeeTypes { get; set; }
        public DbSet<ExtraProductTaken> ExtraProductsTaken { get; set; }
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<InventoryType> InventoryTypes { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<InvoiceStatus> InvoiceStatus { get; set; }
        public DbSet<Machine> Machines { get; set; }
        public DbSet<MachineData> MachineData { get; set; }
        public DbSet<MachineMachineData> MachineMachineData { get; set; }
        public DbSet<MachineType> MachineTypes { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<NotificationType> NotificationTypes { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderExtraProductTaken> OrderExtraProductsTaken { get; set; }
        public DbSet<OrderLine> OrderLines {get;set;}
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Sale> Sales { get; set; }
        public DbSet<SaleStatus> SaleStatuses {  get; set; }
        public DbSet<SaleLine> SaleLines { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<ServiceJob> ServiceJobs { get; set; }
        public DbSet<NotificationTimer> NotificationTimers { get; set; }
        public DbSet<ServiceMachine> ServiceMachines { get; set; }
        public DbSet<ServiceServiceJob> ServiceServiceJobs { get; set; }
        public DbSet<ServiceVehicle> ServiceVehicles { get; set; }
        public DbSet<StockTake> StockTakes { get; set; }
        public DbSet<StockTakeLine> StockTakeLines { get; set; }
        public DbSet<StockWrittenOff> StockWrittenOff { get; set; }
        public DbSet<InvoiceNumber> InvoiceNumbers { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<SupplierContactPerson> SupplierContactPeople { get; set; }
        public DbSet<SupplierOrder> SupplierOrders { get; set; }
        public DbSet<SupplierOrderLine> SupplierOrderLines { get; set; }
        public DbSet<SupplierOrderStatus> SupplierOrderStatuses { get; set; }
        public DbSet<TripSheetStatus> TripSheetStatuses { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<VehicleStatus> VehicleStatuses { get; set; }
        public DbSet<EmployeeAddress> EmployeeAddresses { get; set; }
        public DbSet<InventoryPrice> InventoryPrices {  get; set; }

        public DbSet<VatConfiguration> VatConfigurations { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<ProductPrice> ProductPrices {  get; set; }
        public DbSet<ClientAddress> ClientAddresses { get; set; }
        public DbSet<ClientContactPerson> ClientContactPeople { get; set; }
        public DbSet<TableStatus> TableStatuses { get; set; }
        







    }
}
