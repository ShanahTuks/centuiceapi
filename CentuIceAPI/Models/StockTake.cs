﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class StockTake
    {
        [Key]
        public Guid StockTakeID { get; set; }
        public DateTime DateOfStockTake { get; set; }

        public ICollection<StockTakeLine> StockTakeLines { get; set; }
    }
}
