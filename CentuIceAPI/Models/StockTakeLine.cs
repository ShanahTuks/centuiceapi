﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class StockTakeLine
    {
        [Key]
        public Guid StockTakeLineID { get; set; }
        public Guid StockTakeID { get; set; }
        public Guid InventoryID { get; set; }
        public int StockTakeQuantity { get; set; }
        public int DiscrepancyAmount { get; set; }

        public StockTake StockTake { get; set; }
        public Inventory Inventory { get; set; }

    }
}
