﻿using System;
namespace CentuIceAPI.Models
{
    public class NotificationTimer
    {
        public Guid NotificationTimerID { get; set; }
        public string NotificationType { get; set; }
        public string NotificationReason { get; set; }
        public int TimePeriod { get; set; }
        public DateTime CheckTime { get; set; }
       
    }
}
