﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class ClientTrend
    {
        
        public Guid DayID { get; set; }
        public Guid ClientID { get; set; }

        public Day Day { get; set; }
        public Client Client { get; set; }
    }
}
