﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class OrderLine
    {
      
        public Guid OrderID { get; set; }
        public Guid ProductID { get; set; }
        public Guid ProductPriceID {  get; set; }
        public int Quantity { get; set; }


        public Order Order { get; set; }
        public Product Product { get; set; }
        public ProductPrice ProductPrice { get; set; }
    }
}
