﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class InvoiceStatus
    {
        [Key]
        public Guid InvoiceStatusID { get; set; }
        public string Description { get; set; }
  
       
    }
}
