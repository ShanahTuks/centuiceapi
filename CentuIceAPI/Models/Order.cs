﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class Order
    {
        [Key]
        public Guid OrderID { get; set; }
        public Guid OrderStatusID { get; set; }
        public Guid ClientID { get; set; }
        public Guid AddressID { get; set; }

        public Guid VatConfigurationID { get; set; }
        public Guid? DeliveryTripSheetID { get; set; }
        public DateTime OrderDate { get; set; }

        public bool VatActivated { get; set; }
        public bool Delivered {  get; set; }
        public bool Loaded {  get; set; }
        public DateTime? DeliveryDate { get; set; }
        public ICollection<OrderLine> OrderLine { get; set; }
        public Guid? InvoiceID { get; set; }
        public Invoice Invoice { get; set; }
        public IList<OrderExtraProductTaken> OrderExtraProducts { get; set; }


        public OrderStatus OrderStatus { get; set; }
        public Client Client { get; set; }
        public Address Address { get; set; }
        public DeliveryTripSheet DeliveryTripSheet { get; set; }
        public VatConfiguration VatConfiguration { get; set; }
    }
}
