﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class StockWrittenOff
    {
        [Key]
        public Guid StockWrittenOffID { get; set; }
        public Guid InventoryID { get; set; }
        public int Quantity { get; set; }
        public Guid WrittenOffReasonID { get; set; }
        public DateTime WriteOffDate { get; set; }

        public WrittenOffReason WrittenOffReason { get; set; }
        public Inventory Inventory { get; set; }

    }
}
