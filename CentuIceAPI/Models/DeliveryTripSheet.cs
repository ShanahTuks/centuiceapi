﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CentuIceAPI.Models
{
    public class DeliveryTripSheet
    {
        [Key]
        public Guid DeliveryTripSheetID { get; set; }
        public Guid VehicleID { get; set; }
        [ForeignKey("Driver")]
        public Guid DriverID {  get; set; }
        [ForeignKey("Assistant")]
        public Guid AssistantID {  get; set; }
        [ForeignKey("Supervisor")]
        public Guid SupervisorID { get; set; }
        public Guid TripSheetStatusID { get; set; }
        public string Alias {  get; set; }
        public DateTime Created {  get; set; }
        //public int NumberOfOrders { get; set; }
        public DateTime? DepatureTime { get; set; }
        public DateTime? ReturnTime { get; set; }
        //public float TotalKgLoaded { get; set; }
        [StringLength(100)]
        public string Comment { get; set; }
        public int StartKM { get; set; }
        public int? EndKM { get; set; }
        public string VehicleValidationImage { get; set; }

        public bool FinalLoaded {  get; set; }
        public bool FinalDelivered {  get; set; }
        //public int GroupNumber { get; set; }
        //public int ExtraBagsLoaded { get; set; }
        //public int ExtraBagsReturned { get; set; }

        
        public Vehicle Vehicle { get; set; }
        public TripSheetStatus TripSheetStatus { get; set; }

        public Employee Driver {  get; set; }
        public Employee Assistant {  get; set; }    
        public Employee Supervisor {  get; set; }
    }           
}
