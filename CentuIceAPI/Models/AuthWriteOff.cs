﻿using System;
namespace CentuIceAPI.Models
{
    public class AuthWriteOff
    {
        public Guid AuthWriteOffID { get; set; }
        public Guid InventoryID { get; set; }
        public Guid EmployeeID { get; set; }
        public int Quantity { get; set; }
        public Guid WrittenOffReasonID { get; set; }
        public DateTime WriteOffDate { get; set; }
        public string InventoryItem { get; set; }

  



    }
}
