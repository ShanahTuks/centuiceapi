﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class SaleLine
    {
        
        public Guid SaleID { get; set; }
        public Guid ProductID { get; set; }
        public int Quantity { get; set; }
        public Guid ProductPriceID { get; set; }

        public Sale Sale { get; set; }
        public Product Product { get; set; }
        public ProductPrice ProductPrice { get; set; }

    }
}
