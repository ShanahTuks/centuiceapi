﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class VehicleStatus
    {
        [Key]
        public Guid VehicleStatusID { get; set; }
        [StringLength(20)]
        public string Description { get; set; }

    }
}
