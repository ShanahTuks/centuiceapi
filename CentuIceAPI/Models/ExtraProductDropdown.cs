﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class ExtraProductDropdown
    {
        public Guid ExtraProductTakenID { get; set; }
        public string ProductName {  get; set; }
        public int QuantityRemaining {  get; set; }
    }
}
