﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class Employee
    {
        [Key]
        public Guid EmployeeID { get; set; }
        public Guid BankID { get; set; }
        public Guid AccountTypeID { get; set; }
        public Guid EmployeeQualificationID { get; set; }
        public Guid EmployeeTypeID { get; set; }
        public Guid EmployeeStatusID { get; set; }
        public Guid EmployeeTitleID { get; set; }
        public Guid TableStatusID { get; set; }
        public ICollection<EmployeeAddress> EmployeeAddresses { get; set; }

        [StringLength(20)]
        public string KnownAsName { get; set; }
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string Surname { get; set; }
        [StringLength(50)]
        public string Email { get; set; }
        [StringLength(20)]
        public string IDNumber { get; set; }
        [StringLength(10)]
        public string HomeNumber { get; set; }
        [StringLength(20)]
        public string PassportNumber { get; set; }
        [StringLength(10)]
        public string CellNumber { get; set; }
        [StringLength(10)]
        public string WorkNumber { get; set; }
        [StringLength(10)]
        public string FingerprintAccountNumber { get; set; }
        [StringLength(10)]
        public string Initials { get; set; }
        public DateTime DateOfBirth { get; set; }
        [StringLength(50)]
        public string SystemUsername { get; set; }
        public string SystemPassword { get; set; }
        [StringLength(50)]
        public string BranchCode { get; set; }
        [StringLength(50)]
        public string AccountNumber { get; set; }
        [StringLength(50)]
        public string TaxNumber { get; set; }
        [StringLength(50)]
        public string PassportCountry { get; set; }
        public DateTime YearCompleted { get; set; }
        [StringLength(50)]
        public string InstitutionName { get; set; }
        public bool isLoggedIn { get; set; }
        public string OTP { get; set; }

        public Bank Bank { get; set; }
        public AccountType AccountType { get; set; }
        public EmployeeQualification EmployeeQualification { get; set; }
        public EmployeeType EmployeeType { get; set; }
        public EmployeeStatus EmployeeStatus { get; set; }
        public EmployeeTitle EmployeeTitle { get; set; }
        public TableStatus TableStatus { get; set; }


    }
}
