﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CentuIceAPI.Models
{
    public class VatConfiguration
    {
        public Guid VatConfigurationID { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public decimal VatPercentage { get; set; }
        public DateTime DateSet { get; set; }
        public bool Activated { get; set; }
    }
}
