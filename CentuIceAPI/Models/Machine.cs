﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CentuIceAPI.Models
{
    public class Machine
    {
      [Key]
        public Guid MachineID { get; set; }
        public Guid MachineTypeID { get; set; }
        public Guid TableStatusID { get; set; }
      [StringLength(100)]
      public string Make { get; set; }
      [StringLength(100)]
      public string Model { get; set; }
      public DateTime Last_Service_Date { get; set; }
      public DateTime Year_Made { get; set; }
      public ICollection<ServiceMachine> ServiceMachine { get; set; }
      public ICollection<MachineMachineData> MachineMachineData { get; set; }

        public MachineType MachineType { get; set; }
        public TableStatus TableStatus { get; set; }
    }
}
