﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class Client
    {
        [Key]
        public Guid ClientID { get; set; }
        public Guid TableStatusID { get; set; }
        [StringLength(50)]
        public string ClientName { get; set; }
        public ICollection<ClientTrend> ClientTrend { get; set; }
        public ICollection<ClientAddress> ClientAddresses { get; set; }

       
        [StringLength(200)]
        public string Note { get; set; }

        public TableStatus TableStatus { get; set; }

    }
}
