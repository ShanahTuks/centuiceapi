﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class Supplier
    {
        [Key]
        public Guid SupplierID { get; set; }
        [StringLength(50)]
        public string SupplierName { get; set; }
        public Guid TableStatusID { get; set; }
        [StringLength(100)]
        public string GoodsSupplied { get; set; }


        public TableStatus TableStatus { get; set; }

    }
}
