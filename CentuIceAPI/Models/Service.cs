﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CentuIceAPI.Models
{
    public class Service
    {
        [Key]
        public Guid ServiceID { get; set; }
        [ForeignKey("Supplier")]
        public Guid SupplierID { get; set; }
       public DateTime ServiceDate { get; set; }
        public ICollection<ServiceMachine> ServiceMachine { get; set; }
        public ICollection<ServiceVehicle> ServiceVehicle { get; set; }
        public ICollection<ServiceServiceJob> ServiceServiceJob { get; set; }

        public Supplier Supplier { get; set; }
    }
}
