﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class NotificationType
    {
        public Guid NotificationTypeID {  get; set; }
        public string TypeName {  get; set; }
    }
}
