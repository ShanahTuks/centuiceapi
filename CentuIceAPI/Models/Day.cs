﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class Day
    {
        [Key]
        public Guid DayID { get; set; }
        [StringLength(10)]
        public string DayName { get; set; }
        public ICollection<ClientTrend> ClientTrend { get; set; }
    }
}
