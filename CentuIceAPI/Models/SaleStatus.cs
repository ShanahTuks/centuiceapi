﻿using System;
namespace CentuIceAPI.Models
{
    public class SaleStatus
    {
        public Guid SaleStatusID { get; set; }
        public string Description { get; set; }
    }
}
