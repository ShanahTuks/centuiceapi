﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class EmployeeTitle
    {
        [Key]
        public Guid EmployeeTitleID { get; set; }
        [StringLength(10)]
        public string Description { get; set; }
    }
}
