﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class InventoryStockTake
    {
        [Key]
        public Guid InventoryStockTakeID { get; set; }
        public Guid StockTakeID { get; set; }
        public Guid InventoryID { get; set; }

        public Inventory Inventory { get; set; }
        public StockTake StockTake { get; set; }
    }
}
