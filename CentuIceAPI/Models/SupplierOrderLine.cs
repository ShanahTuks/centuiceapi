﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class SupplierOrderLine
    {
        [Key]
        public Guid SupplierOrderLineID { get; set; }
        public Guid InventoryID { get; set; }
        public Guid SupplierOrderID { get; set; }
        public Guid InventoryPriceID { get; set; }
        public int Quantity { get; set; }

        public Inventory Inventory { get; set; }
        public SupplierOrder SupplierOrder { get; set; }
        public InventoryPrice InventoryPrice { get; set; }

    }
}
