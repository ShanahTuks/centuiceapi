﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class AuthNotification
    {
        [Key]
        public Guid AuthNotificationID { get; set; }
        public Guid NotificationID { get; set; }
        [ForeignKey("Sender")]
        public Guid SenderID { get; set; }
        public string SenderName { get; set; }
        [ForeignKey("Owner")]
        public Guid OwnerID { get; set; }
        public Guid AuthNotificationTypeID { get; set; }
        public DateTime NotificationDateTime { get; set; }
        public string Identifier { get; set; }
        public Guid IdentifierID {  get; set; }
        
        //Records whether the auth response has been handled yet
        public bool isHandled { get; set; }

        //Records the actual reponse of the Auth Request
        public bool AuthResponse { get; set; }

        public AuthNotificationType AuthNotificationType { get; set; }
        public Employee Owner { get; set; }
        public Employee Sender { get; set; }
        public Notification Notification {  get; set; }
    }
}
