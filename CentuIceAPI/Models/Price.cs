﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CentuIceAPI.Models
{
    public class Price
    {

        public Guid PriceID { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public Decimal CurrentPrice { get; set; }


        public ICollection<ProductPrice> ProductPrices { get; set; }
        public ICollection<InventoryPrice> InventoryPrices { get; set; }

    }
}
