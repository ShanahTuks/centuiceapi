﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class Address
    {
        [Key]
        public Guid AddressID { get; set; }
        public Guid AddressTypeID { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string SuburbName { get; set; }
        public int StreetNumber { get; set; }
        [StringLength(100)]
        public string StreetName { get; set; }
        public int UnitNumber { get; set; }
        [StringLength(10)]
        public string PostalCode { get; set; }
        [StringLength(100)]
        public string Province { get; set; }
        [StringLength(100)]
        public string ComplexName { get; set; }
        public bool isActive { get; set; }

        public ICollection<EmployeeAddress> EmployeeAddresses { get; set; }
        public ICollection<ClientAddress> ClientAddresses { get; set; }

        
        public AddressType AddressType { get; set; }
        

    }
}
