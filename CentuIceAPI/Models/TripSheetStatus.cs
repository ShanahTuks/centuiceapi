﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class TripSheetStatus
    {
        [Key]
        public Guid TripSheetStatusID { get; set; }
        [StringLength(50)]
        public string Description { get; set; }
    }
}
