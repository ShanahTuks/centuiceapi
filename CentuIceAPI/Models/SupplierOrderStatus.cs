﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class SupplierOrderStatus
    {
        [Key]
        public Guid SupplierOrderStatusID { get; set; }
        [StringLength(20)]
        public string Description { get; set; }
        
    }
}
