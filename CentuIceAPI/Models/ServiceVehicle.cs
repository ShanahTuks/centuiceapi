﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CentuIceAPI.Models
{
    public class ServiceVehicle
    {
        
        
        public Guid ServiceID { get; set; }
        public Guid VehicleID { get; set; }

       public Service Service { get; set; }
       public Vehicle Vehicle { get; set; }

     

    }
}
