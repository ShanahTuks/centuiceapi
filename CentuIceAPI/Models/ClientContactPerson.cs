﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CentuIceAPI.Models
{
    public class ClientContactPerson
    {
        [Key]
        public Guid ClientContactPersonID { get; set; }
        public Guid ClientID { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(50)]
        public string PhoneNumber { get; set; }
        [StringLength(50)]
        public string EmailAddress { get; set; }

        public Client Client { get; set; }

    }
}
