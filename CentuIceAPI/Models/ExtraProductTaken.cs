﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Models
{
    public class ExtraProductTaken
    {
        [Key]
        public Guid ExtraProductTakenID {  get; set; }

        public Guid ProductPriceID { get; set; }
        public Guid ProductID {  get; set; }
        public Guid DeliveryTripSheetID {  get; set; }
        public int QuantityTaken {  get; set; }
        public int QuantityRemaining {  get; set; }

        public IList<OrderExtraProductTaken> OrderExtraProducts { get; set; }

        public Product Product {  get; set; }
        public DeliveryTripSheet DeliveryTripSheet {  get; set;}
    }
}
