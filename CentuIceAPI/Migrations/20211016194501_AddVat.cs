﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class AddVat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "VatActivated",
                table: "Sales",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isVatApplicable",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "VatActivated",
                table: "Orders",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "VatConfigurations",
                columns: table => new
                {
                    VatConfigurationID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    VatPercentage = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    DateSet = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Activated = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VatConfigurations", x => x.VatConfigurationID);
                });

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("1f4f7372-5fa4-43dc-a91f-8df14bd8943a"),
                column: "PhoneNumber",
                value: "0815377864");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("60cd1ada-02b0-4c3f-8b14-3f89c5bd36b3"),
                column: "PhoneNumber",
                value: "0733714164");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("6a4b3bc3-a1c6-4b67-8fd9-e161d7dcd899"),
                column: "PhoneNumber",
                value: "0764801037");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("780d9d5a-d1f0-4373-bd51-afb70cf0cfa7"),
                column: "PhoneNumber",
                value: "0740789791");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("8a949446-0b47-4c6c-b755-21e671ae0af0"),
                column: "PhoneNumber",
                value: "0814169470");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("8d2fb3d5-5c33-4ef6-a653-ba70258d88b8"),
                column: "PhoneNumber",
                value: "0740789791");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("9191fbfa-8e98-421f-9be4-49b12f3fd779"),
                column: "PhoneNumber",
                value: "0733714164");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("a5d5d77a-89a7-4012-aa6c-a493c01c4d0d"),
                column: "PhoneNumber",
                value: "0764801037");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("b89887de-1b68-48ae-bdf6-c8cfa1f9df07"),
                column: "PhoneNumber",
                value: "0814169470");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("bce06eb4-5efb-4ed4-b020-6ccd9332de4a"),
                column: "PhoneNumber",
                value: "0815377864");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("d5e4ac1b-ece0-428b-9e60-c4a1fbb05344"),
                column: "PhoneNumber",
                value: "0740789791");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("f0899973-8aa7-4749-84e6-aaa83bd35076"),
                column: "PhoneNumber",
                value: "0740789791");

            migrationBuilder.InsertData(
                table: "VatConfigurations",
                columns: new[] { "VatConfigurationID", "Activated", "DateSet", "VatPercentage" },
                values: new object[] { new Guid("a88ed992-45ba-4f71-b5da-613633a606f2"), false, new DateTime(2021, 10, 16, 21, 32, 0, 0, DateTimeKind.Unspecified), 0.15m });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VatConfigurations");

            migrationBuilder.DropColumn(
                name: "VatActivated",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "isVatApplicable",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "VatActivated",
                table: "Orders");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("1f4f7372-5fa4-43dc-a91f-8df14bd8943a"),
                column: "PhoneNumber",
                value: "0824259217");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("60cd1ada-02b0-4c3f-8b14-3f89c5bd36b3"),
                column: "PhoneNumber",
                value: "0126513800");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("6a4b3bc3-a1c6-4b67-8fd9-e161d7dcd899"),
                column: "PhoneNumber",
                value: "0126671948");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("780d9d5a-d1f0-4373-bd51-afb70cf0cfa7"),
                column: "PhoneNumber",
                value: "0780802528");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("8a949446-0b47-4c6c-b755-21e671ae0af0"),
                column: "PhoneNumber",
                value: "0126671948");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("8d2fb3d5-5c33-4ef6-a653-ba70258d88b8"),
                column: "PhoneNumber",
                value: "0126671948");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("9191fbfa-8e98-421f-9be4-49b12f3fd779"),
                column: "PhoneNumber",
                value: "0123456789");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("a5d5d77a-89a7-4012-aa6c-a493c01c4d0d"),
                column: "PhoneNumber",
                value: "0126671948");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("b89887de-1b68-48ae-bdf6-c8cfa1f9df07"),
                column: "PhoneNumber",
                value: "0126671948");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("bce06eb4-5efb-4ed4-b020-6ccd9332de4a"),
                column: "PhoneNumber",
                value: "0123456789");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("d5e4ac1b-ece0-428b-9e60-c4a1fbb05344"),
                column: "PhoneNumber",
                value: "0126634393");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("f0899973-8aa7-4749-84e6-aaa83bd35076"),
                column: "PhoneNumber",
                value: "0126671948");
        }
    }
}
