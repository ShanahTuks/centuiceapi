﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class ChangedVat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "VatConfigurationID",
                table: "Sales",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("a88ed992-45ba-4f71-b5da-613633a606f2"));

            migrationBuilder.AddColumn<Guid>(
                name: "VatConfigurationID",
                table: "Orders",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("a88ed992-45ba-4f71-b5da-613633a606f2"));

            migrationBuilder.CreateIndex(
                name: "IX_Sales_VatConfigurationID",
                table: "Sales",
                column: "VatConfigurationID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_VatConfigurationID",
                table: "Orders",
                column: "VatConfigurationID");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_VatConfigurations_VatConfigurationID",
                table: "Orders",
                column: "VatConfigurationID",
                principalTable: "VatConfigurations",
                principalColumn: "VatConfigurationID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_VatConfigurations_VatConfigurationID",
                table: "Sales",
                column: "VatConfigurationID",
                principalTable: "VatConfigurations",
                principalColumn: "VatConfigurationID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_VatConfigurations_VatConfigurationID",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Sales_VatConfigurations_VatConfigurationID",
                table: "Sales");

            migrationBuilder.DropIndex(
                name: "IX_Sales_VatConfigurationID",
                table: "Sales");

            migrationBuilder.DropIndex(
                name: "IX_Orders_VatConfigurationID",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "VatConfigurationID",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "VatConfigurationID",
                table: "Orders");
        }
    }
}
