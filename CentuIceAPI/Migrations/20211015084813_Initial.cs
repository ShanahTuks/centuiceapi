﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccountTypes",
                columns: table => new
                {
                    AccountTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountTypes", x => x.AccountTypeID);
                });

            migrationBuilder.CreateTable(
                name: "AddressTypes",
                columns: table => new
                {
                    AddressTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AddressTypes", x => x.AddressTypeID);
                });

            migrationBuilder.CreateTable(
                name: "AuthNotificationTypes",
                columns: table => new
                {
                    AuthNotificationTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthNotificationTypes", x => x.AuthNotificationTypeID);
                });

            migrationBuilder.CreateTable(
                name: "AuthWriteOffs",
                columns: table => new
                {
                    AuthWriteOffID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InventoryID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    WrittenOffReasonID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    WriteOffDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    InventoryItem = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthWriteOffs", x => x.AuthWriteOffID);
                });

            migrationBuilder.CreateTable(
                name: "Banks",
                columns: table => new
                {
                    BankID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Banks", x => x.BankID);
                });

            migrationBuilder.CreateTable(
                name: "Days",
                columns: table => new
                {
                    DayID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DayName = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Days", x => x.DayID);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeQualifications",
                columns: table => new
                {
                    EmployeeQualificationID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeQualifications", x => x.EmployeeQualificationID);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeStatuses",
                columns: table => new
                {
                    EmployeeStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeStatuses", x => x.EmployeeStatusID);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeTitles",
                columns: table => new
                {
                    EmployeeTitleID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeTitles", x => x.EmployeeTitleID);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeTripSheetTypes",
                columns: table => new
                {
                    EmployeeTripSheetTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeTripSheetTypes", x => x.EmployeeTripSheetTypeID);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeTypes",
                columns: table => new
                {
                    EmployeeTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeTypes", x => x.EmployeeTypeID);
                });

            migrationBuilder.CreateTable(
                name: "InventoryTypes",
                columns: table => new
                {
                    InventoryTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryTypes", x => x.InventoryTypeID);
                });

            migrationBuilder.CreateTable(
                name: "InvoiceNumbers",
                columns: table => new
                {
                    InvoiceNumberID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Number = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoiceNumbers", x => x.InvoiceNumberID);
                });

            migrationBuilder.CreateTable(
                name: "InvoiceStatus",
                columns: table => new
                {
                    InvoiceStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoiceStatus", x => x.InvoiceStatusID);
                });

            migrationBuilder.CreateTable(
                name: "MachineData",
                columns: table => new
                {
                    MachineDataID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Cycles = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Weight = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    WaterUse = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    FullBin = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    RunTime = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    AVGCycleMin = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MachineData", x => x.MachineDataID);
                });

            migrationBuilder.CreateTable(
                name: "MachineTypes",
                columns: table => new
                {
                    MachineTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MachineTypes", x => x.MachineTypeID);
                });

            migrationBuilder.CreateTable(
                name: "NotificationTimers",
                columns: table => new
                {
                    NotificationTimerID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NotificationType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NotificationReason = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TimePeriod = table.Column<int>(type: "int", nullable: false),
                    CheckTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationTimers", x => x.NotificationTimerID);
                });

            migrationBuilder.CreateTable(
                name: "NotificationTypes",
                columns: table => new
                {
                    NotificationTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TypeName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationTypes", x => x.NotificationTypeID);
                });

            migrationBuilder.CreateTable(
                name: "OrderStatuses",
                columns: table => new
                {
                    OrderStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderStatuses", x => x.OrderStatusID);
                });

            migrationBuilder.CreateTable(
                name: "PaymentMethods",
                columns: table => new
                {
                    PaymentMethodID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentMethods", x => x.PaymentMethodID);
                });

            migrationBuilder.CreateTable(
                name: "Prices",
                columns: table => new
                {
                    PriceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CurrentPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prices", x => x.PriceID);
                });

            migrationBuilder.CreateTable(
                name: "SaleStatuses",
                columns: table => new
                {
                    SaleStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleStatuses", x => x.SaleStatusID);
                });

            migrationBuilder.CreateTable(
                name: "ServiceJobs",
                columns: table => new
                {
                    ServiceJobID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ServiceJobName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    CostOfJob = table.Column<decimal>(type: "decimal(10,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceJobs", x => x.ServiceJobID);
                });

            migrationBuilder.CreateTable(
                name: "StockTakes",
                columns: table => new
                {
                    StockTakeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DateOfStockTake = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StockTakes", x => x.StockTakeID);
                });

            migrationBuilder.CreateTable(
                name: "SupplierOrderStatuses",
                columns: table => new
                {
                    SupplierOrderStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupplierOrderStatuses", x => x.SupplierOrderStatusID);
                });

            migrationBuilder.CreateTable(
                name: "TableStatuses",
                columns: table => new
                {
                    TableStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TableStatuses", x => x.TableStatusID);
                });

            migrationBuilder.CreateTable(
                name: "TripSheetStatuses",
                columns: table => new
                {
                    TripSheetStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TripSheetStatuses", x => x.TripSheetStatusID);
                });

            migrationBuilder.CreateTable(
                name: "VehicleStatuses",
                columns: table => new
                {
                    VehicleStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleStatuses", x => x.VehicleStatusID);
                });

            migrationBuilder.CreateTable(
                name: "WrittenOffReasons",
                columns: table => new
                {
                    WrittenOffReasonID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WrittenOffReasons", x => x.WrittenOffReasonID);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    AddressID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AddressTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CountryName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CityName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SuburbName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StreetNumber = table.Column<int>(type: "int", nullable: false),
                    StreetName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    UnitNumber = table.Column<int>(type: "int", nullable: false),
                    PostalCode = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    Province = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ComplexName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    isActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.AddressID);
                    table.ForeignKey(
                        name: "FK_Addresses_AddressTypes_AddressTypeID",
                        column: x => x.AddressTypeID,
                        principalTable: "AddressTypes",
                        principalColumn: "AddressTypeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Invoices",
                columns: table => new
                {
                    InvoiceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InvoiceStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    PaymentMethodID = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    InvoiceDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    InvoiceNumber = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices", x => x.InvoiceID);
                    table.ForeignKey(
                        name: "FK_Invoices_InvoiceStatus_InvoiceStatusID",
                        column: x => x.InvoiceStatusID,
                        principalTable: "InvoiceStatus",
                        principalColumn: "InvoiceStatusID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Invoices_PaymentMethods_PaymentMethodID",
                        column: x => x.PaymentMethodID,
                        principalTable: "PaymentMethods",
                        principalColumn: "PaymentMethodID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    ClientID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TableStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ClientName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Note = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.ClientID);
                    table.ForeignKey(
                        name: "FK_Clients_TableStatuses_TableStatusID",
                        column: x => x.TableStatusID,
                        principalTable: "TableStatuses",
                        principalColumn: "TableStatusID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    EmployeeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BankID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AccountTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeQualificationID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeTitleID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TableStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    KnownAsName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Surname = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IDNumber = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    HomeNumber = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    PassportNumber = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    CellNumber = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    WorkNumber = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    FingerprintAccountNumber = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    Initials = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    DateOfBirth = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SystemUsername = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    SystemPassword = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BranchCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    AccountNumber = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    TaxNumber = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    PassportCountry = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    YearCompleted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    InstitutionName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    isLoggedIn = table.Column<bool>(type: "bit", nullable: false),
                    OTP = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.EmployeeID);
                    table.ForeignKey(
                        name: "FK_Employees_AccountTypes_AccountTypeID",
                        column: x => x.AccountTypeID,
                        principalTable: "AccountTypes",
                        principalColumn: "AccountTypeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employees_Banks_BankID",
                        column: x => x.BankID,
                        principalTable: "Banks",
                        principalColumn: "BankID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employees_EmployeeQualifications_EmployeeQualificationID",
                        column: x => x.EmployeeQualificationID,
                        principalTable: "EmployeeQualifications",
                        principalColumn: "EmployeeQualificationID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employees_EmployeeStatuses_EmployeeStatusID",
                        column: x => x.EmployeeStatusID,
                        principalTable: "EmployeeStatuses",
                        principalColumn: "EmployeeStatusID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employees_EmployeeTitles_EmployeeTitleID",
                        column: x => x.EmployeeTitleID,
                        principalTable: "EmployeeTitles",
                        principalColumn: "EmployeeTitleID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employees_EmployeeTypes_EmployeeTypeID",
                        column: x => x.EmployeeTypeID,
                        principalTable: "EmployeeTypes",
                        principalColumn: "EmployeeTypeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employees_TableStatuses_TableStatusID",
                        column: x => x.TableStatusID,
                        principalTable: "TableStatuses",
                        principalColumn: "TableStatusID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Machines",
                columns: table => new
                {
                    MachineID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    MachineTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TableStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Make = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Model = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Last_Service_Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Year_Made = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Machines", x => x.MachineID);
                    table.ForeignKey(
                        name: "FK_Machines_MachineTypes_MachineTypeID",
                        column: x => x.MachineTypeID,
                        principalTable: "MachineTypes",
                        principalColumn: "MachineTypeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Machines_TableStatuses_TableStatusID",
                        column: x => x.TableStatusID,
                        principalTable: "TableStatuses",
                        principalColumn: "TableStatusID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Suppliers",
                columns: table => new
                {
                    SupplierID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SupplierName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    TableStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    GoodsSupplied = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Suppliers", x => x.SupplierID);
                    table.ForeignKey(
                        name: "FK_Suppliers_TableStatuses_TableStatusID",
                        column: x => x.TableStatusID,
                        principalTable: "TableStatuses",
                        principalColumn: "TableStatusID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Vehicles",
                columns: table => new
                {
                    VehicleID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    VehicleStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Make = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Model = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Reg_Number = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    LastServicedMileage = table.Column<int>(type: "int", nullable: false),
                    Mileage = table.Column<int>(type: "int", nullable: false),
                    Tracker_Number = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Last_Service_Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Year_Made = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Capacity = table.Column<int>(type: "int", nullable: false),
                    License_Expiry_Date = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicles", x => x.VehicleID);
                    table.ForeignKey(
                        name: "FK_Vehicles_VehicleStatuses_VehicleStatusID",
                        column: x => x.VehicleStatusID,
                        principalTable: "VehicleStatuses",
                        principalColumn: "VehicleStatusID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Sales",
                columns: table => new
                {
                    SaleID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SaleDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SaleStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InvoiceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sales", x => x.SaleID);
                    table.ForeignKey(
                        name: "FK_Sales_Invoices_InvoiceID",
                        column: x => x.InvoiceID,
                        principalTable: "Invoices",
                        principalColumn: "InvoiceID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Sales_SaleStatuses_SaleStatusID",
                        column: x => x.SaleStatusID,
                        principalTable: "SaleStatuses",
                        principalColumn: "SaleStatusID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientAddresses",
                columns: table => new
                {
                    ClientID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AddressID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientAddresses", x => new { x.AddressID, x.ClientID });
                    table.ForeignKey(
                        name: "FK_ClientAddresses_Addresses_AddressID",
                        column: x => x.AddressID,
                        principalTable: "Addresses",
                        principalColumn: "AddressID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientAddresses_Clients_ClientID",
                        column: x => x.ClientID,
                        principalTable: "Clients",
                        principalColumn: "ClientID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientContactPeople",
                columns: table => new
                {
                    ClientContactPersonID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ClientID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    EmailAddress = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientContactPeople", x => x.ClientContactPersonID);
                    table.ForeignKey(
                        name: "FK_ClientContactPeople_Clients_ClientID",
                        column: x => x.ClientID,
                        principalTable: "Clients",
                        principalColumn: "ClientID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientTrends",
                columns: table => new
                {
                    DayID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ClientID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientTrends", x => new { x.DayID, x.ClientID });
                    table.ForeignKey(
                        name: "FK_ClientTrends_Clients_ClientID",
                        column: x => x.ClientID,
                        principalTable: "Clients",
                        principalColumn: "ClientID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientTrends_Days_DayID",
                        column: x => x.DayID,
                        principalTable: "Days",
                        principalColumn: "DayID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AuditTrails",
                columns: table => new
                {
                    AuditTrailID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ActionDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EmployeeAction = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ActionDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ActionData = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuditTrails", x => x.AuditTrailID);
                    table.ForeignKey(
                        name: "FK_AuditTrails_Employees_EmployeeID",
                        column: x => x.EmployeeID,
                        principalTable: "Employees",
                        principalColumn: "EmployeeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DailyAttendances",
                columns: table => new
                {
                    DailyAttendanceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CheckInTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CheckOutTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DailyAttendances", x => x.DailyAttendanceID);
                    table.ForeignKey(
                        name: "FK_DailyAttendances_Employees_EmployeeID",
                        column: x => x.EmployeeID,
                        principalTable: "Employees",
                        principalColumn: "EmployeeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmergencyContacts",
                columns: table => new
                {
                    EmergencyContactID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ContactPerson = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ContactTelephoneNumber = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    RelationshipToEmployee = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmergencyContacts", x => x.EmergencyContactID);
                    table.ForeignKey(
                        name: "FK_EmergencyContacts_Employees_EmployeeID",
                        column: x => x.EmployeeID,
                        principalTable: "Employees",
                        principalColumn: "EmployeeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeAddresses",
                columns: table => new
                {
                    EmployeeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AddressID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeAddresses", x => new { x.AddressID, x.EmployeeID });
                    table.ForeignKey(
                        name: "FK_EmployeeAddresses_Addresses_AddressID",
                        column: x => x.AddressID,
                        principalTable: "Addresses",
                        principalColumn: "AddressID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmployeeAddresses_Employees_EmployeeID",
                        column: x => x.EmployeeID,
                        principalTable: "Employees",
                        principalColumn: "EmployeeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeErrors",
                columns: table => new
                {
                    EmployeeErrorID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ErrorCode = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    DescriptionOfError = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    DateOfError = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeErrors", x => x.EmployeeErrorID);
                    table.ForeignKey(
                        name: "FK_EmployeeErrors_Employees_EmployeeID",
                        column: x => x.EmployeeID,
                        principalTable: "Employees",
                        principalColumn: "EmployeeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    NotificationID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NotificationTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NotificationTitle = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NotificationDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsRead = table.Column<bool>(type: "bit", nullable: false),
                    Show = table.Column<bool>(type: "bit", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.NotificationID);
                    table.ForeignKey(
                        name: "FK_Notifications_Employees_EmployeeID",
                        column: x => x.EmployeeID,
                        principalTable: "Employees",
                        principalColumn: "EmployeeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Notifications_NotificationTypes_NotificationTypeID",
                        column: x => x.NotificationTypeID,
                        principalTable: "NotificationTypes",
                        principalColumn: "NotificationTypeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MachineMachineData",
                columns: table => new
                {
                    MachineID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    MachineDataID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MachineMachineData", x => new { x.MachineID, x.MachineDataID });
                    table.ForeignKey(
                        name: "FK_MachineMachineData_MachineData_MachineDataID",
                        column: x => x.MachineDataID,
                        principalTable: "MachineData",
                        principalColumn: "MachineDataID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MachineMachineData_Machines_MachineID",
                        column: x => x.MachineID,
                        principalTable: "Machines",
                        principalColumn: "MachineID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Inventories",
                columns: table => new
                {
                    InventoryID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SupplierID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InventoryTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TableStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ItemName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inventories", x => x.InventoryID);
                    table.ForeignKey(
                        name: "FK_Inventories_InventoryTypes_InventoryTypeID",
                        column: x => x.InventoryTypeID,
                        principalTable: "InventoryTypes",
                        principalColumn: "InventoryTypeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Inventories_Suppliers_SupplierID",
                        column: x => x.SupplierID,
                        principalTable: "Suppliers",
                        principalColumn: "SupplierID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Inventories_TableStatuses_TableStatusID",
                        column: x => x.TableStatusID,
                        principalTable: "TableStatuses",
                        principalColumn: "TableStatusID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Services",
                columns: table => new
                {
                    ServiceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SupplierID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ServiceDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Services", x => x.ServiceID);
                    table.ForeignKey(
                        name: "FK_Services_Suppliers_SupplierID",
                        column: x => x.SupplierID,
                        principalTable: "Suppliers",
                        principalColumn: "SupplierID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SupplierContactPeople",
                columns: table => new
                {
                    SupplierContactPersonID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SupplierID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    EmailAddress = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupplierContactPeople", x => x.SupplierContactPersonID);
                    table.ForeignKey(
                        name: "FK_SupplierContactPeople_Suppliers_SupplierID",
                        column: x => x.SupplierID,
                        principalTable: "Suppliers",
                        principalColumn: "SupplierID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SupplierOrders",
                columns: table => new
                {
                    SupplierOrderID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SupplierID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SupplierOrderStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Details = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    DateReceived = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DatePlaced = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupplierOrders", x => x.SupplierOrderID);
                    table.ForeignKey(
                        name: "FK_SupplierOrders_SupplierOrderStatuses_SupplierOrderStatusID",
                        column: x => x.SupplierOrderStatusID,
                        principalTable: "SupplierOrderStatuses",
                        principalColumn: "SupplierOrderStatusID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SupplierOrders_Suppliers_SupplierID",
                        column: x => x.SupplierID,
                        principalTable: "Suppliers",
                        principalColumn: "SupplierID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DeliveryTripSheets",
                columns: table => new
                {
                    DeliveryTripSheetID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    VehicleID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DriverID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AssistantID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SupervisorID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TripSheetStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DepatureTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ReturnTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Comment = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    StartKM = table.Column<int>(type: "int", nullable: false),
                    EndKM = table.Column<int>(type: "int", nullable: true),
                    VehicleValidationImage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FinalLoaded = table.Column<bool>(type: "bit", nullable: false),
                    FinalDelivered = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeliveryTripSheets", x => x.DeliveryTripSheetID);
                    table.ForeignKey(
                        name: "FK_DeliveryTripSheets_Employees_AssistantID",
                        column: x => x.AssistantID,
                        principalTable: "Employees",
                        principalColumn: "EmployeeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DeliveryTripSheets_Employees_DriverID",
                        column: x => x.DriverID,
                        principalTable: "Employees",
                        principalColumn: "EmployeeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DeliveryTripSheets_Employees_SupervisorID",
                        column: x => x.SupervisorID,
                        principalTable: "Employees",
                        principalColumn: "EmployeeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DeliveryTripSheets_TripSheetStatuses_TripSheetStatusID",
                        column: x => x.TripSheetStatusID,
                        principalTable: "TripSheetStatuses",
                        principalColumn: "TripSheetStatusID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DeliveryTripSheets_Vehicles_VehicleID",
                        column: x => x.VehicleID,
                        principalTable: "Vehicles",
                        principalColumn: "VehicleID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AuthNotifications",
                columns: table => new
                {
                    AuthNotificationID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NotificationID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SenderID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SenderName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OwnerID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuthNotificationTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NotificationDateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Identifier = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IdentifierID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    isHandled = table.Column<bool>(type: "bit", nullable: false),
                    AuthResponse = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthNotifications", x => x.AuthNotificationID);
                    table.ForeignKey(
                        name: "FK_AuthNotifications_AuthNotificationTypes_AuthNotificationTypeID",
                        column: x => x.AuthNotificationTypeID,
                        principalTable: "AuthNotificationTypes",
                        principalColumn: "AuthNotificationTypeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AuthNotifications_Employees_OwnerID",
                        column: x => x.OwnerID,
                        principalTable: "Employees",
                        principalColumn: "EmployeeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AuthNotifications_Employees_SenderID",
                        column: x => x.SenderID,
                        principalTable: "Employees",
                        principalColumn: "EmployeeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AuthNotifications_Notifications_NotificationID",
                        column: x => x.NotificationID,
                        principalTable: "Notifications",
                        principalColumn: "NotificationID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InventoryPrices",
                columns: table => new
                {
                    InventoryPriceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PriceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InventoryID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryPrices", x => x.InventoryPriceID);
                    table.ForeignKey(
                        name: "FK_InventoryPrices_Inventories_InventoryID",
                        column: x => x.InventoryID,
                        principalTable: "Inventories",
                        principalColumn: "InventoryID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InventoryPrices_Prices_PriceID",
                        column: x => x.PriceID,
                        principalTable: "Prices",
                        principalColumn: "PriceID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    ProductID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InventoryID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TableStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.ProductID);
                    table.ForeignKey(
                        name: "FK_Products_Inventories_InventoryID",
                        column: x => x.InventoryID,
                        principalTable: "Inventories",
                        principalColumn: "InventoryID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_TableStatuses_TableStatusID",
                        column: x => x.TableStatusID,
                        principalTable: "TableStatuses",
                        principalColumn: "TableStatusID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StockTakeLines",
                columns: table => new
                {
                    StockTakeLineID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StockTakeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InventoryID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StockTakeQuantity = table.Column<int>(type: "int", nullable: false),
                    DiscrepancyAmount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StockTakeLines", x => x.StockTakeLineID);
                    table.ForeignKey(
                        name: "FK_StockTakeLines_Inventories_InventoryID",
                        column: x => x.InventoryID,
                        principalTable: "Inventories",
                        principalColumn: "InventoryID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StockTakeLines_StockTakes_StockTakeID",
                        column: x => x.StockTakeID,
                        principalTable: "StockTakes",
                        principalColumn: "StockTakeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StockWrittenOff",
                columns: table => new
                {
                    StockWrittenOffID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InventoryID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    WrittenOffReasonID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    WriteOffDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StockWrittenOff", x => x.StockWrittenOffID);
                    table.ForeignKey(
                        name: "FK_StockWrittenOff_Inventories_InventoryID",
                        column: x => x.InventoryID,
                        principalTable: "Inventories",
                        principalColumn: "InventoryID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StockWrittenOff_WrittenOffReasons_WrittenOffReasonID",
                        column: x => x.WrittenOffReasonID,
                        principalTable: "WrittenOffReasons",
                        principalColumn: "WrittenOffReasonID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceMachines",
                columns: table => new
                {
                    ServiceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    MachineID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceMachines", x => new { x.ServiceID, x.MachineID });
                    table.ForeignKey(
                        name: "FK_ServiceMachines_Machines_MachineID",
                        column: x => x.MachineID,
                        principalTable: "Machines",
                        principalColumn: "MachineID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceMachines_Services_ServiceID",
                        column: x => x.ServiceID,
                        principalTable: "Services",
                        principalColumn: "ServiceID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceServiceJobs",
                columns: table => new
                {
                    ServiceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ServiceJobID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceServiceJobs", x => new { x.ServiceID, x.ServiceJobID });
                    table.ForeignKey(
                        name: "FK_ServiceServiceJobs_ServiceJobs_ServiceJobID",
                        column: x => x.ServiceJobID,
                        principalTable: "ServiceJobs",
                        principalColumn: "ServiceJobID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceServiceJobs_Services_ServiceID",
                        column: x => x.ServiceID,
                        principalTable: "Services",
                        principalColumn: "ServiceID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceVehicles",
                columns: table => new
                {
                    ServiceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    VehicleID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceVehicles", x => new { x.ServiceID, x.VehicleID });
                    table.ForeignKey(
                        name: "FK_ServiceVehicles_Services_ServiceID",
                        column: x => x.ServiceID,
                        principalTable: "Services",
                        principalColumn: "ServiceID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceVehicles_Vehicles_VehicleID",
                        column: x => x.VehicleID,
                        principalTable: "Vehicles",
                        principalColumn: "VehicleID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeTripSheets",
                columns: table => new
                {
                    EmployeeTripSheetID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DeliveryTripSheetID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeTripSheetTypeID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeTripSheets", x => x.EmployeeTripSheetID);
                    table.ForeignKey(
                        name: "FK_EmployeeTripSheets_DeliveryTripSheets_DeliveryTripSheetID",
                        column: x => x.DeliveryTripSheetID,
                        principalTable: "DeliveryTripSheets",
                        principalColumn: "DeliveryTripSheetID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmployeeTripSheets_Employees_EmployeeID",
                        column: x => x.EmployeeID,
                        principalTable: "Employees",
                        principalColumn: "EmployeeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmployeeTripSheets_EmployeeTripSheetTypes_EmployeeTripSheetTypeID",
                        column: x => x.EmployeeTripSheetTypeID,
                        principalTable: "EmployeeTripSheetTypes",
                        principalColumn: "EmployeeTripSheetTypeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    OrderID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OrderStatusID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ClientID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AddressID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DeliveryTripSheetID = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OrderDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Delivered = table.Column<bool>(type: "bit", nullable: false),
                    Loaded = table.Column<bool>(type: "bit", nullable: false),
                    DeliveryDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    InvoiceID = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.OrderID);
                    table.ForeignKey(
                        name: "FK_Orders_Addresses_AddressID",
                        column: x => x.AddressID,
                        principalTable: "Addresses",
                        principalColumn: "AddressID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Clients_ClientID",
                        column: x => x.ClientID,
                        principalTable: "Clients",
                        principalColumn: "ClientID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_DeliveryTripSheets_DeliveryTripSheetID",
                        column: x => x.DeliveryTripSheetID,
                        principalTable: "DeliveryTripSheets",
                        principalColumn: "DeliveryTripSheetID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Invoices_InvoiceID",
                        column: x => x.InvoiceID,
                        principalTable: "Invoices",
                        principalColumn: "InvoiceID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_OrderStatuses_OrderStatusID",
                        column: x => x.OrderStatusID,
                        principalTable: "OrderStatuses",
                        principalColumn: "OrderStatusID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SupplierOrderLines",
                columns: table => new
                {
                    SupplierOrderLineID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InventoryID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SupplierOrderID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InventoryPriceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupplierOrderLines", x => x.SupplierOrderLineID);
                    table.ForeignKey(
                        name: "FK_SupplierOrderLines_Inventories_InventoryID",
                        column: x => x.InventoryID,
                        principalTable: "Inventories",
                        principalColumn: "InventoryID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SupplierOrderLines_InventoryPrices_InventoryPriceID",
                        column: x => x.InventoryPriceID,
                        principalTable: "InventoryPrices",
                        principalColumn: "InventoryPriceID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SupplierOrderLines_SupplierOrders_SupplierOrderID",
                        column: x => x.SupplierOrderID,
                        principalTable: "SupplierOrders",
                        principalColumn: "SupplierOrderID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExtraProductsTaken",
                columns: table => new
                {
                    ExtraProductTakenID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductPriceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DeliveryTripSheetID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    QuantityTaken = table.Column<int>(type: "int", nullable: false),
                    QuantityRemaining = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExtraProductsTaken", x => x.ExtraProductTakenID);
                    table.ForeignKey(
                        name: "FK_ExtraProductsTaken_DeliveryTripSheets_DeliveryTripSheetID",
                        column: x => x.DeliveryTripSheetID,
                        principalTable: "DeliveryTripSheets",
                        principalColumn: "DeliveryTripSheetID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExtraProductsTaken_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ProductID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductPrices",
                columns: table => new
                {
                    ProductPriceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PriceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPrices", x => x.ProductPriceID);
                    table.ForeignKey(
                        name: "FK_ProductPrices_Prices_PriceID",
                        column: x => x.PriceID,
                        principalTable: "Prices",
                        principalColumn: "PriceID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductPrices_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ProductID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrderExtraProductsTaken",
                columns: table => new
                {
                    OrderID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ExtraProductTakenID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderExtraProductsTaken", x => new { x.ExtraProductTakenID, x.OrderID });
                    table.ForeignKey(
                        name: "FK_OrderExtraProductsTaken_ExtraProductsTaken_ExtraProductTakenID",
                        column: x => x.ExtraProductTakenID,
                        principalTable: "ExtraProductsTaken",
                        principalColumn: "ExtraProductTakenID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderExtraProductsTaken_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "OrderID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrderLines",
                columns: table => new
                {
                    OrderID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductPriceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderLines", x => new { x.OrderID, x.ProductID });
                    table.ForeignKey(
                        name: "FK_OrderLines_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "OrderID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderLines_ProductPrices_ProductPriceID",
                        column: x => x.ProductPriceID,
                        principalTable: "ProductPrices",
                        principalColumn: "ProductPriceID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderLines_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ProductID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SaleLines",
                columns: table => new
                {
                    SaleID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    ProductPriceID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleLines", x => new { x.SaleID, x.ProductID });
                    table.ForeignKey(
                        name: "FK_SaleLines_ProductPrices_ProductPriceID",
                        column: x => x.ProductPriceID,
                        principalTable: "ProductPrices",
                        principalColumn: "ProductPriceID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SaleLines_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ProductID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SaleLines_Sales_SaleID",
                        column: x => x.SaleID,
                        principalTable: "Sales",
                        principalColumn: "SaleID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AccountTypes",
                columns: new[] { "AccountTypeID", "Description" },
                values: new object[,]
                {
                    { new Guid("17da522e-0668-497d-978c-23e763a7fbe9"), "Savings" },
                    { new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), "Cheque" },
                    { new Guid("05c6bd34-e24e-468f-a620-724b5daaba1a"), "Money Market Investment" },
                    { new Guid("69d4730c-fbdd-4d3a-890f-77f39d5cb13a"), "Mzansi" }
                });

            migrationBuilder.InsertData(
                table: "AddressTypes",
                columns: new[] { "AddressTypeID", "Description" },
                values: new object[,]
                {
                    { new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Primary Physical Address" },
                    { new Guid("a8901b0d-4a81-440e-8707-bf052a2fef0b"), "Postal Address" },
                    { new Guid("68252ff8-14f6-4c1e-8ca1-954b9e598fea"), "Secondary Physical Address" }
                });

            migrationBuilder.InsertData(
                table: "AuthNotificationTypes",
                columns: new[] { "AuthNotificationTypeID", "Description" },
                values: new object[,]
                {
                    { new Guid("4ae95884-8692-43c6-8829-c0d787518dff"), "DeactivateInventory" },
                    { new Guid("866d1081-598c-46a0-bcd0-fdd8dec76aca"), "DeactivateProduct" },
                    { new Guid("06ae874b-3e31-4845-92e9-7d83fc8db842"), "WriteOffStock" },
                    { new Guid("56e8715b-dee9-4fc6-929d-648cc4dd18a1"), "DeactivateSupplier" },
                    { new Guid("1864db97-436e-4bdc-a4b5-de563435b49d"), "DeactivateMachine" },
                    { new Guid("70a6f99f-2283-45e7-9b1c-8f64b52dae5e"), "DeactivateVehicle" },
                    { new Guid("3eadd87c-2cc3-4537-a0fa-e693d373319f"), "DeactivateClient" },
                    { new Guid("2efc45a3-fece-45c3-80fb-f7386dc0c625"), "DeactivateEmployee" }
                });

            migrationBuilder.InsertData(
                table: "Banks",
                columns: new[] { "BankID", "Name" },
                values: new object[,]
                {
                    { new Guid("4e77bc48-c947-41a0-b0b6-35fd353b828a"), "Ubank Limited" },
                    { new Guid("5b56c0d1-a1e7-4aab-be11-58ae40011fcf"), "Sasfin Bank Limited" },
                    { new Guid("325d4acb-cd0e-4a52-b23f-a1e2a5e76d6a"), "Imperial Bank" },
                    { new Guid("9fa3596d-0d12-4024-a97c-e7f53b86d4a0"), "Grindrod Bank Limited" },
                    { new Guid("906ac52d-6385-4a84-ab25-0f720da3876d"), "FirstRand Bank" },
                    { new Guid("ec5c1000-653f-42b8-bc2b-97e2c044757d"), "Bidvest" },
                    { new Guid("79d156d1-ad69-488b-b40e-a5218c7a1470"), "Discovery Limited" },
                    { new Guid("d2e85b9b-cfce-4719-80b7-32e6a3e09e8c"), "TymeBank" },
                    { new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "Investec" },
                    { new Guid("aa3f969a-418e-4e6d-9169-c4e3d4a01530"), "ABSA" },
                    { new Guid("ac7ad177-71bd-445d-9e35-a989dfcfe53a"), "FNB" },
                    { new Guid("6424c0fe-d596-48fe-aafd-878384d9b661"), "African Bank" },
                    { new Guid("0425a289-c5dc-4532-accc-17666c9dbf6d"), "Standard Bank" },
                    { new Guid("f1635eaf-62a3-41e9-9655-4b8e545e8f82"), "Netbank" },
                    { new Guid("026665f4-2a0c-41e1-b388-c119f8851737"), "Capitec" }
                });

            migrationBuilder.InsertData(
                table: "EmployeeQualifications",
                columns: new[] { "EmployeeQualificationID", "Description" },
                values: new object[,]
                {
                    { new Guid("28db0bf3-8154-44df-8fc3-0be52422e42c"), "NQF 1 / Grade 9" },
                    { new Guid("9000f05f-a9ad-4dd5-83e1-ea9ddd526413"), "NQF 10 / Doctor's Degree" },
                    { new Guid("e4bd1751-dead-46f0-9328-0209e141958a"), "NQF 3 / Grade 11" },
                    { new Guid("f4b1ddc9-a629-4282-96e8-31a515243487"), "NQF 4 / Grade 12" },
                    { new Guid("4d170aa7-4088-4612-956a-6cc04c05196e"), "NQF 5 / Higher Certificates" },
                    { new Guid("bdd214ce-57a8-4452-8fb8-854339abc531"), "NQF 6 / National Diploma" },
                    { new Guid("33639587-9b4f-4afc-8a54-87e291aa5015"), "NQF 7 / Bachelor's Degree" },
                    { new Guid("8b227fe0-5546-43cc-b7da-e6f6ecd87e75"), "NQF 8 / Honours Degree" },
                    { new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), "NQF 9 / Master's Degree" },
                    { new Guid("7eaadea2-117a-4620-9dbe-99040602f6d8"), "NQF 2 / Grade 10" }
                });

            migrationBuilder.InsertData(
                table: "EmployeeStatuses",
                columns: new[] { "EmployeeStatusID", "Description" },
                values: new object[,]
                {
                    { new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), "Not Available" },
                    { new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"), "Available" }
                });

            migrationBuilder.InsertData(
                table: "EmployeeTitles",
                columns: new[] { "EmployeeTitleID", "Description" },
                values: new object[,]
                {
                    { new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), "Mr" },
                    { new Guid("68e8c902-3bd3-495c-ae81-a185bbb28696"), "Mrs" },
                    { new Guid("05b2bda6-1720-4517-86c7-997b574b2d7c"), "Miss" },
                    { new Guid("e2ce7513-a160-4207-ae7f-d1b1adeea81b"), "Ms" },
                    { new Guid("249cadf6-f9eb-4fc4-9c8d-bbf30fcc2b93"), "Dr" }
                });

            migrationBuilder.InsertData(
                table: "EmployeeTripSheetTypes",
                columns: new[] { "EmployeeTripSheetTypeID", "Description" },
                values: new object[,]
                {
                    { new Guid("c54bd8d9-70ca-44d2-8abe-c27e24c6097d"), "Driver" },
                    { new Guid("f6354222-4932-4d07-b2a7-515b54674d94"), "Assistant" }
                });

            migrationBuilder.InsertData(
                table: "EmployeeTypes",
                columns: new[] { "EmployeeTypeID", "Description" },
                values: new object[,]
                {
                    { new Guid("567b6183-fe92-44f0-8536-d121b7ad8e4f"), "Driver" },
                    { new Guid("c525a7cc-a44c-4d62-a06a-1f690afcb19a"), "Supervisor" },
                    { new Guid("0f3c4567-9c03-46ac-8e93-eae2fbc42080"), "Administrator" },
                    { new Guid("d9df26aa-5030-4462-9471-da78c0456785"), "Owner" },
                    { new Guid("07130dec-027e-4f00-a087-4d1dc7ac032a"), "Assistant" }
                });

            migrationBuilder.InsertData(
                table: "InventoryTypes",
                columns: new[] { "InventoryTypeID", "Description" },
                values: new object[,]
                {
                    { new Guid("dfb27012-a657-46f8-89d5-146c063c5385"), "Operation Inventory" },
                    { new Guid("19528739-55a2-45fd-a026-3c0e0f9d1ba1"), "Inventory for Sales" }
                });

            migrationBuilder.InsertData(
                table: "InvoiceNumbers",
                columns: new[] { "InvoiceNumberID", "Number" },
                values: new object[] { new Guid("cd97edfb-2c97-4e49-a778-3cefcabf3b32"), 11 });

            migrationBuilder.InsertData(
                table: "InvoiceStatus",
                columns: new[] { "InvoiceStatusID", "Description" },
                values: new object[,]
                {
                    { new Guid("11f27180-43bb-4ec1-8bbe-71bdadc9c675"), "Refunded" },
                    { new Guid("365b7848-2cb8-4c74-93d9-40ddb17e62f1"), "Cancelled" },
                    { new Guid("2d8a8f51-12d9-4f57-b94c-95a3f181457b"), "Not Paid" },
                    { new Guid("7ad6de50-3811-462b-8118-2257149154be"), "Paid" }
                });

            migrationBuilder.InsertData(
                table: "MachineTypes",
                columns: new[] { "MachineTypeID", "Description" },
                values: new object[,]
                {
                    { new Guid("93a52232-0dca-420c-8318-bae886c0d07c"), "Filter" },
                    { new Guid("665fa397-4390-4b03-854c-f59177afdb80"), "Ice Machine" }
                });

            migrationBuilder.InsertData(
                table: "NotificationTimers",
                columns: new[] { "NotificationTimerID", "CheckTime", "NotificationReason", "NotificationType", "TimePeriod" },
                values: new object[,]
                {
                    { new Guid("afbcbcd8-4d71-4057-9fb8-572cde31958e"), new DateTime(2021, 8, 31, 10, 0, 0, 0, DateTimeKind.Unspecified), "License Expiry", "Vehicle", 31 },
                    { new Guid("c0e43f08-8e14-4730-b65b-c22f6b53d118"), new DateTime(2021, 8, 31, 10, 0, 0, 0, DateTimeKind.Unspecified), "License Expiry", "Vehicle", 20 },
                    { new Guid("e9f944a8-6765-4fd0-bfe2-61daee22837a"), new DateTime(2021, 8, 31, 10, 0, 0, 0, DateTimeKind.Unspecified), "License Expiry", "Vehicle", 5 },
                    { new Guid("5f10ac1d-7ed9-4f41-adbb-78a3d0647b5e"), new DateTime(2021, 8, 31, 10, 0, 0, 0, DateTimeKind.Unspecified), "Filter Replacement", "Filter", 5 }
                });

            migrationBuilder.InsertData(
                table: "NotificationTypes",
                columns: new[] { "NotificationTypeID", "TypeName" },
                values: new object[,]
                {
                    { new Guid("f351c16a-13cb-4572-81a7-cc45a486cee5"), "General" },
                    { new Guid("02893224-9110-4fd4-9bd8-34f804afd621"), "Auth" }
                });

            migrationBuilder.InsertData(
                table: "OrderStatuses",
                columns: new[] { "OrderStatusID", "Description" },
                values: new object[,]
                {
                    { new Guid("37196611-3ac3-4ca5-8982-3fdcdf726b85"), "Delivered" },
                    { new Guid("c10bfe5e-4c56-4551-b1f2-8fd3321a4d1b"), "Out for Delivery" },
                    { new Guid("502d6f63-525a-4efc-bdcb-3560b0edff2c"), "Loaded on Truck" },
                    { new Guid("a4120e1f-1a87-466a-ae62-0f6bcc357877"), "Assigned to Trip Sheet" },
                    { new Guid("aef36c5f-795c-4d54-a840-8d005fad64c2"), "Cancelled" },
                    { new Guid("9fb2e19a-be94-45ac-8de2-96a1d411b796"), "Pending" }
                });

            migrationBuilder.InsertData(
                table: "PaymentMethods",
                columns: new[] { "PaymentMethodID", "Description" },
                values: new object[,]
                {
                    { new Guid("c54255e8-dda0-4788-bf60-172f291e2670"), "EFT" },
                    { new Guid("d074225e-723f-4846-b385-32cff5a0adda"), "Cash" },
                    { new Guid("f4efaa23-7be8-4148-b640-bbc14d6cc08b"), "Card" }
                });

            migrationBuilder.InsertData(
                table: "Prices",
                columns: new[] { "PriceID", "CurrentPrice" },
                values: new object[,]
                {
                    { new Guid("8985e3d5-2b88-43d7-a300-688d7a648a0b"), 12.50m },
                    { new Guid("3c63dbda-4e29-4028-8314-859c7b1351f9"), 26.50m },
                    { new Guid("8cef5816-66f6-4354-af7b-9ff24cf339f7"), 23.50m },
                    { new Guid("a9fd9258-2557-4f48-935f-227a97c87c8b"), 20.50m },
                    { new Guid("8b3a27e7-7d75-4beb-8cc4-cea4f950c902"), 20.50m },
                    { new Guid("582b1b38-0afd-40a8-9ccd-67663d221d67"), 50.50m }
                });

            migrationBuilder.InsertData(
                table: "Prices",
                columns: new[] { "PriceID", "CurrentPrice" },
                values: new object[,]
                {
                    { new Guid("cde17195-4204-4f9f-aed0-079ed23bacdf"), 10.50m },
                    { new Guid("fb453fdd-2259-4d4a-a966-66bbcf3a61e3"), 3.63m },
                    { new Guid("bc871e86-0885-43b9-be14-e1db9492b3e8"), 1.63m },
                    { new Guid("ab60263b-243b-4ce9-a43b-447ba1943ed0"), 0.50m },
                    { new Guid("26746a3d-fc91-4f1e-b429-a9d56c0bd42e"), 1.21m },
                    { new Guid("69eebef6-9c1b-4d6d-9353-fd1a80890c80"), 1.50m },
                    { new Guid("49e4b3a1-0597-4982-a06b-df5a68b6c5fc"), 1.50m },
                    { new Guid("55bb5ecd-ffd7-4e9f-8f53-d2ce47e7b633"), 2.25m }
                });

            migrationBuilder.InsertData(
                table: "SaleStatuses",
                columns: new[] { "SaleStatusID", "Description" },
                values: new object[,]
                {
                    { new Guid("77cd235f-e443-414d-b2fb-8b374a0f7959"), "Paid" },
                    { new Guid("5321a73d-ca18-4770-94e3-4cbf28adffc4"), "Cancelled" }
                });

            migrationBuilder.InsertData(
                table: "SupplierOrderStatuses",
                columns: new[] { "SupplierOrderStatusID", "Description" },
                values: new object[,]
                {
                    { new Guid("4f2ba603-18ad-408d-bf51-abd4f6da08f2"), "Sent To Supplier" },
                    { new Guid("f13b8acc-6d12-4877-b8d7-cc9f174f1734"), "Placed" },
                    { new Guid("20fc8be8-ad9b-4ccd-a300-02d5ef12d8a0"), "Received" },
                    { new Guid("e0a5df74-7054-422f-a397-34ad4060d2f5"), "Cancelled" }
                });

            migrationBuilder.InsertData(
                table: "TableStatuses",
                columns: new[] { "TableStatusID", "Description" },
                values: new object[,]
                {
                    { new Guid("049d36ce-4dd3-468d-9631-4faecd8977d1"), "Deactivated" },
                    { new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "Active" },
                    { new Guid("70813f15-2488-4c61-810b-ed1ef2e0e3a0"), "Pending" }
                });

            migrationBuilder.InsertData(
                table: "TripSheetStatuses",
                columns: new[] { "TripSheetStatusID", "Description" },
                values: new object[,]
                {
                    { new Guid("4c53fe83-fb7d-49f1-92ae-6ff72aaec10c"), "Cancelled" },
                    { new Guid("b67ed3f5-dd4d-4785-a209-97832583f88c"), "Out For Delivery" },
                    { new Guid("29bf4aa1-2c04-47f7-a801-046011236f43"), "Ready for Delivery" },
                    { new Guid("b630f0e0-8e59-4aff-a52e-bbff948eb9ff"), "Ready for Loading" },
                    { new Guid("7cea5c5d-9292-4442-9402-79306c0d9580"), "Pending" },
                    { new Guid("391710b4-dcb0-4ec3-a53c-ba64a055d7ef"), "Delivered" },
                    { new Guid("7f559fbe-0c0b-4985-9369-b1055769a35a"), "Checked In" },
                    { new Guid("0a53ed59-9c2b-44bf-a183-8f73cb7b8563"), "Loading Started" }
                });

            migrationBuilder.InsertData(
                table: "VehicleStatuses",
                columns: new[] { "VehicleStatusID", "Description" },
                values: new object[,]
                {
                    { new Guid("65563356-8382-4c0d-8e2c-f94c98cb786b"), "Available" },
                    { new Guid("8631cd9e-9782-4098-ae61-31847bd47f8a"), "Making Delivery" },
                    { new Guid("e7b83455-818c-409a-9742-bf66b34ed341"), "Being Serviced" },
                    { new Guid("c9359487-3660-4a8b-8919-4935e3a75f36"), "Written-Off" },
                    { new Guid("6ffce810-cbb1-4192-87d6-378487d19913"), "Deactivated" },
                    { new Guid("b0acee21-0379-44a7-96d9-1a04847d6f71"), "Pending" }
                });

            migrationBuilder.InsertData(
                table: "WrittenOffReasons",
                columns: new[] { "WrittenOffReasonID", "Description" },
                values: new object[,]
                {
                    { new Guid("b17ee000-7a47-4be5-8b59-5dea9dd67fcf"), "Broken Bag" },
                    { new Guid("28a065e4-cb8a-4557-8d18-a0e8f77743c4"), "Old Stock" }
                });

            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "AddressID", "AddressTypeID", "CityName", "ComplexName", "CountryName", "PostalCode", "Province", "StreetName", "StreetNumber", "SuburbName", "UnitNumber", "isActive" },
                values: new object[,]
                {
                    { new Guid("6bfe6569-5f4f-4e63-8be5-d2d6ee46e601"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0123", "Gauteng", "Test1", 1, "Lyttleton", 0, true },
                    { new Guid("284c0140-3935-4566-9e95-51b347e42ec2"), new Guid("68252ff8-14f6-4c1e-8ca1-954b9e598fea"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("5ea203e4-a74d-4056-a229-55f678266624"), new Guid("68252ff8-14f6-4c1e-8ca1-954b9e598fea"), "Pretoria", null, "South Africa", "0123", "Gauteng", "Test3", 1, "Lyttleton", 0, true },
                    { new Guid("e17ff651-d643-41eb-bcff-dd05990d5015"), new Guid("68252ff8-14f6-4c1e-8ca1-954b9e598fea"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("c520b2f5-0dac-42ee-b356-08f3e51078ed"), new Guid("68252ff8-14f6-4c1e-8ca1-954b9e598fea"), "Pretoria", null, "South Africa", "0123", "Gauteng", "Test3", 1, "Lyttleton", 0, true },
                    { new Guid("f57e6df5-6cf1-4896-9d75-e8714e8c1c2b"), new Guid("68252ff8-14f6-4c1e-8ca1-954b9e598fea"), "Pretoria", null, "South Africa", "0123", "Gauteng", "Test3", 1, "Lyttleton", 0, true },
                    { new Guid("97491d4b-2fe3-4140-a310-3ed4e5bfdd3d"), new Guid("a8901b0d-4a81-440e-8707-bf052a2fef0b"), "Pretoria", null, "South Africa", "0123", "Gauteng", "Test2", 1, "Lyttleton", 0, true },
                    { new Guid("b896ffb8-fd96-43b8-8fcf-dab959d30503"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Invoice", 4, "Lyttleton", 0, true },
                    { new Guid("9551ded3-130e-43d2-9746-2b0d0e41e7a7"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("d5edf3a1-c693-45a5-a519-b3b2fd037f8f"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("dc3561ab-ebd2-495c-9011-8ddb52873aba"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("3085c334-566d-4c8f-8432-cfc0a20f1c41"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("02b1fcc2-c6e2-432e-9a5d-1bbd379bf3f5"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("63cfb3ec-db5e-421c-9246-1edfe4740fe0"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("7a490548-2ac9-4902-bca0-7aa7b044416b"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("623fba70-b4b3-4e3b-bb63-774ad268a916"), new Guid("68252ff8-14f6-4c1e-8ca1-954b9e598fea"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("9a9a8c53-5c08-4c25-8147-dda25dd62fdf"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("7e483a7b-9f16-4c9a-8f0b-716088714836"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("9127e563-e3d4-4694-814e-2edfa644b518"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("e0d6c471-f8f0-448e-9028-5ea52b92927b"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("c20a118d-56b7-4455-82e3-12383cb171a8"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("aad80912-434e-4924-8ddc-38e5e7c998f7"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("dd44b55c-ceca-46da-9e71-53884ab12fdc"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("b69e2095-3ec0-438f-951e-84f44c6a06d0"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("53c885f8-fa5f-4314-aa51-54aea0031ae2"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("7c6569da-cd8d-4228-9a40-b5d6faed1487"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("c632e660-2d53-4c19-bef0-084b15df97e5"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("eeee2e71-60d0-4167-a357-065a7b2cc497"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("5fdebe6d-13d8-4720-a988-5936677f62d3"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("a601cc74-fefd-4a1e-908d-c814a6598708"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("30ee5dc5-9602-4b2a-bc25-a92039520e1c"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true }
                });

            migrationBuilder.InsertData(
                table: "Clients",
                columns: new[] { "ClientID", "ClientName", "Note", "TableStatusID" },
                values: new object[,]
                {
                    { new Guid("543638f6-d06f-4a0c-b06a-96a175290244"), "CultureClub", null, new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("10e21cc0-001c-438b-bb2f-445e446688dd"), "Grootveli Drankwinkel", "Call Weekly", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("a6e982dd-7dd5-4256-a5f4-68a0a82bc596"), "Egen Safi", "Phone Daily", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("8c316ea4-605a-4af8-9083-dab4a533dfdc"), "Main Road Deli", "Check when delivering to Railways", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("dc3561ab-ebd2-495c-9011-8ddb52873aba"), "Jacaranda", "Check when delivering to Jacaranda", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("b5dd1c75-0029-4bda-be41-7fae78d0f7f0"), "Test Invoice", "Testing the Invoice Email", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("eda60fb4-a8a9-4bdb-8105-1981562d2597"), "Gable's Liquor", "Check when delivering to Gable's Liquor", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("caa33a04-d199-4973-a375-25b7e8b157d4"), "Railways", "On orders", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("50f1ef0e-28a1-4afc-81ec-900ce5ffd1e6"), "Malboer", "Check when delivering to Malboer", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("62c66868-32e2-469c-8224-40e1a7343983"), "Shell Jean Ave", "Phone every Friday & Saturday", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") }
                });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "EmployeeID", "AccountNumber", "AccountTypeID", "BankID", "BranchCode", "CellNumber", "DateOfBirth", "Email", "EmployeeQualificationID", "EmployeeStatusID", "EmployeeTitleID", "EmployeeTypeID", "FingerprintAccountNumber", "FirstName", "HomeNumber", "IDNumber", "Initials", "InstitutionName", "KnownAsName", "OTP", "PassportCountry", "PassportNumber", "Surname", "SystemPassword", "SystemUsername", "TableStatusID", "TaxNumber", "WorkNumber", "YearCompleted", "isLoggedIn" },
                values: new object[] { new Guid("26771adf-20b0-4988-92d9-c0e245b9bf93"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Assistant2@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("07130dec-027e-4f00-a087-4d1dc7ac032a"), null, "Assistant2", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Assistant2", null, "N/A", "1231231312", "Assistant2", "ee279b917597db707e132742ad67569316829067e148f03b14513e4b12beac4d", "Assistant2", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "EmployeeID", "AccountNumber", "AccountTypeID", "BankID", "BranchCode", "CellNumber", "DateOfBirth", "Email", "EmployeeQualificationID", "EmployeeStatusID", "EmployeeTitleID", "EmployeeTypeID", "FingerprintAccountNumber", "FirstName", "HomeNumber", "IDNumber", "Initials", "InstitutionName", "KnownAsName", "OTP", "PassportCountry", "PassportNumber", "Surname", "SystemPassword", "SystemUsername", "TableStatusID", "TaxNumber", "WorkNumber", "YearCompleted", "isLoggedIn" },
                values: new object[,]
                {
                    { new Guid("50084a0b-1663-4f4f-8974-e0fbaacf2d60"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Driver2@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("567b6183-fe92-44f0-8536-d121b7ad8e4f"), null, "Driver2", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Driver2", null, "N/A", "1231231312", "Driver2", "a8770242b240f347844015160a61d98ea8749a32cc9dcf9fde627cabe7738dff", "Driver2", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("aa301feb-440f-42ae-b4a1-0c123bf074af"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Assistant@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("c525a7cc-a44c-4d62-a06a-1f690afcb19a"), null, "Supervisor", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Supervisor", null, "N/A", "1231231312", "Supervisor", "9de790933254b86566029f26d40f9ddac37145ff700b8b8e071db330bdabc56c", "Assistant", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("efdc266e-2dce-4021-968c-cad5adc9c9b5"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Assistant4@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("07130dec-027e-4f00-a087-4d1dc7ac032a"), null, "Assistant4", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Assistant4", null, "N/A", "1231231312", "Assistant4", "b887387fe546570afb0f4a1c516a82fa516949e0d3491fa74300d692ada5cc10", "Assistant4", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("63abb4b4-5d4b-446d-8471-eb3e0f7f053c"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Driver3@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("567b6183-fe92-44f0-8536-d121b7ad8e4f"), null, "Driver3", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Driver3", null, "N/A", "1231231312", "Driver3", "7e2582a24222453f70684ea6876f7e7aee46f15f4a33d750ede5be7d8227be4d", "Driver3", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("eac034be-cbba-4360-9e8e-8a897f16c6a6"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Assistant3@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("07130dec-027e-4f00-a087-4d1dc7ac032a"), null, "Assistant3", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Assistant3", null, "N/A", "1231231312", "Assistant3", "96675a8150973a4fb0b24273d8a895efade87f265b28516550d5d123e10c896b", "Assistant3", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("61cc8c8d-f487-4a7e-b78c-ab4614786586"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Supervisor3@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("c525a7cc-a44c-4d62-a06a-1f690afcb19a"), null, "Supervisor3", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Supervisor3", null, "N/A", "1231231312", "Supervisor3", "41de2281ff0d2d73a537ab633caa7d60cfac0392104daa8e7625e22b3abd529b", "Supervisor3", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("4e5f0b7f-68ba-4254-b6fc-04fab6a99fce"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Driver4@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("567b6183-fe92-44f0-8536-d121b7ad8e4f"), null, "Driver4", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Driver4", null, "N/A", "1231231312", "Driver4", "575d66f2c3fa2d0572837f6af2eed9562e9393059df14edc591959132f85ca37", "Driver4", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("376017bd-2155-4e82-a1a7-fbe7f2d49b83"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Supervisor4@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("c525a7cc-a44c-4d62-a06a-1f690afcb19a"), null, "Supervisor4", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Supervisor4", null, "N/A", "1231231312", "Supervisor4", "db96486cc150710baeccabc6cb9d2d25fb0e5e7c1e64ca5395ade6709f949586", "Supervisor4", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("83fc7bdc-b730-4c4f-b13b-d40f9d282b73"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Driver5@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("567b6183-fe92-44f0-8536-d121b7ad8e4f"), null, "Driver5", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Driver5", null, "N/A", "1231231312", "Driver5", "771542e5ae8a9b877d7af18e4bb7f7fdd027ea03772ccc902e1efff6140c5596", "Driver5", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("2a7ecffa-8673-45c0-8b0a-df03b891dc3c"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Assistant5@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("07130dec-027e-4f00-a087-4d1dc7ac032a"), null, "Assistant5", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Assistant5", null, "N/A", "1231231312", "Assistant5", "d960a1c8d09d05a11dde6961448fd196974e042666af538278dd41d5b7c8f0a1", "Assistant5", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("40525829-4dd0-4352-a149-cea6a86ec01f"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Supervisor5@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("c525a7cc-a44c-4d62-a06a-1f690afcb19a"), null, "Supervisor5", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Supervisor5", null, "N/A", "1231231312", "Supervisor5", "0eac94377eac038a7b6e9d5abb7bcb0e208e2e51fa7b3278c2b8557963d6b2f6", "Supervisor5", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("fb59a87d-59e6-49d4-979b-658eb9986162"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Assistant@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("07130dec-027e-4f00-a087-4d1dc7ac032a"), null, "Assistant", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Assistant", null, "N/A", "1231231312", "Assistant", "391e405152779acc6b0f9429f3a4e27baf2af0deab2c37ba792311efa767d676", "Assistant", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("89d3bb9a-c316-4ef5-b1e2-f8423996ffb2"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Supervisor2@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("c525a7cc-a44c-4d62-a06a-1f690afcb19a"), null, "Supervisor2", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Supervisor2", null, "N/A", "1231231312", "Supervisor2", "1bbebf3f5b5dc013dc859327f60b2207b6b12a550dc4bff48f9fea2dc9077d09", "Supervisor2", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("fa5a66e7-40e8-48e6-8564-4a38e40ddadd"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Driver@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("567b6183-fe92-44f0-8536-d121b7ad8e4f"), null, "Driver", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Driver", null, "N/A", "1231231312", "Driver", "9fe4c68ec20dda7c6b1d3f760e5e6af6b9802897cd59f084b99c68b2ac01574c", "Driver", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("30fc6523-661c-4479-8539-049d8cca6cba"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0815377864", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "admin@centurionice.co.za", new Guid("8b227fe0-5546-43cc-b7da-e6f6ecd87e75"), new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("0f3c4567-9c03-46ac-8e93-eae2fbc42080"), null, "Admin", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Admin", null, "N/A", "1231231312", "Admin", "be530202095bf605816e7a30d9e4bd432ad42318e160eeb83edfcc454407ee34", "NaomiAdmin", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("47d01ee3-0c9b-4118-beea-c21085cf001c"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0733714164", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "puvelin.admin@centurionice.co.za", new Guid("8b227fe0-5546-43cc-b7da-e6f6ecd87e75"), new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("0f3c4567-9c03-46ac-8e93-eae2fbc42080"), null, "Puvelin", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Puvelin Admin", null, "N/A", "1231231312", "Admin", "839404cfecdc1fb397479e286ba788fd0fd9c4c7548c34466b995082a5ffee18", "PuvelinAdmin", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("bc2c2ae9-204f-49b5-8d9d-58da49b8fcd4"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0740789791", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "melissa.admin@centurionice.co.za", new Guid("8b227fe0-5546-43cc-b7da-e6f6ecd87e75"), new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("0f3c4567-9c03-46ac-8e93-eae2fbc42080"), null, "Melissa", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Melissa Admin", null, "N/A", "1231231312", "Admin", "2dcf9419a2f84dff600d9e388117ec8c056252946538d6e20feade6e9e07b8c0", "MelissaAdmin", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("f11765a0-58d2-42e5-9c5c-34022016e8c6"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "admin@centurionice.co.za", new Guid("8b227fe0-5546-43cc-b7da-e6f6ecd87e75"), new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("0f3c4567-9c03-46ac-8e93-eae2fbc42080"), null, "Admin", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Admin", null, "N/A", "1231231312", "Admin", "c1c224b03cd9bc7b6a86d77f5dace40191766c485cd55dc48caf9ac873335d6f", "Admin", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("14e100c5-a695-49fa-ada2-244c117df5c0"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("aa3f969a-418e-4e6d-9169-c4e3d4a01530"), "01231", "0764801037", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "escheepers77@gmail.com", new Guid("8b227fe0-5546-43cc-b7da-e6f6ecd87e75"), new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("d9df26aa-5030-4462-9471-da78c0456785"), "112", "Etienne Owner", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Etienne Owner", null, "N/A", "1231231312", "Scheepers", "83356fe5c3a44f933f3d82a9a0bfe5857bf2dce5442f91afe4f755a680be3a0b", "EtienneOwner", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("7f177922-a5f1-448d-8862-96fd4ba5d633"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("aa3f969a-418e-4e6d-9169-c4e3d4a01530"), "01231", "0814169470", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Owner@centurionice.co.za", new Guid("8b227fe0-5546-43cc-b7da-e6f6ecd87e75"), new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("d9df26aa-5030-4462-9471-da78c0456785"), "111", "Shanah Owner", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Shanah Owner", null, "N/A", "1231231312", "Suping", "4c2e79746a2e993291f4d9c7a502504ae7ba44a74477868fca08d9002aec9d76", "ShanahOwner", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false }
                });

            migrationBuilder.InsertData(
                table: "Invoices",
                columns: new[] { "InvoiceID", "InvoiceDate", "InvoiceNumber", "InvoiceStatusID", "PaymentMethodID" },
                values: new object[,]
                {
                    { new Guid("2e17cf49-6504-4d05-983f-8128a4a0c459"), new DateTime(2021, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), "INV00000001", new Guid("2d8a8f51-12d9-4f57-b94c-95a3f181457b"), null },
                    { new Guid("7e4dcd3b-a893-490f-a469-ca0ea53d3bc7"), new DateTime(2021, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), "INV00000002", new Guid("2d8a8f51-12d9-4f57-b94c-95a3f181457b"), null },
                    { new Guid("4bdbaa80-9f10-4e36-956c-7ab882c97d00"), new DateTime(2021, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), "INV00000003", new Guid("2d8a8f51-12d9-4f57-b94c-95a3f181457b"), null },
                    { new Guid("d74516b2-a88d-485b-b221-adf65e094fe4"), new DateTime(2021, 4, 12, 15, 31, 35, 0, DateTimeKind.Unspecified), "INV00000005", new Guid("7ad6de50-3811-462b-8118-2257149154be"), new Guid("f4efaa23-7be8-4148-b640-bbc14d6cc08b") },
                    { new Guid("5d84a526-2e46-4f9f-ba22-e9e29e0b73ae"), new DateTime(2021, 6, 21, 15, 31, 35, 0, DateTimeKind.Unspecified), "INV00000007", new Guid("7ad6de50-3811-462b-8118-2257149154be"), new Guid("f4efaa23-7be8-4148-b640-bbc14d6cc08b") },
                    { new Guid("1f68d9ba-bf99-46cb-a5a8-4a6031a434f5"), new DateTime(2021, 7, 23, 15, 31, 35, 0, DateTimeKind.Unspecified), "INV00000008", new Guid("7ad6de50-3811-462b-8118-2257149154be"), new Guid("f4efaa23-7be8-4148-b640-bbc14d6cc08b") },
                    { new Guid("d8286dfe-add6-4834-8f7d-9b2bd96ae569"), new DateTime(2021, 8, 6, 15, 31, 35, 0, DateTimeKind.Unspecified), "INV00000009", new Guid("7ad6de50-3811-462b-8118-2257149154be"), new Guid("f4efaa23-7be8-4148-b640-bbc14d6cc08b") },
                    { new Guid("c21d9b1b-27a1-413a-84ea-f5708133e65f"), new DateTime(2021, 9, 2, 15, 31, 35, 0, DateTimeKind.Unspecified), "INV00000010", new Guid("7ad6de50-3811-462b-8118-2257149154be"), new Guid("f4efaa23-7be8-4148-b640-bbc14d6cc08b") },
                    { new Guid("6d619343-bb23-401f-b40f-3bf323a8b706"), new DateTime(2021, 5, 14, 15, 31, 35, 0, DateTimeKind.Unspecified), "INV00000006", new Guid("7ad6de50-3811-462b-8118-2257149154be"), new Guid("f4efaa23-7be8-4148-b640-bbc14d6cc08b") },
                    { new Guid("9eca188d-e389-473d-b46e-ca9a03b27cf3"), new DateTime(2021, 3, 22, 15, 31, 35, 0, DateTimeKind.Unspecified), "INV00000004", new Guid("7ad6de50-3811-462b-8118-2257149154be"), new Guid("f4efaa23-7be8-4148-b640-bbc14d6cc08b") }
                });

            migrationBuilder.InsertData(
                table: "Suppliers",
                columns: new[] { "SupplierID", "GoodsSupplied", "SupplierName", "TableStatusID" },
                values: new object[,]
                {
                    { new Guid("8adb40e5-dde0-44cd-a21f-8a29142c54b2"), "Stickers and Magnets", "Fast Print", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("59347180-43e2-431e-a452-abf6b2b4ce9a"), "Towing Services", "Centow", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("1a7575e9-13cf-43c9-96a2-90ee5d26b2c1"), "Ice Supply", "Frozen Ice", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("d4db4769-4321-4de0-b0a2-f6577386efcf"), "Plumbing Maintenance Parts", "Waterware", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("dccebd02-eb22-49d9-8cde-bcf145ef0964"), "Plastic Bags / Branded Bags", "Linear Plastics", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("74e4f034-7f94-4958-93e7-4879e876170b"), "Trackers", "Pointer SA", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("132add66-6dc0-46de-ab8e-03195fcd788c"), "Sealing Machines Parts and Repairs", "Packway International", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("35836f8d-5b8e-4153-ae15-595cfbee27aa"), "Truck Rental", "Elite Truck Rental", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("9369078c-fa89-4d0e-a8d1-10d91da87e77"), "Wood Supplier / Various", "Ephrahim Bella Bella", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("75aa1a34-bd38-4e93-b980-26be2a6d1e9b"), "Vehicle Maintenance - Kia", "Kia", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("1c4355b2-8129-41c4-b47c-4294bb733a9b"), "Vehicle Maintenance - Nissan", "Nissan", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("bc978cf4-4ce3-46e5-8e5c-1c99ee0d4618"), "Vehicle Maintenance - Tyres, Wheel, Balancing, etc.", "Tyremart", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") }
                });

            migrationBuilder.InsertData(
                table: "Suppliers",
                columns: new[] { "SupplierID", "GoodsSupplied", "SupplierName", "TableStatusID" },
                values: new object[,]
                {
                    { new Guid("01283a20-a3c7-407c-8715-a59752f79783"), "Vehicle Maintenance and Repairs - All Brands", "Advanced Automotive", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("8a76c942-85c3-4de6-94d9-5a6885975113"), "Water Filtration Filters", "The Filter Shop", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("6b4de917-fef4-4cd7-9b97-a4a768ce65d5"), "Plastic Bags - Unmarked Bags 2kg Blue & 10kg Clear", "Waltloo Packaging", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("9d5d963f-5940-4d21-8ecd-9c9e84aff20c"), "Wood Supplier / Various", "Francois", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("1441f7fc-c068-48a0-ab62-dc2c9e0e8bc7"), "Vehicle Freezer Unit Repairs", "AC&R", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("2451e97d-ac43-4788-9768-ad7628b237a9"), "Plastic Bags - Marked/Branded", "Plastic and Packaging", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("2cee437e-041a-42b6-a916-6d7bf8b94267"), "General Maintenance", "Builders Warehouse", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("8f071616-125a-4ba2-8e53-1f1da3b4bfa2"), "Minuteman Press / Invoicing Books", "Minuteman Press", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("b3307864-6945-4aa0-92e7-0095e056889b"), "Blocks Supply", "Ice World", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("1c8e1c73-f5c5-4124-9786-b87322e42f22"), "Bolt Removal", "AT&R", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("f57dc5ed-3b02-4944-ba37-744991275bdc"), "Camera Maintenance", "HGL Electronics", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("0c3fc2e9-75f8-4699-ab0b-eecd79770a9f"), "Charcoal, Briquettes, Firelighters", "Shonnah", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("bfeadaa7-b5dd-4656-a26e-500d53ee9f8d"), "Cleaning Materials", "Naledi Chemicals", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("59b800f7-921a-4a1c-b626-b49004fde301"), "Containers", "Big Box Containers", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("43a88e15-d11f-4b5d-ba42-7639666ca8ee"), "Dry Ice", "Dry Ice International", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("ecc20563-4c08-483b-bdea-54c4db6eea79"), "Electrical Services", "To be Advised", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("fc585747-d8d4-4fdd-a2f5-566b7abb2a0b"), "Freezer Maintenance", "Walk In Freezers / Container Freezer", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("b9475cf2-ca78-4533-a519-8cf60b42a2b0"), "Freezer Repairs", "Winters Business Enterprise", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("ac9da526-396f-4974-8ca5-56a2c3f84085"), "Wood Supplier / Rooikool", "Petrus", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("d1e8ddaf-d1b4-4876-83ac-0f67364d2510"), "General Maintenance", "Chamberlains", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("edb156ee-b20c-4dd6-9220-6321bd6c152e"), "Building Related", "Landlord", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("ca300856-9dc0-4ba5-8dfb-017b71d09c36"), "License Renewal", "Silver Leaf", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("ceb25950-f3fc-428f-aecb-2f1f98c1a4ec"), "Machine Maintenance Repairs and Cleaning Products", "Serve Ice", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("d4996a9a-fc8b-4e30-b99f-ed7c53ee17a5"), "Plastic Bags - Large Wood Bags", "PXD", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") }
                });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "VehicleID", "Capacity", "LastServicedMileage", "Last_Service_Date", "License_Expiry_Date", "Make", "Mileage", "Model", "Reg_Number", "Tracker_Number", "VehicleStatusID", "Year_Made" },
                values: new object[,]
                {
                    { new Guid("6cee0e3e-44e1-4b11-a2cc-7ac4b594918e"), 500, 0, new DateTime(2021, 4, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), new DateTime(2022, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), "Toyota", 100000, "Super Truck", "BBC 123 MP", "TRK12345", new Guid("8631cd9e-9782-4098-ae61-31847bd47f8a"), new DateTime(2016, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified) },
                    { new Guid("3ce6e093-7e02-46f3-a417-4b163eb2ddc0"), 500, 0, new DateTime(2021, 4, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), new DateTime(2022, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), "Nissan", 100000, "Truck Master", "ABC 456 GP", "TRK12345", new Guid("8631cd9e-9782-4098-ae61-31847bd47f8a"), new DateTime(2016, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified) },
                    { new Guid("433253ee-d91e-4604-a627-899e657a042a"), 500, 0, new DateTime(2021, 4, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), new DateTime(2022, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), "Nissan", 100000, "Truck Master", "ABC 241 NP", "TRK12345", new Guid("65563356-8382-4c0d-8e2c-f94c98cb786b"), new DateTime(2016, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified) },
                    { new Guid("e8b76b10-8175-49a9-b1f7-aa506b549cef"), 500, 0, new DateTime(2021, 4, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), new DateTime(2022, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), "Nissan", 100000, "Truck Master", "CFG 245 NP", "TRK12345", new Guid("8631cd9e-9782-4098-ae61-31847bd47f8a"), new DateTime(2016, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "AuditTrails",
                columns: new[] { "AuditTrailID", "ActionData", "ActionDate", "ActionDescription", "EmployeeAction", "EmployeeID" },
                values: new object[,]
                {
                    { new Guid("d513c444-2baa-4c67-8a6d-8b38f15e9637"), null, new DateTime(2021, 8, 15, 15, 40, 46, 0, DateTimeKind.Unspecified), "Created a new Employee", "Create", new Guid("7f177922-a5f1-448d-8862-96fd4ba5d633") },
                    { new Guid("5de18eec-013d-43be-897b-fc8112a5f00c"), null, new DateTime(2021, 8, 15, 15, 31, 35, 0, DateTimeKind.Unspecified), "Created a new Employee", "Create", new Guid("7f177922-a5f1-448d-8862-96fd4ba5d633") }
                });

            migrationBuilder.InsertData(
                table: "ClientAddresses",
                columns: new[] { "AddressID", "ClientID" },
                values: new object[,]
                {
                    { new Guid("d5edf3a1-c693-45a5-a519-b3b2fd037f8f"), new Guid("50f1ef0e-28a1-4afc-81ec-900ce5ffd1e6") },
                    { new Guid("dd44b55c-ceca-46da-9e71-53884ab12fdc"), new Guid("a6e982dd-7dd5-4256-a5f4-68a0a82bc596") },
                    { new Guid("dc3561ab-ebd2-495c-9011-8ddb52873aba"), new Guid("dc3561ab-ebd2-495c-9011-8ddb52873aba") },
                    { new Guid("aad80912-434e-4924-8ddc-38e5e7c998f7"), new Guid("10e21cc0-001c-438b-bb2f-445e446688dd") },
                    { new Guid("c20a118d-56b7-4455-82e3-12383cb171a8"), new Guid("62c66868-32e2-469c-8224-40e1a7343983") },
                    { new Guid("e0d6c471-f8f0-448e-9028-5ea52b92927b"), new Guid("caa33a04-d199-4973-a375-25b7e8b157d4") },
                    { new Guid("9551ded3-130e-43d2-9746-2b0d0e41e7a7"), new Guid("eda60fb4-a8a9-4bdb-8105-1981562d2597") },
                    { new Guid("284c0140-3935-4566-9e95-51b347e42ec2"), new Guid("543638f6-d06f-4a0c-b06a-96a175290244") },
                    { new Guid("6bfe6569-5f4f-4e63-8be5-d2d6ee46e601"), new Guid("543638f6-d06f-4a0c-b06a-96a175290244") },
                    { new Guid("b896ffb8-fd96-43b8-8fcf-dab959d30503"), new Guid("b5dd1c75-0029-4bda-be41-7fae78d0f7f0") },
                    { new Guid("a601cc74-fefd-4a1e-908d-c814a6598708"), new Guid("8c316ea4-605a-4af8-9083-dab4a533dfdc") }
                });

            migrationBuilder.InsertData(
                table: "ClientContactPeople",
                columns: new[] { "ClientContactPersonID", "ClientID", "EmailAddress", "Name", "PhoneNumber" },
                values: new object[,]
                {
                    { new Guid("b89887de-1b68-48ae-bdf6-c8cfa1f9df07"), new Guid("8c316ea4-605a-4af8-9083-dab4a533dfdc"), "admin@gmail.com", "Anyone", "0126671948" },
                    { new Guid("6a4b3bc3-a1c6-4b67-8fd9-e161d7dcd899"), new Guid("dc3561ab-ebd2-495c-9011-8ddb52873aba"), "admin@gmail.com", "Anyone", "0126671948" },
                    { new Guid("8d2fb3d5-5c33-4ef6-a653-ba70258d88b8"), new Guid("eda60fb4-a8a9-4bdb-8105-1981562d2597"), "admin@gmail.com", "Anyone", "0126671948" },
                    { new Guid("f0899973-8aa7-4749-84e6-aaa83bd35076"), new Guid("50f1ef0e-28a1-4afc-81ec-900ce5ffd1e6"), "admin@gmail.com", "Anyone", "0126671948" },
                    { new Guid("1f4f7372-5fa4-43dc-a91f-8df14bd8943a"), new Guid("10e21cc0-001c-438b-bb2f-445e446688dd"), "admin@gmail.com", "Felicity", "0824259217" },
                    { new Guid("a5d5d77a-89a7-4012-aa6c-a493c01c4d0d"), new Guid("b5dd1c75-0029-4bda-be41-7fae78d0f7f0"), "escheepers77@gmail.com", "Etienne", "0126671948" },
                    { new Guid("d5e4ac1b-ece0-428b-9e60-c4a1fbb05344"), new Guid("62c66868-32e2-469c-8224-40e1a7343983"), "admin@gmail.com", "Anyone", "0126634393" },
                    { new Guid("780d9d5a-d1f0-4373-bd51-afb70cf0cfa7"), new Guid("caa33a04-d199-4973-a375-25b7e8b157d4"), "admin@gmail.com", "Johan", "0780802528" },
                    { new Guid("bce06eb4-5efb-4ed4-b020-6ccd9332de4a"), new Guid("543638f6-d06f-4a0c-b06a-96a175290244"), "staff@cultureclub.co.za", "Jenna", "0123456789" },
                    { new Guid("9191fbfa-8e98-421f-9be4-49b12f3fd779"), new Guid("543638f6-d06f-4a0c-b06a-96a175290244"), "admin@cultureclub.co.za", "Danae", "0123456789" },
                    { new Guid("60cd1ada-02b0-4c3f-8b14-3f89c5bd36b3"), new Guid("a6e982dd-7dd5-4256-a5f4-68a0a82bc596"), "admin@gmail.com", "Tony", "0126513800" },
                    { new Guid("8a949446-0b47-4c6c-b755-21e671ae0af0"), new Guid("b5dd1c75-0029-4bda-be41-7fae78d0f7f0"), "shanahjr@gmail.com", "Shanah", "0126671948" }
                });

            migrationBuilder.InsertData(
                table: "DeliveryTripSheets",
                columns: new[] { "DeliveryTripSheetID", "Alias", "AssistantID", "Comment", "Created", "DepatureTime", "DriverID", "EndKM", "FinalDelivered", "FinalLoaded", "ReturnTime", "StartKM", "SupervisorID", "TripSheetStatusID", "VehicleID", "VehicleValidationImage" },
                values: new object[,]
                {
                    { new Guid("741d2ad5-ff78-493b-a8bd-16c10649de04"), "TripSheet3", new Guid("eac034be-cbba-4360-9e8e-8a897f16c6a6"), "No comment", new DateTime(2021, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), null, new Guid("63abb4b4-5d4b-446d-8471-eb3e0f7f053c"), null, true, true, null, 100000, new Guid("61cc8c8d-f487-4a7e-b78c-ab4614786586"), new Guid("b67ed3f5-dd4d-4785-a209-97832583f88c"), new Guid("6cee0e3e-44e1-4b11-a2cc-7ac4b594918e"), null },
                    { new Guid("510ae3b6-37f5-44af-886e-5a4d2117f202"), "TripSheet5", new Guid("2a7ecffa-8673-45c0-8b0a-df03b891dc3c"), "No comment", new DateTime(2021, 6, 22, 15, 31, 35, 0, DateTimeKind.Unspecified), null, new Guid("83fc7bdc-b730-4c4f-b13b-d40f9d282b73"), null, true, true, null, 100000, new Guid("40525829-4dd0-4352-a149-cea6a86ec01f"), new Guid("7f559fbe-0c0b-4985-9369-b1055769a35a"), new Guid("e8b76b10-8175-49a9-b1f7-aa506b549cef"), null },
                    { new Guid("371fefc3-9a45-4340-92ee-e2e9b5ddbd16"), "TripSheet5", new Guid("2a7ecffa-8673-45c0-8b0a-df03b891dc3c"), "No comment", new DateTime(2021, 9, 3, 15, 31, 35, 0, DateTimeKind.Unspecified), null, new Guid("83fc7bdc-b730-4c4f-b13b-d40f9d282b73"), null, true, true, null, 100000, new Guid("40525829-4dd0-4352-a149-cea6a86ec01f"), new Guid("7f559fbe-0c0b-4985-9369-b1055769a35a"), new Guid("e8b76b10-8175-49a9-b1f7-aa506b549cef"), null },
                    { new Guid("2d1966d0-8749-4bb7-af16-2b99c878f6fb"), "TripSheet4", new Guid("efdc266e-2dce-4021-968c-cad5adc9c9b5"), "No comment", new DateTime(2021, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), null, new Guid("4e5f0b7f-68ba-4254-b6fc-04fab6a99fce"), null, true, true, null, 100000, new Guid("376017bd-2155-4e82-a1a7-fbe7f2d49b83"), new Guid("0a53ed59-9c2b-44bf-a183-8f73cb7b8563"), new Guid("3ce6e093-7e02-46f3-a417-4b163eb2ddc0"), null },
                    { new Guid("a7fe7e2e-a308-4c9a-be4d-bf33a25f3c91"), "TripSheet5", new Guid("2a7ecffa-8673-45c0-8b0a-df03b891dc3c"), "No comment", new DateTime(2021, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), null, new Guid("83fc7bdc-b730-4c4f-b13b-d40f9d282b73"), null, true, true, null, 100000, new Guid("40525829-4dd0-4352-a149-cea6a86ec01f"), new Guid("29bf4aa1-2c04-47f7-a801-046011236f43"), new Guid("e8b76b10-8175-49a9-b1f7-aa506b549cef"), null },
                    { new Guid("52238d97-9e77-4107-b8f7-6739d32be3e0"), "TripSheet5", new Guid("2a7ecffa-8673-45c0-8b0a-df03b891dc3c"), "No comment", new DateTime(2021, 5, 15, 15, 31, 35, 0, DateTimeKind.Unspecified), null, new Guid("83fc7bdc-b730-4c4f-b13b-d40f9d282b73"), null, true, true, null, 100000, new Guid("40525829-4dd0-4352-a149-cea6a86ec01f"), new Guid("7f559fbe-0c0b-4985-9369-b1055769a35a"), new Guid("e8b76b10-8175-49a9-b1f7-aa506b549cef"), null }
                });

            migrationBuilder.InsertData(
                table: "EmergencyContacts",
                columns: new[] { "EmergencyContactID", "ContactPerson", "ContactTelephoneNumber", "EmployeeID", "RelationshipToEmployee" },
                values: new object[,]
                {
                    { new Guid("54e3187c-95ed-477c-aba5-07fc1a17deb9"), "John Mulaney", "0814169470", new Guid("40525829-4dd0-4352-a149-cea6a86ec01f"), "Friend" },
                    { new Guid("334dae15-5cad-4743-9afb-731612bbb4ca"), "John Mulaney", "0814169470", new Guid("2a7ecffa-8673-45c0-8b0a-df03b891dc3c"), "Friend" },
                    { new Guid("75eddcb7-a64d-4125-afbf-ffe029bd4f39"), "John Mulaney", "0814169470", new Guid("83fc7bdc-b730-4c4f-b13b-d40f9d282b73"), "Friend" },
                    { new Guid("bc1e0042-b3e0-4c34-a006-c685dd2eb271"), "John Mulaney", "0814169470", new Guid("efdc266e-2dce-4021-968c-cad5adc9c9b5"), "Friend" },
                    { new Guid("0fd740a0-cb3a-4f2f-96bf-77dc613bc1b5"), "John Mulaney", "0814169470", new Guid("4e5f0b7f-68ba-4254-b6fc-04fab6a99fce"), "Friend" },
                    { new Guid("adb89ec9-3cf6-4c99-babc-26c0f6f339dc"), "John Mulaney", "0814169470", new Guid("61cc8c8d-f487-4a7e-b78c-ab4614786586"), "Friend" },
                    { new Guid("874a4006-0d09-479b-8bbd-d7267a0f77ed"), "John Mulaney", "0814169470", new Guid("eac034be-cbba-4360-9e8e-8a897f16c6a6"), "Friend" },
                    { new Guid("6d3c9a10-4e83-474b-afa3-877200e09b75"), "John Mulaney", "0814169470", new Guid("63abb4b4-5d4b-446d-8471-eb3e0f7f053c"), "Friend" },
                    { new Guid("72a0827d-1797-4b63-a8fa-896cb216ccda"), "John Mulaney", "0814169470", new Guid("aa301feb-440f-42ae-b4a1-0c123bf074af"), "Friend" },
                    { new Guid("e323616a-28e8-4b45-bf81-01e765f249ae"), "John Mulaney", "0814169470", new Guid("fb59a87d-59e6-49d4-979b-658eb9986162"), "Friend" },
                    { new Guid("9bc0ce49-3e8c-4397-afb1-a743266a94f8"), "John Mulaney", "0814169470", new Guid("376017bd-2155-4e82-a1a7-fbe7f2d49b83"), "Friend" }
                });

            migrationBuilder.InsertData(
                table: "EmergencyContacts",
                columns: new[] { "EmergencyContactID", "ContactPerson", "ContactTelephoneNumber", "EmployeeID", "RelationshipToEmployee" },
                values: new object[,]
                {
                    { new Guid("9c5f18df-0670-440f-87d9-6f27ab53670a"), "John Mulaney", "0814169470", new Guid("fa5a66e7-40e8-48e6-8564-4a38e40ddadd"), "Friend" },
                    { new Guid("5cc44689-c836-4c8f-bab6-236cdeab8af7"), "John Mulaney", "0814169470", new Guid("47d01ee3-0c9b-4118-beea-c21085cf001c"), "Friend" },
                    { new Guid("c95b77f9-c24c-4ba1-80ce-94abc155757c"), "John Mulaney", "0814169470", new Guid("14e100c5-a695-49fa-ada2-244c117df5c0"), "Friend" },
                    { new Guid("cbd6262c-5a59-465b-85c1-222e6d9d717c"), "John Mulaney", "0814169470", new Guid("7f177922-a5f1-448d-8862-96fd4ba5d633"), "Friend" },
                    { new Guid("25f87857-4d8d-4388-b733-6270d27529be"), "John Mulaney", "0814169470", new Guid("30fc6523-661c-4479-8539-049d8cca6cba"), "Friend" },
                    { new Guid("ade1abfb-7178-4668-a16b-a5e76f58b067"), "John Mulaney", "0814169470", new Guid("f11765a0-58d2-42e5-9c5c-34022016e8c6"), "Friend" },
                    { new Guid("1b62b809-05df-49c1-b93a-b7bab141097b"), "John Mulaney", "0814169470", new Guid("bc2c2ae9-204f-49b5-8d9d-58da49b8fcd4"), "Friend" }
                });

            migrationBuilder.InsertData(
                table: "EmployeeAddresses",
                columns: new[] { "AddressID", "EmployeeID" },
                values: new object[,]
                {
                    { new Guid("3085c334-566d-4c8f-8432-cfc0a20f1c41"), new Guid("40525829-4dd0-4352-a149-cea6a86ec01f") },
                    { new Guid("97491d4b-2fe3-4140-a310-3ed4e5bfdd3d"), new Guid("7f177922-a5f1-448d-8862-96fd4ba5d633") },
                    { new Guid("02b1fcc2-c6e2-432e-9a5d-1bbd379bf3f5"), new Guid("2a7ecffa-8673-45c0-8b0a-df03b891dc3c") },
                    { new Guid("63cfb3ec-db5e-421c-9246-1edfe4740fe0"), new Guid("83fc7bdc-b730-4c4f-b13b-d40f9d282b73") },
                    { new Guid("f57e6df5-6cf1-4896-9d75-e8714e8c1c2b"), new Guid("14e100c5-a695-49fa-ada2-244c117df5c0") },
                    { new Guid("7e483a7b-9f16-4c9a-8f0b-716088714836"), new Guid("efdc266e-2dce-4021-968c-cad5adc9c9b5") },
                    { new Guid("5ea203e4-a74d-4056-a229-55f678266624"), new Guid("f11765a0-58d2-42e5-9c5c-34022016e8c6") },
                    { new Guid("7a490548-2ac9-4902-bca0-7aa7b044416b"), new Guid("376017bd-2155-4e82-a1a7-fbe7f2d49b83") },
                    { new Guid("30ee5dc5-9602-4b2a-bc25-a92039520e1c"), new Guid("61cc8c8d-f487-4a7e-b78c-ab4614786586") },
                    { new Guid("9a9a8c53-5c08-4c25-8147-dda25dd62fdf"), new Guid("4e5f0b7f-68ba-4254-b6fc-04fab6a99fce") },
                    { new Guid("9127e563-e3d4-4694-814e-2edfa644b518"), new Guid("fb59a87d-59e6-49d4-979b-658eb9986162") },
                    { new Guid("b69e2095-3ec0-438f-951e-84f44c6a06d0"), new Guid("aa301feb-440f-42ae-b4a1-0c123bf074af") },
                    { new Guid("53c885f8-fa5f-4314-aa51-54aea0031ae2"), new Guid("50084a0b-1663-4f4f-8974-e0fbaacf2d60") },
                    { new Guid("7c6569da-cd8d-4228-9a40-b5d6faed1487"), new Guid("26771adf-20b0-4988-92d9-c0e245b9bf93") },
                    { new Guid("e17ff651-d643-41eb-bcff-dd05990d5015"), new Guid("47d01ee3-0c9b-4118-beea-c21085cf001c") },
                    { new Guid("623fba70-b4b3-4e3b-bb63-774ad268a916"), new Guid("30fc6523-661c-4479-8539-049d8cca6cba") },
                    { new Guid("eeee2e71-60d0-4167-a357-065a7b2cc497"), new Guid("63abb4b4-5d4b-446d-8471-eb3e0f7f053c") },
                    { new Guid("5fdebe6d-13d8-4720-a988-5936677f62d3"), new Guid("eac034be-cbba-4360-9e8e-8a897f16c6a6") },
                    { new Guid("c520b2f5-0dac-42ee-b356-08f3e51078ed"), new Guid("bc2c2ae9-204f-49b5-8d9d-58da49b8fcd4") },
                    { new Guid("c632e660-2d53-4c19-bef0-084b15df97e5"), new Guid("89d3bb9a-c316-4ef5-b1e2-f8423996ffb2") }
                });

            migrationBuilder.InsertData(
                table: "Inventories",
                columns: new[] { "InventoryID", "InventoryTypeID", "ItemName", "Quantity", "SupplierID", "TableStatusID" },
                values: new object[,]
                {
                    { new Guid("ca8de3b1-a744-4821-9537-81b90d96e153"), new Guid("19528739-55a2-45fd-a026-3c0e0f9d1ba1"), "2KG Ice Bag", 500, new Guid("b3307864-6945-4aa0-92e7-0095e056889b"), new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("f0c7eb5c-6603-455f-96d6-5f9de5854bb6"), new Guid("19528739-55a2-45fd-a026-3c0e0f9d1ba1"), "3KG Ice Bag", 500, new Guid("b3307864-6945-4aa0-92e7-0095e056889b"), new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("97ec7bf3-2fd0-44de-8960-a119ec42beea"), new Guid("19528739-55a2-45fd-a026-3c0e0f9d1ba1"), "1KG Ice Bag", 500, new Guid("b3307864-6945-4aa0-92e7-0095e056889b"), new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("f7974974-613b-40da-9391-838f7dd9a4d5"), new Guid("19528739-55a2-45fd-a026-3c0e0f9d1ba1"), "4KG Ice Bag", 500, new Guid("b3307864-6945-4aa0-92e7-0095e056889b"), new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("d9bcb8d7-db6e-4e26-8095-484ecfab9c5d"), new Guid("19528739-55a2-45fd-a026-3c0e0f9d1ba1"), "Large Wood Bag", 500, new Guid("b3307864-6945-4aa0-92e7-0095e056889b"), new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("98cb7fea-bfe7-46c0-a5f1-70a89b32ff33"), new Guid("19528739-55a2-45fd-a026-3c0e0f9d1ba1"), "Small Wood Bag", 500, new Guid("b3307864-6945-4aa0-92e7-0095e056889b"), new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("bfb2a434-9da4-4f85-93ea-1dc51e20d07b"), new Guid("19528739-55a2-45fd-a026-3c0e0f9d1ba1"), "5KG Ice Bag", 500, new Guid("b3307864-6945-4aa0-92e7-0095e056889b"), new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") }
                });

            migrationBuilder.InsertData(
                table: "Sales",
                columns: new[] { "SaleID", "InvoiceID", "SaleDate", "SaleStatusID" },
                values: new object[,]
                {
                    { new Guid("fab9868a-1b84-4627-94d3-f1ca2de8ec18"), new Guid("d74516b2-a88d-485b-b221-adf65e094fe4"), new DateTime(2021, 4, 12, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("77cd235f-e443-414d-b2fb-8b374a0f7959") },
                    { new Guid("e3e62c41-fba8-4f64-9c3c-b1d0d0a20ec7"), new Guid("1f68d9ba-bf99-46cb-a5a8-4a6031a434f5"), new DateTime(2021, 7, 23, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("77cd235f-e443-414d-b2fb-8b374a0f7959") },
                    { new Guid("2d7fe367-a9fb-4e63-b6ce-a3bb8a597f19"), new Guid("d8286dfe-add6-4834-8f7d-9b2bd96ae569"), new DateTime(2021, 8, 6, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("77cd235f-e443-414d-b2fb-8b374a0f7959") },
                    { new Guid("d7300709-4ddc-4a47-aa41-91ed4b02636f"), new Guid("9eca188d-e389-473d-b46e-ca9a03b27cf3"), new DateTime(2021, 3, 22, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("77cd235f-e443-414d-b2fb-8b374a0f7959") }
                });

            migrationBuilder.InsertData(
                table: "SupplierContactPeople",
                columns: new[] { "SupplierContactPersonID", "EmailAddress", "Name", "PhoneNumber", "SupplierID" },
                values: new object[,]
                {
                    { new Guid("26e63540-1f4a-421f-ad80-520114fcef35"), "", "Shonnah", "", new Guid("0c3fc2e9-75f8-4699-ab0b-eecd79770a9f") },
                    { new Guid("4d9e46e5-b5ab-4493-aa93-e8462983d8ef"), "", "General", "", new Guid("59347180-43e2-431e-a452-abf6b2b4ce9a") },
                    { new Guid("20bf139c-34c3-4a65-b925-7835089f24f2"), "", "General", "", new Guid("74e4f034-7f94-4958-93e7-4879e876170b") },
                    { new Guid("297a48e1-30b6-408b-b7a3-9c486cfc9a5e"), "", "Karen", "", new Guid("35836f8d-5b8e-4153-ae15-595cfbee27aa") }
                });

            migrationBuilder.InsertData(
                table: "SupplierContactPeople",
                columns: new[] { "SupplierContactPersonID", "EmailAddress", "Name", "PhoneNumber", "SupplierID" },
                values: new object[,]
                {
                    { new Guid("1e9e3a5f-8354-42e8-943b-cfe559ce7885"), "", "Wiets", "0113102684", new Guid("1441f7fc-c068-48a0-ab62-dc2c9e0e8bc7") },
                    { new Guid("bbb8cb75-a06c-4fec-8bfd-d7d8a5e7c4ba"), "", "General", "0129418434", new Guid("75aa1a34-bd38-4e93-b980-26be2a6d1e9b") },
                    { new Guid("60fc9f8a-2aea-4c68-9ad5-89ab1549f799"), "", "Natasha", "0877239405", new Guid("1c4355b2-8129-41c4-b47c-4294bb733a9b") },
                    { new Guid("6c2973fb-3359-4008-9411-913bd6474153"), "", "General", "01119740446", new Guid("bc978cf4-4ce3-46e5-8e5c-1c99ee0d4618") },
                    { new Guid("efc38bba-5231-4242-8489-ccd6b5eb1068"), "", "Frikkie", "0126530247", new Guid("01283a20-a3c7-407c-8715-a59752f79783") },
                    { new Guid("44a09b6f-e9e0-450c-b185-8d5090578794"), "", "Herman", "0126538088", new Guid("8a76c942-85c3-4de6-94d9-5a6885975113") },
                    { new Guid("a0af06c0-fc41-47fb-a6cc-74c1d3d32756"), "", "Ephrahim", "0843998732", new Guid("9369078c-fa89-4d0e-a8d1-10d91da87e77") },
                    { new Guid("e85a72a6-d4bb-4236-b4ce-358ba7fc3497"), "", "Francois", "0832277741", new Guid("9d5d963f-5940-4d21-8ecd-9c9e84aff20c") },
                    { new Guid("e1c091f2-7981-4a02-8d8c-5091fa698345"), "", "Petrus", "0764527752", new Guid("ac9da526-396f-4974-8ca5-56a2c3f84085") },
                    { new Guid("d1379325-3999-4653-9aaa-a90c277493d2"), "", "JP", "", new Guid("f57dc5ed-3b02-4944-ba37-744991275bdc") },
                    { new Guid("6b41097b-ed96-40ce-a89c-ec0c58aee382"), "", "General", "", new Guid("1c8e1c73-f5c5-4124-9786-b87322e42f22") },
                    { new Guid("cf9067fd-02a3-4f55-83ed-b97c5b4fecc3"), "", "Steven", "", new Guid("b3307864-6945-4aa0-92e7-0095e056889b") },
                    { new Guid("52d391d2-9893-48c2-8f58-69fde2876a08"), "", "Theressa", "", new Guid("8adb40e5-dde0-44cd-a21f-8a29142c54b2") },
                    { new Guid("7dc365f7-c7ae-4101-a608-942b04c28539"), "", "Christiaan", "", new Guid("bfeadaa7-b5dd-4656-a26e-500d53ee9f8d") },
                    { new Guid("1203d56b-7c8d-4268-8be8-89993fcce5a6"), "", "General", "", new Guid("132add66-6dc0-46de-ab8e-03195fcd788c") },
                    { new Guid("6762674e-fbc5-461c-b31a-6e6936a637ed"), "", "Lawrence", "", new Guid("dccebd02-eb22-49d9-8cde-bcf145ef0964") },
                    { new Guid("718061e2-2f36-4559-8420-f8f8b6ca62c4"), "", "Loraine", "", new Guid("59b800f7-921a-4a1c-b626-b49004fde301") },
                    { new Guid("d7123644-674b-4b2c-90e2-b71bdd91a2e7"), "", "Reception", "", new Guid("43a88e15-d11f-4b5d-ba42-7639666ca8ee") },
                    { new Guid("2d161ca0-fe36-42a0-b568-fa8676c0b6bc"), "", "General", "", new Guid("ecc20563-4c08-483b-bdea-54c4db6eea79") },
                    { new Guid("9ac8a94e-a10b-418e-a2ea-38b0b2a038e5"), "", "Robert", "", new Guid("fc585747-d8d4-4fdd-a2f5-566b7abb2a0b") },
                    { new Guid("b14c26c6-e7d9-4acb-8d08-4c5f44a88849"), "", "Winters", "", new Guid("b9475cf2-ca78-4533-a519-8cf60b42a2b0") },
                    { new Guid("914c0f86-1e87-4ec6-b158-96f8fc055f7b"), "", "General", "", new Guid("2cee437e-041a-42b6-a916-6d7bf8b94267") },
                    { new Guid("b53bb5f4-2aae-46d9-aef4-612f11bfdd13"), "", "General", "", new Guid("d1e8ddaf-d1b4-4876-83ac-0f67364d2510") },
                    { new Guid("3ad2af3e-ecac-4bf2-94b5-ad5bcddcc85e"), "", "Maryke", "", new Guid("d4db4769-4321-4de0-b0a2-f6577386efcf") },
                    { new Guid("7da76e0f-cef2-4a21-b4d9-a72a566a76ac"), "", "Joel", "", new Guid("1a7575e9-13cf-43c9-96a2-90ee5d26b2c1") },
                    { new Guid("10ebbd0c-5079-4efe-b583-df781707081e"), "", "Tyler", "", new Guid("ca300856-9dc0-4ba5-8dfb-017b71d09c36") },
                    { new Guid("3c8190ff-eed4-4ea9-85dd-7f230a55a258"), "", "JP", "", new Guid("ceb25950-f3fc-428f-aecb-2f1f98c1a4ec") },
                    { new Guid("20116db1-13c4-4b22-b4c9-7570a6e057bb"), "", "Ettienne", "", new Guid("8f071616-125a-4ba2-8e53-1f1da3b4bfa2") },
                    { new Guid("dbf9314a-1a55-404d-a587-4f06fb59b6bb"), "", "Meisie", "", new Guid("d4996a9a-fc8b-4e30-b99f-ed7c53ee17a5") },
                    { new Guid("59d9a399-bca8-42c5-b13e-201657bcc37e"), "", "Dawn", "", new Guid("2451e97d-ac43-4788-9768-ad7628b237a9") },
                    { new Guid("3e3b327e-5041-43b8-8bda-8860fa4885ea"), "", "Ettiene", "", new Guid("2451e97d-ac43-4788-9768-ad7628b237a9") },
                    { new Guid("fa89b7e0-5242-4cab-912d-11373a453f48"), "", "Eugene", "", new Guid("6b4de917-fef4-4cd7-9b97-a4a768ce65d5") },
                    { new Guid("eafd605f-a258-4d6b-8ce1-872a4d2109f0"), "", "Braam", "", new Guid("edb156ee-b20c-4dd6-9220-6321bd6c152e") },
                    { new Guid("3ee3a783-5c81-4c03-87e3-75b3e43fa377"), "", "Leaine", "", new Guid("59b800f7-921a-4a1c-b626-b49004fde301") }
                });

            migrationBuilder.InsertData(
                table: "SupplierOrders",
                columns: new[] { "SupplierOrderID", "DatePlaced", "DateReceived", "Details", "SupplierID", "SupplierOrderStatusID" },
                values: new object[] { new Guid("5744cfa9-2835-467e-b146-911f3d955852"), new DateTime(2021, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "No details to note", new Guid("b3307864-6945-4aa0-92e7-0095e056889b"), new Guid("f13b8acc-6d12-4877-b8d7-cc9f174f1734") });

            migrationBuilder.InsertData(
                table: "InventoryPrices",
                columns: new[] { "InventoryPriceID", "Date", "InventoryID", "PriceID" },
                values: new object[,]
                {
                    { new Guid("60339a11-fb71-445a-a88f-7245660481ba"), new DateTime(2021, 10, 15, 10, 48, 12, 889, DateTimeKind.Local).AddTicks(6709), new Guid("bfb2a434-9da4-4f85-93ea-1dc51e20d07b"), new Guid("55bb5ecd-ffd7-4e9f-8f53-d2ce47e7b633") },
                    { new Guid("7c2579c7-1076-4d1c-9ea8-746195540ad7"), new DateTime(2021, 10, 15, 10, 48, 12, 889, DateTimeKind.Local).AddTicks(9591), new Guid("ca8de3b1-a744-4821-9537-81b90d96e153"), new Guid("49e4b3a1-0597-4982-a06b-df5a68b6c5fc") },
                    { new Guid("c363880d-eabe-48ba-9795-757db1bf3e05"), new DateTime(2021, 10, 15, 10, 48, 12, 889, DateTimeKind.Local).AddTicks(9611), new Guid("f0c7eb5c-6603-455f-96d6-5f9de5854bb6"), new Guid("69eebef6-9c1b-4d6d-9353-fd1a80890c80") },
                    { new Guid("742a9949-4ab8-4bf8-831d-5f8cd8b329d6"), new DateTime(2021, 10, 15, 10, 48, 12, 889, DateTimeKind.Local).AddTicks(9617), new Guid("97ec7bf3-2fd0-44de-8960-a119ec42beea"), new Guid("ab60263b-243b-4ce9-a43b-447ba1943ed0") },
                    { new Guid("afbc7b15-d66a-4bca-a155-7337a444deea"), new DateTime(2021, 10, 15, 10, 48, 12, 889, DateTimeKind.Local).AddTicks(9622), new Guid("f7974974-613b-40da-9391-838f7dd9a4d5"), new Guid("bc871e86-0885-43b9-be14-e1db9492b3e8") },
                    { new Guid("62d1c758-5b5b-4b78-b9d8-e9ef136d5b01"), new DateTime(2021, 10, 15, 10, 48, 12, 889, DateTimeKind.Local).AddTicks(9634), new Guid("98cb7fea-bfe7-46c0-a5f1-70a89b32ff33"), new Guid("26746a3d-fc91-4f1e-b429-a9d56c0bd42e") },
                    { new Guid("acc84feb-6da6-46cf-8cc0-cffcb8781621"), new DateTime(2021, 10, 15, 10, 48, 12, 889, DateTimeKind.Local).AddTicks(9628), new Guid("d9bcb8d7-db6e-4e26-8095-484ecfab9c5d"), new Guid("fb453fdd-2259-4d4a-a966-66bbcf3a61e3") }
                });

            migrationBuilder.InsertData(
                table: "Orders",
                columns: new[] { "OrderID", "AddressID", "ClientID", "Delivered", "DeliveryDate", "DeliveryTripSheetID", "InvoiceID", "Loaded", "OrderDate", "OrderStatusID" },
                values: new object[,]
                {
                    { new Guid("5f36368a-b187-42ae-b718-e0cb902b1e4a"), new Guid("aad80912-434e-4924-8ddc-38e5e7c998f7"), new Guid("10e21cc0-001c-438b-bb2f-445e446688dd"), false, new DateTime(2021, 5, 15, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("52238d97-9e77-4107-b8f7-6739d32be3e0"), new Guid("6d619343-bb23-401f-b40f-3bf323a8b706"), true, new DateTime(2021, 5, 14, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("37196611-3ac3-4ca5-8982-3fdcdf726b85") },
                    { new Guid("fc9a5780-6cc4-40a7-8749-ff15361ad458"), new Guid("aad80912-434e-4924-8ddc-38e5e7c998f7"), new Guid("10e21cc0-001c-438b-bb2f-445e446688dd"), false, new DateTime(2021, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("a7fe7e2e-a308-4c9a-be4d-bf33a25f3c91"), new Guid("4bdbaa80-9f10-4e36-956c-7ab882c97d00"), true, new DateTime(2021, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("502d6f63-525a-4efc-bdcb-3560b0edff2c") },
                    { new Guid("9c15fdd9-48e2-4bf7-8bca-c53a9b7f29b5"), new Guid("dd44b55c-ceca-46da-9e71-53884ab12fdc"), new Guid("a6e982dd-7dd5-4256-a5f4-68a0a82bc596"), false, new DateTime(2021, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("2d1966d0-8749-4bb7-af16-2b99c878f6fb"), new Guid("7e4dcd3b-a893-490f-a469-ca0ea53d3bc7"), true, new DateTime(2021, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("502d6f63-525a-4efc-bdcb-3560b0edff2c") },
                    { new Guid("c08a53d5-7926-46cd-b3c1-5e251497ea91"), new Guid("a601cc74-fefd-4a1e-908d-c814a6598708"), new Guid("8c316ea4-605a-4af8-9083-dab4a533dfdc"), false, new DateTime(2021, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("741d2ad5-ff78-493b-a8bd-16c10649de04"), new Guid("2e17cf49-6504-4d05-983f-8128a4a0c459"), true, new DateTime(2021, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("c10bfe5e-4c56-4551-b1f2-8fd3321a4d1b") },
                    { new Guid("c3d6fd3c-a190-41f1-b627-0039eca8e5b1"), new Guid("aad80912-434e-4924-8ddc-38e5e7c998f7"), new Guid("10e21cc0-001c-438b-bb2f-445e446688dd"), false, new DateTime(2021, 9, 3, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("371fefc3-9a45-4340-92ee-e2e9b5ddbd16"), new Guid("c21d9b1b-27a1-413a-84ea-f5708133e65f"), true, new DateTime(2021, 9, 2, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("37196611-3ac3-4ca5-8982-3fdcdf726b85") },
                    { new Guid("57d68757-9ad2-4edc-9ef0-d0d965909154"), new Guid("aad80912-434e-4924-8ddc-38e5e7c998f7"), new Guid("10e21cc0-001c-438b-bb2f-445e446688dd"), false, new DateTime(2021, 6, 22, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("510ae3b6-37f5-44af-886e-5a4d2117f202"), new Guid("5d84a526-2e46-4f9f-ba22-e9e29e0b73ae"), true, new DateTime(2021, 6, 21, 15, 31, 35, 0, DateTimeKind.Unspecified), new Guid("37196611-3ac3-4ca5-8982-3fdcdf726b85") }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "ProductID", "InventoryID", "ProductName", "TableStatusID" },
                values: new object[,]
                {
                    { new Guid("a0cd82e1-670e-4988-b096-b94d008bf10a"), new Guid("98cb7fea-bfe7-46c0-a5f1-70a89b32ff33"), "Small Wood", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("42258ad8-9733-4d44-adae-eb2b5589cf42"), new Guid("97ec7bf3-2fd0-44de-8960-a119ec42beea"), "1KG Ice Blocks", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("1e54f43d-efbd-4211-94ae-cee6ec34014c"), new Guid("f0c7eb5c-6603-455f-96d6-5f9de5854bb6"), "3KG Ice Blocks", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("da004ad8-2f64-4bf5-9c2f-adc9e48070bc"), new Guid("ca8de3b1-a744-4821-9537-81b90d96e153"), "2KG Ice Blocks", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("974e485b-d76f-4f4f-bde7-2b414435890f"), new Guid("bfb2a434-9da4-4f85-93ea-1dc51e20d07b"), "5KG Ice Blocks", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("6fb2b343-db52-42b0-b74d-759d0556a80b"), new Guid("d9bcb8d7-db6e-4e26-8095-484ecfab9c5d"), "Large Wood", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") },
                    { new Guid("629763fc-84f1-4057-bdf6-4167728af1e8"), new Guid("f7974974-613b-40da-9391-838f7dd9a4d5"), "4KG Ice Blocks", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e") }
                });

            migrationBuilder.InsertData(
                table: "ProductPrices",
                columns: new[] { "ProductPriceID", "Date", "PriceID", "ProductID" },
                values: new object[,]
                {
                    { new Guid("82cffdfb-4ca7-4db5-ba1d-32df7e9c8f46"), new DateTime(2021, 10, 15, 10, 48, 12, 900, DateTimeKind.Local).AddTicks(2145), new Guid("582b1b38-0afd-40a8-9ccd-67663d221d67"), new Guid("974e485b-d76f-4f4f-bde7-2b414435890f") },
                    { new Guid("9de02532-d97d-4840-a04d-87aeeef56d3e"), new DateTime(2021, 10, 15, 10, 48, 12, 900, DateTimeKind.Local).AddTicks(2563), new Guid("8b3a27e7-7d75-4beb-8cc4-cea4f950c902"), new Guid("da004ad8-2f64-4bf5-9c2f-adc9e48070bc") },
                    { new Guid("64ec33d1-8bbd-4777-a6b0-716d14ef9f5d"), new DateTime(2021, 10, 15, 10, 48, 12, 900, DateTimeKind.Local).AddTicks(2576), new Guid("a9fd9258-2557-4f48-935f-227a97c87c8b"), new Guid("1e54f43d-efbd-4211-94ae-cee6ec34014c") },
                    { new Guid("67a43150-2aac-47c5-ad0f-289af32ba82d"), new DateTime(2021, 10, 15, 10, 48, 12, 900, DateTimeKind.Local).AddTicks(2584), new Guid("cde17195-4204-4f9f-aed0-079ed23bacdf"), new Guid("42258ad8-9733-4d44-adae-eb2b5589cf42") },
                    { new Guid("199baac9-1afb-48b0-be10-e0a363db3926"), new DateTime(2021, 10, 15, 10, 48, 12, 900, DateTimeKind.Local).AddTicks(2594), new Guid("8cef5816-66f6-4354-af7b-9ff24cf339f7"), new Guid("629763fc-84f1-4057-bdf6-4167728af1e8") },
                    { new Guid("21b6bc5f-9c2f-4646-9491-4461e7ac156c"), new DateTime(2021, 10, 15, 10, 48, 12, 900, DateTimeKind.Local).AddTicks(2602), new Guid("3c63dbda-4e29-4028-8314-859c7b1351f9"), new Guid("6fb2b343-db52-42b0-b74d-759d0556a80b") },
                    { new Guid("aa27d0ae-7b02-4e0e-a389-02fc219af827"), new DateTime(2021, 10, 15, 10, 48, 12, 900, DateTimeKind.Local).AddTicks(2611), new Guid("8985e3d5-2b88-43d7-a300-688d7a648a0b"), new Guid("a0cd82e1-670e-4988-b096-b94d008bf10a") }
                });

            migrationBuilder.InsertData(
                table: "SupplierOrderLines",
                columns: new[] { "SupplierOrderLineID", "InventoryID", "InventoryPriceID", "Quantity", "SupplierOrderID" },
                values: new object[] { new Guid("7e70bb0e-9097-4961-b7fb-1d5c4528bf02"), new Guid("bfb2a434-9da4-4f85-93ea-1dc51e20d07b"), new Guid("60339a11-fb71-445a-a88f-7245660481ba"), 500, new Guid("5744cfa9-2835-467e-b146-911f3d955852") });

            migrationBuilder.InsertData(
                table: "OrderLines",
                columns: new[] { "OrderID", "ProductID", "ProductPriceID", "Quantity" },
                values: new object[,]
                {
                    { new Guid("c08a53d5-7926-46cd-b3c1-5e251497ea91"), new Guid("974e485b-d76f-4f4f-bde7-2b414435890f"), new Guid("82cffdfb-4ca7-4db5-ba1d-32df7e9c8f46"), 100 },
                    { new Guid("9c15fdd9-48e2-4bf7-8bca-c53a9b7f29b5"), new Guid("da004ad8-2f64-4bf5-9c2f-adc9e48070bc"), new Guid("9de02532-d97d-4840-a04d-87aeeef56d3e"), 50 },
                    { new Guid("fc9a5780-6cc4-40a7-8749-ff15361ad458"), new Guid("da004ad8-2f64-4bf5-9c2f-adc9e48070bc"), new Guid("9de02532-d97d-4840-a04d-87aeeef56d3e"), 35 },
                    { new Guid("57d68757-9ad2-4edc-9ef0-d0d965909154"), new Guid("42258ad8-9733-4d44-adae-eb2b5589cf42"), new Guid("67a43150-2aac-47c5-ad0f-289af32ba82d"), 700 },
                    { new Guid("5f36368a-b187-42ae-b718-e0cb902b1e4a"), new Guid("629763fc-84f1-4057-bdf6-4167728af1e8"), new Guid("199baac9-1afb-48b0-be10-e0a363db3926"), 600 },
                    { new Guid("c3d6fd3c-a190-41f1-b627-0039eca8e5b1"), new Guid("6fb2b343-db52-42b0-b74d-759d0556a80b"), new Guid("21b6bc5f-9c2f-4646-9491-4461e7ac156c"), 800 }
                });

            migrationBuilder.InsertData(
                table: "SaleLines",
                columns: new[] { "ProductID", "SaleID", "ProductPriceID", "Quantity" },
                values: new object[,]
                {
                    { new Guid("da004ad8-2f64-4bf5-9c2f-adc9e48070bc"), new Guid("d7300709-4ddc-4a47-aa41-91ed4b02636f"), new Guid("9de02532-d97d-4840-a04d-87aeeef56d3e"), 600 },
                    { new Guid("1e54f43d-efbd-4211-94ae-cee6ec34014c"), new Guid("fab9868a-1b84-4627-94d3-f1ca2de8ec18"), new Guid("64ec33d1-8bbd-4777-a6b0-716d14ef9f5d"), 600 },
                    { new Guid("629763fc-84f1-4057-bdf6-4167728af1e8"), new Guid("e3e62c41-fba8-4f64-9c3c-b1d0d0a20ec7"), new Guid("199baac9-1afb-48b0-be10-e0a363db3926"), 600 },
                    { new Guid("6fb2b343-db52-42b0-b74d-759d0556a80b"), new Guid("2d7fe367-a9fb-4e63-b6ce-a3bb8a597f19"), new Guid("21b6bc5f-9c2f-4646-9491-4461e7ac156c"), 600 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_AddressTypeID",
                table: "Addresses",
                column: "AddressTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_AuditTrails_EmployeeID",
                table: "AuditTrails",
                column: "EmployeeID");

            migrationBuilder.CreateIndex(
                name: "IX_AuthNotifications_AuthNotificationTypeID",
                table: "AuthNotifications",
                column: "AuthNotificationTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_AuthNotifications_NotificationID",
                table: "AuthNotifications",
                column: "NotificationID");

            migrationBuilder.CreateIndex(
                name: "IX_AuthNotifications_OwnerID",
                table: "AuthNotifications",
                column: "OwnerID");

            migrationBuilder.CreateIndex(
                name: "IX_AuthNotifications_SenderID",
                table: "AuthNotifications",
                column: "SenderID");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAddresses_ClientID",
                table: "ClientAddresses",
                column: "ClientID");

            migrationBuilder.CreateIndex(
                name: "IX_ClientContactPeople_ClientID",
                table: "ClientContactPeople",
                column: "ClientID");

            migrationBuilder.CreateIndex(
                name: "IX_Clients_TableStatusID",
                table: "Clients",
                column: "TableStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_ClientTrends_ClientID",
                table: "ClientTrends",
                column: "ClientID");

            migrationBuilder.CreateIndex(
                name: "IX_DailyAttendances_EmployeeID",
                table: "DailyAttendances",
                column: "EmployeeID");

            migrationBuilder.CreateIndex(
                name: "IX_DeliveryTripSheets_AssistantID",
                table: "DeliveryTripSheets",
                column: "AssistantID");

            migrationBuilder.CreateIndex(
                name: "IX_DeliveryTripSheets_DriverID",
                table: "DeliveryTripSheets",
                column: "DriverID");

            migrationBuilder.CreateIndex(
                name: "IX_DeliveryTripSheets_SupervisorID",
                table: "DeliveryTripSheets",
                column: "SupervisorID");

            migrationBuilder.CreateIndex(
                name: "IX_DeliveryTripSheets_TripSheetStatusID",
                table: "DeliveryTripSheets",
                column: "TripSheetStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_DeliveryTripSheets_VehicleID",
                table: "DeliveryTripSheets",
                column: "VehicleID");

            migrationBuilder.CreateIndex(
                name: "IX_EmergencyContacts_EmployeeID",
                table: "EmergencyContacts",
                column: "EmployeeID");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeAddresses_EmployeeID",
                table: "EmployeeAddresses",
                column: "EmployeeID");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeErrors_EmployeeID",
                table: "EmployeeErrors",
                column: "EmployeeID");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_AccountTypeID",
                table: "Employees",
                column: "AccountTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_BankID",
                table: "Employees",
                column: "BankID");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_EmployeeQualificationID",
                table: "Employees",
                column: "EmployeeQualificationID");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_EmployeeStatusID",
                table: "Employees",
                column: "EmployeeStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_EmployeeTitleID",
                table: "Employees",
                column: "EmployeeTitleID");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_EmployeeTypeID",
                table: "Employees",
                column: "EmployeeTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_TableStatusID",
                table: "Employees",
                column: "TableStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeTripSheets_DeliveryTripSheetID",
                table: "EmployeeTripSheets",
                column: "DeliveryTripSheetID");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeTripSheets_EmployeeID",
                table: "EmployeeTripSheets",
                column: "EmployeeID");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeTripSheets_EmployeeTripSheetTypeID",
                table: "EmployeeTripSheets",
                column: "EmployeeTripSheetTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_ExtraProductsTaken_DeliveryTripSheetID",
                table: "ExtraProductsTaken",
                column: "DeliveryTripSheetID");

            migrationBuilder.CreateIndex(
                name: "IX_ExtraProductsTaken_ProductID",
                table: "ExtraProductsTaken",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_Inventories_InventoryTypeID",
                table: "Inventories",
                column: "InventoryTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Inventories_SupplierID",
                table: "Inventories",
                column: "SupplierID");

            migrationBuilder.CreateIndex(
                name: "IX_Inventories_TableStatusID",
                table: "Inventories",
                column: "TableStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryPrices_InventoryID",
                table: "InventoryPrices",
                column: "InventoryID");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryPrices_PriceID",
                table: "InventoryPrices",
                column: "PriceID");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_InvoiceStatusID",
                table: "Invoices",
                column: "InvoiceStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_PaymentMethodID",
                table: "Invoices",
                column: "PaymentMethodID");

            migrationBuilder.CreateIndex(
                name: "IX_MachineMachineData_MachineDataID",
                table: "MachineMachineData",
                column: "MachineDataID");

            migrationBuilder.CreateIndex(
                name: "IX_Machines_MachineTypeID",
                table: "Machines",
                column: "MachineTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Machines_TableStatusID",
                table: "Machines",
                column: "TableStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_EmployeeID",
                table: "Notifications",
                column: "EmployeeID");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_NotificationTypeID",
                table: "Notifications",
                column: "NotificationTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderExtraProductsTaken_OrderID",
                table: "OrderExtraProductsTaken",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderLines_ProductID",
                table: "OrderLines",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderLines_ProductPriceID",
                table: "OrderLines",
                column: "ProductPriceID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_AddressID",
                table: "Orders",
                column: "AddressID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ClientID",
                table: "Orders",
                column: "ClientID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_DeliveryTripSheetID",
                table: "Orders",
                column: "DeliveryTripSheetID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_InvoiceID",
                table: "Orders",
                column: "InvoiceID",
                unique: true,
                filter: "[InvoiceID] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_OrderStatusID",
                table: "Orders",
                column: "OrderStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPrices_PriceID",
                table: "ProductPrices",
                column: "PriceID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPrices_ProductID",
                table: "ProductPrices",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_Products_InventoryID",
                table: "Products",
                column: "InventoryID");

            migrationBuilder.CreateIndex(
                name: "IX_Products_TableStatusID",
                table: "Products",
                column: "TableStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_SaleLines_ProductID",
                table: "SaleLines",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_SaleLines_ProductPriceID",
                table: "SaleLines",
                column: "ProductPriceID");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_InvoiceID",
                table: "Sales",
                column: "InvoiceID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sales_SaleStatusID",
                table: "Sales",
                column: "SaleStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceMachines_MachineID",
                table: "ServiceMachines",
                column: "MachineID");

            migrationBuilder.CreateIndex(
                name: "IX_Services_SupplierID",
                table: "Services",
                column: "SupplierID");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceServiceJobs_ServiceJobID",
                table: "ServiceServiceJobs",
                column: "ServiceJobID");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceVehicles_VehicleID",
                table: "ServiceVehicles",
                column: "VehicleID");

            migrationBuilder.CreateIndex(
                name: "IX_StockTakeLines_InventoryID",
                table: "StockTakeLines",
                column: "InventoryID");

            migrationBuilder.CreateIndex(
                name: "IX_StockTakeLines_StockTakeID",
                table: "StockTakeLines",
                column: "StockTakeID");

            migrationBuilder.CreateIndex(
                name: "IX_StockWrittenOff_InventoryID",
                table: "StockWrittenOff",
                column: "InventoryID");

            migrationBuilder.CreateIndex(
                name: "IX_StockWrittenOff_WrittenOffReasonID",
                table: "StockWrittenOff",
                column: "WrittenOffReasonID");

            migrationBuilder.CreateIndex(
                name: "IX_SupplierContactPeople_SupplierID",
                table: "SupplierContactPeople",
                column: "SupplierID");

            migrationBuilder.CreateIndex(
                name: "IX_SupplierOrderLines_InventoryID",
                table: "SupplierOrderLines",
                column: "InventoryID");

            migrationBuilder.CreateIndex(
                name: "IX_SupplierOrderLines_InventoryPriceID",
                table: "SupplierOrderLines",
                column: "InventoryPriceID");

            migrationBuilder.CreateIndex(
                name: "IX_SupplierOrderLines_SupplierOrderID",
                table: "SupplierOrderLines",
                column: "SupplierOrderID");

            migrationBuilder.CreateIndex(
                name: "IX_SupplierOrders_SupplierID",
                table: "SupplierOrders",
                column: "SupplierID");

            migrationBuilder.CreateIndex(
                name: "IX_SupplierOrders_SupplierOrderStatusID",
                table: "SupplierOrders",
                column: "SupplierOrderStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_Suppliers_TableStatusID",
                table: "Suppliers",
                column: "TableStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_VehicleStatusID",
                table: "Vehicles",
                column: "VehicleStatusID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuditTrails");

            migrationBuilder.DropTable(
                name: "AuthNotifications");

            migrationBuilder.DropTable(
                name: "AuthWriteOffs");

            migrationBuilder.DropTable(
                name: "ClientAddresses");

            migrationBuilder.DropTable(
                name: "ClientContactPeople");

            migrationBuilder.DropTable(
                name: "ClientTrends");

            migrationBuilder.DropTable(
                name: "DailyAttendances");

            migrationBuilder.DropTable(
                name: "EmergencyContacts");

            migrationBuilder.DropTable(
                name: "EmployeeAddresses");

            migrationBuilder.DropTable(
                name: "EmployeeErrors");

            migrationBuilder.DropTable(
                name: "EmployeeTripSheets");

            migrationBuilder.DropTable(
                name: "InvoiceNumbers");

            migrationBuilder.DropTable(
                name: "MachineMachineData");

            migrationBuilder.DropTable(
                name: "NotificationTimers");

            migrationBuilder.DropTable(
                name: "OrderExtraProductsTaken");

            migrationBuilder.DropTable(
                name: "OrderLines");

            migrationBuilder.DropTable(
                name: "SaleLines");

            migrationBuilder.DropTable(
                name: "ServiceMachines");

            migrationBuilder.DropTable(
                name: "ServiceServiceJobs");

            migrationBuilder.DropTable(
                name: "ServiceVehicles");

            migrationBuilder.DropTable(
                name: "StockTakeLines");

            migrationBuilder.DropTable(
                name: "StockWrittenOff");

            migrationBuilder.DropTable(
                name: "SupplierContactPeople");

            migrationBuilder.DropTable(
                name: "SupplierOrderLines");

            migrationBuilder.DropTable(
                name: "AuthNotificationTypes");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "Days");

            migrationBuilder.DropTable(
                name: "EmployeeTripSheetTypes");

            migrationBuilder.DropTable(
                name: "MachineData");

            migrationBuilder.DropTable(
                name: "ExtraProductsTaken");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "ProductPrices");

            migrationBuilder.DropTable(
                name: "Sales");

            migrationBuilder.DropTable(
                name: "Machines");

            migrationBuilder.DropTable(
                name: "ServiceJobs");

            migrationBuilder.DropTable(
                name: "Services");

            migrationBuilder.DropTable(
                name: "StockTakes");

            migrationBuilder.DropTable(
                name: "WrittenOffReasons");

            migrationBuilder.DropTable(
                name: "InventoryPrices");

            migrationBuilder.DropTable(
                name: "SupplierOrders");

            migrationBuilder.DropTable(
                name: "NotificationTypes");

            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "Clients");

            migrationBuilder.DropTable(
                name: "DeliveryTripSheets");

            migrationBuilder.DropTable(
                name: "OrderStatuses");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Invoices");

            migrationBuilder.DropTable(
                name: "SaleStatuses");

            migrationBuilder.DropTable(
                name: "MachineTypes");

            migrationBuilder.DropTable(
                name: "Prices");

            migrationBuilder.DropTable(
                name: "SupplierOrderStatuses");

            migrationBuilder.DropTable(
                name: "AddressTypes");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "TripSheetStatuses");

            migrationBuilder.DropTable(
                name: "Vehicles");

            migrationBuilder.DropTable(
                name: "Inventories");

            migrationBuilder.DropTable(
                name: "InvoiceStatus");

            migrationBuilder.DropTable(
                name: "PaymentMethods");

            migrationBuilder.DropTable(
                name: "AccountTypes");

            migrationBuilder.DropTable(
                name: "Banks");

            migrationBuilder.DropTable(
                name: "EmployeeQualifications");

            migrationBuilder.DropTable(
                name: "EmployeeStatuses");

            migrationBuilder.DropTable(
                name: "EmployeeTitles");

            migrationBuilder.DropTable(
                name: "EmployeeTypes");

            migrationBuilder.DropTable(
                name: "VehicleStatuses");

            migrationBuilder.DropTable(
                name: "InventoryTypes");

            migrationBuilder.DropTable(
                name: "Suppliers");

            migrationBuilder.DropTable(
                name: "TableStatuses");
        }
    }
}
