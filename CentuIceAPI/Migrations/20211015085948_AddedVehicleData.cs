﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class AddedVehicleData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("60339a11-fb71-445a-a88f-7245660481ba"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("62d1c758-5b5b-4b78-b9d8-e9ef136d5b01"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("742a9949-4ab8-4bf8-831d-5f8cd8b329d6"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("7c2579c7-1076-4d1c-9ea8-746195540ad7"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("acc84feb-6da6-46cf-8cc0-cffcb8781621"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("afbc7b15-d66a-4bca-a155-7337a444deea"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("c363880d-eabe-48ba-9795-757db1bf3e05"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "VehicleID", "Capacity", "LastServicedMileage", "Last_Service_Date", "License_Expiry_Date", "Make", "Mileage", "Model", "Reg_Number", "Tracker_Number", "VehicleStatusID", "Year_Made" },
                values: new object[] { new Guid("68768508-c25a-479d-b962-c1ea1cf38f09"), 500, 0, new DateTime(2021, 4, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), new DateTime(2022, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), "MAN", 100000, "Trcuker", "GBV 587 WC", "TRK12345", new Guid("65563356-8382-4c0d-8e2c-f94c98cb786b"), new DateTime(2016, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "VehicleID",
                keyValue: new Guid("68768508-c25a-479d-b962-c1ea1cf38f09"));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("60339a11-fb71-445a-a88f-7245660481ba"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 54, 1, 224, DateTimeKind.Local).AddTicks(9473));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("62d1c758-5b5b-4b78-b9d8-e9ef136d5b01"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 54, 1, 225, DateTimeKind.Local).AddTicks(2200));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("742a9949-4ab8-4bf8-831d-5f8cd8b329d6"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 54, 1, 225, DateTimeKind.Local).AddTicks(2183));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("7c2579c7-1076-4d1c-9ea8-746195540ad7"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 54, 1, 225, DateTimeKind.Local).AddTicks(2159));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("acc84feb-6da6-46cf-8cc0-cffcb8781621"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 54, 1, 225, DateTimeKind.Local).AddTicks(2194));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("afbc7b15-d66a-4bca-a155-7337a444deea"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 54, 1, 225, DateTimeKind.Local).AddTicks(2189));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("c363880d-eabe-48ba-9795-757db1bf3e05"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 54, 1, 225, DateTimeKind.Local).AddTicks(2178));
        }
    }
}
