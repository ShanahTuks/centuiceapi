﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class AddedEmployeeGroup7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "EmergencyContacts",
                keyColumn: "EmergencyContactID",
                keyValue: new Guid("19e79478-c4a3-494a-a206-33d4c80fb2bb"));

            migrationBuilder.DeleteData(
                table: "EmergencyContacts",
                keyColumn: "EmergencyContactID",
                keyValue: new Guid("48adbfa9-be84-4a46-8c07-a859730c35c5"));

            migrationBuilder.DeleteData(
                table: "EmergencyContacts",
                keyColumn: "EmergencyContactID",
                keyValue: new Guid("4a0bdbc4-75b0-4385-83d6-a5d6e9050b5b"));

            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "AddressID", "AddressTypeID", "CityName", "ComplexName", "CountryName", "PostalCode", "Province", "StreetName", "StreetNumber", "SuburbName", "UnitNumber", "isActive" },
                values: new object[,]
                {
                    { new Guid("a359e0dd-1802-4a41-8050-d3f48cbabb75"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("de25e5d8-fe02-4dc1-862d-c1dbfa7e8eac"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("e3761f3f-c771-4c4f-8805-583742ad6996"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Invoice", 4, "Lyttleton", 0, true }
                });

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("46e26f11-c2ac-4409-b119-dbacc9c39ee4"),
                column: "SystemPassword",
                value: "9b8fd319798911f21208b8e5598ccfec3396138ea7b10086d719ca8e4491ad0a");

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("48adbfa9-be84-4a46-8c07-a859730c35c5"),
                column: "SystemPassword",
                value: "0e2875f6e1aedaea87518198737a738ce58229be76bce550687f19439574a017");

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("b07dee5d-9b83-426b-a1c0-a89e9ff39bfd"),
                column: "SystemPassword",
                value: "d4d4aa5c866677e8a6fb7e1d015f7463ae699708ea7dc576d1526434ac9ff15c");

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "EmployeeID", "AccountNumber", "AccountTypeID", "BankID", "BranchCode", "CellNumber", "DateOfBirth", "Email", "EmployeeQualificationID", "EmployeeStatusID", "EmployeeTitleID", "EmployeeTypeID", "FingerprintAccountNumber", "FirstName", "HomeNumber", "IDNumber", "Initials", "InstitutionName", "KnownAsName", "OTP", "PassportCountry", "PassportNumber", "Surname", "SystemPassword", "SystemUsername", "TableStatusID", "TaxNumber", "WorkNumber", "YearCompleted", "isLoggedIn" },
                values: new object[,]
                {
                    { new Guid("5b5ea95c-d324-44ef-9ece-a20e991d31f1"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Driver7@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("567b6183-fe92-44f0-8536-d121b7ad8e4f"), null, "Driver7", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Driver7", null, "N/A", "1231231312", "Driver7", "200cc9f9021201318ddb2b31b4f989ee0b3725d4dd4e896eeeb86e6ce212c970", "Driver7", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("675c1342-2c24-4c4c-bf05-378ce4e5e9c5"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Assistant7@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("07130dec-027e-4f00-a087-4d1dc7ac032a"), null, "Assistant7", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Assistant7", null, "N/A", "1231231312", "Assistant7", "eae3368e4c18f417bd760df408e95c787582f1a580a38f4ab08821affca0ff82", "Assistant7", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("c152a7ba-be20-4299-88b2-3196bd37e697"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Supervisor7@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("c525a7cc-a44c-4d62-a06a-1f690afcb19a"), null, "Supervisor7", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Supervisor7", null, "N/A", "1231231312", "Supervisor7", "a8de72f282d2c73e47b2a625ccb5f5a2a9342cf782ea9e3b910ef04629eaf7b3", "Supervisor7", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false }
                });

            migrationBuilder.InsertData(
                table: "EmergencyContacts",
                columns: new[] { "EmergencyContactID", "ContactPerson", "ContactTelephoneNumber", "EmployeeID", "RelationshipToEmployee" },
                values: new object[,]
                {
                    { new Guid("0134570a-89dc-4b4d-ab49-c2d4ef0bdf05"), "John Mulaney", "0814169470", new Guid("5b5ea95c-d324-44ef-9ece-a20e991d31f1"), "Friend" },
                    { new Guid("5a62fb32-cb64-4fe9-ae2f-1dfdae48cd85"), "John Mulaney", "0814169470", new Guid("675c1342-2c24-4c4c-bf05-378ce4e5e9c5"), "Friend" },
                    { new Guid("5a93a8f8-4541-4ee0-888c-bcc431aaa89e"), "John Mulaney", "0814169470", new Guid("c152a7ba-be20-4299-88b2-3196bd37e697"), "Friend" }
                });

            migrationBuilder.InsertData(
                table: "EmployeeAddresses",
                columns: new[] { "AddressID", "EmployeeID" },
                values: new object[,]
                {
                    { new Guid("a359e0dd-1802-4a41-8050-d3f48cbabb75"), new Guid("5b5ea95c-d324-44ef-9ece-a20e991d31f1") },
                    { new Guid("de25e5d8-fe02-4dc1-862d-c1dbfa7e8eac"), new Guid("675c1342-2c24-4c4c-bf05-378ce4e5e9c5") },
                    { new Guid("e3761f3f-c771-4c4f-8805-583742ad6996"), new Guid("c152a7ba-be20-4299-88b2-3196bd37e697") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "EmergencyContacts",
                keyColumn: "EmergencyContactID",
                keyValue: new Guid("0134570a-89dc-4b4d-ab49-c2d4ef0bdf05"));

            migrationBuilder.DeleteData(
                table: "EmergencyContacts",
                keyColumn: "EmergencyContactID",
                keyValue: new Guid("5a62fb32-cb64-4fe9-ae2f-1dfdae48cd85"));

            migrationBuilder.DeleteData(
                table: "EmergencyContacts",
                keyColumn: "EmergencyContactID",
                keyValue: new Guid("5a93a8f8-4541-4ee0-888c-bcc431aaa89e"));

            migrationBuilder.DeleteData(
                table: "EmployeeAddresses",
                keyColumns: new[] { "AddressID", "EmployeeID" },
                keyValues: new object[] { new Guid("a359e0dd-1802-4a41-8050-d3f48cbabb75"), new Guid("5b5ea95c-d324-44ef-9ece-a20e991d31f1") });

            migrationBuilder.DeleteData(
                table: "EmployeeAddresses",
                keyColumns: new[] { "AddressID", "EmployeeID" },
                keyValues: new object[] { new Guid("de25e5d8-fe02-4dc1-862d-c1dbfa7e8eac"), new Guid("675c1342-2c24-4c4c-bf05-378ce4e5e9c5") });

            migrationBuilder.DeleteData(
                table: "EmployeeAddresses",
                keyColumns: new[] { "AddressID", "EmployeeID" },
                keyValues: new object[] { new Guid("e3761f3f-c771-4c4f-8805-583742ad6996"), new Guid("c152a7ba-be20-4299-88b2-3196bd37e697") });

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "AddressID",
                keyValue: new Guid("a359e0dd-1802-4a41-8050-d3f48cbabb75"));

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "AddressID",
                keyValue: new Guid("de25e5d8-fe02-4dc1-862d-c1dbfa7e8eac"));

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "AddressID",
                keyValue: new Guid("e3761f3f-c771-4c4f-8805-583742ad6996"));

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("5b5ea95c-d324-44ef-9ece-a20e991d31f1"));

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("675c1342-2c24-4c4c-bf05-378ce4e5e9c5"));

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("c152a7ba-be20-4299-88b2-3196bd37e697"));

            migrationBuilder.InsertData(
                table: "EmergencyContacts",
                columns: new[] { "EmergencyContactID", "ContactPerson", "ContactTelephoneNumber", "EmployeeID", "RelationshipToEmployee" },
                values: new object[,]
                {
                    { new Guid("19e79478-c4a3-494a-a206-33d4c80fb2bb"), "John Mulaney", "0814169470", new Guid("46e26f11-c2ac-4409-b119-dbacc9c39ee4"), "Friend" },
                    { new Guid("4a0bdbc4-75b0-4385-83d6-a5d6e9050b5b"), "John Mulaney", "0814169470", new Guid("b07dee5d-9b83-426b-a1c0-a89e9ff39bfd"), "Friend" },
                    { new Guid("48adbfa9-be84-4a46-8c07-a859730c35c5"), "John Mulaney", "0814169470", new Guid("40525829-4dd0-4352-a149-cea6a86ec01f"), "Friend" }
                });

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("46e26f11-c2ac-4409-b119-dbacc9c39ee4"),
                column: "SystemPassword",
                value: "771542e5ae8a9b877d7af18e4bb7f7fdd027ea03772ccc902e1efff6140c5596");

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("48adbfa9-be84-4a46-8c07-a859730c35c5"),
                column: "SystemPassword",
                value: "0eac94377eac038a7b6e9d5abb7bcb0e208e2e51fa7b3278c2b8557963d6b2f6");

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("b07dee5d-9b83-426b-a1c0-a89e9ff39bfd"),
                column: "SystemPassword",
                value: "d960a1c8d09d05a11dde6961448fd196974e042666af538278dd41d5b7c8f0a1");
        }
    }
}
