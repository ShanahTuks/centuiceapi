﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class EditedTripsheets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "DeliveryTripSheets",
                keyColumn: "DeliveryTripSheetID",
                keyValue: new Guid("371fefc3-9a45-4340-92ee-e2e9b5ddbd16"),
                column: "Alias",
                value: "TripSheet8");

            migrationBuilder.UpdateData(
                table: "DeliveryTripSheets",
                keyColumn: "DeliveryTripSheetID",
                keyValue: new Guid("510ae3b6-37f5-44af-886e-5a4d2117f202"),
                column: "Alias",
                value: "TripSheet7");

            migrationBuilder.UpdateData(
                table: "DeliveryTripSheets",
                keyColumn: "DeliveryTripSheetID",
                keyValue: new Guid("52238d97-9e77-4107-b8f7-6739d32be3e0"),
                column: "Alias",
                value: "TripSheet6");

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("30fc6523-661c-4479-8539-049d8cca6cba"),
                column: "CellNumber",
                value: "0815378764");

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("f11765a0-58d2-42e5-9c5c-34022016e8c6"),
                column: "CellNumber",
                value: "0733714164");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "DeliveryTripSheets",
                keyColumn: "DeliveryTripSheetID",
                keyValue: new Guid("371fefc3-9a45-4340-92ee-e2e9b5ddbd16"),
                column: "Alias",
                value: "TripSheet5");

            migrationBuilder.UpdateData(
                table: "DeliveryTripSheets",
                keyColumn: "DeliveryTripSheetID",
                keyValue: new Guid("510ae3b6-37f5-44af-886e-5a4d2117f202"),
                column: "Alias",
                value: "TripSheet5");

            migrationBuilder.UpdateData(
                table: "DeliveryTripSheets",
                keyColumn: "DeliveryTripSheetID",
                keyValue: new Guid("52238d97-9e77-4107-b8f7-6739d32be3e0"),
                column: "Alias",
                value: "TripSheet5");

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("30fc6523-661c-4479-8539-049d8cca6cba"),
                column: "CellNumber",
                value: "0815377864");

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("f11765a0-58d2-42e5-9c5c-34022016e8c6"),
                column: "CellNumber",
                value: "0761111111");
        }
    }
}
