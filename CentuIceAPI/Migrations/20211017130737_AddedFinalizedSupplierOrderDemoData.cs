﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class AddedFinalizedSupplierOrderDemoData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "SupplierOrderStatuses",
                columns: new[] { "SupplierOrderStatusID", "Description" },
                values: new object[] { new Guid("1793c850-80fe-429f-a2c8-eb56dac0d797"), "Finalized" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SupplierOrderStatuses",
                keyColumn: "SupplierOrderStatusID",
                keyValue: new Guid("1793c850-80fe-429f-a2c8-eb56dac0d797"));
        }
    }
}
