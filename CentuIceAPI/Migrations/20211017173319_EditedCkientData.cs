﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class EditedCkientData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "VehicleStatuses",
                keyColumn: "VehicleStatusID",
                keyValue: new Guid("c9359487-3660-4a8b-8919-4935e3a75f36"));

            migrationBuilder.UpdateData(
                table: "Clients",
                keyColumn: "ClientID",
                keyValue: new Guid("10e21cc0-001c-438b-bb2f-445e446688dd"),
                column: "ClientName",
                value: "Grootvlei Drankwinkel");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Clients",
                keyColumn: "ClientID",
                keyValue: new Guid("10e21cc0-001c-438b-bb2f-445e446688dd"),
                column: "ClientName",
                value: "Grootveli Drankwinkel");

            migrationBuilder.InsertData(
                table: "VehicleStatuses",
                columns: new[] { "VehicleStatusID", "Description" },
                values: new object[] { new Guid("c9359487-3660-4a8b-8919-4935e3a75f36"), "Written-Off" });
        }
    }
}
