﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class SupplierContactData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("10ebbd0c-5079-4efe-b583-df781707081e"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("1203d56b-7c8d-4268-8be8-89993fcce5a6"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("1e9e3a5f-8354-42e8-943b-cfe559ce7885"),
                column: "EmailAddress",
                value: "admin@gmail.com");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("20116db1-13c4-4b22-b4c9-7570a6e057bb"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("20bf139c-34c3-4a65-b925-7835089f24f2"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("26e63540-1f4a-421f-ad80-520114fcef35"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("297a48e1-30b6-408b-b7a3-9c486cfc9a5e"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("2d161ca0-fe36-42a0-b568-fa8676c0b6bc"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("3ad2af3e-ecac-4bf2-94b5-ad5bcddcc85e"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("3c8190ff-eed4-4ea9-85dd-7f230a55a258"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("3e3b327e-5041-43b8-8bda-8860fa4885ea"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("3ee3a783-5c81-4c03-87e3-75b3e43fa377"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("44a09b6f-e9e0-450c-b185-8d5090578794"),
                column: "EmailAddress",
                value: "admin@gmail.com");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("4d9e46e5-b5ab-4493-aa93-e8462983d8ef"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("52d391d2-9893-48c2-8f58-69fde2876a08"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("59d9a399-bca8-42c5-b13e-201657bcc37e"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("60fc9f8a-2aea-4c68-9ad5-89ab1549f799"),
                column: "EmailAddress",
                value: "admin@gmail.com");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("6762674e-fbc5-461c-b31a-6e6936a637ed"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("6b41097b-ed96-40ce-a89c-ec0c58aee382"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("6c2973fb-3359-4008-9411-913bd6474153"),
                column: "EmailAddress",
                value: "admin@gmail.com");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("718061e2-2f36-4559-8420-f8f8b6ca62c4"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("7da76e0f-cef2-4a21-b4d9-a72a566a76ac"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("7dc365f7-c7ae-4101-a608-942b04c28539"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("914c0f86-1e87-4ec6-b158-96f8fc055f7b"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("9ac8a94e-a10b-418e-a2ea-38b0b2a038e5"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("a0af06c0-fc41-47fb-a6cc-74c1d3d32756"),
                column: "EmailAddress",
                value: "admin@gmail.com");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("b14c26c6-e7d9-4acb-8d08-4c5f44a88849"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("b53bb5f4-2aae-46d9-aef4-612f11bfdd13"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("bbb8cb75-a06c-4fec-8bfd-d7d8a5e7c4ba"),
                column: "EmailAddress",
                value: "admin@gmail.com");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("cf9067fd-02a3-4f55-83ed-b97c5b4fecc3"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "steven@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("d1379325-3999-4653-9aaa-a90c277493d2"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("d7123644-674b-4b2c-90e2-b71bdd91a2e7"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("dbf9314a-1a55-404d-a587-4f06fb59b6bb"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("e1c091f2-7981-4a02-8d8c-5091fa698345"),
                column: "EmailAddress",
                value: "admin@gmail.com");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("e85a72a6-d4bb-4236-b4ce-358ba7fc3497"),
                column: "EmailAddress",
                value: "admin@gmail.com");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("eafd605f-a258-4d6b-8ce1-872a4d2109f0"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("efc38bba-5231-4242-8489-ccd6b5eb1068"),
                column: "EmailAddress",
                value: "admin@gmail.com");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("fa89b7e0-5242-4cab-912d-11373a453f48"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "admin@gmail.com", "0123456789" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("10ebbd0c-5079-4efe-b583-df781707081e"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("1203d56b-7c8d-4268-8be8-89993fcce5a6"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("1e9e3a5f-8354-42e8-943b-cfe559ce7885"),
                column: "EmailAddress",
                value: "");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("20116db1-13c4-4b22-b4c9-7570a6e057bb"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("20bf139c-34c3-4a65-b925-7835089f24f2"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("26e63540-1f4a-421f-ad80-520114fcef35"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("297a48e1-30b6-408b-b7a3-9c486cfc9a5e"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("2d161ca0-fe36-42a0-b568-fa8676c0b6bc"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("3ad2af3e-ecac-4bf2-94b5-ad5bcddcc85e"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("3c8190ff-eed4-4ea9-85dd-7f230a55a258"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("3e3b327e-5041-43b8-8bda-8860fa4885ea"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("3ee3a783-5c81-4c03-87e3-75b3e43fa377"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("44a09b6f-e9e0-450c-b185-8d5090578794"),
                column: "EmailAddress",
                value: "");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("4d9e46e5-b5ab-4493-aa93-e8462983d8ef"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("52d391d2-9893-48c2-8f58-69fde2876a08"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("59d9a399-bca8-42c5-b13e-201657bcc37e"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("60fc9f8a-2aea-4c68-9ad5-89ab1549f799"),
                column: "EmailAddress",
                value: "");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("6762674e-fbc5-461c-b31a-6e6936a637ed"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("6b41097b-ed96-40ce-a89c-ec0c58aee382"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("6c2973fb-3359-4008-9411-913bd6474153"),
                column: "EmailAddress",
                value: "");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("718061e2-2f36-4559-8420-f8f8b6ca62c4"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("7da76e0f-cef2-4a21-b4d9-a72a566a76ac"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("7dc365f7-c7ae-4101-a608-942b04c28539"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("914c0f86-1e87-4ec6-b158-96f8fc055f7b"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("9ac8a94e-a10b-418e-a2ea-38b0b2a038e5"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("a0af06c0-fc41-47fb-a6cc-74c1d3d32756"),
                column: "EmailAddress",
                value: "");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("b14c26c6-e7d9-4acb-8d08-4c5f44a88849"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("b53bb5f4-2aae-46d9-aef4-612f11bfdd13"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("bbb8cb75-a06c-4fec-8bfd-d7d8a5e7c4ba"),
                column: "EmailAddress",
                value: "");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("cf9067fd-02a3-4f55-83ed-b97c5b4fecc3"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("d1379325-3999-4653-9aaa-a90c277493d2"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("d7123644-674b-4b2c-90e2-b71bdd91a2e7"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("dbf9314a-1a55-404d-a587-4f06fb59b6bb"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("e1c091f2-7981-4a02-8d8c-5091fa698345"),
                column: "EmailAddress",
                value: "");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("e85a72a6-d4bb-4236-b4ce-358ba7fc3497"),
                column: "EmailAddress",
                value: "");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("eafd605f-a258-4d6b-8ce1-872a4d2109f0"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("efc38bba-5231-4242-8489-ccd6b5eb1068"),
                column: "EmailAddress",
                value: "");

            migrationBuilder.UpdateData(
                table: "SupplierContactPeople",
                keyColumn: "SupplierContactPersonID",
                keyValue: new Guid("fa89b7e0-5242-4cab-912d-11373a453f48"),
                columns: new[] { "EmailAddress", "PhoneNumber" },
                values: new object[] { "", "" });
        }
    }
}
