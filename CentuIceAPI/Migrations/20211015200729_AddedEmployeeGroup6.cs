﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class AddedEmployeeGroup6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "AddressID", "AddressTypeID", "CityName", "ComplexName", "CountryName", "PostalCode", "Province", "StreetName", "StreetNumber", "SuburbName", "UnitNumber", "isActive" },
                values: new object[,]
                {
                    { new Guid("8fe720af-306a-49f8-9216-2109b297c4ea"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("bf3efc94-9d1d-4173-8ff6-2dad74c85919"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Test4", 4, "Lyttleton", 0, true },
                    { new Guid("dbffb135-9d1f-4075-96b4-9076a523fbf9"), new Guid("42cb8c8b-3098-4f21-b797-a15166d0964b"), "Pretoria", null, "South Africa", "0124", "Gauteng", "Invoice", 4, "Lyttleton", 0, true }
                });

            migrationBuilder.InsertData(
                table: "EmergencyContacts",
                columns: new[] { "EmergencyContactID", "ContactPerson", "ContactTelephoneNumber", "EmployeeID", "RelationshipToEmployee" },
                values: new object[] { new Guid("48adbfa9-be84-4a46-8c07-a859730c35c5"), "John Mulaney", "0814169470", new Guid("40525829-4dd0-4352-a149-cea6a86ec01f"), "Friend" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "EmployeeID", "AccountNumber", "AccountTypeID", "BankID", "BranchCode", "CellNumber", "DateOfBirth", "Email", "EmployeeQualificationID", "EmployeeStatusID", "EmployeeTitleID", "EmployeeTypeID", "FingerprintAccountNumber", "FirstName", "HomeNumber", "IDNumber", "Initials", "InstitutionName", "KnownAsName", "OTP", "PassportCountry", "PassportNumber", "Surname", "SystemPassword", "SystemUsername", "TableStatusID", "TaxNumber", "WorkNumber", "YearCompleted", "isLoggedIn" },
                values: new object[,]
                {
                    { new Guid("46e26f11-c2ac-4409-b119-dbacc9c39ee4"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Driver6@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("567b6183-fe92-44f0-8536-d121b7ad8e4f"), null, "Driver6", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Driver6", null, "N/A", "1231231312", "Driver6", "771542e5ae8a9b877d7af18e4bb7f7fdd027ea03772ccc902e1efff6140c5596", "Driver6", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("b07dee5d-9b83-426b-a1c0-a89e9ff39bfd"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Assistant5@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("07130dec-027e-4f00-a087-4d1dc7ac032a"), null, "Assistant6", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Assistant6", null, "N/A", "1231231312", "Assistant6", "d960a1c8d09d05a11dde6961448fd196974e042666af538278dd41d5b7c8f0a1", "Assistant6", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false },
                    { new Guid("48adbfa9-be84-4a46-8c07-a859730c35c5"), "112221221312312313", new Guid("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"), new Guid("02b0abaf-4818-4251-b220-d9400f29aedc"), "01231", "0761111111", new DateTime(1922, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Supervisor6@centurionice.co.za", new Guid("73976ce7-4f02-4274-a858-acfd8623e03c"), new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"), new Guid("250753ee-c818-4a50-b713-41d9c8b09002"), new Guid("c525a7cc-a44c-4d62-a06a-1f690afcb19a"), null, "Supervisor6", "0123456789", "1111111111111", "OO", "University Of Pretoria", "Supervisor6", null, "N/A", "1231231312", "Supervisor6", "0eac94377eac038a7b6e9d5abb7bcb0e208e2e51fa7b3278c2b8557963d6b2f6", "Supervisor6", new Guid("c70f248b-1eb9-4f0a-a5be-f95ed556789e"), "S1231312321", "0123456789", new DateTime(1950, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false }
                });

            migrationBuilder.InsertData(
                table: "EmergencyContacts",
                columns: new[] { "EmergencyContactID", "ContactPerson", "ContactTelephoneNumber", "EmployeeID", "RelationshipToEmployee" },
                values: new object[,]
                {
                    { new Guid("19e79478-c4a3-494a-a206-33d4c80fb2bb"), "John Mulaney", "0814169470", new Guid("46e26f11-c2ac-4409-b119-dbacc9c39ee4"), "Friend" },
                    { new Guid("4a0bdbc4-75b0-4385-83d6-a5d6e9050b5b"), "John Mulaney", "0814169470", new Guid("b07dee5d-9b83-426b-a1c0-a89e9ff39bfd"), "Friend" }
                });

            migrationBuilder.InsertData(
                table: "EmployeeAddresses",
                columns: new[] { "AddressID", "EmployeeID" },
                values: new object[,]
                {
                    { new Guid("8fe720af-306a-49f8-9216-2109b297c4ea"), new Guid("46e26f11-c2ac-4409-b119-dbacc9c39ee4") },
                    { new Guid("bf3efc94-9d1d-4173-8ff6-2dad74c85919"), new Guid("b07dee5d-9b83-426b-a1c0-a89e9ff39bfd") },
                    { new Guid("dbffb135-9d1f-4075-96b4-9076a523fbf9"), new Guid("48adbfa9-be84-4a46-8c07-a859730c35c5") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "EmergencyContacts",
                keyColumn: "EmergencyContactID",
                keyValue: new Guid("19e79478-c4a3-494a-a206-33d4c80fb2bb"));

            migrationBuilder.DeleteData(
                table: "EmergencyContacts",
                keyColumn: "EmergencyContactID",
                keyValue: new Guid("48adbfa9-be84-4a46-8c07-a859730c35c5"));

            migrationBuilder.DeleteData(
                table: "EmergencyContacts",
                keyColumn: "EmergencyContactID",
                keyValue: new Guid("4a0bdbc4-75b0-4385-83d6-a5d6e9050b5b"));

            migrationBuilder.DeleteData(
                table: "EmployeeAddresses",
                keyColumns: new[] { "AddressID", "EmployeeID" },
                keyValues: new object[] { new Guid("8fe720af-306a-49f8-9216-2109b297c4ea"), new Guid("46e26f11-c2ac-4409-b119-dbacc9c39ee4") });

            migrationBuilder.DeleteData(
                table: "EmployeeAddresses",
                keyColumns: new[] { "AddressID", "EmployeeID" },
                keyValues: new object[] { new Guid("bf3efc94-9d1d-4173-8ff6-2dad74c85919"), new Guid("b07dee5d-9b83-426b-a1c0-a89e9ff39bfd") });

            migrationBuilder.DeleteData(
                table: "EmployeeAddresses",
                keyColumns: new[] { "AddressID", "EmployeeID" },
                keyValues: new object[] { new Guid("dbffb135-9d1f-4075-96b4-9076a523fbf9"), new Guid("48adbfa9-be84-4a46-8c07-a859730c35c5") });

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "AddressID",
                keyValue: new Guid("8fe720af-306a-49f8-9216-2109b297c4ea"));

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "AddressID",
                keyValue: new Guid("bf3efc94-9d1d-4173-8ff6-2dad74c85919"));

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "AddressID",
                keyValue: new Guid("dbffb135-9d1f-4075-96b4-9076a523fbf9"));

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("46e26f11-c2ac-4409-b119-dbacc9c39ee4"));

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("48adbfa9-be84-4a46-8c07-a859730c35c5"));

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("b07dee5d-9b83-426b-a1c0-a89e9ff39bfd"));
        }
    }
}
