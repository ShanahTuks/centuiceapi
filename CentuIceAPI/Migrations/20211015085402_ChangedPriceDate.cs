﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class ChangedPriceDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("60339a11-fb71-445a-a88f-7245660481ba"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 54, 1, 224, DateTimeKind.Local).AddTicks(9473));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("62d1c758-5b5b-4b78-b9d8-e9ef136d5b01"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 54, 1, 225, DateTimeKind.Local).AddTicks(2200));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("742a9949-4ab8-4bf8-831d-5f8cd8b329d6"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 54, 1, 225, DateTimeKind.Local).AddTicks(2183));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("7c2579c7-1076-4d1c-9ea8-746195540ad7"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 54, 1, 225, DateTimeKind.Local).AddTicks(2159));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("acc84feb-6da6-46cf-8cc0-cffcb8781621"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 54, 1, 225, DateTimeKind.Local).AddTicks(2194));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("afbc7b15-d66a-4bca-a155-7337a444deea"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 54, 1, 225, DateTimeKind.Local).AddTicks(2189));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("c363880d-eabe-48ba-9795-757db1bf3e05"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 54, 1, 225, DateTimeKind.Local).AddTicks(2178));

            migrationBuilder.UpdateData(
                table: "ProductPrices",
                keyColumn: "ProductPriceID",
                keyValue: new Guid("199baac9-1afb-48b0-be10-e0a363db3926"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ProductPrices",
                keyColumn: "ProductPriceID",
                keyValue: new Guid("21b6bc5f-9c2f-4646-9491-4461e7ac156c"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ProductPrices",
                keyColumn: "ProductPriceID",
                keyValue: new Guid("64ec33d1-8bbd-4777-a6b0-716d14ef9f5d"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ProductPrices",
                keyColumn: "ProductPriceID",
                keyValue: new Guid("67a43150-2aac-47c5-ad0f-289af32ba82d"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ProductPrices",
                keyColumn: "ProductPriceID",
                keyValue: new Guid("82cffdfb-4ca7-4db5-ba1d-32df7e9c8f46"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ProductPrices",
                keyColumn: "ProductPriceID",
                keyValue: new Guid("9de02532-d97d-4840-a04d-87aeeef56d3e"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ProductPrices",
                keyColumn: "ProductPriceID",
                keyValue: new Guid("aa27d0ae-7b02-4e0e-a389-02fc219af827"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("60339a11-fb71-445a-a88f-7245660481ba"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 48, 12, 889, DateTimeKind.Local).AddTicks(6709));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("62d1c758-5b5b-4b78-b9d8-e9ef136d5b01"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 48, 12, 889, DateTimeKind.Local).AddTicks(9634));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("742a9949-4ab8-4bf8-831d-5f8cd8b329d6"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 48, 12, 889, DateTimeKind.Local).AddTicks(9617));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("7c2579c7-1076-4d1c-9ea8-746195540ad7"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 48, 12, 889, DateTimeKind.Local).AddTicks(9591));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("acc84feb-6da6-46cf-8cc0-cffcb8781621"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 48, 12, 889, DateTimeKind.Local).AddTicks(9628));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("afbc7b15-d66a-4bca-a155-7337a444deea"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 48, 12, 889, DateTimeKind.Local).AddTicks(9622));

            migrationBuilder.UpdateData(
                table: "InventoryPrices",
                keyColumn: "InventoryPriceID",
                keyValue: new Guid("c363880d-eabe-48ba-9795-757db1bf3e05"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 48, 12, 889, DateTimeKind.Local).AddTicks(9611));

            migrationBuilder.UpdateData(
                table: "ProductPrices",
                keyColumn: "ProductPriceID",
                keyValue: new Guid("199baac9-1afb-48b0-be10-e0a363db3926"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 48, 12, 900, DateTimeKind.Local).AddTicks(2594));

            migrationBuilder.UpdateData(
                table: "ProductPrices",
                keyColumn: "ProductPriceID",
                keyValue: new Guid("21b6bc5f-9c2f-4646-9491-4461e7ac156c"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 48, 12, 900, DateTimeKind.Local).AddTicks(2602));

            migrationBuilder.UpdateData(
                table: "ProductPrices",
                keyColumn: "ProductPriceID",
                keyValue: new Guid("64ec33d1-8bbd-4777-a6b0-716d14ef9f5d"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 48, 12, 900, DateTimeKind.Local).AddTicks(2576));

            migrationBuilder.UpdateData(
                table: "ProductPrices",
                keyColumn: "ProductPriceID",
                keyValue: new Guid("67a43150-2aac-47c5-ad0f-289af32ba82d"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 48, 12, 900, DateTimeKind.Local).AddTicks(2584));

            migrationBuilder.UpdateData(
                table: "ProductPrices",
                keyColumn: "ProductPriceID",
                keyValue: new Guid("82cffdfb-4ca7-4db5-ba1d-32df7e9c8f46"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 48, 12, 900, DateTimeKind.Local).AddTicks(2145));

            migrationBuilder.UpdateData(
                table: "ProductPrices",
                keyColumn: "ProductPriceID",
                keyValue: new Guid("9de02532-d97d-4840-a04d-87aeeef56d3e"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 48, 12, 900, DateTimeKind.Local).AddTicks(2563));

            migrationBuilder.UpdateData(
                table: "ProductPrices",
                keyColumn: "ProductPriceID",
                keyValue: new Guid("aa27d0ae-7b02-4e0e-a389-02fc219af827"),
                column: "Date",
                value: new DateTime(2021, 10, 15, 10, 48, 12, 900, DateTimeKind.Local).AddTicks(2611));
        }
    }
}
