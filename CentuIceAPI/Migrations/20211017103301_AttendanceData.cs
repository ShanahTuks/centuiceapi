﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class AttendanceData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "DailyAttendances",
                columns: new[] { "DailyAttendanceID", "CheckInTime", "CheckOutTime", "Date", "EmployeeID" },
                values: new object[] { new Guid("956b62ed-169a-43a3-9274-4f06220f1a75"), new DateTime(2021, 10, 1, 8, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 10, 1, 17, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("5b5ea95c-d324-44ef-9ece-a20e991d31f1") });

            migrationBuilder.InsertData(
                table: "DailyAttendances",
                columns: new[] { "DailyAttendanceID", "CheckInTime", "CheckOutTime", "Date", "EmployeeID" },
                values: new object[] { new Guid("6c52ac12-156e-48b2-9051-94b9912fd106"), new DateTime(2021, 10, 1, 8, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 10, 1, 17, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("675c1342-2c24-4c4c-bf05-378ce4e5e9c5") });

            migrationBuilder.InsertData(
                table: "DailyAttendances",
                columns: new[] { "DailyAttendanceID", "CheckInTime", "CheckOutTime", "Date", "EmployeeID" },
                values: new object[] { new Guid("8087ce62-e84b-4fa4-ae24-dfa767fc793f"), new DateTime(2021, 10, 1, 8, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 10, 1, 17, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("48adbfa9-be84-4a46-8c07-a859730c35c5") });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "DailyAttendances",
                keyColumn: "DailyAttendanceID",
                keyValue: new Guid("6c52ac12-156e-48b2-9051-94b9912fd106"));

            migrationBuilder.DeleteData(
                table: "DailyAttendances",
                keyColumn: "DailyAttendanceID",
                keyValue: new Guid("8087ce62-e84b-4fa4-ae24-dfa767fc793f"));

            migrationBuilder.DeleteData(
                table: "DailyAttendances",
                keyColumn: "DailyAttendanceID",
                keyValue: new Guid("956b62ed-169a-43a3-9274-4f06220f1a75"));
        }
    }
}
