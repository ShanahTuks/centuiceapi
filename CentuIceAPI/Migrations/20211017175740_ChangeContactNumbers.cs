﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class ChangeContactNumbers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("1f4f7372-5fa4-43dc-a91f-8df14bd8943a"),
                column: "PhoneNumber",
                value: "0815378764");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("bce06eb4-5efb-4ed4-b020-6ccd9332de4a"),
                column: "PhoneNumber",
                value: "0815378764");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("1f4f7372-5fa4-43dc-a91f-8df14bd8943a"),
                column: "PhoneNumber",
                value: "0815377864");

            migrationBuilder.UpdateData(
                table: "ClientContactPeople",
                keyColumn: "ClientContactPersonID",
                keyValue: new Guid("bce06eb4-5efb-4ed4-b020-6ccd9332de4a"),
                column: "PhoneNumber",
                value: "0815377864");
        }
    }
}
