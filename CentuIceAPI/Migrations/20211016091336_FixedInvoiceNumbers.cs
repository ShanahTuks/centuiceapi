﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class FixedInvoiceNumbers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AuthNotificationTypes",
                columns: new[] { "AuthNotificationTypeID", "Description" },
                values: new object[] { new Guid("d0721f9c-011b-43eb-b35f-6a0b5e8b2fbc"), "CancelSale" });

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("30fc6523-661c-4479-8539-049d8cca6cba"),
                columns: new[] { "FirstName", "KnownAsName", "Surname" },
                values: new object[] { "NaomiAdmin", "NaomiAdmin", "NaomiAdmin" });

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("1f68d9ba-bf99-46cb-a5a8-4a6031a434f5"),
                column: "InvoiceNumber",
                value: "INV00000005");

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("2e17cf49-6504-4d05-983f-8128a4a0c459"),
                columns: new[] { "InvoiceDate", "InvoiceNumber" },
                values: new object[] { new DateTime(2021, 8, 30, 15, 31, 37, 0, DateTimeKind.Unspecified), "INV00000009" });

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("4bdbaa80-9f10-4e36-956c-7ab882c97d00"),
                column: "InvoiceNumber",
                value: "INV00000007");

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("5d84a526-2e46-4f9f-ba22-e9e29e0b73ae"),
                column: "InvoiceNumber",
                value: "INV00000004");

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("6d619343-bb23-401f-b40f-3bf323a8b706"),
                column: "InvoiceNumber",
                value: "INV00000003");

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("7e4dcd3b-a893-490f-a469-ca0ea53d3bc7"),
                columns: new[] { "InvoiceDate", "InvoiceNumber" },
                values: new object[] { new DateTime(2021, 8, 30, 15, 31, 36, 0, DateTimeKind.Unspecified), "INV00000008" });

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("9eca188d-e389-473d-b46e-ca9a03b27cf3"),
                column: "InvoiceNumber",
                value: "INV00000001");

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("d74516b2-a88d-485b-b221-adf65e094fe4"),
                column: "InvoiceNumber",
                value: "INV00000002");

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("d8286dfe-add6-4834-8f7d-9b2bd96ae569"),
                column: "InvoiceNumber",
                value: "INV00000006");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AuthNotificationTypes",
                keyColumn: "AuthNotificationTypeID",
                keyValue: new Guid("d0721f9c-011b-43eb-b35f-6a0b5e8b2fbc"));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("30fc6523-661c-4479-8539-049d8cca6cba"),
                columns: new[] { "FirstName", "KnownAsName", "Surname" },
                values: new object[] { "Admin", "Admin", "Admin" });

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("1f68d9ba-bf99-46cb-a5a8-4a6031a434f5"),
                column: "InvoiceNumber",
                value: "INV00000008");

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("2e17cf49-6504-4d05-983f-8128a4a0c459"),
                columns: new[] { "InvoiceDate", "InvoiceNumber" },
                values: new object[] { new DateTime(2021, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), "INV00000001" });

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("4bdbaa80-9f10-4e36-956c-7ab882c97d00"),
                column: "InvoiceNumber",
                value: "INV00000003");

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("5d84a526-2e46-4f9f-ba22-e9e29e0b73ae"),
                column: "InvoiceNumber",
                value: "INV00000007");

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("6d619343-bb23-401f-b40f-3bf323a8b706"),
                column: "InvoiceNumber",
                value: "INV00000006");

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("7e4dcd3b-a893-490f-a469-ca0ea53d3bc7"),
                columns: new[] { "InvoiceDate", "InvoiceNumber" },
                values: new object[] { new DateTime(2021, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), "INV00000002" });

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("9eca188d-e389-473d-b46e-ca9a03b27cf3"),
                column: "InvoiceNumber",
                value: "INV00000004");

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("d74516b2-a88d-485b-b221-adf65e094fe4"),
                column: "InvoiceNumber",
                value: "INV00000005");

            migrationBuilder.UpdateData(
                table: "Invoices",
                keyColumn: "InvoiceID",
                keyValue: new Guid("d8286dfe-add6-4834-8f7d-9b2bd96ae569"),
                column: "InvoiceNumber",
                value: "INV00000009");
        }
    }
}
