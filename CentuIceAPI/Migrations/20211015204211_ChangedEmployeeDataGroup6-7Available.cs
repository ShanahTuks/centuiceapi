﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class ChangedEmployeeDataGroup67Available : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("46e26f11-c2ac-4409-b119-dbacc9c39ee4"),
                column: "EmployeeStatusID",
                value: new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("48adbfa9-be84-4a46-8c07-a859730c35c5"),
                column: "EmployeeStatusID",
                value: new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("5b5ea95c-d324-44ef-9ece-a20e991d31f1"),
                column: "EmployeeStatusID",
                value: new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("675c1342-2c24-4c4c-bf05-378ce4e5e9c5"),
                column: "EmployeeStatusID",
                value: new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("b07dee5d-9b83-426b-a1c0-a89e9ff39bfd"),
                column: "EmployeeStatusID",
                value: new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("c152a7ba-be20-4299-88b2-3196bd37e697"),
                column: "EmployeeStatusID",
                value: new Guid("9664ce89-cfac-4904-b5eb-7dda3a038b21"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("46e26f11-c2ac-4409-b119-dbacc9c39ee4"),
                column: "EmployeeStatusID",
                value: new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("48adbfa9-be84-4a46-8c07-a859730c35c5"),
                column: "EmployeeStatusID",
                value: new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("5b5ea95c-d324-44ef-9ece-a20e991d31f1"),
                column: "EmployeeStatusID",
                value: new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("675c1342-2c24-4c4c-bf05-378ce4e5e9c5"),
                column: "EmployeeStatusID",
                value: new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("b07dee5d-9b83-426b-a1c0-a89e9ff39bfd"),
                column: "EmployeeStatusID",
                value: new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeID",
                keyValue: new Guid("c152a7ba-be20-4299-88b2-3196bd37e697"),
                column: "EmployeeStatusID",
                value: new Guid("bc4de5bb-8761-43f9-a734-5998fa44985a"));
        }
    }
}
