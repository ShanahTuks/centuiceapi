﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CentuIceAPI.Migrations
{
    public partial class AddedMoreVehicleData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "VehicleID", "Capacity", "LastServicedMileage", "Last_Service_Date", "License_Expiry_Date", "Make", "Mileage", "Model", "Reg_Number", "Tracker_Number", "VehicleStatusID", "Year_Made" },
                values: new object[] { new Guid("de1f7a9e-b8be-4753-b500-c7f6faa28377"), 500, 0, new DateTime(2021, 4, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), new DateTime(2022, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), "Nissan", 100000, "Truckla", "CBA 142 NP", "TRK12345", new Guid("65563356-8382-4c0d-8e2c-f94c98cb786b"), new DateTime(2016, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "VehicleID", "Capacity", "LastServicedMileage", "Last_Service_Date", "License_Expiry_Date", "Make", "Mileage", "Model", "Reg_Number", "Tracker_Number", "VehicleStatusID", "Year_Made" },
                values: new object[] { new Guid("397a755a-852b-47a5-8ec8-1d32d488a588"), 500, 0, new DateTime(2021, 4, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), new DateTime(2022, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified), "MAN", 100000, "Truckornator", "VBG 785 CW", "TRK12345", new Guid("65563356-8382-4c0d-8e2c-f94c98cb786b"), new DateTime(2016, 8, 30, 15, 31, 35, 0, DateTimeKind.Unspecified) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "VehicleID",
                keyValue: new Guid("397a755a-852b-47a5-8ec8-1d32d488a588"));

            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "VehicleID",
                keyValue: new Guid("de1f7a9e-b8be-4753-b500-c7f6faa28377"));
        }
    }
}
