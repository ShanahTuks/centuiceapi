﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace CentuIceAPI.Data
{
    public class ProductPriceData : IEntityTypeConfiguration<ProductPrice>
    {
        public void Configure(EntityTypeBuilder<ProductPrice> builder)
        {
            builder.ToTable("ProductPrices");
            builder.Property(it => it.PriceID).IsRequired(true);

            _ = builder.HasData(
                //5kg Ice Blocks
                new ProductPrice
                {
                    ProductPriceID = Guid.Parse("82cffdfb-4ca7-4db5-ba1d-32df7e9c8f46"),
                    PriceID = Guid.Parse("582b1b38-0afd-40a8-9ccd-67663d221d67"),
                    ProductID = Guid.Parse("974e485b-d76f-4f4f-bde7-2b414435890f"),
                    Date = new DateTime(2021, 10, 15)
                },
                //2kg Ice Blocks
                new ProductPrice
                {
                    ProductPriceID = Guid.Parse("9de02532-d97d-4840-a04d-87aeeef56d3e"),
                    PriceID = Guid.Parse("8b3a27e7-7d75-4beb-8cc4-cea4f950c902"),
                    ProductID = Guid.Parse("da004ad8-2f64-4bf5-9c2f-adc9e48070bc"),
                    Date = new DateTime(2021, 10, 15)
                },
                //3kg Ice Blocks
                new ProductPrice
                {
                    ProductPriceID = Guid.Parse("64ec33d1-8bbd-4777-a6b0-716d14ef9f5d"),
                    PriceID = Guid.Parse("a9fd9258-2557-4f48-935f-227a97c87c8b"),
                    ProductID = Guid.Parse("1e54f43d-efbd-4211-94ae-cee6ec34014c"),
                    Date = new DateTime(2021, 10, 15)
                },
                //1kg Ice Blocks
                new ProductPrice
                {
                    ProductPriceID = Guid.Parse("67a43150-2aac-47c5-ad0f-289af32ba82d"),
                    PriceID = Guid.Parse("cde17195-4204-4f9f-aed0-079ed23bacdf"),
                    ProductID = Guid.Parse("42258ad8-9733-4d44-adae-eb2b5589cf42"),
                    Date = new DateTime(2021, 10, 15)
                },
                //4kg Ice Blocks
                new ProductPrice
                {
                    ProductPriceID = Guid.Parse("199baac9-1afb-48b0-be10-e0a363db3926"),
                    PriceID = Guid.Parse("8cef5816-66f6-4354-af7b-9ff24cf339f7"),
                    ProductID = Guid.Parse("629763fc-84f1-4057-bdf6-4167728af1e8"),
                    Date = new DateTime(2021, 10, 15)
                },
                //Large Wood
                new ProductPrice
                {
                    ProductPriceID = Guid.Parse("21b6bc5f-9c2f-4646-9491-4461e7ac156c"),
                    PriceID = Guid.Parse("3c63dbda-4e29-4028-8314-859c7b1351f9"),
                    ProductID = Guid.Parse("6fb2b343-db52-42b0-b74d-759d0556a80b"),
                    Date = new DateTime(2021, 10, 15)
                },
                //Small Wood
                new ProductPrice
                {
                    ProductPriceID = Guid.Parse("aa27d0ae-7b02-4e0e-a389-02fc219af827"),
                    PriceID = Guid.Parse("8985e3d5-2b88-43d7-a300-688d7a648a0b"),
                    ProductID = Guid.Parse("a0cd82e1-670e-4988-b096-b94d008bf10a"),
                    Date = new DateTime(2021, 10, 15)
                }) ;
        }
    }
}

