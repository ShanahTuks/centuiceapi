﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class EmployeeTripSheetTypeData : IEntityTypeConfiguration<EmployeeTripSheetType>
    {


        public void Configure(EntityTypeBuilder<EmployeeTripSheetType> builder)
        {
            builder.ToTable("EmployeeTripSheetTypes");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(
                new EmployeeTripSheetType
                {
                    EmployeeTripSheetTypeID = Guid.Parse("c54bd8d9-70ca-44d2-8abe-c27e24c6097d"),
                    Description = "Driver"

                },
                new EmployeeTripSheetType
                {
                    EmployeeTripSheetTypeID = Guid.Parse("f6354222-4932-4d07-b2a7-515b54674d94"),
                    Description = "Assistant"

                }

            );
        }

    }
}