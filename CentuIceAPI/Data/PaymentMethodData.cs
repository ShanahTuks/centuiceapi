﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Data
{
    public class PaymentMethodData : IEntityTypeConfiguration<PaymentMethod>
    {
        public void Configure(EntityTypeBuilder<PaymentMethod> builder)
        {
            builder.ToTable("PaymentMethods");

            _ = builder.HasData(

                    new PaymentMethod
                    {
                        PaymentMethodID = Guid.Parse("C54255E8-DDA0-4788-BF60-172F291E2670"),
                        Description = "EFT"
                    },
                    new PaymentMethod
                    {
                        PaymentMethodID = Guid.Parse("D074225E-723F-4846-B385-32CFF5A0ADDA"),
                        Description = "Cash"
                    },
                    new PaymentMethod
                    {
                        PaymentMethodID = Guid.Parse("f4efaa23-7be8-4148-b640-bbc14d6cc08b"),
                        Description = "Card"
                    }

                );
        }
    }
}
