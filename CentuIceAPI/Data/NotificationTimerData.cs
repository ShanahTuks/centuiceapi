﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace CentuIceAPI.Data
{
    public class NotificationTimerData : IEntityTypeConfiguration<NotificationTimer>
    {
      
        
            public void Configure(EntityTypeBuilder<NotificationTimer> builder)
            {
                builder.ToTable("NotificationTimers");

            _ = builder.HasData(

                    //Group 1 Out for Delivery
                    new NotificationTimer
                    {
                        NotificationTimerID = Guid.Parse("afbcbcd8-4d71-4057-9fb8-572cde31958e"),
                        NotificationType = "Vehicle",
                        NotificationReason = "License Expiry",
                        TimePeriod = 31,
                        CheckTime = new DateTime(2021, 08, 31, 10, 0, 0)

                    },
                     new NotificationTimer
                     {
                         NotificationTimerID = Guid.Parse("c0e43f08-8e14-4730-b65b-c22f6b53d118"),
                         NotificationType = "Vehicle",
                         NotificationReason = "License Expiry",
                         TimePeriod = 20,
                         CheckTime = new DateTime(2021, 08, 31, 10, 0, 0)

                     },
                      new NotificationTimer
                      {
                          NotificationTimerID = Guid.Parse("e9f944a8-6765-4fd0-bfe2-61daee22837a"),
                          NotificationType = "Vehicle",
                          NotificationReason = "License Expiry",
                          TimePeriod = 5,
                          CheckTime = new DateTime(2021, 08, 31, 10, 0, 0)

                      },
                       new NotificationTimer
                       {
                           NotificationTimerID = Guid.Parse("5f10ac1d-7ed9-4f41-adbb-78a3d0647b5e"),
                           NotificationType = "Filter",
                           NotificationReason = "Filter Replacement",
                           TimePeriod = 5,
                           CheckTime = new DateTime(2021, 08, 31, 10, 0, 0)

                       }



                ); ;
            }
      }
}
