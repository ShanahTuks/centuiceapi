﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class OrderStatusData : IEntityTypeConfiguration<OrderStatus>
    {


        public void Configure(EntityTypeBuilder<OrderStatus> builder)
        {
            builder.ToTable("OrderStatuses");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(
                new OrderStatus
                {
                    OrderStatusID = Guid.Parse("9fb2e19a-be94-45ac-8de2-96a1d411b796"),
                    Description = "Pending"

                },
                 new OrderStatus
                 {
                     OrderStatusID = Guid.Parse("aef36c5f-795c-4d54-a840-8d005fad64c2"),
                     Description = "Cancelled"

                 },
                 new OrderStatus
                 {
                     OrderStatusID = Guid.Parse("a4120e1f-1a87-466a-ae62-0f6bcc357877"),
                     Description = "Assigned to Trip Sheet"

                 },
                 new OrderStatus
                 {
                     OrderStatusID = Guid.Parse("502d6f63-525a-4efc-bdcb-3560b0edff2c"),
                     Description = "Loaded on Truck"

                 },
                 new OrderStatus
                 {
                     OrderStatusID = Guid.Parse("c10bfe5e-4c56-4551-b1f2-8fd3321a4d1b"),
                     Description = "Out for Delivery"

                 },
                 new OrderStatus
                 {
                     OrderStatusID = Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85"),
                     Description = "Delivered"

                 }


            );
        }

    }
}