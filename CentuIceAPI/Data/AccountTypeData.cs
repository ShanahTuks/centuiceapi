﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class AccountTypeData : IEntityTypeConfiguration<AccountType>
    {


        public void Configure(EntityTypeBuilder<AccountType> builder)
        {
            builder.ToTable("AccountTypes");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(
                new AccountType
                {
                    AccountTypeID = Guid.Parse("17da522e-0668-497d-978c-23e763a7fbe9"),
                    Description = "Savings"

                },
                new AccountType
                {
                    AccountTypeID = Guid.Parse("6b28f3ab-bd48-4abc-b5f1-4e2f4967a0e8"),
                    Description = "Cheque"

                },
                 new AccountType
                 {
                     AccountTypeID = Guid.Parse("05c6bd34-e24e-468f-a620-724b5daaba1a"),
                     Description = "Money Market Investment"

                 },
                  new AccountType
                  {
                      AccountTypeID = Guid.Parse("69d4730c-fbdd-4d3a-890f-77f39d5cb13a"),
                      Description = "Mzansi"

                  }

            );
        }

    }
}