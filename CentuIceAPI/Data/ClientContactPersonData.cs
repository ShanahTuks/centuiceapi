﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class ClientContactPersonData : IEntityTypeConfiguration<ClientContactPerson>
    {


        public void Configure(EntityTypeBuilder<ClientContactPerson> builder)
        {
            builder.ToTable("ClientContactPeople");
            builder.Property(s => s.Name).IsRequired(true);

            _ = builder.HasData(
                new ClientContactPerson
                {
                    ClientContactPersonID = Guid.Parse("9191fbfa-8e98-421f-9be4-49b12f3fd779"),
                    ClientID = Guid.Parse("543638f6-d06f-4a0c-b06a-96a175290244"),
                    Name = "Danae",
                    PhoneNumber = "0733714164",
                    EmailAddress = "admin@cultureclub.co.za"

                },
                new ClientContactPerson
                {
                    ClientContactPersonID = Guid.Parse("bce06eb4-5efb-4ed4-b020-6ccd9332de4a"),
                    ClientID = Guid.Parse("543638f6-d06f-4a0c-b06a-96a175290244"),
                    Name = "Jenna",
                    PhoneNumber = "0815378764",
                    EmailAddress = "staff@cultureclub.co.za"

                },
                 new ClientContactPerson
                 {
                     ClientContactPersonID = Guid.Parse("780d9d5a-d1f0-4373-bd51-afb70cf0cfa7"),
                     ClientID = Guid.Parse("caa33a04-d199-4973-a375-25b7e8b157d4"),
                     Name = "Johan",
                     PhoneNumber = "0740789791",
                     EmailAddress = "admin@gmail.com"

                 },
                 new ClientContactPerson
                 {
                     ClientContactPersonID = Guid.Parse("d5e4ac1b-ece0-428b-9e60-c4a1fbb05344"),
                     ClientID = Guid.Parse("62c66868-32e2-469c-8224-40e1a7343983"),
                     Name = "Anyone",
                     PhoneNumber = "0740789791",
                     EmailAddress = "admin@gmail.com"

                 },
                  new ClientContactPerson
                  {
                      ClientContactPersonID = Guid.Parse("1f4f7372-5fa4-43dc-a91f-8df14bd8943a"),
                      ClientID = Guid.Parse("10e21cc0-001c-438b-bb2f-445e446688dd"),
                      Name = "Felicity",
                      PhoneNumber = "0815378764",
                      EmailAddress = "admin@gmail.com"

                  },
                  new ClientContactPerson
                  {
                      ClientContactPersonID = Guid.Parse("60cd1ada-02b0-4c3f-8b14-3f89c5bd36b3"),
                      ClientID = Guid.Parse("a6e982dd-7dd5-4256-a5f4-68a0a82bc596"),
                      Name = "Tony",
                      PhoneNumber = "0733714164",
                      EmailAddress = "admin@gmail.com"

                  },
                   new ClientContactPerson
                   {
                       ClientContactPersonID = Guid.Parse("b89887de-1b68-48ae-bdf6-c8cfa1f9df07"),
                       ClientID = Guid.Parse("8c316ea4-605a-4af8-9083-dab4a533dfdc"),
                       Name = "Anyone",
                       PhoneNumber = "0814169470",
                       EmailAddress = "admin@gmail.com"

                   },
                   //New contacts
                   new ClientContactPerson
                   {
                       ClientContactPersonID = Guid.Parse("6a4b3bc3-a1c6-4b67-8fd9-e161d7dcd899"),
                       ClientID = Guid.Parse("dc3561ab-ebd2-495c-9011-8ddb52873aba"),
                       Name = "Anyone",
                       PhoneNumber = "0764801037",
                       EmailAddress = "admin@gmail.com"

                   },
                   new ClientContactPerson
                   {
                       ClientContactPersonID = Guid.Parse("f0899973-8aa7-4749-84e6-aaa83bd35076"),
                       ClientID = Guid.Parse("50f1ef0e-28a1-4afc-81ec-900ce5ffd1e6"),
                       Name = "Anyone",
                       PhoneNumber = "0740789791",
                       EmailAddress = "admin@gmail.com"

                   },
                   new ClientContactPerson
                   {
                       ClientContactPersonID = Guid.Parse("8d2fb3d5-5c33-4ef6-a653-ba70258d88b8"),
                       ClientID = Guid.Parse("eda60fb4-a8a9-4bdb-8105-1981562d2597"),
                       Name = "Anyone",
                       PhoneNumber = "0740789791",
                       EmailAddress = "admin@gmail.com"

                   },
                   //Contact For Testing Invoice 1
                   new ClientContactPerson
                   {
                       ClientContactPersonID = Guid.Parse("a5d5d77a-89a7-4012-aa6c-a493c01c4d0d"),
                       ClientID = Guid.Parse("b5dd1c75-0029-4bda-be41-7fae78d0f7f0"),
                       Name = "Etienne",
                       PhoneNumber = "0764801037",
                       EmailAddress = "escheepers77@gmail.com"

                   },
                   //Contact For Testing Invoice 2
                   new ClientContactPerson
                   {
                       ClientContactPersonID = Guid.Parse("8a949446-0b47-4c6c-b755-21e671ae0af0"),
                       ClientID = Guid.Parse("b5dd1c75-0029-4bda-be41-7fae78d0f7f0"),
                       Name = "Shanah",
                       PhoneNumber = "0814169470",
                       EmailAddress = "shanahjr@gmail.com"

                   }

                );

        }

    }

}