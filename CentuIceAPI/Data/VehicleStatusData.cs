﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class VehicleStatusData : IEntityTypeConfiguration<VehicleStatus>
    {


        public void Configure(EntityTypeBuilder<VehicleStatus> builder)
        {
            builder.ToTable("VehicleStatuses");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(
                new VehicleStatus
                {
                    VehicleStatusID = Guid.Parse("65563356-8382-4c0d-8e2c-f94c98cb786b"),
                    Description = "Available"

                },
                new VehicleStatus
                {
                    VehicleStatusID = Guid.Parse("8631cd9e-9782-4098-ae61-31847bd47f8a"),
                    Description = "Making Delivery"

                },
                 new VehicleStatus
                 {
                     VehicleStatusID = Guid.Parse("e7b83455-818c-409a-9742-bf66b34ed341"),
                     Description = "Being Serviced"

                 },
          
                 new VehicleStatus
                   {
                       VehicleStatusID = Guid.Parse("6ffce810-cbb1-4192-87d6-378487d19913"),
                       Description = "Deactivated"

                   },
                    new VehicleStatus
                    {
                        VehicleStatusID = Guid.Parse("b0acee21-0379-44a7-96d9-1a04847d6f71"),
                        Description = "Pending"

                    }


            );
        }

    }
}