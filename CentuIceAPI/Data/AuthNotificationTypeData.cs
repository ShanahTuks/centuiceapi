﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class AuthNotificationTypeData : IEntityTypeConfiguration<AuthNotificationType>
    {
        public void Configure(EntityTypeBuilder<AuthNotificationType> builder)
        {
            builder.ToTable("AuthNotificationTypes");

            _ = builder.HasData(
                    new AuthNotificationType
                    {
                        AuthNotificationTypeID = Guid.Parse("2efc45a3-fece-45c3-80fb-f7386dc0c625"),
                        Description = "DeactivateEmployee"
                    },
                    new AuthNotificationType
                    {
                        AuthNotificationTypeID = Guid.Parse("3eadd87c-2cc3-4537-a0fa-e693d373319f"),
                        Description = "DeactivateClient"
                    },
                    new AuthNotificationType
                    {
                        AuthNotificationTypeID = Guid.Parse("70a6f99f-2283-45e7-9b1c-8f64b52dae5e"),
                        Description = "DeactivateVehicle"
                    },
                    new AuthNotificationType
                    {
                        AuthNotificationTypeID = Guid.Parse("1864db97-436e-4bdc-a4b5-de563435b49d"),
                        Description = "DeactivateMachine"
                    }
                    //new
                    ,
                    new AuthNotificationType
                    {
                        AuthNotificationTypeID = Guid.Parse("56e8715b-dee9-4fc6-929d-648cc4dd18a1"),
                        Description = "DeactivateSupplier"
                    }
                    ,
                    new AuthNotificationType
                    {
                        AuthNotificationTypeID = Guid.Parse("06ae874b-3e31-4845-92e9-7d83fc8db842"),
                        Description = "WriteOffStock"
                    }
                    ,
                    new AuthNotificationType
                    {
                        AuthNotificationTypeID = Guid.Parse("866d1081-598c-46a0-bcd0-fdd8dec76aca"),
                        Description = "DeactivateProduct"
                    }
                    ,
                    new AuthNotificationType
                    {
                        AuthNotificationTypeID = Guid.Parse("4ae95884-8692-43c6-8829-c0d787518dff"),
                        Description = "DeactivateInventory"
                    }
                     ,
                    new AuthNotificationType
                    {
                        AuthNotificationTypeID = Guid.Parse("d0721f9c-011b-43eb-b35f-6a0b5e8b2fbc"),
                        Description = "CancelSale"
                    }
                );
        }
    }
}
