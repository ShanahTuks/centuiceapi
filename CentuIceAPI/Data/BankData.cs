﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class BankData : IEntityTypeConfiguration<Bank>
    {


        public void Configure(EntityTypeBuilder<Bank> builder)
        {
            builder.ToTable("Banks");
            builder.Property(s => s.Name).IsRequired(true);

            _ = builder.HasData(
                new Bank
                {
                    BankID = Guid.Parse("aa3f969a-418e-4e6d-9169-c4e3d4a01530"),
                    Name = "ABSA"

                },
                new Bank
                {
                    BankID = Guid.Parse("ac7ad177-71bd-445d-9e35-a989dfcfe53a"),
                    Name = "FNB"

                },
                 new Bank
                 {
                     BankID = Guid.Parse("026665f4-2a0c-41e1-b388-c119f8851737"),
                     Name = "Capitec"

                 },
                  new Bank
                  {
                      BankID = Guid.Parse("0425a289-c5dc-4532-accc-17666c9dbf6d"),
                      Name = "Standard Bank"

                  },
                   new Bank
                   {
                       BankID = Guid.Parse("f1635eaf-62a3-41e9-9655-4b8e545e8f82"),
                       Name = "Netbank"

                   },
                    new Bank
                    {
                        BankID = Guid.Parse("02b0abaf-4818-4251-b220-d9400f29aedc"),
                        Name = "Investec"

                    },
                     new Bank
                     {
                         BankID = Guid.Parse("6424c0fe-d596-48fe-aafd-878384d9b661"),
                         Name = "African Bank"

                     },
                      new Bank
                      {
                          BankID = Guid.Parse("79d156d1-ad69-488b-b40e-a5218c7a1470"),
                          Name = "Discovery Limited"

                      },
                       new Bank
                       {
                           BankID = Guid.Parse("ec5c1000-653f-42b8-bc2b-97e2c044757d"),
                           Name = "Bidvest"

                       },
                        new Bank
                        {
                            BankID = Guid.Parse("906ac52d-6385-4a84-ab25-0f720da3876d"),
                            Name = "FirstRand Bank"

                        },
                         new Bank
                         {
                             BankID = Guid.Parse("9fa3596d-0d12-4024-a97c-e7f53b86d4a0"),
                             Name = "Grindrod Bank Limited"

                         },
                          new Bank
                          {
                              BankID = Guid.Parse("325d4acb-cd0e-4a52-b23f-a1e2a5e76d6a"),
                              Name = "Imperial Bank"

                          },
                           new Bank
                           {
                               BankID = Guid.Parse("5b56c0d1-a1e7-4aab-be11-58ae40011fcf"),
                               Name = "Sasfin Bank Limited"

                           },
                            new Bank
                            {
                                BankID = Guid.Parse("4e77bc48-c947-41a0-b0b6-35fd353b828a"),
                                Name = "Ubank Limited"

                            },
                             new Bank
                             {
                                 BankID = Guid.Parse("d2e85b9b-cfce-4719-80b7-32e6a3e09e8c"),
                                 Name = "TymeBank"

                             }
                             

            );
        }

    }
}
