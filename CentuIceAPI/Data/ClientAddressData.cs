﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class ClientAddressData : IEntityTypeConfiguration<ClientAddress>
    {


        public void Configure(EntityTypeBuilder<ClientAddress> builder)
        {
            builder.ToTable("ClientAddresses");
            builder.Property(s => s.ClientID).IsRequired(true);

            _ = builder.HasData(

                      new ClientAddress
                      {
                          ClientID = Guid.Parse("543638f6-d06f-4a0c-b06a-96a175290244"),
                          AddressID = Guid.Parse("6bfe6569-5f4f-4e63-8be5-d2d6ee46e601")

                      },
                      new ClientAddress
                      {
                          ClientID = Guid.Parse("543638f6-d06f-4a0c-b06a-96a175290244"),
                          AddressID = Guid.Parse("284c0140-3935-4566-9e95-51b347e42ec2"),

                      },
                        new ClientAddress
                        {
                            ClientID = Guid.Parse("caa33a04-d199-4973-a375-25b7e8b157d4"),
                            AddressID = Guid.Parse("e0d6c471-f8f0-448e-9028-5ea52b92927b"),

                        },
                      new ClientAddress
                      {
                          ClientID = Guid.Parse("62c66868-32e2-469c-8224-40e1a7343983"),
                          AddressID = Guid.Parse("c20a118d-56b7-4455-82e3-12383cb171a8"),

                      },
                        new ClientAddress
                        {
                            ClientID = Guid.Parse("10e21cc0-001c-438b-bb2f-445e446688dd"),
                            AddressID = Guid.Parse("aad80912-434e-4924-8ddc-38e5e7c998f7"),

                        },
                      new ClientAddress
                      {
                          ClientID = Guid.Parse("a6e982dd-7dd5-4256-a5f4-68a0a82bc596"),
                          AddressID = Guid.Parse("dd44b55c-ceca-46da-9e71-53884ab12fdc"),

                      },
                      new ClientAddress
                      {
                          ClientID = Guid.Parse("8c316ea4-605a-4af8-9083-dab4a533dfdc"),
                          AddressID = Guid.Parse("a601cc74-fefd-4a1e-908d-c814a6598708"),

                      },
                      new ClientAddress
                      {
                          ClientID = Guid.Parse("dc3561ab-ebd2-495c-9011-8ddb52873aba"),
                          AddressID = Guid.Parse("dc3561ab-ebd2-495c-9011-8ddb52873aba"),

                      },
                      new ClientAddress
                      {
                          ClientID = Guid.Parse("50f1ef0e-28a1-4afc-81ec-900ce5ffd1e6"),
                          AddressID = Guid.Parse("d5edf3a1-c693-45a5-a519-b3b2fd037f8f"),

                      },
                      new ClientAddress
                      {
                          ClientID = Guid.Parse("eda60fb4-a8a9-4bdb-8105-1981562d2597"),
                          AddressID = Guid.Parse("9551ded3-130e-43d2-9746-2b0d0e41e7a7"),

                      },
                      //Test Invoice CLient Address
                      new ClientAddress
                      {
                          ClientID = Guid.Parse("b5dd1c75-0029-4bda-be41-7fae78d0f7f0"),
                          AddressID = Guid.Parse("b896ffb8-fd96-43b8-8fcf-dab959d30503"),

                      }



                      );
                     
        }
    }
}