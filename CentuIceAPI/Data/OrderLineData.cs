﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace CentuIceAPI.Data
{
    public class OrderLineData : IEntityTypeConfiguration<OrderLine>
    {
        public void Configure(EntityTypeBuilder<OrderLine> builder)
        {
            builder.ToTable("OrderLines");

            _ = builder.HasData(

                    new OrderLine
                    {
                        OrderID = Guid.Parse("c08a53d5-7926-46cd-b3c1-5e251497ea91"),
                        ProductID = Guid.Parse("974e485b-d76f-4f4f-bde7-2b414435890f"),
                        ProductPriceID = Guid.Parse("82cffdfb-4ca7-4db5-ba1d-32df7e9c8f46"),
                        Quantity = 100,
                    },
                    new OrderLine
                    {
                        OrderID = Guid.Parse("9c15fdd9-48e2-4bf7-8bca-c53a9b7f29b5"),
                        ProductPriceID = Guid.Parse("9de02532-d97d-4840-a04d-87aeeef56d3e"),
                        ProductID = Guid.Parse("da004ad8-2f64-4bf5-9c2f-adc9e48070bc"),
                        Quantity = 50,
                    },
                    new OrderLine
                    {
                        OrderID = Guid.Parse("fc9a5780-6cc4-40a7-8749-ff15361ad458"),
                        ProductID = Guid.Parse("da004ad8-2f64-4bf5-9c2f-adc9e48070bc"),
                        ProductPriceID = Guid.Parse("9de02532-d97d-4840-a04d-87aeeef56d3e"),
                        Quantity = 35,
                    }
                    , //Order May 2021
                    new OrderLine
                    {
                        OrderID = Guid.Parse("5f36368a-b187-42ae-b718-e0cb902b1e4a"),
                        ProductPriceID = Guid.Parse("199baac9-1afb-48b0-be10-e0a363db3926"),
                        ProductID = Guid.Parse("629763fc-84f1-4057-bdf6-4167728af1e8"),
                        Quantity = 600,
                    }
                    ,//Order June 2021
                    new OrderLine
                    {
                        OrderID = Guid.Parse("57d68757-9ad2-4edc-9ef0-d0d965909154"),
                        ProductPriceID = Guid.Parse("67a43150-2aac-47c5-ad0f-289af32ba82d"),
                        ProductID = Guid.Parse("42258ad8-9733-4d44-adae-eb2b5589cf42"),
                        Quantity = 700,
                    }
                    ,//Order September 2021
                    new OrderLine
                    {
                        OrderID = Guid.Parse("c3d6fd3c-a190-41f1-b627-0039eca8e5b1"),
                        ProductPriceID = Guid.Parse("21b6bc5f-9c2f-4646-9491-4461e7ac156c"),
                        ProductID = Guid.Parse("6fb2b343-db52-42b0-b74d-759d0556a80b"),
                        Quantity = 800,
                    }

                );
        }
    }
}
