﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace CentuIceAPI.Data
{
    public class PriceData : IEntityTypeConfiguration<Price>
    {
        public void Configure(EntityTypeBuilder<Price> builder)
        {
            builder.ToTable("Prices");
            builder.Property(it => it.PriceID).IsRequired(true);

            _ = builder.HasData(
                //5kg Ice Bag Price
                new Price
                {
                    PriceID = Guid.Parse("55bb5ecd-ffd7-4e9f-8f53-d2ce47e7b633"),
                    CurrentPrice = 2.25m,
                },
                //2kg Ice Bag Price
                new Price
                {
                    PriceID = Guid.Parse("49e4b3a1-0597-4982-a06b-df5a68b6c5fc"),
                    CurrentPrice = 1.50m,
                },
                //3kg Ice Bag Price
                new Price
                {
                    PriceID = Guid.Parse("69eebef6-9c1b-4d6d-9353-fd1a80890c80"),
                    CurrentPrice = 1.50m,
                },
                //1kg Ice Bag Price
                new Price
                {
                    PriceID = Guid.Parse("ab60263b-243b-4ce9-a43b-447ba1943ed0"),
                    CurrentPrice = 0.50m,
                },
                //4kg Ice Bag Price
                new Price
                {
                    PriceID = Guid.Parse("bc871e86-0885-43b9-be14-e1db9492b3e8"),
                    CurrentPrice = 1.63m,
                },
                //Large Wood Bag Price
                new Price
                {
                    PriceID = Guid.Parse("fb453fdd-2259-4d4a-a966-66bbcf3a61e3"),
                    CurrentPrice = 3.63m,
                },
                //Small Bag Price
                new Price
                {
                    PriceID = Guid.Parse("26746a3d-fc91-4f1e-b429-a9d56c0bd42e"),
                    CurrentPrice = 1.21m,
                },  //5kg Ice Blocks Price
                new Price
                {
                    PriceID = Guid.Parse("582b1b38-0afd-40a8-9ccd-67663d221d67"),
                    CurrentPrice = 50.50m,
                },
                //2kg Ice Blocks Price
                new Price
                {
                    PriceID = Guid.Parse("8b3a27e7-7d75-4beb-8cc4-cea4f950c902"),
                    CurrentPrice = 20.50m,
                },
                //3kg Ice Blocks Price
                new Price
                {
                    PriceID = Guid.Parse("a9fd9258-2557-4f48-935f-227a97c87c8b"),
                    CurrentPrice = 20.50m,
                },
                //1kg Ice Blocks Price
                new Price
                {
                    PriceID = Guid.Parse("cde17195-4204-4f9f-aed0-079ed23bacdf"),
                    CurrentPrice = 10.50m,
                },
                //4kg Ice Blocks Price
                new Price
                {
                    PriceID = Guid.Parse("8cef5816-66f6-4354-af7b-9ff24cf339f7"),
                    CurrentPrice = 23.50m,
                },
                //Large Wood Price
                new Price
                {
                    PriceID = Guid.Parse("3c63dbda-4e29-4028-8314-859c7b1351f9"),
                    CurrentPrice = 26.50m,
                },
                //Small Wood Price
                new Price
                {
                    PriceID = Guid.Parse("8985e3d5-2b88-43d7-a300-688d7a648a0b"),
                    CurrentPrice = 12.50m,
                }

                );

        }
    }
}
