﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class EmployeeAddressData : IEntityTypeConfiguration<EmployeeAddress>
    {


        public void Configure(EntityTypeBuilder<EmployeeAddress> builder)
        {
            builder.ToTable("EmployeeAddresses");
            builder.Property(s => s.EmployeeID).IsRequired(true);

            _ = builder.HasData(

                      //Shanah Owner
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("7f177922-a5f1-448d-8862-96fd4ba5d633"),
                          AddressID = Guid.Parse("97491d4b-2fe3-4140-a310-3ed4e5bfdd3d")
                      },
                      //Etiennne Owner
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("14e100c5-a695-49fa-ada2-244c117df5c0"),
                          AddressID = Guid.Parse("f57e6df5-6cf1-4896-9d75-e8714e8c1c2b"),
                      },

                        //Member Employee Addresses

                        //Melissa Admin
                        new EmployeeAddress
                        {
                            EmployeeID = Guid.Parse("bc2c2ae9-204f-49b5-8d9d-58da49b8fcd4"),
                            AddressID = Guid.Parse("c520b2f5-0dac-42ee-b356-08f3e51078ed"),
                        },

                      //Naomi Admin
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("30fc6523-661c-4479-8539-049d8cca6cba"),
                          AddressID = Guid.Parse("623fba70-b4b3-4e3b-bb63-774ad268a916"),
                      },

                      //Puvelin Admin
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("47d01ee3-0c9b-4118-beea-c21085cf001c"),
                          AddressID = Guid.Parse("e17ff651-d643-41eb-bcff-dd05990d5015"),
                      },

                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("f11765a0-58d2-42e5-9c5c-34022016e8c6"),
                          AddressID = Guid.Parse("5ea203e4-a74d-4056-a229-55f678266624"),
                      },
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("fb59a87d-59e6-49d4-979b-658eb9986162"),
                          AddressID = Guid.Parse("9127e563-e3d4-4694-814e-2edfa644b518"),
                      },
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("aa301feb-440f-42ae-b4a1-0c123bf074af"),
                          AddressID = Guid.Parse("b69e2095-3ec0-438f-951e-84f44c6a06d0"),
                      },
                      //Assistant Address
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("26771adf-20b0-4988-92d9-c0e245b9bf93"),
                          AddressID = Guid.Parse("7c6569da-cd8d-4228-9a40-b5d6faed1487"),
                      },
                      //SuperVisor Address
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("89d3bb9a-c316-4ef5-b1e2-f8423996ffb2"),
                          AddressID = Guid.Parse("c632e660-2d53-4c19-bef0-084b15df97e5"),
                      },
                      //Driver Address
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("50084a0b-1663-4f4f-8974-e0fbaacf2d60"),
                          AddressID = Guid.Parse("53c885f8-fa5f-4314-aa51-54aea0031ae2"),
                      },
                      //Assistant3 Address
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("eac034be-cbba-4360-9e8e-8a897f16c6a6"),
                          AddressID = Guid.Parse("5fdebe6d-13d8-4720-a988-5936677f62d3"),
                      },
                      //SuperVisor3 Address
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("61cc8c8d-f487-4a7e-b78c-ab4614786586"),
                          AddressID = Guid.Parse("30ee5dc5-9602-4b2a-bc25-a92039520e1c"),
                      },
                      //Driver Address3
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("63abb4b4-5d4b-446d-8471-eb3e0f7f053c"),
                          AddressID = Guid.Parse("eeee2e71-60d0-4167-a357-065a7b2cc497"),
                      },
                      //Assistant4 Address
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("efdc266e-2dce-4021-968c-cad5adc9c9b5"),
                          AddressID = Guid.Parse("7e483a7b-9f16-4c9a-8f0b-716088714836"),
                      },
                      //SuperVisor4 Address
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("376017bd-2155-4e82-a1a7-fbe7f2d49b83"),
                          AddressID = Guid.Parse("7a490548-2ac9-4902-bca0-7aa7b044416b"),
                      },
                      //Driver Address4
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("4e5f0b7f-68ba-4254-b6fc-04fab6a99fce"),
                          AddressID = Guid.Parse("9a9a8c53-5c08-4c25-8147-dda25dd62fdf"),
                      },
                      //Assistant5 Address
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("2a7ecffa-8673-45c0-8b0a-df03b891dc3c"),
                          AddressID = Guid.Parse("02b1fcc2-c6e2-432e-9a5d-1bbd379bf3f5"),
                      },
                      //SuperVisor5 Address
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("40525829-4dd0-4352-a149-cea6a86ec01f"),
                          AddressID = Guid.Parse("3085c334-566d-4c8f-8432-cfc0a20f1c41"),
                      },
                      //Driver Address5
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("83fc7bdc-b730-4c4f-b13b-d40f9d282b73"),
                          AddressID = Guid.Parse("63cfb3ec-db5e-421c-9246-1edfe4740fe0"),
                      },

                      //More Demo Employees 1
                      //Driver 6 Address
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("46e26f11-c2ac-4409-b119-dbacc9c39ee4"),
                          AddressID = Guid.Parse("8fe720af-306a-49f8-9216-2109b297c4ea"),
                      },
                      //Assistant 6
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("b07dee5d-9b83-426b-a1c0-a89e9ff39bfd"),
                          AddressID = Guid.Parse("bf3efc94-9d1d-4173-8ff6-2dad74c85919"),
                      },
                      //Supervisor 6
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("48adbfa9-be84-4a46-8c07-a859730c35c5"),
                          AddressID = Guid.Parse("dbffb135-9d1f-4075-96b4-9076a523fbf9"),
                      },

                    //More Demo Employees 2
                    //Driver 7 Address
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("5b5ea95c-d324-44ef-9ece-a20e991d31f1"),
                          AddressID = Guid.Parse("a359e0dd-1802-4a41-8050-d3f48cbabb75"),
                      },
                      //Assistant 7
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("675c1342-2c24-4c4c-bf05-378ce4e5e9c5"),
                          AddressID = Guid.Parse("de25e5d8-fe02-4dc1-862d-c1dbfa7e8eac"),
                      },
                      //Supervisor 7
                      new EmployeeAddress
                      {
                          EmployeeID = Guid.Parse("c152a7ba-be20-4299-88b2-3196bd37e697"),
                          AddressID = Guid.Parse("e3761f3f-c771-4c4f-8805-583742ad6996"),
                      }
                );

        }
    }
}