﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace CentuIceAPI.Data
{
    public class InventoryPriceData : IEntityTypeConfiguration<InventoryPrice>
    {
        public void Configure(EntityTypeBuilder<InventoryPrice> builder)
        {
            builder.ToTable("InventoryPrices");
            builder.Property(it => it.PriceID).IsRequired(true);

            _ = builder.HasData(
                //5kg Ice Bag 
                new InventoryPrice
                {
                    InventoryPriceID = Guid.Parse("60339a11-fb71-445a-a88f-7245660481ba"),
                    PriceID = Guid.Parse("55bb5ecd-ffd7-4e9f-8f53-d2ce47e7b633"),
                    InventoryID = Guid.Parse("bfb2a434-9da4-4f85-93ea-1dc51e20d07b"),
                    Date = new DateTime(2021, 10, 15)

                },
                //2kg Ice Bag 
                new InventoryPrice
                {
                    InventoryPriceID = Guid.Parse("7c2579c7-1076-4d1c-9ea8-746195540ad7"),
                    PriceID = Guid.Parse("49e4b3a1-0597-4982-a06b-df5a68b6c5fc"),
                    InventoryID = Guid.Parse("ca8de3b1-a744-4821-9537-81b90d96e153"),
                    Date = new DateTime(2021, 10, 15)

                },
                //3kg Ice Bag 
                new InventoryPrice
                {
                    InventoryPriceID = Guid.Parse("c363880d-eabe-48ba-9795-757db1bf3e05"),
                    PriceID = Guid.Parse("69eebef6-9c1b-4d6d-9353-fd1a80890c80"),
                    InventoryID = Guid.Parse("f0c7eb5c-6603-455f-96d6-5f9de5854bb6"),
                    Date = new DateTime(2021, 10, 15)

                },
                //1kg Ice Bag 
                new InventoryPrice
                {
                    InventoryPriceID = Guid.Parse("742a9949-4ab8-4bf8-831d-5f8cd8b329d6"),
                    PriceID = Guid.Parse("ab60263b-243b-4ce9-a43b-447ba1943ed0"),
                    InventoryID = Guid.Parse("97ec7bf3-2fd0-44de-8960-a119ec42beea"),
                    Date = new DateTime(2021, 10, 15)

                },
                //4kg Ice Bag 
                new InventoryPrice
                {
                    InventoryPriceID = Guid.Parse("afbc7b15-d66a-4bca-a155-7337a444deea"),
                    PriceID = Guid.Parse("bc871e86-0885-43b9-be14-e1db9492b3e8"),
                    InventoryID = Guid.Parse("f7974974-613b-40da-9391-838f7dd9a4d5"),
                    Date = new DateTime(2021, 10, 15)

                },
                //Large Wood Bag 
                new InventoryPrice
                {
                    InventoryPriceID = Guid.Parse("acc84feb-6da6-46cf-8cc0-cffcb8781621"),
                    PriceID = Guid.Parse("fb453fdd-2259-4d4a-a966-66bbcf3a61e3"),
                    InventoryID = Guid.Parse("d9bcb8d7-db6e-4e26-8095-484ecfab9c5d"),
                    Date = new DateTime(2021, 10, 15)

                },
                //Small Wood Bag 
                new InventoryPrice
                {
                    InventoryPriceID = Guid.Parse("62d1c758-5b5b-4b78-b9d8-e9ef136d5b01"),
                    PriceID = Guid.Parse("26746a3d-fc91-4f1e-b429-a9d56c0bd42e"),
                    InventoryID = Guid.Parse("98cb7fea-bfe7-46c0-a5f1-70a89b32ff33"),
                    Date = new DateTime(2021, 10, 15)

                });
        }
    }
}
