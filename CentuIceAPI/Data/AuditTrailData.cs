﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Data
{
    public class AuditTrailData : IEntityTypeConfiguration<AuditTrail>
    {
        public void Configure(EntityTypeBuilder<AuditTrail> builder)
        {
            builder.ToTable("AuditTrails");

            _ = builder.HasData(
                
                    new AuditTrail
                    {
                        AuditTrailID = Guid.Parse("5de18eec-013d-43be-897b-fc8112a5f00c"),
                        ActionDate = new DateTime(2021, 8, 15,15,31,35),
                        ActionDescription = "Created a new Employee",
                        EmployeeAction = "Create",
                        EmployeeID = Guid.Parse("7f177922-a5f1-448d-8862-96fd4ba5d633"),
                    },
                    new AuditTrail
                    {
                        AuditTrailID = Guid.Parse("d513c444-2baa-4c67-8a6d-8b38f15e9637"),
                        ActionDate = new DateTime(2021, 8, 15, 15, 40, 46),
                        ActionDescription = "Created a new Employee",
                        EmployeeAction = "Create",
                        EmployeeID = Guid.Parse("7f177922-a5f1-448d-8862-96fd4ba5d633"),
                    }

                );
        }
    }
}
