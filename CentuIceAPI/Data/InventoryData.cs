﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Data
{
    public class InventoryData : IEntityTypeConfiguration<Inventory>
    {
        public void Configure(EntityTypeBuilder<Inventory> builder)
        {
            builder.ToTable("Inventories");
            builder.Property(it => it.InventoryTypeID).IsRequired(true);

            _ = builder.HasData(
                new Inventory
                {
                    InventoryID = Guid.Parse("bfb2a434-9da4-4f85-93ea-1dc51e20d07b"),
                    SupplierID = Guid.Parse("b3307864-6945-4aa0-92e7-0095e056889b"),
                    InventoryTypeID = Guid.Parse("19528739-55a2-45fd-a026-3c0e0f9d1ba1"),
                    ItemName = "5KG Ice Bag",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                    Quantity = 500
                },
                new Inventory
                {
                    InventoryID = Guid.Parse("ca8de3b1-a744-4821-9537-81b90d96e153"),
                    SupplierID = Guid.Parse("b3307864-6945-4aa0-92e7-0095e056889b"),
                    InventoryTypeID = Guid.Parse("19528739-55a2-45fd-a026-3c0e0f9d1ba1"),
                    ItemName = "2KG Ice Bag",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                    Quantity = 500
                },
                new Inventory
                {
                    InventoryID = Guid.Parse("f0c7eb5c-6603-455f-96d6-5f9de5854bb6"),
                    SupplierID = Guid.Parse("b3307864-6945-4aa0-92e7-0095e056889b"),
                    InventoryTypeID = Guid.Parse("19528739-55a2-45fd-a026-3c0e0f9d1ba1"),
                    ItemName = "3KG Ice Bag",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                    Quantity = 500
                },
                new Inventory
                {
                    InventoryID = Guid.Parse("97ec7bf3-2fd0-44de-8960-a119ec42beea"),
                    SupplierID = Guid.Parse("b3307864-6945-4aa0-92e7-0095e056889b"),
                    InventoryTypeID = Guid.Parse("19528739-55a2-45fd-a026-3c0e0f9d1ba1"),
                    ItemName = "1KG Ice Bag",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                    Quantity = 500
                },
                new Inventory
                {
                    InventoryID = Guid.Parse("f7974974-613b-40da-9391-838f7dd9a4d5"),
                    SupplierID = Guid.Parse("b3307864-6945-4aa0-92e7-0095e056889b"),
                    InventoryTypeID = Guid.Parse("19528739-55a2-45fd-a026-3c0e0f9d1ba1"),
                    ItemName = "4KG Ice Bag",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                    Quantity = 500
                },
                new Inventory
                {
                    InventoryID = Guid.Parse("d9bcb8d7-db6e-4e26-8095-484ecfab9c5d"),
                    SupplierID = Guid.Parse("b3307864-6945-4aa0-92e7-0095e056889b"),
                    InventoryTypeID = Guid.Parse("19528739-55a2-45fd-a026-3c0e0f9d1ba1"),
                    ItemName = "Large Wood Bag",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                    Quantity = 500
                },
                new Inventory
                {
                    InventoryID = Guid.Parse("98cb7fea-bfe7-46c0-a5f1-70a89b32ff33"),
                    SupplierID = Guid.Parse("b3307864-6945-4aa0-92e7-0095e056889b"),
                    InventoryTypeID = Guid.Parse("19528739-55a2-45fd-a026-3c0e0f9d1ba1"),
                    ItemName = "Small Wood Bag",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                    Quantity = 500
                }
            );
        }
    }
}
