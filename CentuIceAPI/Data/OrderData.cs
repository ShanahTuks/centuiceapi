﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Data
{
    public class OrderData : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable("Orders");

            _ = builder.HasData(

                    //Group 3 Order Out for Delivery
                    new Order
                    {
                        OrderID = Guid.Parse("c08a53d5-7926-46cd-b3c1-5e251497ea91"),
                        AddressID = Guid.Parse("a601cc74-fefd-4a1e-908d-c814a6598708"),
                        ClientID = Guid.Parse("8c316ea4-605a-4af8-9083-dab4a533dfdc"),
                        Delivered = false,
                        Loaded = true,
                        DeliveryTripSheetID = Guid.Parse("741d2ad5-ff78-493b-a8bd-16c10649de04"),
                        DeliveryDate = new DateTime(2021, 8, 30, 15, 31, 35),
                        InvoiceID = Guid.Parse("2e17cf49-6504-4d05-983f-8128a4a0c459"),
                        OrderDate = new DateTime(2021, 8, 30, 15, 31, 35),
                        OrderStatusID = Guid.Parse("c10bfe5e-4c56-4551-b1f2-8fd3321a4d1b"),
                        
                    },
                    //Group 4 Order Loading Started
                    new Order
                    {
                        OrderID = Guid.Parse("9c15fdd9-48e2-4bf7-8bca-c53a9b7f29b5"),
                        AddressID = Guid.Parse("dd44b55c-ceca-46da-9e71-53884ab12fdc"),
                        ClientID = Guid.Parse("a6e982dd-7dd5-4256-a5f4-68a0a82bc596"),
                        Delivered = false,
                        Loaded = true,
                        DeliveryTripSheetID = Guid.Parse("2d1966d0-8749-4bb7-af16-2b99c878f6fb"),
                        DeliveryDate = new DateTime(2021, 8, 30, 15, 31, 35),
                        InvoiceID = Guid.Parse("7e4dcd3b-a893-490f-a469-ca0ea53d3bc7"),
                        OrderDate = new DateTime(2021, 8, 30, 15, 31, 35),
                        OrderStatusID = Guid.Parse("502d6f63-525a-4efc-bdcb-3560b0edff2c"),

                    },
                    //Group 5 Ready for Delivery
                    new Order
                    {
                        OrderID = Guid.Parse("fc9a5780-6cc4-40a7-8749-ff15361ad458"),
                        AddressID = Guid.Parse("aad80912-434e-4924-8ddc-38e5e7c998f7"),
                        ClientID = Guid.Parse("10e21cc0-001c-438b-bb2f-445e446688dd"),
                        Delivered = false,
                        Loaded = true,
                        DeliveryTripSheetID = Guid.Parse("a7fe7e2e-a308-4c9a-be4d-bf33a25f3c91"),
                        DeliveryDate = new DateTime(2021, 8, 30, 15, 31, 35),
                        InvoiceID = Guid.Parse("4bdbaa80-9f10-4e36-956c-7ab882c97d00"),
                        OrderDate = new DateTime(2021, 8, 30, 15, 31, 35),
                        OrderStatusID = Guid.Parse("502d6f63-525a-4efc-bdcb-3560b0edff2c"),

                    },
                    //Order May 2021
                    new Order
                    {
                        OrderID = Guid.Parse("5f36368a-b187-42ae-b718-e0cb902b1e4a"),
                        AddressID = Guid.Parse("aad80912-434e-4924-8ddc-38e5e7c998f7"),
                        ClientID = Guid.Parse("10e21cc0-001c-438b-bb2f-445e446688dd"),
                        Delivered = false,
                        Loaded = true,
                        DeliveryTripSheetID = Guid.Parse("52238d97-9e77-4107-b8f7-6739d32be3e0"),
                        DeliveryDate = new DateTime(2021, 5, 15, 15, 31, 35),
                        InvoiceID = Guid.Parse("6d619343-bb23-401f-b40f-3bf323a8b706"),
                        OrderDate = new DateTime(2021, 5, 14, 15, 31, 35),
                        OrderStatusID = Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85"),

                    },
                    //Order June 2021
                    new Order
                    {
                        OrderID = Guid.Parse("57d68757-9ad2-4edc-9ef0-d0d965909154"),
                        AddressID = Guid.Parse("aad80912-434e-4924-8ddc-38e5e7c998f7"),
                        ClientID = Guid.Parse("10e21cc0-001c-438b-bb2f-445e446688dd"),
                        Delivered = false,
                        Loaded = true,
                        DeliveryTripSheetID = Guid.Parse("510ae3b6-37f5-44af-886e-5a4d2117f202"),
                        DeliveryDate = new DateTime(2021, 6, 22, 15, 31, 35),
                        InvoiceID = Guid.Parse("5d84a526-2e46-4f9f-ba22-e9e29e0b73ae"),
                        OrderDate = new DateTime(2021, 6, 21, 15, 31, 35),
                        OrderStatusID = Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85"),

                    },
                    //Order September 2021
                    new Order
                    {
                        OrderID = Guid.Parse("c3d6fd3c-a190-41f1-b627-0039eca8e5b1"),
                        AddressID = Guid.Parse("aad80912-434e-4924-8ddc-38e5e7c998f7"),
                        ClientID = Guid.Parse("10e21cc0-001c-438b-bb2f-445e446688dd"),
                        Delivered = false,
                        Loaded = true,
                        DeliveryTripSheetID = Guid.Parse("371fefc3-9a45-4340-92ee-e2e9b5ddbd16"),
                        DeliveryDate = new DateTime(2021, 9, 3, 15, 31, 35),
                        InvoiceID = Guid.Parse("c21d9b1b-27a1-413a-84ea-f5708133e65f"),
                        OrderDate = new DateTime(2021, 9, 2, 15, 31, 35),
                        OrderStatusID = Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85"),

                    }

                ) ;
        }
    }
}
