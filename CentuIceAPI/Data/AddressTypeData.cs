﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class AddressTypeData : IEntityTypeConfiguration<AddressType>
    {


        public void Configure(EntityTypeBuilder<AddressType> builder)
        {
            builder.ToTable("AddressTypes");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(

                      new AddressType
                      {
                          AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                          Description = "Primary Physical Address"
                      },
                      new AddressType
                      {
                          AddressTypeID = Guid.Parse("a8901b0d-4a81-440e-8707-bf052a2fef0b"),
                          Description = "Postal Address"
                      },
                      new AddressType
                      {
                          AddressTypeID = Guid.Parse("68252ff8-14f6-4c1e-8ca1-954b9e598fea"),
                          Description = "Secondary Physical Address"
                      }
                      );
        }
    }
}