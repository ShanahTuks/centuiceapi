﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Data
{
    public class EmergencyContactData : IEntityTypeConfiguration<EmergencyContact>
    {
        public void Configure(EntityTypeBuilder<EmergencyContact> builder)
        {
            builder.ToTable("EmergencyContacts");

            builder.HasData
                (
                    //Shanah Owner
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("cbd6262c-5a59-465b-85c1-222e6d9d717c"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("7f177922-a5f1-448d-8862-96fd4ba5d633"),
                        RelationshipToEmployee = "Friend"
                    },
                    //Etienne Owner
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("c95b77f9-c24c-4ba1-80ce-94abc155757c"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("14e100c5-a695-49fa-ada2-244c117df5c0"),
                        RelationshipToEmployee = "Friend"
                    },

                    // Member Emergency Contacts

                    //Melissa Admin
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("1b62b809-05df-49c1-b93a-b7bab141097b"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("bc2c2ae9-204f-49b5-8d9d-58da49b8fcd4"),
                        RelationshipToEmployee = "Friend"
                    },

                    //Naomi Admin
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("25f87857-4d8d-4388-b733-6270d27529be"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("30fc6523-661c-4479-8539-049d8cca6cba"),
                        RelationshipToEmployee = "Friend"
                    },

                    //Puvelin Admin
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("5cc44689-c836-4c8f-bab6-236cdeab8af7"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("47d01ee3-0c9b-4118-beea-c21085cf001c"),
                        RelationshipToEmployee = "Friend"
                    },

                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("ade1abfb-7178-4668-a16b-a5e76f58b067"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("f11765a0-58d2-42e5-9c5c-34022016e8c6"),
                        RelationshipToEmployee = "Friend"
                    },
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("9c5f18df-0670-440f-87d9-6f27ab53670a"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("fa5a66e7-40e8-48e6-8564-4a38e40ddadd"),
                        RelationshipToEmployee = "Friend"
                    },
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("e323616a-28e8-4b45-bf81-01e765f249ae"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("fb59a87d-59e6-49d4-979b-658eb9986162"),
                        RelationshipToEmployee = "Friend"
                    },
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("72a0827d-1797-4b63-a8fa-896cb216ccda"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("aa301feb-440f-42ae-b4a1-0c123bf074af"),
                        RelationshipToEmployee = "Friend"
                    }

                    //Driver 3
                    ,
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("6d3c9a10-4e83-474b-afa3-877200e09b75"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("63abb4b4-5d4b-446d-8471-eb3e0f7f053c"),
                        RelationshipToEmployee = "Friend"
                    },
                    //Assistant 3
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("874a4006-0d09-479b-8bbd-d7267a0f77ed"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("eac034be-cbba-4360-9e8e-8a897f16c6a6"),
                        RelationshipToEmployee = "Friend"
                    },
                    //Supervisor 3
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("adb89ec9-3cf6-4c99-babc-26c0f6f339dc"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("61cc8c8d-f487-4a7e-b78c-ab4614786586"),
                        RelationshipToEmployee = "Friend"
                    }

                    //Driver 4
                    ,
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("0fd740a0-cb3a-4f2f-96bf-77dc613bc1b5"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("4e5f0b7f-68ba-4254-b6fc-04fab6a99fce"),
                        RelationshipToEmployee = "Friend"
                    },
                    //Assistant 4
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("bc1e0042-b3e0-4c34-a006-c685dd2eb271"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("efdc266e-2dce-4021-968c-cad5adc9c9b5"),
                        RelationshipToEmployee = "Friend"
                    },
                    //Supervisor 4
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("9bc0ce49-3e8c-4397-afb1-a743266a94f8"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("376017bd-2155-4e82-a1a7-fbe7f2d49b83"),
                        RelationshipToEmployee = "Friend"
                    }

                    //Driver 5
                    ,
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("75eddcb7-a64d-4125-afbf-ffe029bd4f39"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("83fc7bdc-b730-4c4f-b13b-d40f9d282b73"),
                        RelationshipToEmployee = "Friend"
                    },
                    //Assistant 5
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("334dae15-5cad-4743-9afb-731612bbb4ca"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("2a7ecffa-8673-45c0-8b0a-df03b891dc3c"),
                        RelationshipToEmployee = "Friend"
                    },
                    //Supervisor 5
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("54e3187c-95ed-477c-aba5-07fc1a17deb9"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("40525829-4dd0-4352-a149-cea6a86ec01f"),
                        RelationshipToEmployee = "Friend"
                    }

                    //More Demo Employees 2
                    //Driver 7
                    ,
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("0134570a-89dc-4b4d-ab49-c2d4ef0bdf05"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("5b5ea95c-d324-44ef-9ece-a20e991d31f1"),
                        RelationshipToEmployee = "Friend"
                    },
                    //Assistant 7
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("5a62fb32-cb64-4fe9-ae2f-1dfdae48cd85"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("675c1342-2c24-4c4c-bf05-378ce4e5e9c5"),
                        RelationshipToEmployee = "Friend"
                    },
                    //Supervisor 7
                    new EmergencyContact
                    {
                        EmergencyContactID = Guid.Parse("5a93a8f8-4541-4ee0-888c-bcc431aaa89e"),
                        ContactPerson = "John Mulaney",
                        ContactTelephoneNumber = "0814169470",
                        EmployeeID = Guid.Parse("c152a7ba-be20-4299-88b2-3196bd37e697"),
                        RelationshipToEmployee = "Friend"
                    }

                );
        }
    }
}
