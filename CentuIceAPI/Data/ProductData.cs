﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Data
{
    public class ProductData : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Products");

            _ = builder.HasData
                (
                    new Product
                    {
                        ProductID = Guid.Parse("974e485b-d76f-4f4f-bde7-2b414435890f"),
                        InventoryID = Guid.Parse("bfb2a434-9da4-4f85-93ea-1dc51e20d07b"),
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                        ProductName = "5KG Ice Blocks",
                      
                    },
                    new Product
                    {
                        ProductID = Guid.Parse("da004ad8-2f64-4bf5-9c2f-adc9e48070bc"),
                        InventoryID = Guid.Parse("ca8de3b1-a744-4821-9537-81b90d96e153"),
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                        ProductName = "2KG Ice Blocks",
                      
                    },
                    new Product
                    {
                        ProductID = Guid.Parse("1e54f43d-efbd-4211-94ae-cee6ec34014c"),
                        InventoryID = Guid.Parse("f0c7eb5c-6603-455f-96d6-5f9de5854bb6"),
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                        ProductName = "3KG Ice Blocks",
                        
                    },
                    new Product
                    {
                        ProductID = Guid.Parse("42258ad8-9733-4d44-adae-eb2b5589cf42"),
                        InventoryID = Guid.Parse("97ec7bf3-2fd0-44de-8960-a119ec42beea"),
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                        ProductName = "1KG Ice Blocks",
                       
                    },
                    new Product
                    {
                        ProductID = Guid.Parse("629763fc-84f1-4057-bdf6-4167728af1e8"),
                        InventoryID = Guid.Parse("f7974974-613b-40da-9391-838f7dd9a4d5"),
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                        ProductName = "4KG Ice Blocks",
                       
                    }
                    ,
                    new Product
                    {
                        ProductID = Guid.Parse("6fb2b343-db52-42b0-b74d-759d0556a80b"),
                        InventoryID = Guid.Parse("d9bcb8d7-db6e-4e26-8095-484ecfab9c5d"),
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                        ProductName = "Large Wood",
                       
                    },
                    new Product
                    {
                        ProductID = Guid.Parse("a0cd82e1-670e-4988-b096-b94d008bf10a"),
                        InventoryID = Guid.Parse("98cb7fea-bfe7-46c0-a5f1-70a89b32ff33"),
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                        ProductName = "Small Wood",
                        
                    }



                );
        }
    }
}
