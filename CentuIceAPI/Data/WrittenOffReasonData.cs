﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace CentuIceAPI.Data
{
    public class WrittenOffReasonData : IEntityTypeConfiguration<WrittenOffReason>
    {
        public void Configure(EntityTypeBuilder<WrittenOffReason> builder)
        {
            builder.ToTable("WrittenOffReasons");
            _ = builder.HasData(

                    new WrittenOffReason
                    {
                        WrittenOffReasonID = Guid.Parse("b17ee000-7a47-4be5-8b59-5dea9dd67fcf"),
                        Description = "Broken Bag"
                    },
                    new WrittenOffReason
                    {
                        WrittenOffReasonID = Guid.Parse("28a065e4-cb8a-4557-8d18-a0e8f77743c4"),
                        Description = "Old Stock"
                    }

                );
        }
    }
}
