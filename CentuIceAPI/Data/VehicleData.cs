﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace CentuIceAPI.Data
{
    public class VehicleData : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder.ToTable("Vehicles");

            _ = builder.HasData(
                    new Vehicle
                    {
                        VehicleID = Guid.Parse("433253ee-d91e-4604-a627-899e657a042a"),
                        //Available
                        VehicleStatusID = Guid.Parse("65563356-8382-4c0d-8e2c-f94c98cb786b"),
                        Capacity = 500,
                        Make = "Nissan",
                        Model = "Truck Master",
                        Reg_Number = "ABC 241 NP",
                        Mileage = 100000,
                        Tracker_Number = "TRK12345",
                        Last_Service_Date = new DateTime(2021, 4, 30, 15, 31, 35),
                        Year_Made = new DateTime(2016, 8, 30, 15, 31, 35),
                        License_Expiry_Date = new DateTime(2022, 8, 30, 15, 31, 35),
                    },
                    new Vehicle
                    {
                        VehicleID = Guid.Parse("68768508-c25a-479d-b962-c1ea1cf38f09"),
                        //Available
                        VehicleStatusID = Guid.Parse("65563356-8382-4c0d-8e2c-f94c98cb786b"),
                        Capacity = 500,
                        Make = "MAN",
                        Model = "Trcuker",
                        Reg_Number = "GBV 587 WC",
                        Mileage = 100000,
                        Tracker_Number = "TRK12345",
                        Last_Service_Date = new DateTime(2021, 4, 30, 15, 31, 35),
                        Year_Made = new DateTime(2016, 8, 30, 15, 31, 35),
                        License_Expiry_Date = new DateTime(2022, 8, 30, 15, 31, 35),
                    },
                    //new Vehicles
                    new Vehicle
                    {
                        VehicleID = Guid.Parse("de1f7a9e-b8be-4753-b500-c7f6faa28377"),
                        //Available
                        VehicleStatusID = Guid.Parse("65563356-8382-4c0d-8e2c-f94c98cb786b"),
                        Capacity = 500,
                        Make = "Nissan",
                        Model = "Truckla",
                        Reg_Number = "CBA 142 NP",
                        Mileage = 100000,
                        Tracker_Number = "TRK12345",
                        Last_Service_Date = new DateTime(2021, 4, 30, 15, 31, 35),
                        Year_Made = new DateTime(2016, 8, 30, 15, 31, 35),
                        License_Expiry_Date = new DateTime(2022, 8, 30, 15, 31, 35),
                    },
                    new Vehicle
                    {
                        VehicleID = Guid.Parse("397a755a-852b-47a5-8ec8-1d32d488a588"),
                        //Available
                        VehicleStatusID = Guid.Parse("65563356-8382-4c0d-8e2c-f94c98cb786b"),
                        Capacity = 500,
                        Make = "MAN",
                        Model = "Truckornator",
                        Reg_Number = "VBG 785 CW",
                        Mileage = 100000,
                        Tracker_Number = "TRK12345",
                        Last_Service_Date = new DateTime(2021, 4, 30, 15, 31, 35),
                        Year_Made = new DateTime(2016, 8, 30, 15, 31, 35),
                        License_Expiry_Date = new DateTime(2022, 8, 30, 15, 31, 35),
                    },


                    //Group 3
                    new Vehicle
                    {
                        VehicleID = Guid.Parse("6cee0e3e-44e1-4b11-a2cc-7ac4b594918e"),
                        //Making Delivery
                        VehicleStatusID = Guid.Parse("8631cd9e-9782-4098-ae61-31847bd47f8a"),
                        Capacity = 500,
                        Make = "Toyota",
                        Model = "Super Truck",
                        Reg_Number = "BBC 123 MP",
                        Mileage = 100000,
                        Tracker_Number = "TRK12345",
                        Last_Service_Date = new DateTime(2021, 4, 30, 15, 31, 35),
                        Year_Made = new DateTime(2016, 8, 30, 15, 31, 35),
                        License_Expiry_Date = new DateTime(2022, 8, 30, 15, 31, 35),
                    },
                    //Group 4
                    new Vehicle
                    {
                        VehicleID = Guid.Parse("3ce6e093-7e02-46f3-a417-4b163eb2ddc0"),
                        //Making Delivery
                        VehicleStatusID = Guid.Parse("8631cd9e-9782-4098-ae61-31847bd47f8a"),
                        Capacity = 500,
                        Make = "Nissan",
                        Model = "Truck Master",
                        Reg_Number = "ABC 456 GP",
                        Mileage = 100000,
                        Tracker_Number = "TRK12345",
                        Last_Service_Date = new DateTime(2021, 4, 30, 15, 31, 35),
                        Year_Made = new DateTime(2016, 8, 30, 15, 31, 35),
                        License_Expiry_Date = new DateTime(2022, 8, 30, 15, 31, 35),
                    },
                    //Group 5
                    new Vehicle
                    {
                        VehicleID = Guid.Parse("e8b76b10-8175-49a9-b1f7-aa506b549cef"),
                        //Making Delivery
                        VehicleStatusID = Guid.Parse("8631cd9e-9782-4098-ae61-31847bd47f8a"),
                        Capacity = 500,
                        Make = "Nissan",
                        Model = "Truck Master",
                        Reg_Number = "CFG 245 NP",
                        Mileage = 100000,
                        Tracker_Number = "TRK12345",
                        Last_Service_Date = new DateTime(2021, 4, 30, 15, 31, 35),
                        Year_Made = new DateTime(2016, 8, 30, 15, 31, 35),
                        License_Expiry_Date = new DateTime(2022, 8, 30, 15, 31, 35),
                    }

                );
        }
    }
}
