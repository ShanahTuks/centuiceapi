﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class SupplierContactPersonData : IEntityTypeConfiguration<SupplierContactPerson>
    {


        public void Configure(EntityTypeBuilder<SupplierContactPerson> builder)
        {
            builder.ToTable("SupplierContactPeople");
            builder.Property(s => s.Name).IsRequired(true);

            _ = builder.HasData(
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("cf9067fd-02a3-4f55-83ed-b97c5b4fecc3"),
                    SupplierID = Guid.Parse("b3307864-6945-4aa0-92e7-0095e056889b"),
                    Name = "Steven",
                    PhoneNumber = "0123456789",
                    EmailAddress = "steven@gmail.com"
                },
                new SupplierContactPerson
                 {
                     SupplierContactPersonID = Guid.Parse("6b41097b-ed96-40ce-a89c-ec0c58aee382"),
                     SupplierID = Guid.Parse("1c8e1c73-f5c5-4124-9786-b87322e42f22"),
                     Name = "General",
                     PhoneNumber = "0123456789",
                     EmailAddress = "admin@gmail.com"
                 },
                new SupplierContactPerson
                 {
                      SupplierContactPersonID = Guid.Parse("d1379325-3999-4653-9aaa-a90c277493d2"),
                      SupplierID = Guid.Parse("f57dc5ed-3b02-4944-ba37-744991275bdc"),
                      Name = "JP",
                      PhoneNumber = "0123456789",
                      EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("26e63540-1f4a-421f-ad80-520114fcef35"),
                    SupplierID = Guid.Parse("0c3fc2e9-75f8-4699-ab0b-eecd79770a9f"),
                    Name = "Shonnah",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("7dc365f7-c7ae-4101-a608-942b04c28539"),
                    SupplierID = Guid.Parse("bfeadaa7-b5dd-4656-a26e-500d53ee9f8d"),
                    Name = "Christiaan",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("3ee3a783-5c81-4c03-87e3-75b3e43fa377"),
                    SupplierID = Guid.Parse("59b800f7-921a-4a1c-b626-b49004fde301"),
                    Name = "Leaine",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("718061e2-2f36-4559-8420-f8f8b6ca62c4"),
                    SupplierID = Guid.Parse("59b800f7-921a-4a1c-b626-b49004fde301"),
                    Name = "Loraine",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("d7123644-674b-4b2c-90e2-b71bdd91a2e7"),
                    SupplierID = Guid.Parse("43a88e15-d11f-4b5d-ba42-7639666ca8ee"),
                    Name = "Reception",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("2d161ca0-fe36-42a0-b568-fa8676c0b6bc"),
                    SupplierID = Guid.Parse("ecc20563-4c08-483b-bdea-54c4db6eea79"),
                    Name = "General",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("9ac8a94e-a10b-418e-a2ea-38b0b2a038e5"),
                    SupplierID = Guid.Parse("fc585747-d8d4-4fdd-a2f5-566b7abb2a0b"),
                    Name = "Robert",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("b14c26c6-e7d9-4acb-8d08-4c5f44a88849"),
                    SupplierID = Guid.Parse("b9475cf2-ca78-4533-a519-8cf60b42a2b0"),
                    Name = "Winters",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("914c0f86-1e87-4ec6-b158-96f8fc055f7b"),
                    SupplierID = Guid.Parse("2cee437e-041a-42b6-a916-6d7bf8b94267"),
                    Name = "General",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("b53bb5f4-2aae-46d9-aef4-612f11bfdd13"),
                    SupplierID = Guid.Parse("d1e8ddaf-d1b4-4876-83ac-0f67364d2510"),
                    Name = "General",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("7da76e0f-cef2-4a21-b4d9-a72a566a76ac"),
                    SupplierID = Guid.Parse("1a7575e9-13cf-43c9-96a2-90ee5d26b2c1"),
                    Name = "Joel",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("eafd605f-a258-4d6b-8ce1-872a4d2109f0"),
                    SupplierID = Guid.Parse("edb156ee-b20c-4dd6-9220-6321bd6c152e"),
                    Name = "Braam",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("10ebbd0c-5079-4efe-b583-df781707081e"),
                    SupplierID = Guid.Parse("ca300856-9dc0-4ba5-8dfb-017b71d09c36"),
                    Name = "Tyler",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("3c8190ff-eed4-4ea9-85dd-7f230a55a258"),
                    SupplierID = Guid.Parse("ceb25950-f3fc-428f-aecb-2f1f98c1a4ec"),
                    Name = "JP",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("20116db1-13c4-4b22-b4c9-7570a6e057bb"),
                    SupplierID = Guid.Parse("8f071616-125a-4ba2-8e53-1f1da3b4bfa2"),
                    Name = "Ettienne",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("dbf9314a-1a55-404d-a587-4f06fb59b6bb"),
                    SupplierID = Guid.Parse("d4996a9a-fc8b-4e30-b99f-ed7c53ee17a5"),
                    Name = "Meisie",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("59d9a399-bca8-42c5-b13e-201657bcc37e"),
                    SupplierID = Guid.Parse("2451e97d-ac43-4788-9768-ad7628b237a9"),
                    Name = "Dawn",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("3e3b327e-5041-43b8-8bda-8860fa4885ea"),
                    SupplierID = Guid.Parse("2451e97d-ac43-4788-9768-ad7628b237a9"),
                    Name = "Ettiene",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("fa89b7e0-5242-4cab-912d-11373a453f48"),
                    SupplierID = Guid.Parse("6b4de917-fef4-4cd7-9b97-a4a768ce65d5"),
                    Name = "Eugene",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("6762674e-fbc5-461c-b31a-6e6936a637ed"),
                    SupplierID = Guid.Parse("dccebd02-eb22-49d9-8cde-bcf145ef0964"),
                    Name = "Lawrence",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("3ad2af3e-ecac-4bf2-94b5-ad5bcddcc85e"),
                    SupplierID = Guid.Parse("d4db4769-4321-4de0-b0a2-f6577386efcf"),
                    Name = "Maryke",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("1203d56b-7c8d-4268-8be8-89993fcce5a6"),
                    SupplierID = Guid.Parse("132add66-6dc0-46de-ab8e-03195fcd788c"),
                    Name = "General",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("52d391d2-9893-48c2-8f58-69fde2876a08"),
                    SupplierID = Guid.Parse("8adb40e5-dde0-44cd-a21f-8a29142c54b2"),
                    Name = "Theressa",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("4d9e46e5-b5ab-4493-aa93-e8462983d8ef"),
                    SupplierID = Guid.Parse("59347180-43e2-431e-a452-abf6b2b4ce9a"),
                    Name = "General",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("20bf139c-34c3-4a65-b925-7835089f24f2"),
                    SupplierID = Guid.Parse("74e4f034-7f94-4958-93e7-4879e876170b"),
                    Name = "General",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("297a48e1-30b6-408b-b7a3-9c486cfc9a5e"),
                    SupplierID = Guid.Parse("35836f8d-5b8e-4153-ae15-595cfbee27aa"),
                    Name = "Karen",
                    PhoneNumber = "0123456789",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("1e9e3a5f-8354-42e8-943b-cfe559ce7885"),
                    SupplierID = Guid.Parse("1441f7fc-c068-48a0-ab62-dc2c9e0e8bc7"),
                    Name = "Wiets",
                    PhoneNumber = "0113102684",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("bbb8cb75-a06c-4fec-8bfd-d7d8a5e7c4ba"),
                    SupplierID = Guid.Parse("75aa1a34-bd38-4e93-b980-26be2a6d1e9b"),
                    Name = "General",
                    PhoneNumber = "0129418434",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("60fc9f8a-2aea-4c68-9ad5-89ab1549f799"),
                    SupplierID = Guid.Parse("1c4355b2-8129-41c4-b47c-4294bb733a9b"),
                    Name = "Natasha",
                    PhoneNumber = "0877239405",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("6c2973fb-3359-4008-9411-913bd6474153"),
                    SupplierID = Guid.Parse("bc978cf4-4ce3-46e5-8e5c-1c99ee0d4618"),
                    Name = "General",
                    PhoneNumber = "01119740446",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("efc38bba-5231-4242-8489-ccd6b5eb1068"),
                    SupplierID = Guid.Parse("01283a20-a3c7-407c-8715-a59752f79783"),
                    Name = "Frikkie",
                    PhoneNumber = "0126530247",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("44a09b6f-e9e0-450c-b185-8d5090578794"),
                    SupplierID = Guid.Parse("8a76c942-85c3-4de6-94d9-5a6885975113"),
                    Name = "Herman",
                    PhoneNumber = "0126538088",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("a0af06c0-fc41-47fb-a6cc-74c1d3d32756"),
                    SupplierID = Guid.Parse("9369078c-fa89-4d0e-a8d1-10d91da87e77"),
                    Name = "Ephrahim",
                    PhoneNumber = "0843998732",
                    EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                 {
                     SupplierContactPersonID = Guid.Parse("e85a72a6-d4bb-4236-b4ce-358ba7fc3497"),
                     SupplierID = Guid.Parse("9d5d963f-5940-4d21-8ecd-9c9e84aff20c"),
                     Name = "Francois",
                     PhoneNumber = "0832277741",
                     EmailAddress = "admin@gmail.com"
                },
                new SupplierContactPerson
                {
                    SupplierContactPersonID = Guid.Parse("e1c091f2-7981-4a02-8d8c-5091fa698345"),
                    SupplierID = Guid.Parse("ac9da526-396f-4974-8ca5-56a2c3f84085"),
                    Name = "Petrus",
                    PhoneNumber = "0764527752",
                    EmailAddress = "admin@gmail.com"
                }











            );
        }

    }
}
