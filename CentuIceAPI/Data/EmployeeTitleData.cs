﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class EmployeeTitleData : IEntityTypeConfiguration<EmployeeTitle>
    {


        public void Configure(EntityTypeBuilder<EmployeeTitle> builder)
        {
            builder.ToTable("EmployeeTitles");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(
                new EmployeeTitle
                {
                    EmployeeTitleID = Guid.Parse("250753ee-c818-4a50-b713-41d9c8b09002"),
                    Description = "Mr"

                },
                new EmployeeTitle
                {
                    EmployeeTitleID = Guid.Parse("68e8c902-3bd3-495c-ae81-a185bbb28696"),
                    Description = "Mrs"

                },
                new EmployeeTitle
                {
                    EmployeeTitleID = Guid.Parse("05b2bda6-1720-4517-86c7-997b574b2d7c"),
                    Description = "Miss"

                },
                new EmployeeTitle
                {
                    EmployeeTitleID = Guid.Parse("e2ce7513-a160-4207-ae7f-d1b1adeea81b"),
                    Description = "Ms"

                },
                new EmployeeTitle
                {
                    EmployeeTitleID = Guid.Parse("249cadf6-f9eb-4fc4-9c8d-bbf30fcc2b93"),
                    Description = "Dr"

                }
            );
        }

    }
}