﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Data
{
    public class NotificationTypeData : IEntityTypeConfiguration<NotificationType>
    {
        public void Configure(EntityTypeBuilder<NotificationType> builder)
        {
            builder.ToTable("NotificationTypes");

            _ = builder.HasData(
                
                    new NotificationType()
                    {
                        NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                        TypeName = "General"
                    },
                    new NotificationType()
                    {
                        NotificationTypeID = Guid.Parse("02893224-9110-4fd4-9bd8-34f804afd621"),
                        TypeName = "Auth"
                    }

                );
        }
    }
}
