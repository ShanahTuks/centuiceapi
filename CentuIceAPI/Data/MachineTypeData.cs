﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class MachineTypeData : IEntityTypeConfiguration<MachineType>
    {


        public void Configure(EntityTypeBuilder<MachineType> builder)
        {
            builder.ToTable("MachineTypes");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(
                new MachineType
                {
                    MachineTypeID = Guid.Parse("93a52232-0dca-420c-8318-bae886c0d07c"),
                    Description = "Filter"

                },
                new MachineType
                {
                    MachineTypeID = Guid.Parse("665fa397-4390-4b03-854c-f59177afdb80"),
                    Description = "Ice Machine"

                }


            );
        }

    }
}