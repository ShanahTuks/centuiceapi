﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class TableStatusData : IEntityTypeConfiguration<TableStatus>
    {


        public void Configure(EntityTypeBuilder<TableStatus> builder)
        {
            builder.ToTable("TableStatuses");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(
                new TableStatus
                {
                    TableStatusID = Guid.Parse("70813f15-2488-4c61-810b-ed1ef2e0e3a0"),
                    Description = "Pending"

                },
                new TableStatus
                {
                    TableStatusID = Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1"),
                    Description = "Deactivated"

                },
                 new TableStatus
                 {
                     TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"),
                     Description = "Active"

                 }
               


            );
        }

    }
}