﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;
namespace CentuIceAPI.Data
{
    public class VatConfigurationData : IEntityTypeConfiguration<VatConfiguration>
    {

        public void Configure(EntityTypeBuilder<VatConfiguration> builder)
        {
            builder.ToTable("VatConfigurations");
            builder.Property(s => s.VatPercentage).IsRequired(true);

            _ = builder.HasData(
                new VatConfiguration
                {
                    VatConfigurationID = Guid.Parse("a88ed992-45ba-4f71-b5da-613633a606f2"),
                    VatPercentage = 0.15m,
                    DateSet = new DateTime(2021,10,16,21,32,00),
                    Activated = false

                });
        }
    }
}
