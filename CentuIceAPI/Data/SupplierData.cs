﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class SupplierData : IEntityTypeConfiguration<Supplier>
    {


        public void Configure(EntityTypeBuilder<Supplier> builder)
        {
            builder.ToTable("Suppliers");
            builder.Property(s => s.SupplierName).IsRequired(true);

            _ = builder.HasData(
                new Supplier
                {
                    SupplierID = Guid.Parse("b3307864-6945-4aa0-92e7-0095e056889b"),
                    SupplierName = "Ice World",
                    GoodsSupplied = "Blocks Supply",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")
                },
                new Supplier
                {
                    SupplierID = Guid.Parse("1c8e1c73-f5c5-4124-9786-b87322e42f22"),
                    SupplierName = "AT&R",
                    GoodsSupplied = "Bolt Removal",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("f57dc5ed-3b02-4944-ba37-744991275bdc"),
                    SupplierName = "HGL Electronics",
                    GoodsSupplied = "Camera Maintenance",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("0c3fc2e9-75f8-4699-ab0b-eecd79770a9f"),
                    SupplierName = "Shonnah",
                    GoodsSupplied = "Charcoal, Briquettes, Firelighters",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("bfeadaa7-b5dd-4656-a26e-500d53ee9f8d"),
                    SupplierName = "Naledi Chemicals",
                    GoodsSupplied = "Cleaning Materials",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("59b800f7-921a-4a1c-b626-b49004fde301"),
                    SupplierName = "Big Box Containers",
                    GoodsSupplied = "Containers",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("43a88e15-d11f-4b5d-ba42-7639666ca8ee"),
                    SupplierName = "Dry Ice International",
                    GoodsSupplied = "Dry Ice",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("ecc20563-4c08-483b-bdea-54c4db6eea79"),
                    SupplierName = "To be Advised",
                    GoodsSupplied = "Electrical Services",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("fc585747-d8d4-4fdd-a2f5-566b7abb2a0b"),
                    SupplierName = "Walk In Freezers / Container Freezer",
                    GoodsSupplied = "Freezer Maintenance",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("b9475cf2-ca78-4533-a519-8cf60b42a2b0"),
                    SupplierName = "Winters Business Enterprise",
                    GoodsSupplied = "Freezer Repairs",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("2cee437e-041a-42b6-a916-6d7bf8b94267"),
                    SupplierName = "Builders Warehouse",
                    GoodsSupplied = "General Maintenance",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("d1e8ddaf-d1b4-4876-83ac-0f67364d2510"),
                    SupplierName = "Chamberlains",
                    GoodsSupplied = "General Maintenance",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("1a7575e9-13cf-43c9-96a2-90ee5d26b2c1"),
                    SupplierName = "Frozen Ice",
                    GoodsSupplied = "Ice Supply",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("edb156ee-b20c-4dd6-9220-6321bd6c152e"),
                    SupplierName = "Landlord",
                    GoodsSupplied = "Building Related",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("ca300856-9dc0-4ba5-8dfb-017b71d09c36"),
                    SupplierName = "Silver Leaf",
                    GoodsSupplied = "License Renewal",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")
                },
                new Supplier
                {
                    SupplierID = Guid.Parse("ceb25950-f3fc-428f-aecb-2f1f98c1a4ec"),
                    SupplierName = "Serve Ice",
                    GoodsSupplied = "Machine Maintenance Repairs and Cleaning Products",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("8f071616-125a-4ba2-8e53-1f1da3b4bfa2"),
                    SupplierName = "Minuteman Press",
                    GoodsSupplied = "Minuteman Press / Invoicing Books",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("d4996a9a-fc8b-4e30-b99f-ed7c53ee17a5"),
                    SupplierName = "PXD",
                    GoodsSupplied = "Plastic Bags - Large Wood Bags",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("2451e97d-ac43-4788-9768-ad7628b237a9"),
                    SupplierName = "Plastic and Packaging",
                    GoodsSupplied = "Plastic Bags - Marked/Branded",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("6b4de917-fef4-4cd7-9b97-a4a768ce65d5"),
                    SupplierName = "Waltloo Packaging",
                    GoodsSupplied = "Plastic Bags - Unmarked Bags 2kg Blue & 10kg Clear",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("dccebd02-eb22-49d9-8cde-bcf145ef0964"),
                    SupplierName = "Linear Plastics",
                    GoodsSupplied = "Plastic Bags / Branded Bags",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("d4db4769-4321-4de0-b0a2-f6577386efcf"),
                    SupplierName = "Waterware",
                    GoodsSupplied = "Plumbing Maintenance Parts",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("132add66-6dc0-46de-ab8e-03195fcd788c"),
                    SupplierName = "Packway International",
                    GoodsSupplied = "Sealing Machines Parts and Repairs",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("8adb40e5-dde0-44cd-a21f-8a29142c54b2"),
                    SupplierName = "Fast Print",
                    GoodsSupplied = "Stickers and Magnets",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("59347180-43e2-431e-a452-abf6b2b4ce9a"),
                    SupplierName = "Centow",
                    GoodsSupplied = "Towing Services",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("74e4f034-7f94-4958-93e7-4879e876170b"),
                    SupplierName = "Pointer SA",
                    GoodsSupplied = "Trackers",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("35836f8d-5b8e-4153-ae15-595cfbee27aa"),
                    SupplierName = "Elite Truck Rental",
                    GoodsSupplied = "Truck Rental",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("1441f7fc-c068-48a0-ab62-dc2c9e0e8bc7"),
                    SupplierName = "AC&R",
                    GoodsSupplied = "Vehicle Freezer Unit Repairs",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("75aa1a34-bd38-4e93-b980-26be2a6d1e9b"),
                    SupplierName = "Kia",
                    GoodsSupplied = "Vehicle Maintenance - Kia",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("1c4355b2-8129-41c4-b47c-4294bb733a9b"),
                    SupplierName = "Nissan",
                    GoodsSupplied = "Vehicle Maintenance - Nissan",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("bc978cf4-4ce3-46e5-8e5c-1c99ee0d4618"),
                    SupplierName = "Tyremart",
                    GoodsSupplied = "Vehicle Maintenance - Tyres, Wheel, Balancing, etc.",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("01283a20-a3c7-407c-8715-a59752f79783"),
                    SupplierName = "Advanced Automotive",
                    GoodsSupplied = "Vehicle Maintenance and Repairs - All Brands",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("8a76c942-85c3-4de6-94d9-5a6885975113"),
                    SupplierName = "The Filter Shop",
                    GoodsSupplied = "Water Filtration Filters",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("9369078c-fa89-4d0e-a8d1-10d91da87e77"),
                    SupplierName = "Ephrahim Bella Bella",
                    GoodsSupplied = "Wood Supplier / Various",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("9d5d963f-5940-4d21-8ecd-9c9e84aff20c"),
                    SupplierName = "Francois",
                    GoodsSupplied = "Wood Supplier / Various",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                },
                new Supplier
                {
                    SupplierID = Guid.Parse("ac9da526-396f-4974-8ca5-56a2c3f84085"),
                    SupplierName = "Petrus",
                    GoodsSupplied = "Wood Supplier / Rooikool",
                    TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                }

            );
        }

    }
}