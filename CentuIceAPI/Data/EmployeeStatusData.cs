﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class EmployeeStatusData : IEntityTypeConfiguration<EmployeeStatus>
    {


        public void Configure(EntityTypeBuilder<EmployeeStatus> builder)
        {
            builder.ToTable("EmployeeStatuses");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(
                new EmployeeStatus
                {
                    EmployeeStatusID = Guid.Parse("9664ce89-cfac-4904-b5eb-7dda3a038b21"),
                    Description = "Available"

                },
                new EmployeeStatus
                {
                    EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a"),
                    Description = "Not Available"

                }
            );
        }

    }
}