﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Data
{
    public class SupplierOrderLineData : IEntityTypeConfiguration<SupplierOrderLine>
    {
        public void Configure(EntityTypeBuilder<SupplierOrderLine> builder)
        {
            builder.ToTable("SupplierOrderLines");

            _ = builder.HasData(
                
                    new SupplierOrderLine
                    {
                        SupplierOrderLineID = Guid.Parse("7e70bb0e-9097-4961-b7fb-1d5c4528bf02"),
                        InventoryID = Guid.Parse("bfb2a434-9da4-4f85-93ea-1dc51e20d07b"),
                        InventoryPriceID = Guid.Parse("60339a11-fb71-445a-a88f-7245660481ba"),
                        Quantity = 500,
                        SupplierOrderID = Guid.Parse("5744cfa9-2835-467e-b146-911f3d955852"),
                    }
     

                );
        }
    }
}
