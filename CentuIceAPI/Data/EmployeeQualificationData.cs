﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class EmployeeQualificationData : IEntityTypeConfiguration<EmployeeQualification>
    {


        public void Configure(EntityTypeBuilder<EmployeeQualification> builder)
        {
            builder.ToTable("EmployeeQualifications");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(
                new EmployeeQualification
                {
                    EmployeeQualificationID = Guid.Parse("28db0bf3-8154-44df-8fc3-0be52422e42c"),
                    Description = "NQF 1 / Grade 9"
                },
                new EmployeeQualification
                {
                    EmployeeQualificationID = Guid.Parse("7eaadea2-117a-4620-9dbe-99040602f6d8"),
                    Description = "NQF 2 / Grade 10"
                },
                new EmployeeQualification
                {
                    EmployeeQualificationID = Guid.Parse("e4bd1751-dead-46f0-9328-0209e141958a"),
                    Description = "NQF 3 / Grade 11"
                },
                new EmployeeQualification
                {
                    EmployeeQualificationID = Guid.Parse("f4b1ddc9-a629-4282-96e8-31a515243487"),
                    Description = "NQF 4 / Grade 12"
                },
                new EmployeeQualification
                {
                    EmployeeQualificationID = Guid.Parse("4d170aa7-4088-4612-956a-6cc04c05196e"),
                    Description = "NQF 5 / Higher Certificates"
                },
                new EmployeeQualification
                {
                    EmployeeQualificationID = Guid.Parse("bdd214ce-57a8-4452-8fb8-854339abc531"),
                    Description = "NQF 6 / National Diploma"
                },
                new EmployeeQualification
                {
                    EmployeeQualificationID = Guid.Parse("33639587-9b4f-4afc-8a54-87e291aa5015"),
                    Description = "NQF 7 / Bachelor's Degree"
                },
                new EmployeeQualification
                {
                    EmployeeQualificationID = Guid.Parse("8b227fe0-5546-43cc-b7da-e6f6ecd87e75"),
                    Description = "NQF 8 / Honours Degree"

                },
                new EmployeeQualification
                {
                    EmployeeQualificationID = Guid.Parse("73976ce7-4f02-4274-a858-acfd8623e03c"),
                    Description = "NQF 9 / Master's Degree"

                },
                new EmployeeQualification
                {
                    EmployeeQualificationID = Guid.Parse("9000f05f-a9ad-4dd5-83e1-ea9ddd526413"),
                    Description = "NQF 10 / Doctor's Degree"
                }

            );
        }

    }
}