﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Data
{
    public class InvoiceStatusData : IEntityTypeConfiguration<InvoiceStatus>
    {
        public void Configure(EntityTypeBuilder<InvoiceStatus> builder)
        {
            builder.ToTable("InvoiceStatus");
            _ = builder.HasData(

                    new InvoiceStatus
                    {
                        InvoiceStatusID = Guid.Parse("7ad6de50-3811-462b-8118-2257149154be"),
                        Description = "Paid"
                    }, 
                    new InvoiceStatus
                    {
                        InvoiceStatusID = Guid.Parse("2d8a8f51-12d9-4f57-b94c-95a3f181457b"),
                        Description = "Not Paid"
                    },
                    new InvoiceStatus
                    {
                        InvoiceStatusID = Guid.Parse("365b7848-2cb8-4c74-93d9-40ddb17e62f1"),
                        Description = "Cancelled"
                    }
                    ,
                    new InvoiceStatus
                    {
                        InvoiceStatusID = Guid.Parse("11f27180-43bb-4ec1-8bbe-71bdadc9c675"),
                        Description = "Refunded"
                    }

                );
        }
    }
}
