﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{ 
       public class TripSheetStatusData : IEntityTypeConfiguration<TripSheetStatus>
       {

       
        public void Configure(EntityTypeBuilder<TripSheetStatus> builder)
        {
            builder.ToTable("TripSheetStatuses");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(
                new TripSheetStatus
                {
                    TripSheetStatusID = Guid.Parse("b67ed3f5-dd4d-4785-a209-97832583f88c"),
                    Description = "Out For Delivery"
                },
                new TripSheetStatus
                {
                    TripSheetStatusID = Guid.Parse("29bf4aa1-2c04-47f7-a801-046011236f43"),
                    Description = "Ready for Delivery"
                },
                new TripSheetStatus
                {
                    TripSheetStatusID = Guid.Parse("b630f0e0-8e59-4aff-a52e-bbff948eb9ff"),
                    Description = "Ready for Loading"
                },
                new TripSheetStatus
                {
                    TripSheetStatusID = Guid.Parse("7cea5c5d-9292-4442-9402-79306c0d9580"),
                    Description = "Pending"
                },
                new TripSheetStatus
                {
                    TripSheetStatusID = Guid.Parse("391710b4-dcb0-4ec3-a53c-ba64a055d7ef"),
                    Description = "Delivered"
                },
                new TripSheetStatus
                {
                    TripSheetStatusID = Guid.Parse("7f559fbe-0c0b-4985-9369-b1055769a35a"),
                    Description = "Checked In"
                },
                new TripSheetStatus
                {
                    TripSheetStatusID = Guid.Parse("0a53ed59-9c2b-44bf-a183-8f73cb7b8563"),
                    Description = "Loading Started"
                },
                new TripSheetStatus
                {
                    TripSheetStatusID = Guid.Parse("4c53fe83-fb7d-49f1-92ae-6ff72aaec10c"),
                    Description = "Cancelled"
                }
            ); 
        }

    }
}



    