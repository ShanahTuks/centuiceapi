﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class SupplierOrderStatusData : IEntityTypeConfiguration<SupplierOrderStatus>
    {


        public void Configure(EntityTypeBuilder<SupplierOrderStatus> builder)
        {
            builder.ToTable("SupplierOrderStatuses");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(
                new SupplierOrderStatus
                {
                    SupplierOrderStatusID = Guid.Parse("4f2ba603-18ad-408d-bf51-abd4f6da08f2"),
                    Description = "Sent To Supplier"

                },
                new SupplierOrderStatus
                {
                    SupplierOrderStatusID = Guid.Parse("f13b8acc-6d12-4877-b8d7-cc9f174f1734"),
                    Description = "Placed"

                },
                new SupplierOrderStatus
                {
                    SupplierOrderStatusID = Guid.Parse("20fc8be8-ad9b-4ccd-a300-02d5ef12d8a0"),
                    Description = "Received"

                },
                 new SupplierOrderStatus
                 {
                     SupplierOrderStatusID = Guid.Parse("e0a5df74-7054-422f-a397-34ad4060d2f5"),
                     Description = "Cancelled"

                 },
                 new SupplierOrderStatus
                 {
                     SupplierOrderStatusID = Guid.Parse("1793c850-80fe-429f-a2c8-eb56dac0d797"),
                     Description = "Finalized"

                 }

            );
        }

    }
}
