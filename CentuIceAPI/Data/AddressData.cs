﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class AddressData : IEntityTypeConfiguration<Address>
    {


        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.ToTable("Addresses");
            builder.Property(s => s.AddressID).IsRequired(true);

            _ = builder.HasData(
                new Address
                {
                    AddressID = Guid.Parse("6bfe6569-5f4f-4e63-8be5-d2d6ee46e601"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",


                    StreetName = "Test1",
                    StreetNumber = 1,
                    PostalCode = "0123",
                    Province = "Gauteng",
                    isActive = true

                },
                //Shanah Owner
                new Address
                {
                    AddressID = Guid.Parse("97491d4b-2fe3-4140-a310-3ed4e5bfdd3d"),
                    AddressTypeID = Guid.Parse("a8901b0d-4a81-440e-8707-bf052a2fef0b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test2",
                    StreetNumber = 1,
                    PostalCode = "0123",
                    Province = "Gauteng",
                    isActive = true
                },
                //Etienne Owner
                new Address
                {
                    AddressID = Guid.Parse("f57e6df5-6cf1-4896-9d75-e8714e8c1c2b"),
                    AddressTypeID = Guid.Parse("68252ff8-14f6-4c1e-8ca1-954b9e598fea"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test3",
                    StreetNumber = 1,
                    PostalCode = "0123",
                    Province = "Gauteng",
                    isActive = true
                },

                //Member Employee Addressses

                //Melissa Admin
                new Address
                {
                    AddressID = Guid.Parse("c520b2f5-0dac-42ee-b356-08f3e51078ed"),
                    AddressTypeID = Guid.Parse("68252ff8-14f6-4c1e-8ca1-954b9e598fea"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test3",
                    StreetNumber = 1,
                    PostalCode = "0123",
                    Province = "Gauteng",
                    isActive = true
                },

                //Naomi Admin
                new Address
                {
                    AddressID = Guid.Parse("623fba70-b4b3-4e3b-bb63-774ad268a916"),
                    AddressTypeID = Guid.Parse("68252ff8-14f6-4c1e-8ca1-954b9e598fea"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },

                //Puvelin Admin
                new Address
                {
                    AddressID = Guid.Parse("e17ff651-d643-41eb-bcff-dd05990d5015"),
                    AddressTypeID = Guid.Parse("68252ff8-14f6-4c1e-8ca1-954b9e598fea"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },

                new Address
                {
                    AddressID = Guid.Parse("5ea203e4-a74d-4056-a229-55f678266624"),
                    AddressTypeID = Guid.Parse("68252ff8-14f6-4c1e-8ca1-954b9e598fea"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test3",
                    StreetNumber = 1,
                    PostalCode = "0123",
                    Province = "Gauteng",
                    isActive = true
                },
                new Address
                {
                    AddressID = Guid.Parse("284c0140-3935-4566-9e95-51b347e42ec2"),
                    AddressTypeID = Guid.Parse("68252ff8-14f6-4c1e-8ca1-954b9e598fea"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                new Address
                {
                    AddressID = Guid.Parse("9127e563-e3d4-4694-814e-2edfa644b518"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                new Address
                {
                    AddressID = Guid.Parse("b69e2095-3ec0-438f-951e-84f44c6a06d0"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                new Address
                {
                    AddressID = Guid.Parse("e0d6c471-f8f0-448e-9028-5ea52b92927b"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                new Address
                {
                    AddressID = Guid.Parse("c20a118d-56b7-4455-82e3-12383cb171a8"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                new Address
                {
                    AddressID = Guid.Parse("aad80912-434e-4924-8ddc-38e5e7c998f7"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                new Address
                {
                    AddressID = Guid.Parse("dd44b55c-ceca-46da-9e71-53884ab12fdc"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                new Address
                {
                    AddressID = Guid.Parse("a601cc74-fefd-4a1e-908d-c814a6598708"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //Driver2
                new Address
                {
                    AddressID = Guid.Parse("53c885f8-fa5f-4314-aa51-54aea0031ae2"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //Assistant 2
                new Address
                {
                    AddressID = Guid.Parse("7c6569da-cd8d-4228-9a40-b5d6faed1487"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //Supervisor 2
                new Address
                {
                    AddressID = Guid.Parse("c632e660-2d53-4c19-bef0-084b15df97e5"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },

                //New Addresses

                //Driver3
                new Address
                {
                    AddressID = Guid.Parse("eeee2e71-60d0-4167-a357-065a7b2cc497"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //Assistant 3
                new Address
                {
                    AddressID = Guid.Parse("5fdebe6d-13d8-4720-a988-5936677f62d3"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //Supervisor 3
                new Address
                {
                    AddressID = Guid.Parse("30ee5dc5-9602-4b2a-bc25-a92039520e1c"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },

                //New Addreses 2 

                //Driver 4
                new Address
                {
                    AddressID = Guid.Parse("9a9a8c53-5c08-4c25-8147-dda25dd62fdf"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //Assistant 4
                new Address
                {
                    AddressID = Guid.Parse("7e483a7b-9f16-4c9a-8f0b-716088714836"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //Supervisor 4
                new Address
                {
                    AddressID = Guid.Parse("7a490548-2ac9-4902-bca0-7aa7b044416b"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },

                //New Addreses 3

                //Driver 5
                new Address
                {
                    AddressID = Guid.Parse("63cfb3ec-db5e-421c-9246-1edfe4740fe0"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //Assistant 5
                new Address
                {
                    AddressID = Guid.Parse("02b1fcc2-c6e2-432e-9a5d-1bbd379bf3f5"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //Supervisor 5
                new Address
                {
                    AddressID = Guid.Parse("3085c334-566d-4c8f-8432-cfc0a20f1c41"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //New Clients
                new Address
                {
                    AddressID = Guid.Parse("dc3561ab-ebd2-495c-9011-8ddb52873aba"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //New Clients
                new Address
                {
                    AddressID = Guid.Parse("d5edf3a1-c693-45a5-a519-b3b2fd037f8f"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //New Clients
                new Address
                {
                    AddressID = Guid.Parse("9551ded3-130e-43d2-9746-2b0d0e41e7a7"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //Test Invoice Client Address
                new Address
                {
                    AddressID = Guid.Parse("b896ffb8-fd96-43b8-8fcf-dab959d30503"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Invoice",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },

                //More Demo Employees 1

                //Driver 6
                new Address
                {
                    AddressID = Guid.Parse("8fe720af-306a-49f8-9216-2109b297c4ea"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //Assistant 6
                new Address
                {
                    AddressID = Guid.Parse("bf3efc94-9d1d-4173-8ff6-2dad74c85919"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //Supervisor 6
                new Address
                {
                    AddressID = Guid.Parse("dbffb135-9d1f-4075-96b4-9076a523fbf9"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Invoice",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },

                //More Demo Employees 1

                //Driver 7
                new Address
                {
                    AddressID = Guid.Parse("a359e0dd-1802-4a41-8050-d3f48cbabb75"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //Assistant 7
                new Address
                {
                    AddressID = Guid.Parse("de25e5d8-fe02-4dc1-862d-c1dbfa7e8eac"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Test4",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                },
                //Supervisor 7
                new Address
                {
                    AddressID = Guid.Parse("e3761f3f-c771-4c4f-8805-583742ad6996"),
                    AddressTypeID = Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b"),
                    CountryName = "South Africa",
                    SuburbName = "Lyttleton",
                    CityName = "Pretoria",

                    StreetName = "Invoice",
                    StreetNumber = 4,
                    PostalCode = "0124",
                    Province = "Gauteng",
                    isActive = true
                }

            );
        }

    }
}
