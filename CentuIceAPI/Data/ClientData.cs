﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class ClientData : IEntityTypeConfiguration<Client>
    {


        public void Configure(EntityTypeBuilder<Client> builder)
        {
            builder.ToTable("Clients");
            builder.Property(s => s.ClientName).IsRequired(true);

            _ = builder.HasData(

                    new Client
                    {
                        ClientID = Guid.Parse("543638f6-d06f-4a0c-b06a-96a175290244"),
                        ClientName = "CultureClub",
                        Note = null,
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")
                    },
                    new Client
                    {
                        ClientID = Guid.Parse("caa33a04-d199-4973-a375-25b7e8b157d4"),
                        ClientName = "Railways",
                        Note = "On orders",
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")
                    },
                    new Client
                    {
                        ClientID = Guid.Parse("62c66868-32e2-469c-8224-40e1a7343983"),
                        ClientName = "Shell Jean Ave",
                        Note = "Phone every Friday & Saturday",
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")
                    },
                    new Client
                    {
                        ClientID = Guid.Parse("10e21cc0-001c-438b-bb2f-445e446688dd"),
                        ClientName = "Grootvlei Drankwinkel",
                        Note = "Call Weekly",
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")
                    },
                    new Client
                    {
                        ClientID = Guid.Parse("a6e982dd-7dd5-4256-a5f4-68a0a82bc596"),
                        ClientName = "Egen Safi",
                        Note = "Phone Daily",
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")
                    },
                    new Client
                    {
                        ClientID = Guid.Parse("8c316ea4-605a-4af8-9083-dab4a533dfdc"),
                        ClientName = "Main Road Deli",
                        Note = "Check when delivering to Railways",
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")
                    },
                    //New
                    new Client
                    {
                        ClientID = Guid.Parse("dc3561ab-ebd2-495c-9011-8ddb52873aba"),
                        ClientName = "Jacaranda",
                        Note = "Check when delivering to Jacaranda",
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")
                    },
                    //New
                    new Client
                    {
                        ClientID = Guid.Parse("50f1ef0e-28a1-4afc-81ec-900ce5ffd1e6"),
                        ClientName = "Malboer",
                        Note = "Check when delivering to Malboer",
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")
                    },
                    //New
                    new Client
                    {
                        ClientID = Guid.Parse("eda60fb4-a8a9-4bdb-8105-1981562d2597"),
                        ClientName = "Gable's Liquor",
                        Note = "Check when delivering to Gable's Liquor",
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")
                    }
                    ,
                    //Test Invoice Client
                    new Client
                    {
                        ClientID = Guid.Parse("b5dd1c75-0029-4bda-be41-7fae78d0f7f0"),
                        ClientName = "Test Invoice",
                        Note = "Testing the Invoice Email",
                        TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

                    }
                );
        }
    }
}