﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;
namespace CentuIceAPI.Data
{
    public class DailyAttendanceData : IEntityTypeConfiguration<DailyAttendance>
    {

        public void Configure(EntityTypeBuilder<DailyAttendance> builder)
        {
            builder.ToTable("DailyAttendances");
            builder.Property(s => s.EmployeeID).IsRequired(true);

            _ = builder.HasData(
                //Driver7 Attendance
                new DailyAttendance
                {
                    DailyAttendanceID = Guid.Parse("956b62ed-169a-43a3-9274-4f06220f1a75"),
                    EmployeeID = Guid.Parse("5b5ea95c-d324-44ef-9ece-a20e991d31f1"),
                    Date = new DateTime(2021,10,1),
                    CheckInTime = new DateTime(2021,10,1,8,0,0),
                    CheckOutTime = new DateTime(2021,10,1,17,0,0)
                   
                },
                //Assistant7 Attendance
                new DailyAttendance
                {
                    DailyAttendanceID = Guid.Parse("6c52ac12-156e-48b2-9051-94b9912fd106"),
                    EmployeeID = Guid.Parse("675c1342-2c24-4c4c-bf05-378ce4e5e9c5"),
                    Date = new DateTime(2021, 10, 1),
                    CheckInTime = new DateTime(2021, 10, 1, 8, 0, 0),
                    CheckOutTime = new DateTime(2021, 10, 1, 17, 0, 0)

                },
                //Supervisor6 Attendance
                new DailyAttendance
                {
                    DailyAttendanceID = Guid.Parse("8087ce62-e84b-4fa4-ae24-dfa767fc793f"),
                    EmployeeID = Guid.Parse("48adbfa9-be84-4a46-8c07-a859730c35c5"),
                    Date = new DateTime(2021, 10, 1),
                    CheckInTime = new DateTime(2021, 10, 1, 8, 0, 0),
                    CheckOutTime = new DateTime(2021, 10, 1, 17, 0, 0)

                }
                ) ;
        }
    }
}
