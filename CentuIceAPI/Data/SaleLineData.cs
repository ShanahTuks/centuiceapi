﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Data
{
    public class SaleLineData : IEntityTypeConfiguration<SaleLine>
    {
        public void Configure(EntityTypeBuilder<SaleLine> builder)
        {
            builder.ToTable("SaleLines");

            _ = builder.HasData(
                //Sale March 2021
                new SaleLine
                {
                    SaleID = Guid.Parse("d7300709-4ddc-4a47-aa41-91ed4b02636f"),
                    ProductPriceID = Guid.Parse("9de02532-d97d-4840-a04d-87aeeef56d3e"),
                    ProductID = Guid.Parse("da004ad8-2f64-4bf5-9c2f-adc9e48070bc"),
                    Quantity = 600,
                },//Sale April 2021
                new SaleLine
                {
                    SaleID = Guid.Parse("fab9868a-1b84-4627-94d3-f1ca2de8ec18"),
                    ProductPriceID = Guid.Parse("64ec33d1-8bbd-4777-a6b0-716d14ef9f5d"),
                    ProductID = Guid.Parse("1e54f43d-efbd-4211-94ae-cee6ec34014c"),
                    Quantity = 600,
                },//Sale July 2021
                new SaleLine
                {
                    SaleID = Guid.Parse("e3e62c41-fba8-4f64-9c3c-b1d0d0a20ec7"),
                    ProductPriceID = Guid.Parse("199baac9-1afb-48b0-be10-e0a363db3926"),
                    ProductID = Guid.Parse("629763fc-84f1-4057-bdf6-4167728af1e8"),
                    Quantity = 600,
                },//Sale August 2021
                new SaleLine
                {
                    SaleID = Guid.Parse("2d7fe367-a9fb-4e63-b6ce-a3bb8a597f19"),
                    ProductPriceID = Guid.Parse("21b6bc5f-9c2f-4646-9491-4461e7ac156c"),
                    ProductID = Guid.Parse("6fb2b343-db52-42b0-b74d-759d0556a80b"),
                    Quantity = 600,
                }
                ); 
        }
    }
}
