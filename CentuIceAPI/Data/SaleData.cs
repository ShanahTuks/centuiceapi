﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Data
{
    public class SaleData : IEntityTypeConfiguration<Sale>
    {
        public void Configure(EntityTypeBuilder<Sale> builder)
        {
            builder.ToTable("Sales");

            _ = builder.HasData(
                //Sale March 2021
                new Sale
                {
                    SaleID = Guid.Parse("d7300709-4ddc-4a47-aa41-91ed4b02636f"),
                    SaleDate = new DateTime(2021, 3, 22, 15, 31, 35),
                    InvoiceID = Guid.Parse("9eca188d-e389-473d-b46e-ca9a03b27cf3"),
                    SaleStatusID = Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")
                },
                //Sale April 2021
                new Sale
                {
                    SaleID = Guid.Parse("fab9868a-1b84-4627-94d3-f1ca2de8ec18"),
                    SaleDate = new DateTime(2021, 4, 12, 15, 31, 35),
                    InvoiceID = Guid.Parse("d74516b2-a88d-485b-b221-adf65e094fe4"),
                    SaleStatusID = Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")
                },
                //Sale July 2021
                new Sale
                {
                    SaleID = Guid.Parse("e3e62c41-fba8-4f64-9c3c-b1d0d0a20ec7"),
                    SaleDate = new DateTime(2021, 7, 23, 15, 31, 35),
                    InvoiceID = Guid.Parse("1f68d9ba-bf99-46cb-a5a8-4a6031a434f5"),
                    SaleStatusID = Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")
                },
                //Sale August 2021
                new Sale
                {
                    SaleID = Guid.Parse("2d7fe367-a9fb-4e63-b6ce-a3bb8a597f19"),
                    SaleDate = new DateTime(2021, 8, 6, 15, 31, 35),
                    InvoiceID = Guid.Parse("d8286dfe-add6-4834-8f7d-9b2bd96ae569"),
                    SaleStatusID = Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")
                }
                );
        }
    }
}
