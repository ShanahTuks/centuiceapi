﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class EmployeeTypeData : IEntityTypeConfiguration<EmployeeType>
    {


        public void Configure(EntityTypeBuilder<EmployeeType> builder)
        {
            builder.ToTable("EmployeeTypes");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(
                new EmployeeType
                {
                    EmployeeTypeID = Guid.Parse("d9df26aa-5030-4462-9471-da78c0456785"),
                    Description = "Owner"

                },
                new EmployeeType
                {
                    EmployeeTypeID = Guid.Parse("0f3c4567-9c03-46ac-8e93-eae2fbc42080"),
                    Description = "Administrator"

                },
                 new EmployeeType
                 {
                     EmployeeTypeID = Guid.Parse("07130dec-027e-4f00-a087-4d1dc7ac032a"),
                     Description = "Assistant"

                 },
                  new EmployeeType
                  {
                      EmployeeTypeID = Guid.Parse("c525a7cc-a44c-4d62-a06a-1f690afcb19a"),
                      Description = "Supervisor"

                  },
                   new EmployeeType
                   {
                       EmployeeTypeID = Guid.Parse("567b6183-fe92-44f0-8536-d121b7ad8e4f"),
                       Description = "Driver"

                   }

            );
        }

    }
}