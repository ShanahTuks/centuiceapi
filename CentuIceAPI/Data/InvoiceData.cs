﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Data
{
    public class InvoiceData : IEntityTypeConfiguration<Invoice>
    {
        public void Configure(EntityTypeBuilder<Invoice> builder)
        {
            builder.ToTable("Invoices");

            _ = builder.HasData(

                    //Group 1 Out for Delivery
                    new Invoice
                    {
                        InvoiceID = Guid.Parse("2e17cf49-6504-4d05-983f-8128a4a0c459"),
                        InvoiceNumber = "INV00000009",
                        InvoiceDate = new DateTime(2021, 8, 30, 15, 31, 37),
                        InvoiceStatusID = Guid.Parse("2d8a8f51-12d9-4f57-b94c-95a3f181457b"),
                        PaymentMethodID = null
                    },
                    //Group 2 Loading Started
                    new Invoice
                    {
                        InvoiceID = Guid.Parse("7e4dcd3b-a893-490f-a469-ca0ea53d3bc7"),
                        InvoiceNumber = "INV00000008",
                        InvoiceDate = new DateTime(2021, 8, 30, 15, 31, 36),
                        InvoiceStatusID = Guid.Parse("2d8a8f51-12d9-4f57-b94c-95a3f181457b"),
                        PaymentMethodID = null
                    },
                    //Group 3 Ready for Delivery
                    new Invoice
                    {
                        InvoiceID = Guid.Parse("4bdbaa80-9f10-4e36-956c-7ab882c97d00"),
                        InvoiceNumber = "INV00000007",
                        InvoiceDate = new DateTime(2021, 8, 30, 15, 31, 35),
                        InvoiceStatusID = Guid.Parse("2d8a8f51-12d9-4f57-b94c-95a3f181457b"),
                        PaymentMethodID = null
                    },
                    //Sale March 2021
                    new Invoice
                    {
                        InvoiceID = Guid.Parse("9eca188d-e389-473d-b46e-ca9a03b27cf3"),
                        InvoiceNumber = "INV00000001",
                        InvoiceDate = new DateTime(2021, 3, 22, 15, 31, 35),
                        InvoiceStatusID = Guid.Parse("7ad6de50-3811-462b-8118-2257149154be"),
                        PaymentMethodID = Guid.Parse("f4efaa23-7be8-4148-b640-bbc14d6cc08b")
                    },
                    //Sale April 2021
                    new Invoice
                    {
                        InvoiceID = Guid.Parse("d74516b2-a88d-485b-b221-adf65e094fe4"),
                        InvoiceNumber = "INV00000002",
                        InvoiceDate = new DateTime(2021, 4, 12, 15, 31, 35),
                        InvoiceStatusID = Guid.Parse("7ad6de50-3811-462b-8118-2257149154be"),
                        PaymentMethodID = Guid.Parse("f4efaa23-7be8-4148-b640-bbc14d6cc08b")
                    }
                    ,
                    //Order May 2021
                    new Invoice
                    {
                        InvoiceID = Guid.Parse("6d619343-bb23-401f-b40f-3bf323a8b706"),
                        InvoiceNumber = "INV00000003",
                        InvoiceDate = new DateTime(2021, 5, 14, 15, 31, 35),
                        InvoiceStatusID = Guid.Parse("7ad6de50-3811-462b-8118-2257149154be"),
                        PaymentMethodID = Guid.Parse("f4efaa23-7be8-4148-b640-bbc14d6cc08b")
                    },
                    //Order June 2021
                    new Invoice
                    {
                        InvoiceID = Guid.Parse("5d84a526-2e46-4f9f-ba22-e9e29e0b73ae"),
                        InvoiceNumber = "INV00000004",
                        InvoiceDate = new DateTime(2021, 6, 21, 15, 31, 35),
                        InvoiceStatusID = Guid.Parse("7ad6de50-3811-462b-8118-2257149154be"),
                        PaymentMethodID = Guid.Parse("f4efaa23-7be8-4148-b640-bbc14d6cc08b")
                    },
                    //Sale July 2021
                    new Invoice
                    {
                        InvoiceID = Guid.Parse("1f68d9ba-bf99-46cb-a5a8-4a6031a434f5"),
                        InvoiceNumber = "INV00000005",
                        InvoiceDate = new DateTime(2021, 7, 23, 15, 31, 35),
                        InvoiceStatusID = Guid.Parse("7ad6de50-3811-462b-8118-2257149154be"),
                        PaymentMethodID = Guid.Parse("f4efaa23-7be8-4148-b640-bbc14d6cc08b")
                    },
                    //Sale August 2021
                    new Invoice
                    {
                        InvoiceID = Guid.Parse("d8286dfe-add6-4834-8f7d-9b2bd96ae569"),
                        InvoiceNumber = "INV00000006",
                        InvoiceDate = new DateTime(2021, 8, 6, 15, 31, 35),
                        InvoiceStatusID = Guid.Parse("7ad6de50-3811-462b-8118-2257149154be"),
                        PaymentMethodID = Guid.Parse("f4efaa23-7be8-4148-b640-bbc14d6cc08b")
                    },
                    //Order September 2021
                    new Invoice
                    {
                        InvoiceID = Guid.Parse("c21d9b1b-27a1-413a-84ea-f5708133e65f"),
                        InvoiceNumber = "INV00000010",
                        InvoiceDate = new DateTime(2021, 9, 2, 15, 31, 35),
                        InvoiceStatusID = Guid.Parse("7ad6de50-3811-462b-8118-2257149154be"),
                        PaymentMethodID = Guid.Parse("f4efaa23-7be8-4148-b640-bbc14d6cc08b")
                    }

                );
        }
    }
}
