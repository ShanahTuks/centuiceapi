﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Data
{
    public class SupplierOrderData : IEntityTypeConfiguration<SupplierOrder>
    {
        public void Configure(EntityTypeBuilder<SupplierOrder> builder)
        {
            builder.ToTable("SupplierOrders");

            _ = builder.HasData(
                    new SupplierOrder
                    {
                        SupplierOrderID = Guid.Parse("5744cfa9-2835-467e-b146-911f3d955852"),
                        SupplierID = Guid.Parse("b3307864-6945-4aa0-92e7-0095e056889b"),
                        DatePlaced = new DateTime(2021, 8 , 11),
                        DateReceived = null,
                        Details = "No details to note",
                        SupplierOrderStatusID = Guid.Parse("f13b8acc-6d12-4877-b8d7-cc9f174f1734")
                    }
                );
        }
    }
}
