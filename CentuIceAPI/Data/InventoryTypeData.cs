﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;

namespace CentuIceAPI.Data
{
    public class InventoryTypeData : IEntityTypeConfiguration<InventoryType>
    {


        public void Configure(EntityTypeBuilder<InventoryType> builder)
        {
            builder.ToTable("InventoryTypes");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(
                new InventoryType
                {
                    InventoryTypeID = Guid.Parse("dfb27012-a657-46f8-89d5-146c063c5385"),
                    Description = "Operation Inventory"

                },
                new InventoryType
                {
                    InventoryTypeID = Guid.Parse("19528739-55a2-45fd-a026-3c0e0f9d1ba1"),
                    Description = "Inventory for Sales"

                }

            );
        }

    }
}

