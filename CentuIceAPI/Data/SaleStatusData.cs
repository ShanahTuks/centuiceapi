﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CentuIceAPI.Models;
namespace CentuIceAPI.Data
{
    public class SaleStatusData : IEntityTypeConfiguration<SaleStatus>
    {
        public void Configure(EntityTypeBuilder<SaleStatus> builder)
        {
            builder.ToTable("SaleStatuses");
            builder.Property(s => s.Description).IsRequired(true);

            _ = builder.HasData(
                new SaleStatus
                {
                    SaleStatusID = Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959"),
                    Description = "Paid"

                },
                new SaleStatus
                {
                    SaleStatusID = Guid.Parse("5321a73d-ca18-4770-94e3-4cbf28adffc4"),
                    Description = "Cancelled"

                }



            );
        }

    }
}

