﻿using System;
using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CentuIceAPI.Data
{
    public class InvoiceNumberData : IEntityTypeConfiguration<InvoiceNumber>
    {
        public void Configure(EntityTypeBuilder<InvoiceNumber> builder)
        {
            builder.ToTable("InvoiceNumbers");

            _ = builder.HasData(

                    new InvoiceNumber
                    {
                        InvoiceNumberID = Guid.Parse("cd97edfb-2c97-4e49-a778-3cefcabf3b32"),
                        Number = 11
                    }

                );
        }
    }
}
