using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using CentuIceAPI.DTO;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly CentuIceDBContext _context;

        public OrderController(CentuIceDBContext context)
        {
            _context = context;
        }

        // GET: api/Order
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Order>>> GetOrders()
        {
            return await _context.Orders.ToListAsync();
        }

        // GET: api/Order/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Order>> GetOrder(Guid id)
        {
            var order = await _context.Orders.FindAsync(id);

            if (order == null)
            {
                return NotFound();
            }

            return order;
        }

        // PUT: api/Order/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrder(Guid id, Order order)
        {
            if (id != order.OrderID)
            {
                return BadRequest();
            }

            _context.Entry(order).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Order
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Order>> PostOrder(Order order)
        {
            _context.Orders.Add(order);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetOrder", new { id = order.OrderID }, order);
        }

        // DELETE: api/Order/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrder(Guid id)
        {
            var order = await _context.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }

            _context.Orders.Remove(order);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool OrderExists(Guid id)
        {
            return _context.Orders.Any(e => e.OrderID == id);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderVM>>> GetOrderVM()
        {

            try
            {
                var Orders = await _context.Orders.Include(zz => zz.OrderStatus).Include(zz => zz.Client).OrderByDescending(zz => zz.OrderDate).ToListAsync();

                List<OrderVM> orderVMs = new();

                foreach (var order in Orders)
                {
                    OrderVM orderVM = new();

                    orderVM.OrderID = order.OrderID;
                    orderVM.OrderStatus = order.OrderStatus.Description;
                    orderVM.ClientName = order.Client.ClientName;
                    orderVM.OrderDate = order.OrderDate;
                    orderVM.DeliveryDate = order.DeliveryDate;

                    orderVMs.Add(orderVM);
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "View Orders",
                    ActionData = "N/A",
                    EmployeeAction = "View",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();

                return Ok(orderVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }



        }

        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult<OrderDTO>> CreateOrder(OrderDTO orderDTO)
        {

            try
            {
                Order neworder = new();

                neworder.OrderID = Guid.NewGuid();
                neworder.ClientID = orderDTO.ClientID;
                neworder.OrderStatusID = Guid.Parse("9fb2e19a-be94-45ac-8de2-96a1d411b796");
                neworder.AddressID = orderDTO.AddressID;
                neworder.OrderDate = DateTime.Now;
                neworder.InvoiceID = Guid.NewGuid();
                neworder.DeliveryDate = orderDTO.DeliveryDate;

                var VatConfig = await _context.VatConfigurations.OrderByDescending(zz => zz.DateSet).FirstOrDefaultAsync();
                neworder.VatConfigurationID = VatConfig.VatConfigurationID;

                if (VatConfig.Activated == true)
                {
                    neworder.VatActivated = true;
                }
                else
                {
                    neworder.VatActivated = false;
                }




                _context.Orders.Add(neworder);

                Invoice newInvoice = new();

                var Number = await _context.InvoiceNumbers.FirstOrDefaultAsync();
                newInvoice.InvoiceID = Guid.Parse(neworder.InvoiceID.ToString());
                newInvoice.InvoiceDate = neworder.OrderDate;
                newInvoice.InvoiceStatusID = Guid.Parse("2d8a8f51-12d9-4f57-b94c-95a3f181457b");
                newInvoice.PaymentMethodID = null;
                newInvoice.InvoiceNumber = "INV" + Number.Number.ToString("D8");

                Number.Number++;

                _context.InvoiceNumbers.Update(Number);

                _context.Invoices.Add(newInvoice);

                foreach (var order in orderDTO.OrderLines)
                {
                    var ProductPriceID = await _context.ProductPrices.Where(zz => zz.ProductID == order.ProductID).OrderByDescending(zz => zz.Date).Select(zz => zz.ProductPriceID).FirstOrDefaultAsync();
                    var InventoryStock = await _context.Products.Where(zz => zz.ProductID == order.ProductID).Include(zz => zz.Inventory).FirstOrDefaultAsync();

                    if (InventoryStock.Inventory.Quantity < order.Quantity)
                    {
                        return Conflict(new { message = "Inventory Quantity To Little!" });
                    }
                    
                    OrderLine newLine = new();

                    newLine.OrderID = neworder.OrderID;
                    newLine.ProductID = order.ProductID;
                    newLine.Quantity = order.Quantity;
                    newLine.ProductPriceID = ProductPriceID;

                    _context.OrderLines.Add(newLine);
                }
                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Create Order",
                    ActionData = "Create Order : " + neworder.OrderID,
                    EmployeeAction = "Create",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);


                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }



        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderLineDTO>>> GetOrderLines(Guid ID)
        {
            try
            {
                var OrderLines = await _context.OrderLines.Include(zz => zz.Product).Where(zz => zz.OrderID == ID).Include(zz => zz.Order).ToListAsync();
                var Order = await _context.Orders.Include(zz => zz.VatConfiguration).Where(zz => zz.OrderID == ID).FirstOrDefaultAsync();
                List<OrderLineDTO> orderLineDTOs = new();

                foreach (var order in OrderLines)
                {
                    var ProductPrice = await _context.ProductPrices.Where(zz => zz.ProductPriceID == order.ProductPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();
                    OrderLineDTO line = new();

                    line.OrderID = order.OrderID;
                    line.ProductID = order.ProductID;
                    line.ProductName = order.Product.ProductName;
                    line.ProductPrice = ProductPrice;
                    line.Quantity = order.Quantity;


                    if (order.Product.isVatApplicable == true)
                    {
                        line.ProductVatActivated = true;
                    }
                    else
                    {
                        line.ProductVatActivated = false;
                    }

                    if (order.Order.VatActivated == true)
                    {
                        line.OrderVatActivated = true;
                    }
                    else
                    {
                        line.OrderVatActivated = false;
                    }


                    if (line.OrderVatActivated == true && line.ProductVatActivated == true)
                    {
                      

                        line.VatPercentage = Order.VatConfiguration.VatPercentage;
                    }
                    else
                    {
                        line.VatPercentage = 0;
                    }

                    orderLineDTOs.Add(line);
                }


                return Ok(orderLineDTOs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderStatus>>> GetOrderStatuses()
        {
            var Statuses = await _context.OrderStatuses.ToListAsync();

            return Ok(Statuses);
        }

        [Route("[action]")]
        [HttpPut]
        public async Task<ActionResult> CancelOrder(Guid ID, StatusDTO status)
        {
            try
            {
                var Order = await _context.Orders.Include(zz => zz.Invoice).Where(zz => zz.OrderID == ID).FirstOrDefaultAsync();

                if (Order == null)
                {
                    return NoContent();
                }
                else if (Order.Delivered == false && Order.Loaded == false)
                {
                    Order.OrderStatusID = status.OrderStatusID;

                    Order.DeliveryTripSheet = null;




                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var UserID = claimsIdentity.FindFirst("UserId").Value;
                    AuditTrail auditTrail = new()
                    {
                        AuditTrailID = Guid.NewGuid(),
                        ActionDate = DateTime.Now,
                        ActionDescription = "Cancel Order",
                        ActionData = "Cancelled Order " + Order.OrderID,
                        EmployeeAction = "Cancel",
                        EmployeeID = Guid.Parse(UserID)

                    };

                    _context.AuditTrails.Add(auditTrail);

                    Order.Invoice.InvoiceStatusID = Guid.Parse("365b7848-2cb8-4c74-93d9-40ddb17e62f1");
                    _context.Invoices.Update(Order.Invoice);
                    _context.Orders.Update(Order);
                    await _context.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return Forbid();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
                throw;
            }

            
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> GetOrderDate(Guid id)
        {
            var Order = await _context.Orders.Where(zz => zz.OrderID == id).FirstOrDefaultAsync();

            return Ok(Order);
        }

        [Route("[action]")]
        [HttpPut]
        public async Task<ActionResult> RescheduleDelivery(Guid ID, OrderRescheduleDTO delivery)
        {
            var Order = await _context.Orders.Where(zz => zz.OrderID == ID).FirstOrDefaultAsync();



            if (Order != null && Order.Delivered == false && Order.Loaded == false)
            {
                Order.DeliveryDate = delivery.NewDeliveryDate;
                Order.DeliveryTripSheetID = null;
                _context.Orders.Update(Order);

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Reschedule Delivery",
                    ActionData = "Rescheduled Delivery " + Order.OrderID,
                    EmployeeAction = "Update",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                

                await _context.SaveChangesAsync();
                return Ok();
            }
           else
            {
                return Forbid();
            }

           

        }



        }
}
