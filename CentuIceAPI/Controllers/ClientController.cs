using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using Microsoft.AspNetCore.Authorization;
using CentuIceAPI.ViewModels;
using System.Security.Claims;
using CentuIceAPI.Services.ServiceBase;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {

     
        private readonly CentuIceDBContext _context;

        public ClientController(CentuIceDBContext context)
        {
            _context = context;
        }

        // GET: api/Client
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Client>>> GetClients()
        {
            return await _context.Clients.ToListAsync();
        }

        // GET: api/Client/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Client>> GetClient(Guid id)
        {
            var client = await _context.Clients.FindAsync(id);

            if (client == null)
            {
                return NotFound();
            }

            return client;
        }

        // PUT: api/Client/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutClient(Guid id, Client client)
        {
            if (id != client.ClientID)
            {
                return BadRequest();
            }

            _context.Entry(client).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Client
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Client>> PostClient(Client client)
        {
            _context.Clients.Add(client);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetClient", new { id = client.ClientID }, client);
        }

        // DELETE: api/Client/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClient(Guid id)
        {
            var client = await _context.Clients.FindAsync(id);
            var Contact = await _context.ClientContactPeople.Where(zz => zz.ClientID == id).ToListAsync();
            var Addresses = await _context.ClientAddresses.Include(zz => zz.Address).Where(zz => zz.ClientID == id).ToListAsync();
            if (client == null)
            {
                return NotFound();
            }

            foreach (var person in Contact)
            {
                _context.ClientContactPeople.Remove(person);
            }    

            

            foreach (var address in Addresses)
            {
                _context.ClientAddresses.Remove(address);
                _context.Addresses.Remove(address.Address);
            }

            var claimsIdentity = User.Identity as ClaimsIdentity;
            var UserID = claimsIdentity.FindFirst("UserId").Value;
            AuditTrail auditTrail = new()
            {
                AuditTrailID = Guid.NewGuid(),
                ActionDate = DateTime.Now,
                ActionDescription = "Delete Client",
                ActionData = "Delete Client " + client.ClientName,
                EmployeeAction = "Delete",
                EmployeeID = Guid.Parse(UserID)

            };

            _context.AuditTrails.Add(auditTrail);

            _context.Clients.Remove(client);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ClientExists(Guid id)
        {
            return _context.Clients.Any(e => e.ClientID == id);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<ClientDetailsVM>> GetClientDetailsVM(Guid id)
        {

            try
            {
                var Client = await _context.Clients.Where(zz => zz.ClientID == id).FirstOrDefaultAsync();
                var Addresses = await _context.ClientAddresses.Include(zz => zz.Address).Where(zz => zz.ClientID == id && zz.Address.isActive == true).ToListAsync();
                var Contacts = await _context.ClientContactPeople.Where(zz => zz.ClientID == id).ToListAsync();



                List<Addresses> allAddresses = new();

                foreach (var address in Addresses)
                {
                    Addresses currentAddress = new();

                    currentAddress.CityName = address.Address.CityName;
                    currentAddress.SuburbName = address.Address.SuburbName;
                    currentAddress.StreetNumber = address.Address.StreetNumber;
                    currentAddress.StreetName = address.Address.StreetName;
                    currentAddress.PostalCode = address.Address.PostalCode;
                    currentAddress.ComplexName = address.Address.ComplexName;
                    currentAddress.Province = address.Address.Province;
                    currentAddress.UnitNumber = address.Address.UnitNumber;
                    currentAddress.CountryName = address.Address.CountryName;


                    allAddresses.Add(currentAddress);
                }

                List<ClientContactPerson> allContacts = new();

                foreach (var contact in Contacts)
                {
                    ClientContactPerson Person = new();

                    Person.Name = contact.Name;
                    Person.PhoneNumber = contact.PhoneNumber;
                    Person.EmailAddress = contact.EmailAddress;

                    allContacts.Add(Person);


                }


                ClientDetailsVM clientVM = new();

                clientVM.ClientID = Client.ClientID;
                clientVM.ClientName = Client.ClientName;
                clientVM.Note = Client.Note;
                clientVM.Addresses = allAddresses;
                clientVM.ContactPeople = allContacts;


                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;

                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Viewing Client Details",
                    ActionData = "Viewing Details of Client " + clientVM.ClientName,
                    EmployeeAction = "View",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();




                return Ok(clientVM);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }



        }


        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClientVM>>> GetClientsVM()
        {


            try
            {
                var ClientList = await _context.Clients.Where(zz=> zz.TableStatusID != Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1")).OrderBy(zz => zz.ClientName).ToListAsync();


                List<ClientVM> ClientVMList = new();



                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;

                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Viewing Client List",
                    ActionData = "N/A",
                    EmployeeAction = "View",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();

                foreach (var client in ClientList)
                {

                    try
                    {
                        var ClientContact = await _context.ClientContactPeople.Where(zz => zz.ClientID == client.ClientID).FirstOrDefaultAsync();

                        ClientVM clientVM = new();

                        clientVM.ClientID = client.ClientID;
                        clientVM.ClientName = client.ClientName;
                        clientVM.ContactPerson = ClientContact.Name;
                        clientVM.PhoneNumber = ClientContact.PhoneNumber;
                        clientVM.EmailAddress = ClientContact.EmailAddress;


                        ClientVMList.Add(clientVM);

                       


                    }
                    catch (InvalidCastException error)
                    {
                        return StatusCode(500, error);
                    }







                }

                

                

                return Ok(ClientVMList);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }



        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClientContactPerson>>> GetContacts(Guid ID)
        {
            var ContactList = await _context.ClientContactPeople.Where(zz => zz.ClientID == ID).ToListAsync();

            return Ok(ContactList);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClientAddressVM>>> GetAddresses(Guid ID)
        {

            try
            {
                List<ClientAddressVM> AddressList = new();

                var Addresses = await _context.ClientAddresses.Include(zz => zz.Address).Where(zz => zz.ClientID == ID && zz.Address.isActive == true).ToListAsync();

                foreach (var Address in Addresses)
                {

                    ClientAddressVM CurrentAddress = new();

                    CurrentAddress.AddressID = Address.AddressID;
                    CurrentAddress.AddressTypeID = Address.Address.AddressTypeID;
                    CurrentAddress.StreetNumber = Address.Address.StreetNumber;
                    CurrentAddress.StreetName = Address.Address.StreetName;
                    CurrentAddress.PostalCode = Address.Address.PostalCode;
                    CurrentAddress.CountryName = Address.Address.CountryName;
                    CurrentAddress.SuburbName = Address.Address.SuburbName;
                    CurrentAddress.CityName = Address.Address.CityName;
                    CurrentAddress.Province = Address.Address.Province;
                    CurrentAddress.ComplexName = Address.Address.ComplexName;
                    CurrentAddress.UnitNumber = Address.Address.UnitNumber;
                    CurrentAddress.isActive = Address.Address.isActive;


                    AddressList.Add(CurrentAddress);
                }

                return Ok(AddressList);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }

        [Route("[action]")]
        [HttpPut]
        public async Task<IActionResult> UpdateClient(FullClientVM clientVM, Guid id)
        {
           

            var Client = await _context.Clients.AsNoTracking().Where(zz => zz.ClientID == id).FirstOrDefaultAsync();



            clientVM.Addresses.ForEach(address =>
            {
                var currentAddress = _context.Addresses.AsNoTracking().Where(zz => zz.AddressID == address.AddressID).FirstOrDefault();

                if (currentAddress == null)
                {
                    address.AddressID = Guid.NewGuid();
                    address.isActive = true;
                    ClientAddress clientAddress = new()
                    {
                        AddressID = address.AddressID,
                        ClientID = clientVM.Client.ClientID
                    };

                    _context.Addresses.Add(address);
                    _context.ClientAddresses.Add(clientAddress);
                }
                else
                {
                     
                    _context.Addresses.Update(address);
                }
            });

            clientVM.Contacts.ForEach(contact =>
            {
                var currentContact = _context.ClientContactPeople.AsNoTracking().Where(zz => zz.ClientContactPersonID == contact.ClientContactPersonID).FirstOrDefault();

                if (currentContact == null)
                {
                    contact.ClientContactPersonID = Guid.NewGuid();
                    contact.ClientID = clientVM.Client.ClientID;
                    _context.ClientContactPeople.Add(contact);
                }
                else
                {
                    _context.ClientContactPeople.Update(contact);
                }
            });

            try
            {
                if (Client != null)
                {

                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var UserID = claimsIdentity.FindFirst("UserId").Value;

                    AuditTrail auditTrail = new()
                    {
                        AuditTrailID = Guid.NewGuid(),
                        ActionDate = DateTime.Now,
                        ActionDescription = "Updating Client Details",
                        ActionData = "Updating Details of Client " + Client.ClientName,
                        EmployeeAction = "Update",
                        EmployeeID = Guid.Parse(UserID)

                    };

                    _context.AuditTrails.Add(auditTrail);

                    _context.Clients.Update(clientVM.Client);

                    _context.SaveChanges();
                    return Ok();

                }
                
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();

        }


        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<TableStatus>>> GetTableStatuses()
        {
            return await _context.TableStatuses.ToListAsync();
        }


        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> CreateClient(FullClientVM clientVM)
        {

            try
            {
                clientVM.Client.ClientID = Guid.NewGuid();
                

                foreach (var contact in clientVM.Contacts)
                {
                    contact.ClientID = clientVM.Client.ClientID;
                    contact.ClientContactPersonID = Guid.NewGuid();

                    _context.ClientContactPeople.Add(contact);
                }


                foreach (var address in clientVM.Addresses)
                {
                    address.AddressID = Guid.NewGuid();
                    address.isActive = true;

                    var clientAddress = new ClientAddress
                    {
                        AddressID = address.AddressID,
                        ClientID = clientVM.Client.ClientID
                    };

                    _context.ClientAddresses.Add(clientAddress);
                    _context.Addresses.Add(address);



                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;

                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Creating Client",
                    ActionData = "Created Client " + clientVM.Client.ClientName,
                    EmployeeAction = "Create",
                    EmployeeID = Guid.Parse(UserID)

                };
                _context.AuditTrails.Add(auditTrail);


                _context.Clients.Add(clientVM.Client);

                await _context.SaveChangesAsync();



                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [Route("[action]")]
        [HttpDelete]
        public async Task<IActionResult> DeactivateClient(Guid id)
        {
            try
            {
                var client = await _context.Clients.Where(zz => zz.ClientID == id).FirstOrDefaultAsync();

                if (client == null)
                {
                    return NotFound();
                }
                else
                {
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var UserID = claimsIdentity.FindFirst("UserId").Value;

                    AuditTrail auditTrail = new()
                    {
                        AuditTrailID = Guid.NewGuid(),
                        ActionDate = DateTime.Now,
                        ActionDescription = "Deactivated Client",
                        ActionData = "Deactivated Client " + client.ClientName,
                        EmployeeAction = "Deactivate",
                        EmployeeID = Guid.Parse(UserID)

                    };
                    _context.AuditTrails.Add(auditTrail);

                    client.TableStatusID = Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1");
                    _context.Clients.Update(client);
                    _context.SaveChanges();
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
           



            
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<ClientDropdownVM>>> GetClientDropdown()
        {
            var Clients = await _context.Clients.Where(zz => zz.TableStatusID != Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1")).OrderBy(zz => zz.ClientName).ToListAsync();


            List<ClientDropdownVM> clientDropdownVMs = new();

            foreach (var client in Clients)
            {
                ClientDropdownVM clientDropdownVM = new();

                clientDropdownVM.ClientID = client.ClientID;
                clientDropdownVM.ClientName = client.ClientName;

                clientDropdownVMs.Add(clientDropdownVM);
                
            }

            return Ok(clientDropdownVMs);
           


        }
        [HttpDelete]
        [Route("[action]")]
        public async Task<IActionResult> DeleteAddress(Guid id)
        {
            var address = await _context.Addresses
                .AsNoTracking()
                .Where(a => a.AddressID == id)
                .FirstOrDefaultAsync();

            var ClientAddress = await _context.ClientAddresses
                .AsNoTracking()
                .Where(ea => ea.AddressID == id).Include(zz => zz.Client)
                .FirstOrDefaultAsync();

            var OrderExists = await _context.Orders.Where(zz => zz.AddressID == id).FirstOrDefaultAsync();

            if (OrderExists != null)
            {
                return Conflict(new { message = "Order Exists" });
            }

            if (address != null)
            {
                _context.ClientAddresses.Remove(ClientAddress);
                _context.Addresses.Remove(address);
                try
                {
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var UserID = claimsIdentity.FindFirst("UserId").Value;
                    AuditTrail auditTrail = new()
                    {
                        AuditTrailID = Guid.NewGuid(),
                        ActionDate = DateTime.Now,
                        ActionDescription = "Deleted Address",
                        ActionData = "Removed Address " + ClientAddress.Address.StreetNumber + ClientAddress.Address.StreetName  + "From Client " + ClientAddress.Client.ClientName,
                        EmployeeAction = "Delete",
                        EmployeeID = Guid.Parse(UserID)

                    };

                    _context.AuditTrails.Add(auditTrail);

                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    return StatusCode(500);
                    throw;
                }
            }

            return Ok();
        }

        [HttpDelete]
        [Route("[action]")]
        public async Task<IActionResult> DeleteContactPerson(Guid id)
        {
            try
            {
                var contact = await _context.ClientContactPeople.Include(zz => zz.Client).Where(zz => zz.ClientContactPersonID == id).FirstOrDefaultAsync();

                if (contact != null)
                {
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var UserID = claimsIdentity.FindFirst("UserId").Value;

                    AuditTrail auditTrail = new()
                    {
                        AuditTrailID = Guid.NewGuid(),
                        ActionDate = DateTime.Now,
                        ActionDescription = "Deleted Contact Person",
                        ActionData = "Deleted Contacted Person " + contact.Name + "From Client " + contact.Client.ClientName,
                        EmployeeAction = "Delete",
                        EmployeeID = Guid.Parse(UserID)

                    };

                    _context.AuditTrails.Add(auditTrail);

                    _context.ClientContactPeople.Remove(contact);
                    await _context.SaveChangesAsync();
                }

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }


        [HttpDelete]
        [Route("[action]")]
        public async Task<IActionResult> HideAddress(Guid ID)
        {
           try
            {
                var Address = await _context.Addresses.Where(zz => zz.AddressID == ID).FirstOrDefaultAsync();
                Address.isActive = false;

                _context.Update(Address);

                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> GetOrdersForClient(Guid ID)
        {
            try
            {
                var OrdersInProcess = await _context.Orders.Where(zz => zz.ClientID == ID && zz.OrderStatusID != Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85") && zz.OrderStatusID != Guid.Parse("aef36c5f-795c-4d54-a840-8d005fad64c2")).ToListAsync();
                var AllOrders =  await _context.Orders.Where(zz => zz.ClientID == ID).ToListAsync();
               
                if (AllOrders.Count == 0)
                {
                    //Delete
                    return Ok();
                }
                else if (OrdersInProcess.Count > 0)
                {
                    //Do Nothing
                    return Conflict(new { message = "Order in Process" });
                }
                else if (OrdersInProcess.Count == 0 && AllOrders.Count > 0)
                {
                    //Deactivate
                    return Conflict(new { message = "Deactivate" });
                }


                return Ok();
                


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }





    }

    
}
