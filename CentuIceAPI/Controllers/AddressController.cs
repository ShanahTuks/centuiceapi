﻿using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AddressController : ControllerBase
    {

        private readonly CentuIceDBContext _context;

        public AddressController(CentuIceDBContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<AddressType>>> GetAddressTypes()
        {
            return await _context.AddressTypes.ToListAsync();
        }

        [HttpGet]
       


       



        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClientAddressVM>>> GetAddresses(Guid ID)
        {

            try
            {
                List<ClientAddressVM> AddressList = new();

                var Addresses = await _context.ClientAddresses.Include(zz => zz.Address).Where(zz => zz.ClientID == ID && zz.Address.isActive == true).ToListAsync();

                foreach (var Address in Addresses)
                {

                    ClientAddressVM CurrentAddress = new();

                    CurrentAddress.AddressID = Address.AddressID;
                    CurrentAddress.AddressTypeID = Address.Address.AddressTypeID;
                    CurrentAddress.ComplexName = Address.Address.ComplexName;
                    CurrentAddress.StreetNumber = Address.Address.StreetNumber;
                    CurrentAddress.StreetName = Address.Address.StreetName;
                    CurrentAddress.PostalCode = Address.Address.PostalCode;
                    CurrentAddress.CountryName = Address.Address.CountryName;
                    CurrentAddress.SuburbName = Address.Address.SuburbName;
                    CurrentAddress.CityName = Address.Address.CityName;
                    CurrentAddress.UnitNumber = Address.Address.UnitNumber;



                    AddressList.Add(CurrentAddress);
                }

                return Ok(AddressList);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }



    }
}
