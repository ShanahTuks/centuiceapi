﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using CentuIceAPI.DTO;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CentuIceAPI.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly CentuIceDBContext _context;

        public DashboardController(CentuIceDBContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<DashboardSalesVM>>> GetSalesDataCurrentYear()
        {

            try
            {
                DateTime currentYear = DateTime.Now;

                if (currentYear.Month >= 01 && currentYear.Month <= 02)
                {
                    var year = currentYear.Year;
                    year--;

                    currentYear = new DateTime(year, 01, 12);
                }
                var OrderListMarch = await _context.Orders.Where(zz => zz.OrderDate.Year == currentYear.Year && zz.OrderDate.Month == 03 && zz.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).ToListAsync();
                var OrderListApril = await _context.Orders.Where(zz => zz.OrderDate.Year == currentYear.Year && zz.OrderDate.Month == 04 && zz.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).ToListAsync();
                var OrderListMay = await _context.Orders.Where(zz => zz.OrderDate.Year == currentYear.Year && zz.OrderDate.Month == 05 && zz.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).ToListAsync();
                var OrderListJun = await _context.Orders.Where(zz => zz.OrderDate.Year == currentYear.Year && zz.OrderDate.Month == 06 && zz.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).ToListAsync();
                var OrderListJul = await _context.Orders.Where(zz => zz.OrderDate.Year == currentYear.Year && zz.OrderDate.Month == 07 && zz.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).ToListAsync();
                var OrderListAug = await _context.Orders.Where(zz => zz.OrderDate.Year == currentYear.Year && zz.OrderDate.Month == 08 && zz.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).ToListAsync();
                var OrderListSep = await _context.Orders.Where(zz => zz.OrderDate.Year == currentYear.Year && zz.OrderDate.Month == 09 && zz.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).ToListAsync();
                var OrderListOct = await _context.Orders.Where(zz => zz.OrderDate.Year == currentYear.Year && zz.OrderDate.Month == 10 && zz.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).ToListAsync();
                var OrderListNov = await _context.Orders.Where(zz => zz.OrderDate.Year == currentYear.Year && zz.OrderDate.Month == 11 && zz.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).ToListAsync();
                var OrderListDec = await _context.Orders.Where(zz => zz.OrderDate.Year == currentYear.Year && zz.OrderDate.Month == 12 && zz.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).ToListAsync();
                var OrderListJan = await _context.Orders.Where(zz => zz.OrderDate.Year == currentYear.Year + 1 && zz.OrderDate.Month == 01 && zz.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).ToListAsync();
                var OrderListFeb = await _context.Orders.Where(zz => zz.OrderDate.Year == currentYear.Year + 1 && zz.OrderDate.Month == 02 && zz.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).ToListAsync();

                var SaleListMarch = await _context.Sales.Where(zz => zz.SaleDate.Year == currentYear.Year && zz.SaleDate.Month == 03 && zz.SaleStatusID == Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")).ToListAsync();
                var SaleListApril = await _context.Sales.Where(zz => zz.SaleDate.Year == currentYear.Year && zz.SaleDate.Month == 04 && zz.SaleStatusID == Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")).ToListAsync();
                var SaleListMay = await _context.Sales.Where(zz => zz.SaleDate.Year == currentYear.Year && zz.SaleDate.Month == 05 && zz.SaleStatusID == Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")).ToListAsync();
                var SaleListJun = await _context.Sales.Where(zz => zz.SaleDate.Year == currentYear.Year && zz.SaleDate.Month == 06 && zz.SaleStatusID == Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")).ToListAsync();
                var SaleListJul = await _context.Sales.Where(zz => zz.SaleDate.Year == currentYear.Year && zz.SaleDate.Month == 07 && zz.SaleStatusID == Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")).ToListAsync();
                var SaleListAug = await _context.Sales.Where(zz => zz.SaleDate.Year == currentYear.Year && zz.SaleDate.Month == 08 && zz.SaleStatusID == Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")).ToListAsync();
                var SaleListSep = await _context.Sales.Where(zz => zz.SaleDate.Year == currentYear.Year && zz.SaleDate.Month == 09 && zz.SaleStatusID == Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")).ToListAsync();
                var SaleListOct = await _context.Sales.Where(zz => zz.SaleDate.Year == currentYear.Year && zz.SaleDate.Month == 10 && zz.SaleStatusID == Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")).ToListAsync();
                var SaleListNov = await _context.Sales.Where(zz => zz.SaleDate.Year == currentYear.Year && zz.SaleDate.Month == 11 && zz.SaleStatusID == Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")).ToListAsync();
                var SaleListDec = await _context.Sales.Where(zz => zz.SaleDate.Year == currentYear.Year && zz.SaleDate.Month == 12 && zz.SaleStatusID == Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")).ToListAsync();
                var SaleListJan = await _context.Sales.Where(zz => zz.SaleDate.Year == currentYear.Year + 1 && zz.SaleDate.Month == 01 && zz.SaleStatusID == Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")).ToListAsync();
                var SaleListFeb = await _context.Sales.Where(zz => zz.SaleDate.Year == currentYear.Year + 1 && zz.SaleDate.Month == 02 && zz.SaleStatusID == Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")).ToListAsync();


                List<List<Order>> orders = new();
                List<List<Sale>> sales = new();

                orders.Add(OrderListMarch);
                orders.Add(OrderListApril);
                orders.Add(OrderListMay);
                orders.Add(OrderListJun);
                orders.Add(OrderListJul);
                orders.Add(OrderListAug);
                orders.Add(OrderListSep);
                orders.Add(OrderListOct);
                orders.Add(OrderListNov);
                orders.Add(OrderListDec);
                orders.Add(OrderListJan);
                orders.Add(OrderListFeb);

                sales.Add(SaleListMarch);
                sales.Add(SaleListApril);
                sales.Add(SaleListMay);
                sales.Add(SaleListJun);
                sales.Add(SaleListJul);
                sales.Add(SaleListAug);
                sales.Add(SaleListSep);
                sales.Add(SaleListOct);
                sales.Add(SaleListNov);
                sales.Add(SaleListDec);
                sales.Add(SaleListJan);
                sales.Add(SaleListFeb);


                List<DashboardSalesVM> dashboardSalesVMs = new();
                for (int x = 0; x < 12; x++)
                {
                    DashboardSalesVM dashboard = new();

                    foreach (var order in orders[x])
                    {
                        dashboard.Month = order.OrderDate.Month.ToString();
                        var OrderLine = await _context.OrderLines.Include(zz => zz.Order).Include(zz => zz.Product).Where(zz => zz.OrderID == order.OrderID).ToListAsync();
                        var CurrentVat = await _context.Orders.Include(zz => zz.VatConfiguration).Where(zz => zz.OrderID == order.OrderID).Select(zz => zz.VatConfiguration.VatPercentage).FirstOrDefaultAsync();
                     

                        foreach (var line in OrderLine)
                        {
                            var Price = await _context.ProductPrices.Where(zz => zz.ProductPriceID == line.ProductPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();

                            if (line.Order.VatActivated == true)
                            {
                                if (line.Product.isVatApplicable == true)
                                {
                                    var total = Price * line.Quantity;
                                    dashboard.Total += total * ((100 + CurrentVat * 100) / 100);
                                }
                                else
                                {
                                    dashboard.Total += Price * line.Quantity;
                                }
                            }
                            else
                            {
                                dashboard.Total += Price * line.Quantity;
                            }
                            
                        }
                    }

                    foreach (var sale in sales[x])
                    {
                        
                        var SaleLine = await _context.SaleLines.Include(zz => zz.Sale).Include(zz => zz.Product).Where(zz => zz.SaleID == sale.SaleID).ToListAsync();
                        var CurrentVat = await _context.Sales.Include(zz => zz.VatConfiguration).Where(zz => zz.SaleID == sale.SaleID).Select(zz => zz.VatConfiguration.VatPercentage).FirstOrDefaultAsync();
                       

                        foreach (var line in SaleLine)
                        {

                            var Price = await _context.ProductPrices.Where(zz => zz.ProductPriceID == line.ProductPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();

                            if (line.Sale.VatActivated == true)
                            {
                                if (line.Product.isVatApplicable == true)
                                {
                                    var total = Price * line.Quantity;
                                    dashboard.Total += total * ((100 + CurrentVat * 100) / 100);
                                }
                                else
                                {
                                    dashboard.Total += Price * line.Quantity;
                                }
                            }
                            else
                            {
                                dashboard.Total += Price * line.Quantity;
                            }
                            
                        }
                    }

                    dashboardSalesVMs.Add(dashboard);
                }


                return Ok(dashboardSalesVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<DashboardNotificationsVM>>> GetNotifications(Guid id)
        {

            try
            {
                var NotificationList = await _context.Notifications.Where(zz => zz.NotificationTypeID == Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5") && zz.EmployeeID == id).Include(zz => zz.Employee).ToListAsync();

                List<DashboardNotificationsVM> dashboardNotificationsVMs = new();

                foreach (var notification in NotificationList)
                {
                    DashboardNotificationsVM dashboard = new();

                    dashboard.NotificationID = notification.NotificationID;
                    dashboard.EmployeeName = notification.Employee.FirstName + " " + notification.Employee.Surname;
                    dashboard.NotificationTitle = notification.NotificationTitle;
                    dashboard.NotificationDescription = notification.NotificationDescription;
                    dashboard.isRead = notification.IsRead;
                    dashboard.Created = notification.Created;

                    dashboardNotificationsVMs.Add(dashboard);
                }

                return Ok(dashboardNotificationsVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<DashboardNotificationsVM>>> UpdateRead(Guid id,DashboardNotificationsVM dashboard)
        {
            try
            {
                var Notification = await _context.Notifications.Where(zz => zz.NotificationID == id).FirstOrDefaultAsync();


                if (Notification == null)
                {
                    return NoContent();
                }
                else
                {
                    Notification.IsRead = dashboard.isRead;

                    _context.Update(Notification);

                    await _context.SaveChangesAsync();
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
           
            
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<DashboardOrdersVM>>> GetOrderList()
        {

            try
            {
                var Orders = await _context.Orders.Include(zz => zz.Client).Include(zz => zz.OrderStatus).Include(zz => zz.Address).Where(zz => zz.OrderDate.Date == DateTime.Now.Date).ToListAsync();


                List<DashboardOrdersVM> dashboardOrdersVMs = new();

                foreach (var order in Orders)
                {
                    DashboardOrdersVM dashboard = new();
                    dashboard.OrderID = order.OrderID;
                    dashboard.ClientName = order.Client.ClientName;
                    dashboard.Address = order.Address.StreetNumber + " " + order.Address.StreetName + " " + order.Address.CityName;
                    dashboard.OrderStatus = order.OrderStatus.Description;


                    var OrderLines = await _context.OrderLines.Include(zz => zz.Product).Where(zz => zz.OrderID == order.OrderID).ToListAsync();

                    foreach (var line in OrderLines)
                    {
                        var Price = await _context.ProductPrices.Where(zz => zz.ProductPriceID == line.ProductPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();
                        dashboard.OrderTotal += Price * line.Quantity;
                    }

                    dashboardOrdersVMs.Add(dashboard);
                }

                return Ok(dashboardOrdersVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<DashboardVehicleVM>>> GetVehicleData()
        {
            try
            {
                var TotalVehicles = await _context.Vehicles.CountAsync();
                var OutForDelivery = await _context.Vehicles.Where(zz => zz.VehicleStatusID == Guid.Parse("8631cd9e-9782-4098-ae61-31847bd47f8a")).CountAsync();
                var Deactivated = await _context.Vehicles.Where(zz => zz.VehicleStatusID == Guid.Parse("6ffce810-cbb1-4192-87d6-378487d19913")).CountAsync();
                var BeingServiced = await _context.Vehicles.Where(zz => zz.VehicleStatusID == Guid.Parse("e7b83455-818c-409a-9742-bf66b34ed341")).CountAsync();
                var Available = await _context.Vehicles.Where(zz => zz.VehicleStatusID == Guid.Parse("65563356-8382-4c0d-8e2c-f94c98cb786b")).CountAsync();
                var Pending = await _context.Vehicles.Where(zz => zz.VehicleStatusID == Guid.Parse("b0acee21-0379-44a7-96d9-1a04847d6f71")).CountAsync();

                DashboardVehicleVM vehicleVM = new();

                vehicleVM.Available = Available;
                vehicleVM.BeingServiced = BeingServiced;
                vehicleVM.Deactivated = Deactivated;
                vehicleVM.Pending = Pending;
                vehicleVM.TotalVehicles = TotalVehicles;
                vehicleVM.OutForDelivery = OutForDelivery;

                return Ok(vehicleVM);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<Notification>>> GetNotificationDetails(Guid ID)
        {
            try
            {
                var Notification = await _context.Notifications.Where(zz => zz.NotificationID == ID).FirstOrDefaultAsync();


                return Ok(Notification);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
           
        }

        [HttpDelete]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<Notification>>> DeleteNotification(Guid ID)
        {
            try
            {
                var Notification = await _context.Notifications.Where(zz => zz.NotificationID == ID).FirstOrDefaultAsync();


                if (Notification != null)
                {
                    _context.Notifications.Remove(Notification);
                    await _context.SaveChangesAsync();

                }

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
         
        }

    }
}
