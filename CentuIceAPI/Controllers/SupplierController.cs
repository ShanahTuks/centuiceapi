using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SupplierController : ControllerBase
    {
        private readonly CentuIceDBContext _context;

        public SupplierController(CentuIceDBContext context)
        {
            _context = context;
        }

        // GET: api/Supplier
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Supplier>>> GetSuppliers()
        {

            var claimsIdentity = User.Identity as ClaimsIdentity;
            var UserID = claimsIdentity.FindFirst("UserId").Value;
            AuditTrail auditTrail = new()
            {
                AuditTrailID = Guid.NewGuid(),
                ActionDate = DateTime.Now,
                ActionDescription = "View Suppliers",
                ActionData = "N/A",
                EmployeeAction = "View",
                EmployeeID = Guid.Parse(UserID)

            };

            _context.AuditTrails.Add(auditTrail);


            

            await _context.SaveChangesAsync();

            return await _context.Suppliers.Where(zz => zz.TableStatusID != Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1")).OrderBy(zz => zz.SupplierName).ToListAsync();
        }

        // GET: api/Supplier/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Supplier>> GetSupplier(Guid id)
        {
            var supplier = await _context.Suppliers
                .Where(s => s.SupplierID == id)
                .FirstOrDefaultAsync();

            if (supplier == null)
            {
                return NotFound();
            }

            return supplier;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<SupplierDropdownVM>>> GetSupplierDropdown()
        {
            var supplierList = await _context.Suppliers.Where(zz => zz.TableStatusID != Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1")).OrderBy(zz => zz.SupplierName).ToListAsync();

            List<SupplierDropdownVM> SupplierDropdownVMList = new();

            supplierList.ForEach((supplier) =>
            {
                SupplierDropdownVM supplierDropdownVM = new()
                {
                    SupplierID = supplier.SupplierID,
                    SupplierName = supplier.SupplierName
                };

                SupplierDropdownVMList.Add(supplierDropdownVM);
            });

            return SupplierDropdownVMList;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<SupplierDetailsVM>> GetSupplierDetails(Guid id)
        {
            var supplier = await _context.Suppliers.FindAsync(id);

            if (supplier == null)
            {
                return NotFound();
            }

            SupplierDetailsVM supplierVM = new SupplierDetailsVM();

            supplierVM.SupplierID = supplier.SupplierID;
            supplierVM.GoodsSupplied = supplier.GoodsSupplied;
            supplierVM.SupplierName = supplier.SupplierName;

            var ContactPersonsList = await _context.SupplierContactPeople.Where(scp => scp.SupplierID == id).ToListAsync();

            supplierVM.SupplierContactPeople = ContactPersonsList;

            var claimsIdentity = User.Identity as ClaimsIdentity;
            var UserID = claimsIdentity.FindFirst("UserId").Value;
            AuditTrail auditTrail = new()
            {
                AuditTrailID = Guid.NewGuid(),
                ActionDate = DateTime.Now,
                ActionDescription = "View Supplier Details",
                ActionData = "View Supplier Details : " + supplierVM.SupplierID,
                EmployeeAction = "View",
                EmployeeID = Guid.Parse(UserID)

            };

            _context.AuditTrails.Add(auditTrail);




            await _context.SaveChangesAsync();

            return supplierVM;
        }

        // PUT: api/Supplier/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSupplier(Guid id, Supplier supplier)
        {
            if (id != supplier.SupplierID)
            {
                return BadRequest();
            }

            _context.Entry(supplier).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SupplierExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Supplier
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Supplier>> PostSupplier(SupplierDetailsVM supplier)
        {
            Supplier supplier1 = new()
            {
                SupplierID = Guid.NewGuid(),
                SupplierName = supplier.SupplierName,
                GoodsSupplied = supplier.GoodsSupplied,
                TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

            };

            _context.Suppliers.Add(supplier1);


            supplier.SupplierContactPeople.ForEach((contact) =>
            {
                SupplierContactPerson supplierContactPerson = new()
                {
                    EmailAddress = contact.EmailAddress,
                    Name = contact.Name,
                    PhoneNumber = contact.PhoneNumber,
                    SupplierID = supplier1.SupplierID,
                    SupplierContactPersonID = Guid.NewGuid()
                };

                _context.SupplierContactPeople.Add(supplierContactPerson);
            });

            try
            {
                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Create Supplier",
                    ActionData = "Create Supplier " + supplier.SupplierID,
                    EmployeeAction = "Create",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);




                await _context.SaveChangesAsync();
                return await GetSupplier(supplier1.SupplierID);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return BadRequest();
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<Supplier>> UpdateSupplier(SupplierDetailsVM supplier)
        {
            Supplier supplier1 = new()
            {
                SupplierID = supplier.SupplierID,
                SupplierName = supplier.SupplierName,
                GoodsSupplied = supplier.GoodsSupplied,
                TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")

            };

            _context.Suppliers.Update(supplier1);

            supplier.SupplierContactPeople.ForEach((contact) =>
            {
                var SearchContact = _context.SupplierContactPeople
                .AsNoTracking()
                .Where(scp => scp.SupplierContactPersonID == contact.SupplierContactPersonID)
                .FirstOrDefault();

                if (SearchContact == null)
                {
                    contact.SupplierContactPersonID = Guid.NewGuid();
                    contact.SupplierID = supplier.SupplierID;
                    _context.SupplierContactPeople.Add(contact);
                }
                else
                {
                    _context.SupplierContactPeople.Update(contact);
                }
            });

            try
            {
                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Update Supplier",
                    ActionData = "Update Supplier " + supplier.SupplierID,
                    EmployeeAction = "Update",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);




               
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpDelete]
        [Route("[action]")]
        public async Task<IActionResult> DeleteContactPerson(Guid id)
        {
            var contact = await _context.SupplierContactPeople.FindAsync(id);

            if (contact != null)
            {

                _context.SupplierContactPeople.Remove(contact);
                await _context.SaveChangesAsync();
            }

            return Ok();
        }

        // DELETE: api/Supplier/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSupplier(Guid id)
        {
            try
            {
                var supplier = await _context.Suppliers.FindAsync(id);
                if (supplier == null)
                {
                    return NotFound();
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Delete Supplier",
                    ActionData = "Delete Supplier " + supplier.SupplierID,
                    EmployeeAction = "Delete",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                var ContactPeople = await _context.SupplierContactPeople.Where(zz => zz.SupplierID == id).ToListAsync();

                foreach (var contact in ContactPeople)
                {
                    _context.SupplierContactPeople.Remove(contact);
                }



                _context.Suppliers.Remove(supplier);
                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
            }
        
        }

        private bool SupplierExists(Guid id)
        {
            return _context.Suppliers.Any(e => e.SupplierID == id);
        }


        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> CheckSupplierDeleteStatus(Guid id)
        {

            try
            {
                var AllSupplierOrders = await _context.SupplierOrders.Where(zz => zz.SupplierID == id).ToListAsync();
                var OrderinProgres = await _context.SupplierOrders.Where(zz => zz.SupplierID == id && zz.SupplierOrderStatusID != Guid.Parse("20fc8be8-ad9b-4ccd-a300-02d5ef12d8a0") && zz.SupplierOrderStatusID != Guid.Parse("e0a5df74-7054-422f-a397-34ad4060d2f5")).ToListAsync();
                //InventoryItems on Supplier
                var InventoryConnected = await _context.Inventories.Where(zz => zz.SupplierID == id && zz.TableStatusID != Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1")).ToListAsync();

                if (AllSupplierOrders.Count == 0 && InventoryConnected.Count == 0)
                {
                    //Delete
                    return Ok();
                }
                else if (AllSupplierOrders.Count > 0 && InventoryConnected.Count == 0 && OrderinProgres.Count == 0)
                {
                    //Deactivate
                    return Conflict(new { message = "Deactivate" });
                }
                else if (OrderinProgres.Count > 0 || InventoryConnected.Count > 0)
                {
                    //Do Nothing
                    return Conflict(new { message = "Order in Progress" });
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                
                return BadRequest(ex);
                throw;
            }

           


        }



    }

    
}
