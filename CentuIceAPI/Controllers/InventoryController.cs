using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using CentuIceAPI.DTO;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class InventoryController : ControllerBase
    {
        private readonly CentuIceDBContext _context;

        public InventoryController(CentuIceDBContext context)
        {
            _context = context;
        }

        // GET: api/Inventory
        [HttpGet]
        public async Task<ActionResult<IEnumerable<InventoryVM>>> GetInventories()
        {
            var inventories = await _context.Inventories.AsNoTracking()
                .Include(i => i.Supplier)
                .Include(i => i.InventoryType).Where(i => i.TableStatusID != Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1"))
                .ToListAsync();

            List<InventoryVM> InventoryVMList = new List<InventoryVM>();

            
            inventories.ForEach((inventory) =>
            {
                
                var ItemPrice =  _context.InventoryPrices.AsNoTracking().Where(zz => zz.InventoryID == inventory.InventoryID).OrderByDescending(zz => zz.Date).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefault();
                InventoryVM inventoryVM = new()
                {
                    InventoryID = inventory.InventoryID,
                    InventoryType = inventory.InventoryType.Description,
                    ItemName = inventory.ItemName,
                    ItemPrice = ItemPrice,
                    Quantity = inventory.Quantity,
                    Supplier = inventory.Supplier.SupplierName
                };
                InventoryVMList.Add(inventoryVM);
            });


            var claimsIdentity = User.Identity as ClaimsIdentity;
            var UserID = claimsIdentity.FindFirst("UserId").Value;
            AuditTrail auditTrail = new()
            {
                AuditTrailID = Guid.NewGuid(),
                ActionDate = DateTime.Now,
                ActionDescription = "View Inventory",
                ActionData = "View Inventory",
                EmployeeAction = "View",
                EmployeeID = Guid.Parse(UserID)

            };

            _context.AuditTrails.Add(auditTrail);

            await _context.SaveChangesAsync();
            return InventoryVMList;
        }

        // GET: api/Inventory/5
        [HttpGet("{id}")]
        public async Task<ActionResult<InventoryVM>> GetInventory(Guid id)
        {
            var inventory = await _context.Inventories
                .Include(i => i.InventoryType)
                .Include(i => i.Supplier)
                .Where(i => i.InventoryID == id)
                .FirstOrDefaultAsync();

            if (inventory == null)
            {
                return NotFound();
            }
            var Price = await _context.InventoryPrices.Where(zz => zz.InventoryID == inventory.InventoryID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();
            InventoryVM inventoryVM = new()
            {
                InventoryID = inventory.InventoryID,
                InventoryType = inventory.InventoryType.Description,
                ItemName = inventory.ItemName,
                ItemPrice = Price,
                Quantity = inventory.Quantity,
                Supplier = inventory.Supplier.SupplierName
            };

            return inventoryVM;
        }

        // PUT: api/Inventory/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInventory(Guid id, Inventory inventory)
        {
            if (id != inventory.InventoryID)
            {
                return BadRequest();
            }

            _context.Entry(inventory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InventoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Inventory
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<InventoryVM>> PostInventory(CreateInventoryVM inventory)
        {
            inventory.InventoryID = Guid.NewGuid();
            inventory.TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e");

            Inventory newInventory = new Inventory()
            {
                InventoryID = inventory.InventoryID,
                SupplierID = inventory.SupplierID,
                InventoryTypeID = inventory.InventoryTypeID,
                TableStatusID = inventory.TableStatusID,
                ItemName = inventory.ItemName,
                Quantity = inventory.Quantity,
            };

            Price newPrice = new Price()
            {
                PriceID = Guid.NewGuid(),
                CurrentPrice = inventory.ItemPrice
            };

            InventoryPrice inventoryPrice = new InventoryPrice()
            {
                InventoryPriceID = Guid.NewGuid(),
                PriceID = newPrice.PriceID,
                Date = DateTime.Now,
                InventoryID = inventory.InventoryID
            };

            _context.Prices.Add(newPrice);
            _context.InventoryPrices.Add(inventoryPrice);

            _context.Inventories.Add(newInventory);
            await _context.SaveChangesAsync();

            var inventoryVM = await GetInventory(inventory.InventoryID);

            return inventoryVM;
        }

        // DELETE: api/Inventory/5
        [HttpDelete]
        [Route("[action]")]
        public async Task<IActionResult> DeleteInventory(Guid ID)
        {
            var inventory = await _context.Inventories.FindAsync(ID);
            if (inventory == null)
            {
                return NotFound();
            }

            var Prices = await _context.InventoryPrices.Where(zz => zz.InventoryID == ID).Include(zz => zz.Price).ToListAsync();

            foreach (var price in Prices)
            {
                _context.InventoryPrices.Remove(price);
                _context.Prices.Remove(price.Price);
            }

            _context.Inventories.Remove(inventory);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool InventoryExists(Guid id)
        {
            return _context.Inventories.Any(e => e.InventoryID == id);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> WriteOffStock(string Employee, StockWrittenOff WriteOff)
        {

            
            var inventory = _context.Inventories
                .AsNoTracking()
                .Where(i => i.InventoryID == WriteOff.InventoryID)
                .FirstOrDefault();

            inventory.Quantity = inventory.Quantity - WriteOff.Quantity;

            _context.Inventories.Update(inventory);

            WriteOff.StockWrittenOffID = Guid.NewGuid();
            WriteOff.WriteOffDate = DateTime.Now;

            _context.StockWrittenOff.Add(WriteOff);

            if (Employee != null)
            {
                EmployeeError employeeError = new()
                {
                    EmployeeErrorID = Guid.NewGuid(),
                    DateOfError = DateTime.Now,
                    DescriptionOfError = "Employee Broke " + WriteOff.Quantity + " " + inventory.ItemName,
                    EmployeeID = Guid.Parse(Employee),
                    ErrorCode = "BRKBAG"
                 };

                _context.EmployeeErrors.Add(employeeError);
            }

            try
            {

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Write-off Stock",
                    ActionData = "Write-off Stock for " + inventory.ItemName,
                    EmployeeAction = "Write-off",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
                throw;
            }
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> AddStockTake(List<StockTakeLine> stockTakeLines)
        {
            //Create New Stock Take
            StockTake NewStockTake = new()
            {
                DateOfStockTake = DateTime.Now,
                StockTakeID = Guid.NewGuid()
            };

            _context.StockTakes.Add(NewStockTake);

            //Loop through each stock take Line (In Parameter)
            stockTakeLines.ForEach((stockTakeLine) =>
            {
                //Add id's to each stockTakeLine
                stockTakeLine.StockTakeID = NewStockTake.StockTakeID;
                stockTakeLine.StockTakeLineID = Guid.NewGuid();


                //If there is a discrepency, add changes to each inventory item
                if (stockTakeLine.DiscrepancyAmount != 0)
                {
                    //Get the inventory item
                    var inventory = _context.Inventories
                        .AsNoTracking()
                        .Where(i => i.InventoryID == stockTakeLine.InventoryID)
                        .FirstOrDefault();

                    inventory.Quantity = stockTakeLine.StockTakeQuantity;
                    _context.Inventories.Update(inventory);
                }
                _context.StockTakeLines.Add(stockTakeLine);
            });

            try
            {
                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Stock Take",
                    ActionData = "Do Stock Take",
                    EmployeeAction = "Stock-Take",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
                throw;
            }
        }


        //-------------------------------- Lookups ------------------------------//
        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<InventoryType>>> GetInventoryTypes()
        {
            return await _context.InventoryTypes.ToListAsync();
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<InventoryDropdownVM>>> GetInventoryDropdowns()
        {

            var inventoryList = await _context.Inventories.Where(zz => zz.TableStatusID != Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1")).ToListAsync();

            List<InventoryDropdownVM> inventoryDropdownList = new List<InventoryDropdownVM>();

            inventoryList.ForEach((inventory) => 
            {
                
                var ItemPrice = _context.InventoryPrices.Where(zz => zz.InventoryID == inventory.InventoryID).OrderByDescending(zz => zz.Date).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefault();
                InventoryDropdownVM inventoryDropdownVM = new()
                {
                    InventoryID = inventory.InventoryID,
                    ItemName = inventory.ItemName,
                    Quantity = inventory.Quantity,
                    Price = ItemPrice
                };
                inventoryDropdownList.Add(inventoryDropdownVM);
            });

            return inventoryDropdownList;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<SupplierVM>>> GetSuppliers()
        {
            var Suppliers =  await _context.Suppliers.ToListAsync();

            List<SupplierVM> SupplierVMList = new List<SupplierVM>();

            Suppliers.ForEach((supplier) => 
            {
                SupplierVM supplierVM = new()
                {
                    SupplierID = supplier.SupplierID,
                    SupplierName = supplier.SupplierName
                };
                SupplierVMList.Add(supplierVM);
            });

            return SupplierVMList;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<WrittenOffReason>>> GetReasons()
        {
            try
            {
                var Reasons = await _context.WrittenOffReasons.ToListAsync();

                return Ok(Reasons);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;

            }
        }


        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> StoreAuthWriteOff(string Employee, StockWrittenOff WriteOff)
        {

            var inventory = await _context.Inventories.Where(zz => zz.InventoryID == WriteOff.InventoryID).FirstOrDefaultAsync();
            try
            {
                
                Guid EmployeeID = Guid.Empty;
                if (Employee != null)
                {
                    EmployeeID = Guid.Parse(Employee);
                }
                AuthWriteOff authWriteOff = new()
                {
                    AuthWriteOffID = Guid.NewGuid(),
                    InventoryID = WriteOff.InventoryID,
                    EmployeeID = EmployeeID,
                    Quantity = WriteOff.Quantity,
                    WrittenOffReasonID = WriteOff.WrittenOffReasonID,
                    WriteOffDate = WriteOff.WriteOffDate,
                    InventoryItem = inventory.ItemName
                };

                _context.AuthWriteOffs.Add(authWriteOff);
                await _context.SaveChangesAsync();
                return Ok(authWriteOff);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;

            }

        }


        [HttpGet]
        [Route("[action]")]

        public async Task<IActionResult> GetUsedInventory(Guid ID)
        {

            try
            {
                var Products = await _context.Products.Where(zz => zz.InventoryID == ID && zz.TableStatusID != Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1")).ToListAsync();
                var DeactivatedProducts = await _context.Products.Where(zz => zz.InventoryID == ID && zz.TableStatusID == Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1")).ToListAsync();
                var Orders = await _context.SupplierOrderLines.Include(zz => zz.SupplierOrder).Where(zz => zz.InventoryID == ID && zz.SupplierOrder.SupplierOrderStatusID != Guid.Parse("20fc8be8-ad9b-4ccd-a300-02d5ef12d8a0") && zz.SupplierOrder.SupplierOrderStatusID != Guid.Parse("e0a5df74-7054-422f-a397-34ad4060d2f5")).ToListAsync();

                var AllOrders = await _context.SupplierOrderLines.Include(zz => zz.SupplierOrder).Where(zz => zz.InventoryID == ID).ToListAsync();

                if (Products.Count == 0 && AllOrders.Count == 0 && DeactivatedProducts.Count == 0)
                {
                    //Delete
                    return Ok();

                }
                else if (DeactivatedProducts.Count != 0|| Products.Count == 0 && Products.Count == 0 && Orders.Count == 0)
                {

                    //Deactivate
                    return Conflict(new { message = "Deactivate" });
                }
                else if (Products.Count != 0 || Orders.Count != 0)
                {
                    //Do Nothing
                    return Conflict(new { message = "Product Exists/Order In Process" });
                }
               

                return NoContent();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
                throw;
            }
            
        }




    }
}
