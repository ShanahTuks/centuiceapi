﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using CentuIceAPI.DTO;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class VATController : ControllerBase
    {
        private readonly CentuIceDBContext _context;

        public VATController(CentuIceDBContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<VatConfiguration>>> GetVatConfigurations()
        {
            try
            {
                List<VatConfiguration> vatConfigurations = new List<VatConfiguration>();

                var VatsList =  _context.VatConfigurations.OrderByDescending(zz => zz.DateSet).Take(5);

                foreach (var vat in VatsList)
                {
                    VatConfiguration vatConfiguration = new VatConfiguration
                    {
                        VatConfigurationID = vat.VatConfigurationID,
                        VatPercentage = vat.VatPercentage,
                        DateSet = vat.DateSet,
                        Activated = vat.Activated,
                    };

                    vatConfigurations.Add(vatConfiguration);
                }

                

                return Ok(vatConfigurations);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<VatConfiguration>>> AddNewVat(VatConfiguration NewVat)
        {
            try
            {
                NewVat.VatConfigurationID = Guid.NewGuid();
                NewVat.DateSet = DateTime.Now;
                _context.VatConfigurations.Add(NewVat);

                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<VatConfiguration>>> ChangeActivationStatus(Guid ID, VatActivationStatusVM Status)
        {

            try
            {
                var CurrentVat = await _context.VatConfigurations.Where(zz => zz.VatConfigurationID == ID).FirstOrDefaultAsync();

                CurrentVat.Activated = Status.NewStatus;

                _context.VatConfigurations.Update(CurrentVat);

                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }


        [HttpDelete]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<VatConfiguration>>> DeleteVat(Guid ID)
        {
            try
            {
                var Sales = await _context.Orders.Where(zz => zz.VatConfigurationID == ID).ToListAsync();
                var Orders = await _context.Sales.Where(zz => zz.VatConfigurationID == ID).ToListAsync();
                var VAT = await _context.VatConfigurations.Where(zz => zz.VatConfigurationID == ID).FirstOrDefaultAsync();
                var VatCount =  _context.VatConfigurations.Count();


                if (Sales.Count == 0 && Orders.Count == 0)
                {
                    if ((VatCount - 1 > 0))
                    {
                        _context.VatConfigurations.Remove(VAT);
                        await _context.SaveChangesAsync();
                    }

                    else
                    { return Conflict(new { message = "No Vat Left" }); }
                }
                else
                {
                    return Conflict(new { message = "Vat Used" });
                }
              

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }


    }
}
