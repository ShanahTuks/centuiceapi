using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using CentuIceAPI.DTO;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SupplierOrderController : ControllerBase
    {
        private readonly CentuIceDBContext _context;

        public SupplierOrderController(CentuIceDBContext context)
        {
            _context = context;
        }

        // GET: api/SupplierOrder
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViewSupplierOrderVM>>> GetSupplierOrders()
        {
            var supplierOrders = await _context.SupplierOrders
                .Include(so => so.Supplier)
                .Include(so => so.SupplierOrderStatus).OrderByDescending(so => so.DatePlaced)
                .ToListAsync();

            List<ViewSupplierOrderVM> ViewSupplierOrderList = new();

            supplierOrders.ForEach((order) => 
            {
                ViewSupplierOrderVM viewSupplierOrderVM = new()
                {
                    DatePlaced = order.DatePlaced,
                    DateReceived = order.DateReceived,
                    OrderStatus = order.SupplierOrderStatus.Description,
                    SupplierName = order.Supplier.SupplierName,
                    SupplierOrderID = order.SupplierOrderID
                };
                ViewSupplierOrderList.Add(viewSupplierOrderVM);
            });

            var claimsIdentity = User.Identity as ClaimsIdentity;
            var UserID = claimsIdentity.FindFirst("UserId").Value;
            AuditTrail auditTrail = new()
            {
                AuditTrailID = Guid.NewGuid(),
                ActionDate = DateTime.Now,
                ActionDescription = "View Supplier Orders",
                ActionData = "N/A",
                EmployeeAction = "View",
                EmployeeID = Guid.Parse(UserID)

            };

            _context.AuditTrails.Add(auditTrail);




            await _context.SaveChangesAsync();

            return ViewSupplierOrderList;
        }

        // GET: api/SupplierOrder/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SupplierOrder>> GetSupplierOrder(Guid id)
        {
            var supplierOrder = await _context.SupplierOrders.FindAsync(id);

            if (supplierOrder == null)
            {
                return NotFound();
            }

            return supplierOrder;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<FullSupplierOrderDTO>> GetFullSupplierOrder(Guid id)
        {
            var supplierOrder = await _context.SupplierOrders
                .AsNoTracking()
                .Include(so => so.SupplierOrderLine)
                .Where(so => so.SupplierOrderID == id)
                .FirstOrDefaultAsync();

            var supplierOrderLines = await _context.SupplierOrderLines
                .Where(sol => sol.SupplierOrderID == id)
                .ToListAsync();

            FullSupplierOrderDTO fullSupplierOrderDTO = new();

            SupplierOrderDTO supplierOrderDTO = new();
            fullSupplierOrderDTO.SupplierOrders = supplierOrderDTO;

            List<SupplierOrderLineDTO> supplierOrderLineDTOs = new();
            fullSupplierOrderDTO.SupplierOrderLines = supplierOrderLineDTOs;


            fullSupplierOrderDTO.SupplierOrders.SupplierOrderID = supplierOrder.SupplierOrderID;
            fullSupplierOrderDTO.SupplierOrders.SupplierOrderStatusID = supplierOrder.SupplierOrderStatusID;
            fullSupplierOrderDTO.SupplierOrders.SupplierID = supplierOrder.SupplierID;
            fullSupplierOrderDTO.SupplierOrders.Details = supplierOrder.Details;
            fullSupplierOrderDTO.SupplierOrders.DateReceived = supplierOrder.DateReceived;
            fullSupplierOrderDTO.SupplierOrders.DatePlaced = supplierOrder.DatePlaced;

            supplierOrderLines.ForEach((order) =>
            {
                SupplierOrderLineDTO supplierOrderLineDTO = new()
                {
                    InventoryID = order.InventoryID,
                    Quantity = order.Quantity,
                    SupplierOrderID = order.SupplierOrderID,
                    SupplierOrderLineID = order.SupplierOrderLineID
                };
                fullSupplierOrderDTO.SupplierOrderLines.Add(supplierOrderLineDTO);
            });

            return fullSupplierOrderDTO;

        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<SupplierOrderDetailsVM>> GetSupplierOrderDetails(Guid id)
        {
            var supplierOrder = await _context.SupplierOrders
                .AsNoTracking()
                .Include(so => so.SupplierOrderStatus)
                .Include(so => so.Supplier)
                .Where(so => so.SupplierOrderID == id)
                .FirstOrDefaultAsync();

            var supplierOrderLines = await _context.SupplierOrderLines
                .AsNoTracking()
                .Include(sol => sol.Inventory)
                .Where(sol => sol.SupplierOrderID == id)
                .ToListAsync();

            SupplierOrderDetailsVM SupplierOrderDetails = new()
            {
                DatePlaced = supplierOrder.DatePlaced,
                DateReceived = supplierOrder.DateReceived,
                OrderStatus = supplierOrder.SupplierOrderStatus.Description,
                SupplierName = supplierOrder.Supplier.SupplierName,
                SupplierOrderLines = new List<SupplierOrderLineVM>()
            };

            supplierOrderLines.ForEach((orderLine) =>
            {
                var ItemPrice =  _context.InventoryPrices.Where(zz => zz.InventoryPriceID == orderLine.InventoryPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefault();
                SupplierOrderLineVM orderLineVM = new()
                {
                    ItemName = orderLine.Inventory.ItemName,
                    ItemPrice = ItemPrice,
                    Quantity = orderLine.Quantity
                };

                SupplierOrderDetails.SupplierOrderLines.Add(orderLineVM);
            });

            var claimsIdentity = User.Identity as ClaimsIdentity;
            var UserID = claimsIdentity.FindFirst("UserId").Value;
            AuditTrail auditTrail = new()
            {
                AuditTrailID = Guid.NewGuid(),
                ActionDate = DateTime.Now,
                ActionDescription = "View Supplier Order Details",
                ActionData = "View Supplier Order Details for " + supplierOrder.SupplierOrderID,
                EmployeeAction = "View",
                EmployeeID = Guid.Parse(UserID)

            };

            _context.AuditTrails.Add(auditTrail);




            await _context.SaveChangesAsync();
            return SupplierOrderDetails;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<FullSupplierOrderDTO>> UpdateSupplierOrder(FullSupplierOrderVM supplierOrder)
        {
            _context.SupplierOrders.Update(supplierOrder.SupplierOrders);

            supplierOrder.SupplierOrderLines.ForEach((order) =>
            {
                var currentOrderLine = _context.SupplierOrderLines
                .AsNoTracking()
                .Where(sol => sol.SupplierOrderLineID == order.SupplierOrderLineID)
                .FirstOrDefault();

                if (currentOrderLine == null)
                {
                    order.SupplierOrderID = supplierOrder.SupplierOrders.SupplierOrderID;
                    order.SupplierOrderLineID = Guid.NewGuid();

                    _context.SupplierOrderLines.Add(order);
                }
                else
                {
                    _context.SupplierOrderLines.Update(order);
                }

            });

            try
            {
                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Update Supplier Order",
                    ActionData = "Update Supplier Order " + supplierOrder.SupplierOrders.SupplierOrderID,
                    EmployeeAction = "Update",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return BadRequest();
                throw;
            }
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<FullSupplierOrderDTO>> FinalizeSupplierOrder(FinalizeSupplierOrderVM supplierOrder)
        {

            try
            {
                supplierOrder.FinalizeSupplierOrderLineVM.ForEach((orderLine) =>
                {
                    var currentPrice = _context.InventoryPrices
                        .AsNoTracking()
                        .Where(ip => ip.InventoryID == orderLine.InventoryID)
                        .OrderByDescending(ip => ip.Date).Include(ip => ip.Price)
                        .Select(ip => ip.Price.CurrentPrice)
                        .FirstOrDefault();

                        var currentOrderLine = _context.SupplierOrderLines
                            .Include(col => col.InventoryPrice)
                            .Include(col => col.InventoryPrice.Price)
                            .Where(col => col.SupplierOrderLineID == orderLine.SupplierOrderLineID)
                            .FirstOrDefault();


                        if (currentOrderLine != null)
                        {
                            currentOrderLine.Quantity = orderLine.Quantity;
                            if (currentOrderLine.InventoryPrice.Price.CurrentPrice != orderLine.Price)
                            {
                                var searchPrice = _context.Prices.Where(p => p.CurrentPrice == orderLine.Price).FirstOrDefault();

                                if (searchPrice != null)
                                {
                                    InventoryPrice inventoryPrice = new()
                                    {
                                        InventoryID = orderLine.InventoryID,
                                        Date = DateTime.Now,
                                        InventoryPriceID = Guid.NewGuid(),
                                        PriceID = searchPrice.PriceID,
                                    };

                                    currentOrderLine.InventoryPriceID = inventoryPrice.PriceID;
                                    _context.SupplierOrderLines.Update(currentOrderLine);

                                    _context.InventoryPrices.Add(inventoryPrice);
                                }
                                else
                                {

                                    Price newPrice = new()
                                    {
                                        PriceID = Guid.NewGuid(),
                                        CurrentPrice = orderLine.Price
                                    };

                                    InventoryPrice inventoryPrice = new()
                                    {
                                        InventoryID = orderLine.InventoryID,
                                        Date = DateTime.Now,
                                        InventoryPriceID = Guid.NewGuid(),
                                        PriceID = newPrice.PriceID
                                    };

                                _context.Prices.Add(newPrice);
                                _context.InventoryPrices.Add(inventoryPrice);

                                currentOrderLine.InventoryPriceID = inventoryPrice.InventoryPriceID;
                                    _context.SupplierOrderLines.Update(currentOrderLine);


                                }
                            }
                        }
                        else
                        {
                            var searchPrice = _context.Prices.Where(p => p.CurrentPrice == orderLine.Price).FirstOrDefault();

                            if (searchPrice != null)
                            {
                                InventoryPrice inventoryPrice = new()
                                {
                                    InventoryID = orderLine.InventoryID,
                                    Date = DateTime.Now,
                                    InventoryPriceID = Guid.NewGuid(),
                                    PriceID = searchPrice.PriceID
                                };

                                SupplierOrderLine newOrderLine = new()
                                {
                                    SupplierOrderID = supplierOrder.SupplierOrders.SupplierOrderID,
                                    InventoryID = orderLine.InventoryID,
                                    InventoryPriceID = inventoryPrice.InventoryPriceID,
                                    Quantity = orderLine.Quantity,
                                    SupplierOrderLineID = orderLine.SupplierOrderLineID
                                };

                                _context.SupplierOrderLines.Add(newOrderLine);
                                _context.InventoryPrices.Add(inventoryPrice);
                            }
                            else
                            {
                                InventoryPrice inventoryPrice = new()
                                {
                                    InventoryID = orderLine.InventoryID,
                                    Date = DateTime.Now,
                                    InventoryPriceID = Guid.NewGuid(),
                                    PriceID = searchPrice.PriceID
                                };

                                SupplierOrderLine newOrderLine = new()
                                {
                                    SupplierOrderID = supplierOrder.SupplierOrders.SupplierOrderID,
                                    InventoryID = orderLine.InventoryID,
                                    InventoryPriceID = inventoryPrice.InventoryPriceID,
                                    Quantity = orderLine.Quantity,
                                    SupplierOrderLineID = orderLine.SupplierOrderLineID
                                };

                                _context.SupplierOrderLines.Add(newOrderLine);
                                _context.InventoryPrices.Add(inventoryPrice);
                            }

                        }

                });

                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }

        // PUT: api/SupplierOrder/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSupplierOrder(Guid id, SupplierOrder supplierOrder)
        {
            if (id != supplierOrder.SupplierOrderID)
            {
                return BadRequest();
            }

            _context.Entry(supplierOrder).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SupplierOrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SupplierOrder
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<SupplierOrder>> PostSupplierOrder(FullSupplierOrderVM supplierOrder)
        {

            supplierOrder.SupplierOrders.SupplierOrderID = Guid.NewGuid();
            //Placed
            supplierOrder.SupplierOrders.SupplierOrderStatusID = Guid.Parse("f13b8acc-6d12-4877-b8d7-cc9f174f1734");
            supplierOrder.SupplierOrders.DatePlaced = DateTime.Now;
            supplierOrder.SupplierOrders.DateReceived = null;

            supplierOrder.SupplierOrderLines.ForEach((order) =>
            {
                
                var InventoryPriceID =  _context.InventoryPrices.Where(zz => zz.InventoryID == order.InventoryID).OrderByDescending(zz => zz.Date).Select(zz => zz.InventoryPriceID).FirstOrDefault();
                order.SupplierOrderID = supplierOrder.SupplierOrders.SupplierOrderID;
                order.SupplierOrderLineID = Guid.NewGuid();
                order.InventoryPriceID = InventoryPriceID;

                _context.SupplierOrderLines.Add(order);
            });

            _context.SupplierOrders.Add(supplierOrder.SupplierOrders);

            try
            {
                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Create Supplier Order",
                    ActionData = "Create Supplier Order " + supplierOrder.SupplierOrders.SupplierOrderID,
                    EmployeeAction = "Create",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);




         

                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return BadRequest();
                throw;
            }
            

            
        }

        // DELETE: api/SupplierOrder/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSupplierOrder(Guid id)
        {
            var supplierOrder = await _context.SupplierOrders.FindAsync(id);
            if (supplierOrder == null)
            {
                return NotFound();
            }

            _context.SupplierOrders.Remove(supplierOrder);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool SupplierOrderExists(Guid id)
        {
            return _context.SupplierOrders.Any(e => e.SupplierOrderID == id);
        }

        [Route("[action]")]
        [HttpDelete]
        public async Task<IActionResult> SetOrderReceived(Guid ID)
        {
            try
            {
                var supplierOrder = await _context.SupplierOrders.Where(zz => zz.SupplierOrderID == ID).FirstOrDefaultAsync();

                supplierOrder.DateReceived = DateTime.Now;
                supplierOrder.SupplierOrderStatusID = Guid.Parse("20fc8be8-ad9b-4ccd-a300-02d5ef12d8a0");

                _context.SupplierOrders.Update(supplierOrder);

                var SupplierOrderLines = await _context.SupplierOrderLines.Include(zz => zz.Inventory).Where(zz => zz.SupplierOrderID == ID).ToListAsync();

                foreach (var line in SupplierOrderLines)
                {
                    var inventoryItem = await _context.Inventories.Where(zz => zz.InventoryID == line.InventoryID).FirstOrDefaultAsync();

                    inventoryItem.Quantity = inventoryItem.Quantity + line.Quantity;

                    _context.Inventories.Update(inventoryItem);
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Set Supplier Order as Received",
                    ActionData = "Set Supplier Order " + supplierOrder.SupplierOrderID + "as Recieved",
                    EmployeeAction = "Update",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);




                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [Route("[action]")]
        [HttpDelete]
        public async Task<IActionResult> RemoveLine(Guid ID)
        {
            try
            {
                var Line = await _context.SupplierOrderLines.Where(zz => zz.SupplierOrderLineID == ID).FirstOrDefaultAsync();

                _context.SupplierOrderLines.Remove(Line);

                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }



        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<InventoryDropdownVM>>> GetInventoryDropdowns(Guid ID)
        {

            var inventoryList = await _context.Inventories
                .Where(zz => zz.SupplierID == ID && zz.TableStatusID != Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1")).ToListAsync();

            List<InventoryDropdownVM> inventoryDropdownList = new List<InventoryDropdownVM>();

            inventoryList.ForEach((inventory) =>
            {
                var ItemPrice =  _context.InventoryPrices.Where(zz => zz.InventoryID == inventory.InventoryID).OrderByDescending(zz => zz.Date).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefault();
                InventoryDropdownVM inventoryDropdownVM = new()
                {
                    InventoryID = inventory.InventoryID,
                    ItemName = inventory.ItemName,
                    Quantity = inventory.Quantity,
                    Price = ItemPrice
                };
                inventoryDropdownList.Add(inventoryDropdownVM);
            });

            return inventoryDropdownList;
        }

        [HttpDelete]
        [Route("[action]")]
        public async Task<IActionResult> CancelOrder(Guid ID)
        {
            try
            {
                var Order = await _context.SupplierOrders.Where(zz => zz.SupplierOrderID == ID).FirstOrDefaultAsync();

                if (Order != null)
                {
                    Order.SupplierOrderStatusID = Guid.Parse("e0a5df74-7054-422f-a397-34ad4060d2f5");

                    _context.SupplierOrders.Update(Order);

                    await _context.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
               


            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

    }
}
