﻿using CentuIceAPI.DTO;
using CentuIceAPI.Models;
using CentuIceAPI.Services.Hubs;
using CentuIceAPI.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DeliveryController : ControllerBase
    {
        private readonly CentuIceDBContext _context;
        private readonly NotificationHub _NotHub;

        public DeliveryController(CentuIceDBContext context, NotificationHub _hubContext, IConfiguration configuration)
        {
            _context = context;
            _NotHub = _hubContext;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<ViewTripSheetVM>>> GetTripSheets()
        {
            try
            {
                var TripSheets = await _context.DeliveryTripSheets
                    .AsNoTracking()
                    .Include(ts => ts.TripSheetStatus)
                    .Include(ts => ts.Driver)
                    .Include(ts => ts.Assistant)
                    .Include(ts => ts.Supervisor)
                    .Include(ts => ts.Vehicle)
                    .OrderByDescending(ts => ts.Created)
                    .ToListAsync();

                List<ViewTripSheetVM> TripSheetVMs = new();

                TripSheets.ForEach((tripSheet) =>
                {
                    ViewTripSheetVM viewTripSheetVM = new()
                    {
                        DeliveryTripSheetID = tripSheet.DeliveryTripSheetID,
                        Alias = tripSheet.Alias,
                        AssistantName = tripSheet.Assistant.FirstName + ' ' + tripSheet.Assistant.Surname,
                        CreationDate = tripSheet.Created,
                        DriverName = tripSheet.Driver.FirstName + ' ' + tripSheet.Driver.Surname,
                        SupervisorName = tripSheet.Supervisor.FirstName + ' ' + tripSheet.Supervisor.Surname,
                        TripSheetStatus = tripSheet.TripSheetStatus.Description,
                        VehicleRegNumber = tripSheet.Vehicle.Reg_Number
                    };

                    TripSheetVMs.Add(viewTripSheetVM);
                });

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "View Tripsheets",
                    ActionData = "N/A",
                    EmployeeAction = "View",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return TripSheetVMs;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<TripSheetGroupVM>>> GetLockedTripSheets()
        {
            try
            {
                var tripSheets = await _context.DeliveryTripSheets
                    .Include(ts => ts.TripSheetStatus)
                    .Where(ts => ts.TripSheetStatus.Description != "Cancelled")
                    .Where(ts => ts.TripSheetStatus.Description != "Checked In")
                    .Where(ts => ts.TripSheetStatus.Description != "Ready for Loading")
                    .Where(ts => ts.TripSheetStatus.Description != "Pending")
                    .ToListAsync();

                //var tripSheetsGrouped = tripSheets.GroupBy(ts => ts.TripSheetStatusID);

                List<UpdateDeliveryGroupVM> DeliveryGroupList = new();

                tripSheets.ForEach(tripSheet =>
                {
                    //Just to initalize the Deliveries in the deliveryGroupVM
                    List<ClientDeliveryVM> ClientOrdersList = new();

                    UpdateDeliveryGroupVM deliveryGroupVM = new()
                    {
                        Alias = tripSheet.Alias,
                        AssistantID = tripSheet.AssistantID,
                        DeliveryTripSheetID = tripSheet.DeliveryTripSheetID,
                        DriverID = tripSheet.DriverID,
                        SupervisorID = tripSheet.SupervisorID,
                        TripSheetStatusID = tripSheet.TripSheetStatusID,
                        VehicleID = tripSheet.VehicleID,
                        Deliveries = ClientOrdersList
                    };

                    //Get Orders 
                    var orders = _context.Orders
                        .Include(o => o.Client)
                        .Where(o => o.DeliveryTripSheetID == tripSheet.DeliveryTripSheetID)
                        .ToList();

                    orders.ForEach((order) =>
                    {
                        //Get Address
                        var clientAddress = _context.Addresses
                            .Where(ad => ad.AddressID == order.AddressID)
                            .FirstOrDefault();

                        AddressDTO addressDTO = new()
                        {
                            AddressID = clientAddress.AddressID,
                            AddressTypeID = clientAddress.AddressTypeID,
                            CityName = clientAddress.CityName,
                            ComplexName = clientAddress.ComplexName,
                            CountryName = clientAddress.CountryName,
                            isActive = clientAddress.isActive,
                            PostalCode = clientAddress.PostalCode,
                            Province = clientAddress.Province,
                            StreetName = clientAddress.StreetName,
                            StreetNumber = clientAddress.StreetNumber,
                            SuburbName = clientAddress.SuburbName,
                            UnitNumber = clientAddress.UnitNumber,
                        };

                        List<ClientOrderProductVM> clientOrderProducts = new();

                        var orderLines = _context.OrderLines
                            .AsNoTracking()
                            .Include(orderLine => orderLine.Product)
                            .Where(orderLine => orderLine.OrderID == order.OrderID)
                            .ToList();

                        foreach (var line in orderLines)
                        {
                            ClientOrderProductVM clientOrderProductVM = new ClientOrderProductVM()
                            {
                                ProductID = line.ProductID,
                                ProductName = line.Product.ProductName,
                                Quantity = line.Quantity
                            };

                            clientOrderProducts.Add(clientOrderProductVM);
                        }

                        ClientDeliveryVM clientOrder = new()
                        {
                            ClientID = order.ClientID,
                            ClientName = order.Client.ClientName,
                            OrderID = order.OrderID,
                            ClientAddress = addressDTO,
                            ClientOrderProducts = clientOrderProducts
                        };
                        deliveryGroupVM.Deliveries.Add(clientOrder);
                    });

                    DeliveryGroupList.Add(deliveryGroupVM);

                });// end of tripSheets.ForEach

                var groupByTripSheetStatus = DeliveryGroupList.GroupBy(dgl => dgl.TripSheetStatusID);

                List<TripSheetGroupVM> TripSheetGroups = new();

                foreach (var tripSheetStatusGroup in groupByTripSheetStatus)
                {
                    var groupKey = tripSheetStatusGroup.Key;

                    TripSheetGroupVM tripSheetGroup = new();
                    tripSheetGroup.Key = groupKey;

                    List<UpdateDeliveryGroupVM> deliveryGroups = new();

                    foreach (var tripSheet in tripSheetStatusGroup)
                    {
                        deliveryGroups.Add(tripSheet);
                    }

                    tripSheetGroup.DeliveryGroups = deliveryGroups;

                    TripSheetGroups.Add(tripSheetGroup);
                }

                return TripSheetGroups;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

            
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<UpdateDeliveryGroupVM>>> GetUnlockedTripSheets()
        {
            try
            {
                var tripSheets = await _context.DeliveryTripSheets
                .Include(ts => ts.TripSheetStatus)
                .Where(ts => ts.TripSheetStatus.Description == "Ready for Loading" || ts.TripSheetStatus.Description == "Pending")
                .ToListAsync();

                //var tripSheetsGrouped = tripSheets.GroupBy(ts => ts.TripSheetStatusID);

                List<UpdateDeliveryGroupVM> DeliveryGroupList = new();

                tripSheets.ForEach(tripSheet =>
                {
                    //Pending
                    tripSheet.TripSheetStatusID = Guid.Parse("7cea5c5d-9292-4442-9402-79306c0d9580");
                    _context.DeliveryTripSheets.Update(tripSheet);

                    //Just to initalize the Deliveries in the deliveryGroupVM
                    List<ClientDeliveryVM> ClientOrdersList = new();

                    UpdateDeliveryGroupVM deliveryGroupVM = new()
                    {
                        Alias = tripSheet.Alias,
                        AssistantID = tripSheet.AssistantID,
                        DeliveryTripSheetID = tripSheet.DeliveryTripSheetID,
                        DriverID = tripSheet.DriverID,
                        SupervisorID = tripSheet.SupervisorID,
                        TripSheetStatusID = tripSheet.TripSheetStatusID,
                        VehicleID = tripSheet.VehicleID,
                        Deliveries = ClientOrdersList
                    };

                    //Get Orders 
                    var orders = _context.Orders
                        .Include(o => o.Client)
                        .Where(o => o.DeliveryTripSheetID == tripSheet.DeliveryTripSheetID)
                        .ToList();

                    orders.ForEach((order) =>
                    {
                        //Get Address
                        var clientAddress = _context.Addresses
                            .Where(ad => ad.AddressID == order.AddressID)
                            .FirstOrDefault();

                        AddressDTO addressDTO = new()
                        {
                            AddressID = clientAddress.AddressID,
                            AddressTypeID = clientAddress.AddressTypeID,
                            CityName = clientAddress.CityName,
                            ComplexName = clientAddress.ComplexName,
                            CountryName = clientAddress.CountryName,
                            isActive = clientAddress.isActive,
                            PostalCode = clientAddress.PostalCode,
                            Province = clientAddress.Province,
                            StreetName = clientAddress.StreetName,
                            StreetNumber = clientAddress.StreetNumber,
                            SuburbName = clientAddress.SuburbName,
                            UnitNumber = clientAddress.UnitNumber,
                        };

                        List<ClientOrderProductVM> clientOrderProducts = new();

                        var orderLines = _context.OrderLines
                            .AsNoTracking()
                            .Include(orderLine => orderLine.Product)
                            .Where(orderLine => orderLine.OrderID == order.OrderID)
                            .ToList();

                        foreach (var line in orderLines)
                        {
                            ClientOrderProductVM clientOrderProductVM = new ClientOrderProductVM()
                            {
                                ProductID = line.ProductID,
                                ProductName = line.Product.ProductName,
                                Quantity = line.Quantity
                            };

                            clientOrderProducts.Add(clientOrderProductVM);
                        }

                        ClientDeliveryVM clientOrder = new()
                        {
                            ClientID = order.ClientID,
                            ClientName = order.Client.ClientName,
                            OrderID = order.OrderID,
                            ClientAddress = addressDTO,
                            ClientOrderProducts = clientOrderProducts
                        };
                        deliveryGroupVM.Deliveries.Add(clientOrder);
                    });

                    DeliveryGroupList.Add(deliveryGroupVM);

                    //Get Vehicle
                    var vehicle = _context.Vehicles
                        .Where(v => v.VehicleID == tripSheet.VehicleID)
                        .FirstOrDefault();
                    //Available
                    vehicle.VehicleStatusID = Guid.Parse("65563356-8382-4c0d-8e2c-f94c98cb786b");
                    _context.Vehicles.Update(vehicle);

                    //Get Driver
                    var driver = _context.Employees
                        .Where(e => e.EmployeeID == tripSheet.DriverID)
                        .FirstOrDefault();
                    //Available
                    driver.EmployeeStatusID = Guid.Parse("9664ce89-cfac-4904-b5eb-7dda3a038b21");
                    _context.Employees.Update(driver);

                    //Get Assitant
                    var assistant = _context.Employees
                        .Where(e => e.EmployeeID == tripSheet.AssistantID)
                        .FirstOrDefault();
                    //Available
                    assistant.EmployeeStatusID = Guid.Parse("9664ce89-cfac-4904-b5eb-7dda3a038b21");
                    _context.Employees.Update(assistant);

                    // Get Supervisor
                    var supervisor = _context.Employees
                        .Where(e => e.EmployeeID == tripSheet.SupervisorID)
                        .FirstOrDefault();
                    //Available
                    supervisor.EmployeeStatusID = Guid.Parse("9664ce89-cfac-4904-b5eb-7dda3a038b21");
                    _context.Employees.Update(supervisor);

                });// end of tripSheets.ForEach

                await _context.SaveChangesAsync();
                return DeliveryGroupList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<UpdateDeliveryGroupVM>>> UndoUpdateTripSheet()
        {
            try
            {
                var tripSheets = await _context.DeliveryTripSheets
                .Include(ts => ts.TripSheetStatus)
                .Where(ts => ts.TripSheetStatus.Description == "Pending")
                .ToListAsync();

                tripSheets.ForEach(tripSheet =>
                {
                    //Ready for Loading
                    tripSheet.TripSheetStatusID = Guid.Parse("b630f0e0-8e59-4aff-a52e-bbff948eb9ff");
                    _context.DeliveryTripSheets.Update(tripSheet);

                    //Get Vehicle
                    var vehicle = _context.Vehicles
                        .Where(v => v.VehicleID == tripSheet.VehicleID)
                        .FirstOrDefault();
                    //Making Delivery
                    vehicle.VehicleStatusID = Guid.Parse("8631cd9e-9782-4098-ae61-31847bd47f8a");
                    _context.Vehicles.Update(vehicle);

                    //Get Driver
                    var driver = _context.Employees
                        .Where(e => e.EmployeeID == tripSheet.DriverID)
                        .FirstOrDefault();
                    //Not Available
                    driver.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                    _context.Employees.Update(driver);

                    //Get Assitant
                    var assistant = _context.Employees
                        .Where(e => e.EmployeeID == tripSheet.AssistantID)
                        .FirstOrDefault();
                    //Not Available
                    assistant.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                    _context.Employees.Update(assistant);

                    // Get Supervisor
                    var supervisor = _context.Employees
                        .Where(e => e.EmployeeID == tripSheet.SupervisorID)
                        .FirstOrDefault();
                    //Not Available
                    supervisor.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                    _context.Employees.Update(supervisor);

                });// end of tripSheets.ForEach

                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<VehicleDropdownVM>>> GetCheckinVehicles()
        {
            try
            {
                var tripSheets = await _context.DeliveryTripSheets
                    .AsNoTracking()
                    .Include(ts => ts.TripSheetStatus)
                    .Include(ts => ts.Vehicle)
                    .Where(ts => ts.TripSheetStatus.Description == "Delivered")
                    .OrderBy(ts => ts.Created)
                    .ToListAsync();

                List<VehicleDropdownVM> vehicleDropdownVMs = new();

                foreach (var tripSheet in tripSheets)
                {
                    VehicleDropdownVM vehicleDropdown = new()
                    {
                        Reg_Number = tripSheet.Vehicle.Reg_Number,
                        VehicleID = tripSheet.VehicleID
                    };

                    vehicleDropdownVMs.Add(vehicleDropdown);
                }

                return vehicleDropdownVMs;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> AddVehiclePicture(VehiclePictureVM picture)
        {
            try
            {
                //Get TripSheet
                var tripSheet = await _context.DeliveryTripSheets
                    .Where(ts => ts.DeliveryTripSheetID == picture.DeliveryTripSheetID)
                    .FirstOrDefaultAsync();

                tripSheet.VehicleValidationImage = picture.Picture;
                _context.DeliveryTripSheets.Update(tripSheet);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<ClientDeliveryVM>>> GetClientOrderList()
        {
            try
            {
                var todaysDate = DateTime.Now;

                var fullOrderList = await _context.Orders
                    .Include(o => o.OrderStatus)
                    .Include(o => o.Client)
                    .ToListAsync();

                List<Order> orders = new List<Order>();

                if (fullOrderList == null)
                {
                    return NotFound();
                }

                fullOrderList.ForEach((order) =>
                {
                    //Make sure the DeliveryDate is not null
                    if (order.DeliveryDate.HasValue)
                    {
                        if (order.DeliveryDate.Value.Date == todaysDate.Date
                            && order.OrderStatus.Description != "Cancelled"
                            && order.OrderStatus.Description != "Assigned to Trip Sheet"
                            && order.OrderStatus.Description != "Out for Delivery"
                            && order.OrderStatus.Description != "Delivered"
                            && order.OrderStatus.Description != "Loaded on Truck"
                        )
                        {
                            orders.Add(order);
                        }
                    }
                });

                List<ClientDeliveryVM> clientDeliveries = new();

                orders.ForEach((order) =>
                {
                    //Get Address
                    var clientAddress = _context.Addresses
                        .Where(ad => ad.AddressID == order.AddressID)
                        .FirstOrDefault();

                    AddressDTO addressDTO = new()
                    {
                        AddressID = clientAddress.AddressID,
                        AddressTypeID = clientAddress.AddressTypeID,
                        CityName = clientAddress.CityName,
                        ComplexName = clientAddress.ComplexName,
                        CountryName = clientAddress.CountryName,
                        isActive = clientAddress.isActive,
                        PostalCode= clientAddress.PostalCode,
                        Province = clientAddress.Province,
                        StreetName = clientAddress.StreetName,
                        StreetNumber = clientAddress.StreetNumber,
                        SuburbName = clientAddress.SuburbName,
                        UnitNumber = clientAddress.UnitNumber,
                    };


                    List<ClientOrderProductVM> clientOrderProducts = new();

                    var orderLines = _context.OrderLines
                        .AsNoTracking()
                        .Include(orderLine => orderLine.Product)
                        .Where(orderLine => orderLine.OrderID == order.OrderID)
                        .ToList();

                    foreach (var line in orderLines)
                    {
                        ClientOrderProductVM clientOrderProductVM = new ClientOrderProductVM()
                        {
                            ProductID = line.ProductID,
                            ProductName = line.Product.ProductName,
                            Quantity = line.Quantity
                        };

                        clientOrderProducts.Add(clientOrderProductVM);
                    }

                    //order.OrderLine.ToList().ForEach(orderline =>
                    //{
                    //    ClientOrderProductVM clientOrderProduct = new()
                    //    {
                    //        ProductID = orderline.ProductID,
                    //        ProductName = orderline.Product.ProductName
                    //    };
                    //});

                    ClientDeliveryVM clientDeliveryVM = new()
                    {
                        ClientID = order.ClientID,
                        ClientName = order.Client.ClientName,
                        OrderID = order.OrderID,
                        ClientAddress = addressDTO,
                        ClientOrderProducts = clientOrderProducts
                    };
                    clientDeliveries.Add(clientDeliveryVM);
                });

                return clientDeliveries;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> GenerateTripSheets(List<DeliveryGroupVM> deliveryGroups)
        {
            try
            {
                deliveryGroups.ForEach((group) =>
                {
                    var vehicle = _context.Vehicles
                        .AsNoTracking()
                        .Where(v => v.VehicleID == group.VehicleID)
                        .FirstOrDefault();
                    

                    DeliveryTripSheet tripSheet = new()
                    {
                        DeliveryTripSheetID = Guid.NewGuid(),
                        Alias = group.Alias,
                        Created = DateTime.Now,
                        //InvoiceBookID = null,
                        VehicleID = group.VehicleID,
                        DriverID = group.DriverID,
                        AssistantID = group.AssistantID,
                        SupervisorID = group.SupervisorID,
                        //Ready for Loading
                        TripSheetStatusID = Guid.Parse("b630f0e0-8e59-4aff-a52e-bbff948eb9ff"),
                        DepatureTime = null,
                        ReturnTime = null,
                        Comment = null,
                        StartKM = vehicle.Mileage,
                        EndKM = null
                    };
                    _context.DeliveryTripSheets.Add(tripSheet);

                    //Making Delivery
                    vehicle.VehicleStatusID = Guid.Parse("8631cd9e-9782-4098-ae61-31847bd47f8a");
                    _context.Vehicles.Update(vehicle);

                    //Get Driver
                    var driver = _context.Employees
                        .Where(e => e.EmployeeID == tripSheet.DriverID)
                        .FirstOrDefault();
                    //Not Available
                    driver.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                    _context.Employees.Update(driver);

                    //Get Assitant
                    var assistant = _context.Employees
                        .Where(e => e.EmployeeID == tripSheet.AssistantID)
                        .FirstOrDefault();
                    //Not Available
                    assistant.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                    _context.Employees.Update(assistant);

                    // Get Supervisor
                    var supervisor = _context.Employees
                        .Where(e => e.EmployeeID == tripSheet.SupervisorID)
                        .FirstOrDefault();
                    //Not Available
                    supervisor.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                    _context.Employees.Update(supervisor);

                    group.Deliveries.ForEach((delivery) =>
                    {
                        var order = _context.Orders
                            //.AsNoTracking()
                            .Where(o => o.OrderID == delivery.OrderID)
                            .FirstOrDefault();

                        order.DeliveryTripSheetID = tripSheet.DeliveryTripSheetID;
                        //Assigned to TripSheet
                        order.OrderStatusID = Guid.Parse("a4120e1f-1a87-466a-ae62-0f6bcc357877");

                        _context.Orders.Update(order);
                    });

                });

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Create Tripsheet",
                    ActionData = "Multiple Tripsheets Created",
                    EmployeeAction = "Create",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> UpdateTripSheets(ReturnUpdateDeliveryGroupVM FullTripSheet)
        {
            try
            {
                FullTripSheet.DeliveryGroups.ForEach((group) =>
                {
                    // Get TripSheet
                    var tripsheet = _context.DeliveryTripSheets
                        .Where(ts => ts.DeliveryTripSheetID == group.DeliveryTripSheetID)
                        .FirstOrDefault();

                    if (tripsheet == null)
                    {
                        var vehicle = _context.Vehicles
                            .Where(v => v.VehicleID == group.VehicleID)
                            .FirstOrDefault();

                        //Create new TripSheet
                        DeliveryTripSheet newTripSheet = new()
                        {
                            DeliveryTripSheetID = Guid.NewGuid(),
                            Alias = group.Alias,
                            Created = DateTime.Now,
                            //InvoiceBookID = null,
                            VehicleID = group.VehicleID,
                            DriverID = group.DriverID,
                            AssistantID = group.AssistantID,
                            SupervisorID = group.SupervisorID,
                            //Ready for Loading
                            TripSheetStatusID = Guid.Parse("b630f0e0-8e59-4aff-a52e-bbff948eb9ff"),
                            DepatureTime = null,
                            ReturnTime = null,
                            Comment = null,
                            StartKM = vehicle.Mileage,
                            EndKM = null
                        };
                        _context.DeliveryTripSheets.Add(newTripSheet);

                        group.Deliveries.ForEach((clientOrder) =>
                        {
                            //Get order
                            var order = _context.Orders
                                .Where(o => o.OrderID == clientOrder.OrderID)
                                .FirstOrDefault();

                            order.DeliveryTripSheetID = newTripSheet.DeliveryTripSheetID;
                            //Assigned to TripSheet
                            order.OrderStatusID = Guid.Parse("a4120e1f-1a87-466a-ae62-0f6bcc357877");
                            _context.Orders.Update(order);
                        });

                        //Making Delivery
                        vehicle.VehicleStatusID = Guid.Parse("8631cd9e-9782-4098-ae61-31847bd47f8a");
                        _context.Vehicles.Update(vehicle);

                        //Get Driver
                        var driver = _context.Employees
                            .Where(e => e.EmployeeID == newTripSheet.DriverID)
                            .FirstOrDefault();
                        //Not Available
                        driver.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                        _context.Employees.Update(driver);

                        //Get Assitant
                        var assistant = _context.Employees
                            .Where(e => e.EmployeeID == newTripSheet.AssistantID)
                            .FirstOrDefault();
                        //Not Available
                        assistant.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                        _context.Employees.Update(assistant);

                        // Get Supervisor
                        var supervisor = _context.Employees
                            .Where(e => e.EmployeeID == newTripSheet.SupervisorID)
                            .FirstOrDefault();
                        //Not Available
                        supervisor.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                        _context.Employees.Update(supervisor);


                    }
                    else
                    {
                        //Ready for Loading
                        tripsheet.TripSheetStatusID = Guid.Parse("b630f0e0-8e59-4aff-a52e-bbff948eb9ff");
                        _context.DeliveryTripSheets.Update(tripsheet);
                        //Get Vehicle
                        var vehicle = _context.Vehicles
                            .Where(v => v.VehicleID == tripsheet.VehicleID)
                            .FirstOrDefault();
                        //Making Delivery
                        vehicle.VehicleStatusID = Guid.Parse("8631cd9e-9782-4098-ae61-31847bd47f8a");
                        _context.Vehicles.Update(vehicle);

                        //Get Driver
                        var driver = _context.Employees
                            .Where(e => e.EmployeeID == tripsheet.DriverID)
                            .FirstOrDefault();
                        //Not Available
                        driver.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                        _context.Employees.Update(driver);

                        //Get Assitant
                        var assistant = _context.Employees
                            .Where(e => e.EmployeeID == tripsheet.AssistantID)
                            .FirstOrDefault();
                        //Not Available
                        assistant.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                        _context.Employees.Update(assistant);

                        // Get Supervisor
                        var supervisor = _context.Employees
                            .Where(e => e.EmployeeID == tripsheet.SupervisorID)
                            .FirstOrDefault();
                        //Not Available
                        supervisor.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                        _context.Employees.Update(supervisor);

                        group.Deliveries.ForEach((clientOrder) =>
                        {
                            //Get order
                            var order = _context.Orders
                                .Where(o => o.OrderID == clientOrder.OrderID)
                                .FirstOrDefault();

                            order.DeliveryTripSheetID = tripsheet.DeliveryTripSheetID;
                            //Assigned to TripSheet
                            order.OrderStatusID = Guid.Parse("a4120e1f-1a87-466a-ae62-0f6bcc357877");
                            _context.Orders.Update(order);
                        });
                    }
                });

                FullTripSheet.ClientOrders.ForEach((order) =>
                {
                    //Get Order
                    var ClientOrder = _context.Orders
                        .Where(co => co.OrderID == order.OrderID)
                        .FirstOrDefault();
                    //Pending
                    ClientOrder.OrderStatusID = Guid.Parse("9fb2e19a-be94-45ac-8de2-96a1d411b796");
                    ClientOrder.DeliveryTripSheetID = null;
                    _context.Orders.Update(ClientOrder);
                });

                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> RemoveTripSheet(UpdateDeliveryGroupVM DeliveryGroup)
        {
            try
            {
                var tripSheet = await _context.DeliveryTripSheets
                    .Where(ts => ts.DeliveryTripSheetID == DeliveryGroup.DeliveryTripSheetID)
                    .FirstOrDefaultAsync();

                //Cancelled
                tripSheet.TripSheetStatusID = Guid.Parse("4c53fe83-fb7d-49f1-92ae-6ff72aaec10c");
                _context.DeliveryTripSheets.Update(tripSheet);

                //Get Vehicle
                var vehicle = _context.Vehicles
                    .Where(v => v.VehicleID == tripSheet.VehicleID)
                    .FirstOrDefault();
                //Available
                vehicle.VehicleStatusID = Guid.Parse("65563356-8382-4c0d-8e2c-f94c98cb786b");
                _context.Vehicles.Update(vehicle);

                //Get Driver
                var driver = _context.Employees
                    .Where(e => e.EmployeeID == tripSheet.DriverID)
                    .FirstOrDefault();
                //Available
                driver.EmployeeStatusID = Guid.Parse("9664ce89-cfac-4904-b5eb-7dda3a038b21");
                _context.Employees.Update(driver);

                //Get Assitant
                var assistant = _context.Employees
                    .Where(e => e.EmployeeID == tripSheet.AssistantID)
                    .FirstOrDefault();
                //Available
                assistant.EmployeeStatusID = Guid.Parse("9664ce89-cfac-4904-b5eb-7dda3a038b21");
                _context.Employees.Update(assistant);

                // Get Supervisor
                var supervisor = _context.Employees
                    .Where(e => e.EmployeeID == tripSheet.SupervisorID)
                    .FirstOrDefault();
                //Available
                supervisor.EmployeeStatusID = Guid.Parse("9664ce89-cfac-4904-b5eb-7dda3a038b21");
                _context.Employees.Update(supervisor);

                var orders = await _context.Orders
                    .Where(o => o.DeliveryTripSheetID == tripSheet.DeliveryTripSheetID)
                    .ToListAsync();

                orders.ForEach((order =>
                {
                    order.DeliveryTripSheetID = null;
                    //Pending
                    order.OrderStatusID = Guid.Parse("9fb2e19a-be94-45ac-8de2-96a1d411b796");

                    _context.Orders.Update(order);
                }));

                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<TripSheetVM>> GetDriverTripSheetVM(Guid EmployeeID)
        {
            try
            {
                var employee = await _context.Employees
                    .AsNoTracking()
                    .Include(em => em.EmployeeType)
                    .Where(em => em.EmployeeID == EmployeeID)
                    .FirstOrDefaultAsync();

                DeliveryTripSheet tripSheet = new();

                if (employee.EmployeeType.Description == "Driver")
                {
                    tripSheet = await _context.DeliveryTripSheets
                        .AsNoTracking()
                        .Include(ts => ts.TripSheetStatus)
                        .Include(ts => ts.Driver)
                        .Include(ts => ts.Assistant)
                        .Include(ts => ts.Supervisor)
                        .Include(ts => ts.Vehicle)
                        .Where(ts => ts.DriverID == EmployeeID 
                        && (ts.TripSheetStatus.Description == "Ready for Loading" 
                        || ts.TripSheetStatus.Description == "Ready for Delivery"
                        || ts.TripSheetStatus.Description == "Loading Started"
                        || ts.TripSheetStatus.Description == "Delivered"
                        || ts.TripSheetStatus.Description == "Out For Delivery"))
                        .OrderBy(ts => ts.Created)
                        .FirstOrDefaultAsync();
                }
                else if (employee.EmployeeType.Description == "Assistant")
                {
                    tripSheet = await _context.DeliveryTripSheets
                        .AsNoTracking()
                        .Include(ts => ts.TripSheetStatus)
                        .Include(ts => ts.Driver)
                        .Include(ts => ts.Assistant)
                        .Include(ts => ts.Supervisor)
                        .Include(ts => ts.Vehicle)
                        .Where(ts => ts.AssistantID == EmployeeID
                        && (ts.TripSheetStatus.Description == "Ready for Loading"
                        || ts.TripSheetStatus.Description == "Ready for Delivery"
                        || ts.TripSheetStatus.Description == "Loading Started"
                        || ts.TripSheetStatus.Description == "Delivered"
                        || ts.TripSheetStatus.Description == "Out For Delivery"))
                        .OrderByDescending(ts => ts.Created)
                        .FirstOrDefaultAsync();
                }
                else if (employee.EmployeeType.Description == "Supervisor")
                {
                    tripSheet = await _context.DeliveryTripSheets
                        .AsNoTracking()
                        .Include(ts => ts.TripSheetStatus)
                        .Include(ts => ts.Driver)
                        .Include(ts => ts.Assistant)
                        .Include(ts => ts.Supervisor)
                        .Include(ts => ts.Vehicle)
                        .Where(ts => ts.SupervisorID == EmployeeID
                        && (ts.TripSheetStatus.Description == "Ready for Loading"
                        || ts.TripSheetStatus.Description == "Ready for Delivery"
                        || ts.TripSheetStatus.Description == "Loading Started"
                        || ts.TripSheetStatus.Description == "Delivered"
                        || ts.TripSheetStatus.Description == "Out For Delivery"))
                        .OrderByDescending(ts => ts.Created)
                        .FirstOrDefaultAsync();
                }
                else
                {
                    return Unauthorized();
                }

                if (tripSheet == null)
                {
                    return NotFound();
                }

                var orders = await _context.Orders
                    .AsNoTracking()
                    .Include(o => o.Client)
                    .Where(o => o.DeliveryTripSheetID == tripSheet.DeliveryTripSheetID)
                    .ToListAsync();

                if (orders == null)
                {
                    return NotFound();
                }

                var ExtraProducts = await _context.ExtraProductsTaken
                    .AsNoTracking()
                    .Include(eb => eb.Product)
                    .Where(eb => eb.DeliveryTripSheetID == tripSheet.DeliveryTripSheetID)
                    .ToListAsync();

                TripSheetVM tripSheetVM = new TripSheetVM()
                {
                    DeliveryTripSheetID = tripSheet.DeliveryTripSheetID,
                    TripSheetStatusID = tripSheet.TripSheetStatusID,
                    Alias = tripSheet.Alias,
                    AssistantName = tripSheet.Assistant.FirstName + ' ' + tripSheet.Assistant.Surname,
                    DriverName = tripSheet.Driver.FirstName + ' ' + tripSheet.Driver.Surname,
                    SupervisorName = tripSheet.Supervisor.FirstName + ' ' + tripSheet.Supervisor.Surname,
                    VehicleRegNumber = tripSheet.Vehicle.Reg_Number,
                    Created = tripSheet.Created,
                    DepartureTime = tripSheet.DepatureTime,
                    FinalLoaded = tripSheet.FinalLoaded,
                    FinalDelivered = tripSheet.FinalDelivered,
                    VehicleValidationImage = tripSheet.VehicleValidationImage,
                    TripSheetOrders = new List<TripSheetOrderVM>(),
                    ExtraProducts = new List<ExtraProductsVM>()
                };

                List<TripSheetOrderVM> TripSheetOrders = new();

                orders.ForEach((order) =>
                {
                    TripSheetOrderVM orderVM = new()
                    {
                        DeliveryTripSheetID = tripSheet.DeliveryTripSheetID,
                        ClientName = order.Client.ClientName,
                        Delivered = order.Delivered,
                        Loaded = order.Loaded,
                        OrderID = order.OrderID,
                    };
                    TripSheetOrders.Add(orderVM);
                });

                tripSheetVM.TripSheetOrders = TripSheetOrders;

                List<ExtraProductsVM> ExtraProductVMs = new();

                if (ExtraProducts.Count == 0)
                {
                    tripSheetVM.ExtraProducts = ExtraProductVMs;
                }
                else
                {
                    ExtraProducts.ForEach((product) =>
                    {
                        ExtraProductsVM extraProductVM = new()
                        {
                            DeliveryTripSheetID = product.DeliveryTripSheetID,
                            ExtraProductsTakenID = product.ExtraProductTakenID,
                            ProductID = product.ProductID,
                            QuantityTaken = product.QuantityTaken,
                            ProductName = product.Product.ProductName,
                            QuantityRemaining = product.QuantityRemaining
                        };

                        ExtraProductVMs.Add(extraProductVM);
                    });

                    tripSheetVM.ExtraProducts = ExtraProductVMs;
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "View Tripsheet On Mobile App",
                    ActionData = "N/A",
                    EmployeeAction = "View",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return tripSheetVM;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> AddExtraProduct(ExtraProductTaken extraProduct)
        {

            try
            {
                var checkExtraProduct = await _context.ExtraProductsTaken
                    .Where(cep => cep.DeliveryTripSheetID == extraProduct.DeliveryTripSheetID)
                    .FirstOrDefaultAsync();

                if (checkExtraProduct == null)
                {
                    var CurrentID = await _context.ProductPrices.Where(zz => zz.ProductID == extraProduct.ProductID).OrderByDescending(zz => zz.Date).Select(zz => zz.PriceID).FirstOrDefaultAsync();
                   
                    extraProduct.ExtraProductTakenID = Guid.NewGuid();
                    extraProduct.ProductPriceID = CurrentID;
                    _context.ExtraProductsTaken.Add(extraProduct);
                }
                else
                {
                    checkExtraProduct.QuantityTaken = checkExtraProduct.QuantityTaken + extraProduct.QuantityTaken;
                    _context.ExtraProductsTaken.Update(checkExtraProduct);
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Create Extra Product",
                    ActionData = "Added Extra Product",
                    EmployeeAction = "Create",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

               

                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<ExtraProductsVM>>> GetExtraProducts(Guid TripSheetID)
        {
            try
            {
                var extraProducts = await _context.ExtraProductsTaken
                    .Include(ep => ep.Product)
                    .Where(ep => ep.DeliveryTripSheetID == TripSheetID)
                    .ToListAsync();

                List<ExtraProductsVM> extraProductsVMs = new();

                if (extraProducts.Count == 0)
                {
                    return extraProductsVMs;
                }
                else
                {
                    extraProducts.ForEach((extraProduct) =>
                    {
                        ExtraProductsVM productsVM = new()
                        {
                            DeliveryTripSheetID = TripSheetID,
                            ExtraProductsTakenID = extraProduct.ExtraProductTakenID,
                            ProductID = extraProduct.ProductID,
                            ProductName = extraProduct.Product.ProductName,
                            QuantityTaken = extraProduct.QuantityTaken,
                            QuantityRemaining = extraProduct.QuantityRemaining
                        };
                        extraProductsVMs.Add(productsVM);
                    });

                    return extraProductsVMs;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> AddOrderExtraProduct(OrderExtraProductTaken orderExtraProduct)
        {
            try
            {

                var extraProduct = await _context.ExtraProductsTaken
                    .Include(ept => ept.Product)
                    .Where(ept => ept.ExtraProductTakenID == orderExtraProduct.ExtraProductTakenID)
                    .FirstOrDefaultAsync();

                extraProduct.QuantityRemaining -= orderExtraProduct.Quantity;

                _context.ExtraProductsTaken.Update(extraProduct);

                var checkOrderExtraProduct = await _context.OrderExtraProductsTaken
                    .Where(oep => oep.ExtraProductTakenID == orderExtraProduct.ExtraProductTakenID)
                    .FirstOrDefaultAsync();

                if (checkOrderExtraProduct == null)
                {
                    _context.OrderExtraProductsTaken.Add(orderExtraProduct);
                }
                else
                {
                    checkOrderExtraProduct.Quantity = checkOrderExtraProduct.Quantity + orderExtraProduct.Quantity;
                    _context.OrderExtraProductsTaken.Update(checkOrderExtraProduct);
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Add Extra Product To Order",
                    ActionData = "Added Extra Product To Order",
                    EmployeeAction = "Create",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return Ok();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<ExtraProductDropdown>>> GetExtraProductDropdown(Guid TripSheetID)
        {
            try
            {
                var extraProductsTaken = await _context.ExtraProductsTaken
                    .AsNoTracking()
                    .Include(ept => ept.Product)
                    .Where(ept => ept.DeliveryTripSheetID == TripSheetID)
                    .ToListAsync();

                List<ExtraProductDropdown> extraProductDropdowns = new();

                if (extraProductsTaken.Count == 0)
                {
                    return extraProductDropdowns;
                }

                extraProductsTaken.ForEach((extraProduct) =>
                {
                    ExtraProductDropdown extraProductDropdown = new()
                    {
                        ExtraProductTakenID = extraProduct.ExtraProductTakenID,
                        QuantityRemaining = extraProduct.QuantityRemaining,
                        ProductName = extraProduct.Product.ProductName,
                    };
                    extraProductDropdowns.Add(extraProductDropdown);
                });

                return extraProductDropdowns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<ClientOrderVM>> GetClientOrder(Guid OrderID)
        {
            try
            {
                decimal OrderTotal = 0;
                var order = await _context.Orders
                    .AsNoTracking()
                    .Include(o => o.Client)
                    .Include(o => o.Address)
                    .Where(o => o.OrderID == OrderID)
                    .FirstOrDefaultAsync();

                var orderLines = await _context.OrderLines
                    .AsNoTracking()
                    .Include(ol => ol.Product)
                    .Where(ol => ol.OrderID == OrderID)
                    .ToListAsync();

                var OrderExtraProducts = await _context.OrderExtraProductsTaken
                    .AsNoTracking()
                    .Include(ep => ep.ExtraProductTaken)
                    .Include(ep => ep.ExtraProductTaken.Product)
                    .Where(ep => ep.OrderID == OrderID)
                    .ToListAsync();

                List<OrderExtraProductVM> orderExtraProductsVM = new();

                if (OrderExtraProducts.Count > 0)
                {
                    OrderExtraProducts.ForEach((extraProduct) =>
                    {
                        OrderExtraProductVM orderExtraProduct = new()
                        {
                            ExtraProductTakenID = extraProduct.ExtraProductTakenID,
                            OrderID = extraProduct.OrderID,
                            ProductName = extraProduct.ExtraProductTaken.Product.ProductName,
                            Quantity = extraProduct.Quantity
                        };

                        var Price = _context.ProductPrices.Where(zz => zz.ProductPriceID == extraProduct.ExtraProductTaken.ProductPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefault();
                    
                        OrderTotal += (extraProduct.Quantity * Price);

                        orderExtraProductsVM.Add(orderExtraProduct);
                    });
                }

                List<ClientOrderLineVM> ClientOrderList = new();

                orderLines.ForEach((line) =>
                {
                    ClientOrderLineVM orderVM = new()
                    {
                        ProductName = line.Product.ProductName,
                        Quantity = line.Quantity
                    };

                    ClientOrderList.Add(orderVM);

                    
                    var Price = _context.ProductPrices.Where(zz => zz.ProductPriceID == line.ProductPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefault();
                    OrderTotal += line.Quantity * Price;
                });

                AddressVM addressVM = new()
                {
                    City = order.Address.CityName,
                    Country = order.Address.CountryName,
                    ComplexName = order.Address.ComplexName,
                    PostalCode = order.Address.PostalCode,
                    Province = order.Address.Province,
                    StreetName = order.Address.StreetName,
                    StreetNumber = order.Address.StreetNumber,
                    Suburb = order.Address.SuburbName,
                    UnitNumber = order.Address.UnitNumber,
                };

                ClientOrderVM clientOrder = new();
                clientOrder.ClientName = order.Client.ClientName;
                clientOrder.OrderID = OrderID;
                clientOrder.OrderDate = order.OrderDate;
                clientOrder.OrderTotal = OrderTotal;
                clientOrder.ClientAddress = addressVM;
                clientOrder.OrderLines = ClientOrderList;
                clientOrder.OrderExtraProducts = orderExtraProductsVM;

                return clientOrder;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> ChangeOrderLoaded(Guid OrderID)
        {
            try
            {
                var order = await _context.Orders
                    .Where(o => o.OrderID == OrderID)
                    .FirstOrDefaultAsync();
                var tripsheet = await _context.DeliveryTripSheets
                    .Where(ts => ts.DeliveryTripSheetID == order.DeliveryTripSheetID)
                    .FirstOrDefaultAsync();

                //Get Vehicle
                var vehicle = _context.Vehicles
                    .Where(v => v.VehicleID == tripsheet.VehicleID)
                    .FirstOrDefault();
                //Making Delivery
                vehicle.VehicleStatusID = Guid.Parse("8631cd9e-9782-4098-ae61-31847bd47f8a");
                _context.Vehicles.Update(vehicle);

                //Get Driver
                var driver = _context.Employees
                    .Where(e => e.EmployeeID == tripsheet.DriverID)
                    .FirstOrDefault();
                //Not available
                driver.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                _context.Employees.Update(driver);

                //Get Assitant
                var assistant = _context.Employees
                    .Where(e => e.EmployeeID == tripsheet.AssistantID)
                    .FirstOrDefault();
                //Not available
                assistant.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                _context.Employees.Update(assistant);

                // Get Supervisor
                var supervisor = _context.Employees
                    .Where(e => e.EmployeeID == tripsheet.SupervisorID)
                    .FirstOrDefault();
                //Not available
                assistant.EmployeeStatusID = Guid.Parse("bc4de5bb-8761-43f9-a734-5998fa44985a");
                _context.Employees.Update(supervisor);

                if (order.Loaded)
                {
                    order.Loaded = false;
                    //Assigned to Trip Sheet
                    order.OrderStatusID = Guid.Parse("a4120e1f-1a87-466a-ae62-0f6bcc357877");
                }
                else
                {
                    order.Loaded = true;
                    //Loaded on Truck
                    order.OrderStatusID = Guid.Parse("502d6f63-525a-4efc-bdcb-3560b0edff2c");
                    // Loading Started
                    tripsheet.TripSheetStatusID = Guid.Parse("0a53ed59-9c2b-44bf-a183-8f73cb7b8563");
                }

                _context.Orders.Update(order);
                _context.DeliveryTripSheets.Update(tripsheet);

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Set Order as Loaded",
                    ActionData = "Set Order as Loaded for Order : " + order.OrderID,
                    EmployeeAction = "Update",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> ChangeOrderDelivered(Guid OrderID)
        {
            try
            {
                var order = await _context.Orders
                    .Where(o => o.OrderID == OrderID)
                    .FirstOrDefaultAsync();

                if (order.Delivered)
                {
                    order.Delivered = false;
                    //Out for Delivery
                    order.OrderStatusID = Guid.Parse("c10bfe5e-4c56-4551-b1f2-8fd3321a4d1b");
                }
                else
                {
                    order.Delivered = true;
                    //Delivered
                    order.OrderStatusID = Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85");
                }

                List<Inventory> InventoryToUpdate = new();

                var orderLines = await _context.OrderLines
                    .Include(ol => ol.Product)
                    .Include(ol => ol.Product.Inventory)
                    .Where(o => o.OrderID == OrderID)
                    .ToListAsync();

                orderLines.ForEach((orderline) =>
                {
                    //get inventory Item
                    var orderLineInventory = _context.Inventories
                    .Where(i => i.InventoryID == orderline.Product.InventoryID)
                    .FirstOrDefault();

                    orderLineInventory.Quantity = orderLineInventory.Quantity - orderline.Quantity;

                    InventoryToUpdate.Add(orderLineInventory);

                    //_context.Inventories.Update(orderLineInventory);
                });

                var orderExtraProducts = await _context.OrderExtraProductsTaken
                    .Include(oep => oep.ExtraProductTaken)
                    .Include(oep => oep.ExtraProductTaken.Product)
                    .Where(oep => oep.OrderID == OrderID)
                    .ToListAsync();

                if (orderExtraProducts.Count > 0)
                {
                    orderExtraProducts.ForEach((orderExtraProduct) =>
                    {
                        // get inventory Item

                        var extraProductInventory = _context.Inventories
                            .Where(i => i.InventoryID == orderExtraProduct.ExtraProductTaken.Product.InventoryID)
                            .FirstOrDefault();

                        var extraInventoryToUpdate = InventoryToUpdate
                            .Where(itu => itu.InventoryID == extraProductInventory.InventoryID)
                            .FirstOrDefault();

                        if (extraInventoryToUpdate == null)
                        {
                            extraProductInventory.Quantity = extraProductInventory.Quantity - extraProductInventory.Quantity;
                            InventoryToUpdate.Add(extraProductInventory);
                        }
                        else
                        {
                            extraInventoryToUpdate.Quantity = extraInventoryToUpdate.Quantity - extraInventoryToUpdate.Quantity;
                            var index = InventoryToUpdate.FindIndex(itu => itu.InventoryID == extraProductInventory.InventoryID);
                            InventoryToUpdate[index].Quantity = extraInventoryToUpdate.Quantity;
                        };
                    });
                }
                NotificationDTO notificationDTO = new();
                Notification notification = new();

                InventoryToUpdate.ForEach((inventory) =>
                {
                    if (inventory.Quantity < 100)
                    {
                        

                        notificationDTO.NotificationID = Guid.NewGuid();
                        notificationDTO.NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5");
                        notificationDTO.EmployeeID = Guid.Parse("f11765a0-58d2-42e5-9c5c-34022016e8c6");
                        notificationDTO.NotificationTitle = "Inventory Item" + " " + inventory.ItemName + " Stock is Low";
                        notificationDTO.NotificationDescription = "Inventory Item" + " " + inventory.ItemName + " Stock is currently: "  + inventory.Quantity;
                        notificationDTO.IsRead = false;
                        notificationDTO.Created = DateTime.Now;

                        
                      

                        notification.NotificationID = notificationDTO.NotificationID;
                        notification.NotificationTypeID = notificationDTO.NotificationTypeID;
                        notification.EmployeeID = notificationDTO.EmployeeID;
                        notification.NotificationTitle = notificationDTO.NotificationTitle;
                        notification.NotificationDescription = notificationDTO.NotificationDescription;
                        notification.IsRead = notificationDTO.IsRead;
                        notification.Created = notificationDTO.Created;


                        _context.Notifications.Add(notification);
                        
                       
                    }
                    _context.Inventories.Update(inventory);
                });

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Set Order as Delivered",
                    ActionData = "Set Order as Delivered for Order : " + order.OrderID,
                    EmployeeAction = "Update",
                    EmployeeID = Guid.Parse(UserID)

                };

                
                _context.AuditTrails.Add(auditTrail);
                _context.Orders.Update(order);
                await _context.SaveChangesAsync();
                _ = _NotHub.SendNotification(notificationDTO);

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> ChangeFinalLoaded(Guid TripSheetID)
        {
            try
            {
                var tripSheet = await _context.DeliveryTripSheets
                    .Where(ts => ts.DeliveryTripSheetID == TripSheetID)
                    .FirstOrDefaultAsync();

                if (tripSheet.FinalLoaded)
                {
                    tripSheet.FinalLoaded = false;
                }
                else
                {
                    tripSheet.FinalLoaded = true;
                    //Ready for Delivery
                    tripSheet.TripSheetStatusID = Guid.Parse("29bf4aa1-2c04-47f7-a801-046011236f43");
                }

                _context.DeliveryTripSheets.Update(tripSheet);

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Set Tripsheet as Loaded",
                    ActionData = "Set as Loaded for Tripsheet : " + tripSheet.DeliveryTripSheetID,
                    EmployeeAction = "Update",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> ChangeFinalDelivered(Guid TripSheetID)
        {
            try
            {
                var tripSheet = await _context.DeliveryTripSheets
                    .Where(ts => ts.DeliveryTripSheetID == TripSheetID)
                    .FirstOrDefaultAsync();

                if (tripSheet.FinalDelivered)
                {
                    tripSheet.FinalDelivered = false;
                }
                else
                {
                    tripSheet.FinalDelivered = true;
                    //Delivered
                    tripSheet.TripSheetStatusID = Guid.Parse("391710b4-dcb0-4ec3-a53c-ba64a055d7ef");
                }

                _context.DeliveryTripSheets.Update(tripSheet);
                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Set Tripsheet as Delivered",
                    ActionData = "Set as Delivered for Tripsheet : " + tripSheet.DeliveryTripSheetID,
                    EmployeeAction = "Update",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> ChangeOutForDelivery(Guid TripSheetID)
        {
            try
            {
                var tripSheet = await _context.DeliveryTripSheets
                    .Where(ts => ts.DeliveryTripSheetID == TripSheetID)
                    .FirstOrDefaultAsync();

                var orders = await _context.Orders
                    .Where(o => o.DeliveryTripSheetID == TripSheetID)
                    .ToListAsync();

                orders.ForEach(async (order) =>
                {
                    //Out for Delivery
                    order.OrderStatusID = Guid.Parse("c10bfe5e-4c56-4551-b1f2-8fd3321a4d1b");

                    var Contacts = _context.ClientContactPeople.Where(zz => zz.ClientID == order.ClientID).ToList();

                    
                    foreach (var contact in Contacts)
                    {
                        //string accountSid = Environment.GetEnvironmentVariable("TWILIO_ACCOUNT_SID");
                        //string authToken = Environment.GetEnvironmentVariable("TWILIO_AUTH_TOKEN");

                        string accountSid = Configuration["TWILIO:TWILIO_ACCOUNT_SID"];
                        string authToken = Configuration["TWILIO:TWILIO_AUTH_TOKEN"];

                        TwilioClient.Init(accountSid, authToken);

                        var message = MessageResource.Create(
                            body: "Good News Your Order is On the Way!",
                            from: new Twilio.Types.PhoneNumber("+18787787556"),
                            to: new Twilio.Types.PhoneNumber("+27" + contact.PhoneNumber.Substring(1, 9))
                        );
                    }
                        
                    
                    _context.Update(order);
                });
                // Out for Delivery
                tripSheet.TripSheetStatusID = Guid.Parse("b67ed3f5-dd4d-4785-a209-97832583f88c");
                tripSheet.DepatureTime = DateTime.Now;

                _context.DeliveryTripSheets.Update(tripSheet);

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Set Tripsheet as Out For Delivery",
                    ActionData = "Set Out for Delivery for Tripsheet : " + tripSheet.DeliveryTripSheetID,
                    EmployeeAction = "Update",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<CheckInVM>> GetCheckInInfo(Guid VehicleID)
        {
            try
            {
                var tripSheet = await _context.DeliveryTripSheets
                .Include(ts => ts.Driver)
                .Include(ts => ts.Assistant)
                .Include(ts => ts.Supervisor)
                .Include(ts => ts.Vehicle)
                .Where(ts => ts.VehicleID == VehicleID)
                .FirstOrDefaultAsync();

                var extraProducts = await _context.ExtraProductsTaken
                    .Include(ep => ep.Product)
                    .Where(ep => ep.DeliveryTripSheetID == tripSheet.DeliveryTripSheetID)
                    .ToListAsync();

                List<ExtraProductsVM> extraProductsVMs = new();

                extraProducts.ForEach((extraProduct) =>
                {
                    ExtraProductsVM extraProductsVM = new()
                    {
                        DeliveryTripSheetID = tripSheet.DeliveryTripSheetID,
                        ExtraProductsTakenID = extraProduct.DeliveryTripSheetID,
                        ProductID = extraProduct.ProductID,
                        ProductName = extraProduct.Product.ProductName,
                        QuantityRemaining = extraProduct.QuantityRemaining,
                        QuantityTaken = extraProduct.QuantityTaken,
                    };

                    extraProductsVMs.Add(extraProductsVM);

                });

                CheckInVM checkInVM = new()
                {
                    Alias = tripSheet.Alias,
                    AssistantName = tripSheet.Assistant.FirstName + ' ' + tripSheet.Assistant.Surname,
                    DriverName = tripSheet.Driver.FirstName + ' ' + tripSheet.Driver.Surname,
                    SupervisorName = tripSheet.Supervisor.FirstName + ' ' + tripSheet.Supervisor.Surname,
                    VehicleRegNumber = tripSheet.Vehicle.Reg_Number,
                    VehicleID = tripSheet.VehicleID,
                    Created = tripSheet.Created,
                    DeliveryTripSheetID = tripSheet.DeliveryTripSheetID,
                    DepatureTime = tripSheet.DepatureTime,
                    Mileage = tripSheet.Vehicle.Mileage,
                    ExtraProducts = extraProductsVMs
                };

                return checkInVM;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> CheckInVehicle(CheckInSubmitVM checkInVM)
        {
            try
            {
                var tripSheet = await _context.DeliveryTripSheets
                    .Where(ts => ts.DeliveryTripSheetID == checkInVM.DeliveryTripSheetID)
                    .FirstOrDefaultAsync();;

                var vehicle = await _context.Vehicles
                    .Where(v => v.VehicleID == tripSheet.VehicleID)
                    .FirstOrDefaultAsync();
                vehicle.Mileage = checkInVM.EndKM;
                //Available
                vehicle.VehicleStatusID = Guid.Parse("65563356-8382-4c0d-8e2c-f94c98cb786b");

                tripSheet.EndKM = checkInVM.EndKM;
                tripSheet.ReturnTime = checkInVM.ReturnTime;
                tripSheet.TripSheetStatusID = Guid.Parse("7f559fbe-0c0b-4985-9369-b1055769a35a");

                _context.DeliveryTripSheets.Update(tripSheet);
                _context.Vehicles.Update(vehicle);

                //Get Driver
                var driver = _context.Employees
                    .Where(e => e.EmployeeID == tripSheet.DriverID)
                    .FirstOrDefault();
                // Available
                driver.EmployeeStatusID = Guid.Parse("9664ce89-cfac-4904-b5eb-7dda3a038b21");
                _context.Employees.Update(driver);

                //Get Assitant
                var assistant = _context.Employees
                    .Where(e => e.EmployeeID == tripSheet.AssistantID)
                    .FirstOrDefault();
                // Available
                assistant.EmployeeStatusID = Guid.Parse("9664ce89-cfac-4904-b5eb-7dda3a038b21");
                _context.Employees.Update(assistant);

                // Get Supervisor
                var supervisor = _context.Employees
                    .Where(e => e.EmployeeID == tripSheet.SupervisorID)
                    .FirstOrDefault();
                // Available
                supervisor.EmployeeStatusID = Guid.Parse("9664ce89-cfac-4904-b5eb-7dda3a038b21");
                _context.Employees.Update(supervisor);

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Set Tripsheet as Checked-in",
                    ActionData = "Set as Checked for Tripsheet : " + tripSheet.DeliveryTripSheetID,
                    EmployeeAction = "Update",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return Ok();
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }
        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult> ViewTripsheetDetails(Guid ID)
        {

            try
            {
                var tripsheet = await _context.DeliveryTripSheets.Where(zz => zz.DeliveryTripSheetID == ID).Include(zz => zz.Assistant).Include(zz => zz.Supervisor).Include(zz => zz.Driver).Include(zz => zz.Vehicle).Include(zz => zz.TripSheetStatus).FirstOrDefaultAsync();


                DeliveryTriphsheetVM deliveryTriphsheetVM = new()
                {
                    VehicleReg = tripsheet.Vehicle.Reg_Number,
                    DepartureTime = tripsheet.DepatureTime,
                    ReturnTime = tripsheet.ReturnTime,
                    StartKM = tripsheet.StartKM,
                    EndKM = tripsheet.EndKM,
                    Status = tripsheet.TripSheetStatus.Description,
                    AssistantName = tripsheet.Assistant.FirstName + " " + tripsheet.Assistant.Surname,
                    DriverName = tripsheet.Driver.FirstName + " " + tripsheet.Driver.Surname,
                    SupervisorName = tripsheet.Supervisor.FirstName + " " + tripsheet.Supervisor.Surname,
                    Alias = tripsheet.Alias,
                    Created = tripsheet.Created,
                    VehicleValidationImage = tripsheet.VehicleValidationImage
                };

                return Ok(deliveryTriphsheetVM);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }

        

    }



}
