using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using CentuIceAPI.DTO;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly CentuIceDBContext _context;

        public ProductController(CentuIceDBContext context)
        {
            _context = context;
        }

        // GET: api/Product
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViewProductVM>>> GetProducts()
        {
            var productList = await _context.Products
                .Include(p => p.Inventory).Where(zz => zz.TableStatusID != Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1"))
                .ToListAsync();

            List<ViewProductVM> ProductVMList = new List<ViewProductVM>();

            productList.ForEach(product =>
            {
                ProductVMList.Add(GenerateProductVM(product));
            });

            var claimsIdentity = User.Identity as ClaimsIdentity;
            var UserID = claimsIdentity.FindFirst("UserId").Value;
            AuditTrail auditTrail = new()
            {
                AuditTrailID = Guid.NewGuid(),
                ActionDate = DateTime.Now,
                ActionDescription = "View Products",
                ActionData = "N/A",
                EmployeeAction = "View",
                EmployeeID = Guid.Parse(UserID)

            };

            _context.AuditTrails.Add(auditTrail);
            await _context.SaveChangesAsync();

            return ProductVMList;

        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<ViewProductVM>> GetProductVM(Guid id)
        {
            var product = await _context.Products
                .AsNoTracking()
                .Include(p => p.Inventory)
                .Where(p => p.InventoryID == id)
                .FirstOrDefaultAsync();

            if (product == null)
            {
                return NotFound();
            }

            return GenerateProductVM(product);
        }

        private ViewProductVM GenerateProductVM(Product product)
        {

            var CurrentPrice = _context.ProductPrices.Where(zz => zz.ProductID == product.ProductID).OrderByDescending(zz => zz.Date).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefault();
            ViewProductVM viewProductVM = new()
            { 
                ProductID = product.ProductID,
                Inventory = product.Inventory.ItemName,
                ProductName = product.ProductName,
                ProductPrice = CurrentPrice,
                isVatApplicable = product.isVatApplicable

            };
            return viewProductVM;
        }

        // GET: api/Product/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductDTO>> GetProduct(Guid id)
        {
            var product = await _context.Products.FindAsync(id);

            var CurrentPrice = _context.ProductPrices.Where(zz => zz.ProductID == product.ProductID).OrderByDescending(zz => zz.Date).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefault();
            if (product == null)
            {
                return NotFound();
            }

            ProductDTO productDTO = new()
            {
                ProductID = product.ProductID,
                InventoryID = product.InventoryID,
                ProductName = product.ProductName,
                ProductPrice = CurrentPrice,
                isVatApplicable = product.isVatApplicable
            };

            return productDTO;
        }

        // PUT: api/Product/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(Guid id, Product product)
        {
            if (id != product.ProductID)
            {
                return BadRequest();
            }

            _context.Entry(product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Product
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ViewProductVM>> PostProduct(ProductVM product)
        {
            Product NewProduct = new Product();
            NewProduct.TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e");
            NewProduct.ProductID = Guid.NewGuid();
            NewProduct.ProductName = product.ProductName;
            NewProduct.InventoryID = product.InventoryID;

            Price newPrice = new Price()
            {
                PriceID = Guid.NewGuid(),
                CurrentPrice = product.ProductPrice
            };

            ProductPrice productPrice = new ProductPrice()
            {
                ProductPriceID = Guid.NewGuid(),
                PriceID = newPrice.PriceID,
                Date = DateTime.Now,
                ProductID = NewProduct.ProductID
            };
         



            try
            {
                _context.Prices.Add(newPrice);
                _context.ProductPrices.Add(productPrice);
                _context.Products.Add(NewProduct);
                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
                throw;
            }
            
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<ViewProductVM>> UpdateProduct(ProductVM product)
        {

            try
            {
                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Update Product",
                    ActionData = "Update Product " + product.ProductID,
                    EmployeeAction = "Update",
                    EmployeeID = Guid.Parse(UserID)

                };

                
                _context.AuditTrails.Add(auditTrail);

                Product NewProduct = new Product();
                NewProduct.TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e");
                NewProduct.ProductID = product.ProductID;
                NewProduct.ProductName = product.ProductName;
                NewProduct.InventoryID = product.InventoryID;
                NewProduct.isVatApplicable = product.isVatApplicable;


                var CurrentPrice = await _context.ProductPrices.Where(zz => zz.ProductID == product.ProductID).OrderByDescending(zz => zz.Date).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();

                if (CurrentPrice != product.ProductPrice)
                {
                    Price newPrice = new Price()
                    {
                        PriceID = Guid.NewGuid(),
                        CurrentPrice = product.ProductPrice
                    };

                    ProductPrice productPrice = new ProductPrice()
                    {
                        ProductPriceID = Guid.NewGuid(),
                        PriceID = newPrice.PriceID,
                        Date = DateTime.Now,
                        ProductID = product.ProductID
                    };

                    _context.Prices.Add(newPrice);
                    _context.ProductPrices.Add(productPrice);
                }
              

                
                _context.Products.Update(NewProduct);
                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
                throw;
            }

        }

        
        [HttpDelete]
        [Route("[action]")]
        public async Task<IActionResult> DeleteProduct(Guid id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            var claimsIdentity = User.Identity as ClaimsIdentity;
            var UserID = claimsIdentity.FindFirst("UserId").Value;
            AuditTrail auditTrail = new()
            {
                AuditTrailID = Guid.NewGuid(),
                ActionDate = DateTime.Now,
                ActionDescription = "Delete Product",
                ActionData = "Delete Product " + product.ProductID,
                EmployeeAction = "Delete",
                EmployeeID = Guid.Parse(UserID)

            };

            var Prices = await _context.ProductPrices.Where(zz => zz.ProductID == id).Include(zz => zz.Price).ToListAsync();

            foreach (var price in Prices)
            {
                _context.ProductPrices.Remove(price);
                _context.Prices.Remove(price.Price);
            }

            _context.AuditTrails.Add(auditTrail);
            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ProductExists(Guid id)
        {
            return _context.Products.Any(e => e.ProductID == id);
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<ProductDropdownVM>>> GetProductDropdowns()
        {

            var productList = await _context.Products.Where(zz => zz.TableStatusID != Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1")).ToListAsync();

            List<ProductDropdownVM> productDropdownList = new List<ProductDropdownVM>();

            productList.ForEach((product) =>
            {
                var CurrentPrice =  _context.ProductPrices.Where(zz => zz.ProductID == product.ProductID).OrderByDescending(zz => zz.Date).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefault();
                ProductDropdownVM productDropdownVM = new()
                {
                    ProductID = product.ProductID,
                    ProductName = product.ProductName,
                    ProductPrice = CurrentPrice
                };
                productDropdownList.Add(productDropdownVM);
            });

            return productDropdownList;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult> GetProductOrders(Guid ID)
        {
            try
            {
                var OrdersInProcess = await _context.OrderLines.Include(zz => zz.Order).Where(zz => zz.ProductID == ID && zz.Order.OrderStatusID != Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85") && zz.Order.OrderStatusID != Guid.Parse("aef36c5f-795c-4d54-a840-8d005fad64c2")).ToListAsync();
                var Sales = await _context.SaleLines.Where(zz => zz.ProductID == ID).ToListAsync();

                var AllOrders = await _context.OrderLines.Include(zz => zz.Order).Where(zz => zz.ProductID == ID).ToListAsync();


                if (AllOrders.Count == 0 && Sales.Count == 0)
                {
                    //Delete
                    return Conflict(new { message = "None Exists" });
                }
                else if (OrdersInProcess.Count > 0)
                {
                    //Do Nothing
                    return Conflict(new { message = "Order in Process" });
                }
                else if (OrdersInProcess.Count == 0 && AllOrders.Count > 0)
                {
                    //Deactivate
                    return Ok();
                }
               

                return Ok();
               
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
            }
        }


        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<PricesGraphVM>>> GetPricesGraphVM(Guid ID)
        {
            try
            {
                var ProductPrices = await _context.ProductPrices.Where(zz => zz.ProductID == ID).Include(zz => zz.Price).OrderBy(zz => zz.Date).ToListAsync();

                List<PricesGraphVM> graphdata = new List<PricesGraphVM>();

                foreach (var price in ProductPrices)
                {
                    PricesGraphVM pricesGraph = new PricesGraphVM()
                    {
                        Price = price.Price.CurrentPrice,
                        Date = price.Date.ToString()
                        
                    };

                    graphdata.Add(pricesGraph);
                }

                return Ok(graphdata);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
            }
        }



    }
}
