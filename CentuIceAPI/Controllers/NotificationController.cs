﻿using CentuIceAPI.DTO;
using CentuIceAPI.Models;
using CentuIceAPI.Services.Hubs;
using CentuIceAPI.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {

        //private IHubContext<NotificationHub> _NotHub;
        private readonly NotificationHub _NotHub;
        private readonly CentuIceDBContext _context;

        public NotificationController(NotificationHub _hubContext, CentuIceDBContext context)
        {
            _NotHub = _hubContext;
            _context = context;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<NotificationDTO>>> GetNotifications(Guid EmployeeID)
        {
            try
            {
                var notifications = await _context.Notifications
                    .Where(n => n.EmployeeID == EmployeeID && n.Show == true)
                    .OrderByDescending(n => n.Created)
                    .ToListAsync();

                List<NotificationDTO> notificationDTOs = new();

                notifications.ForEach((notification) =>
                {
                    notificationDTOs.Add(GetNotificationDTO(notification));
                });

                return notificationDTOs;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<AuthNotificationDTO>> GetAuthNotification(Guid NotificationID)
        {
            try
            {
                var authNotifcation = await _context.AuthNotifications
                    .Where(an => an.NotificationID == NotificationID)
                    .FirstOrDefaultAsync();

                return GetAuthNotificationDTO(authNotifcation);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> LoggedIn(string EmployeeID)
        {
            var Id = Guid.Parse(EmployeeID);
            var CurrentEmployee = _context.Employees.Find(Id);
            CurrentEmployee.isLoggedIn = true;
            _context.Entry(CurrentEmployee).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return Ok();
        }

        //Function gets the AuthVM from the user and attaches aditional information to it and Generates 
        //an AuthNotification, and returns it to the user
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> RequestAuth(AuthRequestVM authVM)
        {
            try
            {
                var Sender = await _context.Employees
                .AsNoTracking()
                .Where(s => s.EmployeeID == authVM.SenderID)
                .FirstOrDefaultAsync();

                var SenderName = Sender.FirstName + ' ' + Sender.Surname;

                var notification = new Notification()
                {
                    NotificationID = Guid.NewGuid(),
                    NotificationTypeID = Guid.Parse("02893224-9110-4fd4-9bd8-34f804afd621"),
                    NotificationTitle = "Authorization has been Requested",
                    NotificationDescription = "Authorization has been requested from " + Sender.FirstName + " " + Sender.Surname,
                    Created = DateTime.Now,
                    EmployeeID = authVM.OwnerID,
                    IsRead = false,
                    Show = true
                };

                var AuthNotification = new AuthNotification()
                {
                    AuthNotificationID = Guid.NewGuid(),
                    AuthNotificationTypeID = authVM.AuthNotificationTypeID,
                    OwnerID = authVM.OwnerID,
                    SenderID = authVM.SenderID,
                    NotificationDateTime = DateTime.Now,
                    Identifier = authVM.Identifier,
                    SenderName = SenderName,
                    isHandled = false,
                    AuthResponse = false,
                    NotificationID = notification.NotificationID,
                    IdentifierID = authVM.IdentifierID
                };

                OwnerAuthRequestVM ownerAuth = new()
                {
                    AuthNotification = GetAuthNotificationDTO(AuthNotification),
                    Notification = GetNotificationDTO(notification),
                };

                _context.AuthNotifications.Add(AuthNotification);
                _context.Notifications.Add(notification);
                await _context.SaveChangesAsync();

                await _NotHub.RequestAuth(ownerAuth);

                return Ok(ownerAuth);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
            
        }

        public AuthNotificationDTO GetAuthNotificationDTO(AuthNotification auth)
        {
            AuthNotificationDTO newAuth = new()
            {
                AuthNotificationID = auth.AuthNotificationID,
                AuthNotificationTypeID = auth.AuthNotificationTypeID,
                AuthResponse = auth.AuthResponse,
                OwnerID = auth.OwnerID,
                Identifier = auth.Identifier,
                isHandled = auth.isHandled,
                NotificationDateTime = auth.NotificationDateTime,
                NotificationID = auth.NotificationID,
                SenderID = auth.SenderID,
                SenderName = auth.SenderName,
                IdentifierID = auth.IdentifierID,
            };

            return newAuth;
        }

        public NotificationDTO GetNotificationDTO(Notification notification)
        {
            NotificationDTO notificationDTO = new()
            {
                NotificationID = notification.NotificationID,
                NotificationTitle = notification.NotificationTitle,
                NotificationDescription = notification.NotificationDescription,
                IsRead = notification.IsRead,
                EmployeeID = notification.EmployeeID,
                Created = notification.Created,
                NotificationTypeID = notification.NotificationTypeID,
                Show = notification.Show,
            };

            return notificationDTO;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> AuthResponse(AuthNotification authNotification)
        {

            try
            {
                authNotification.isHandled = true;
                _context.AuthNotifications.Update(authNotification);

                //Get Notification
                var currentNotification = await _context.Notifications 
                    .Where(cu => cu.NotificationID == authNotification.NotificationID)
                    .FirstOrDefaultAsync(); //Get the Notification that was sent to the Owner along with the AuthNotificatipn
                currentNotification.IsRead = true;
                _context.Notifications.Update(currentNotification);

                Notification newNotification = new();// Store the notification generated in the Switch Statement here

                //Switch Statement that handles the Auth Response from the Owner
                switch (authNotification.AuthNotificationTypeID.ToString())
                {
                    //DeactivateEmployee
                    case "2efc45a3-fece-45c3-80fb-f7386dc0c625":
                        if (authNotification.AuthResponse == true) //True means the auth request has been accepted
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Deactivate employee with identifier " +
                                    authNotification.Identifier + " has been accepted",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;

                            var Employee = await _context.Employees.Where(zz => zz.EmployeeID == authNotification.IdentifierID).FirstOrDefaultAsync();

                            Employee.TableStatusID = Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1");

                            var claimsIdentity = User.Identity as ClaimsIdentity;
                            var UserID = claimsIdentity.FindFirst("UserId").Value;
                            AuditTrail auditTrail = new()
                            {
                                AuditTrailID = Guid.NewGuid(),
                                ActionDate = DateTime.Now,
                                ActionDescription = "Deactivate Employee",
                                ActionData = "Decativate Employee " + Employee.FirstName + " " + Employee.Surname,
                                EmployeeAction = "Deactivate",
                                EmployeeID = Guid.Parse(UserID)

                            };

                            _context.AuditTrails.Add(auditTrail);

                            _context.Employees.Update(Employee);
                        }
                        else
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Deactivate employee with identifier " + 
                                    authNotification.Identifier + " has been rejected",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;
                        }
                    break;// End of Deactivate Employee Case

                    //DeactivateClient
                    case "3eadd87c-2cc3-4537-a0fa-e693d373319f":
                        if (authNotification.AuthResponse == true) //True means the auth request has been accepted
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Deactivate Client with identifier " +
                                    authNotification.Identifier + " has been accepted",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;



                            var Client = await _context.Clients.Where(zz => zz.ClientID == authNotification.IdentifierID).FirstOrDefaultAsync();

                            

                            Client.TableStatusID = Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1");

                            var claimsIdentity = User.Identity as ClaimsIdentity;
                            var UserID = claimsIdentity.FindFirst("UserId").Value;
                            AuditTrail auditTrail = new()
                            {
                                AuditTrailID = Guid.NewGuid(),
                                ActionDate = DateTime.Now,
                                ActionDescription = "Deactivate Client",
                                ActionData = "Decativate Client " + Client.ClientName,
                                EmployeeAction = "Deactivate",
                                EmployeeID = Guid.Parse(UserID)

                            };

                            _context.AuditTrails.Add(auditTrail);

                            _context.Clients.Update(Client);
                        }
                        else
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Deactivate Client with identifier " +
                                    authNotification.Identifier + " has been rejected",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;
                        }
                        break;// End of Deactivate Client Case


                    //DeactivateVehicle
                    case "70a6f99f-2283-45e7-9b1c-8f64b52dae5e":
                        if (authNotification.AuthResponse == true) //True means the auth request has been accepted
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Deactivate Vehicle with identifier " +
                                    authNotification.Identifier + " has been accepted",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;

                            var Vehicle = await _context.Vehicles.Where(zz => zz.VehicleID == authNotification.IdentifierID).FirstOrDefaultAsync();

                            Vehicle.VehicleStatusID = Guid.Parse("6ffce810-cbb1-4192-87d6-378487d19913");

                            var claimsIdentity = User.Identity as ClaimsIdentity;
                            var UserID = claimsIdentity.FindFirst("UserId").Value;
                            AuditTrail auditTrail = new()
                            {
                                AuditTrailID = Guid.NewGuid(),
                                ActionDate = DateTime.Now,
                                ActionDescription = "Deactivate Vehicle",
                                ActionData = "Decativate Vehicle " + Vehicle.Reg_Number,
                                EmployeeAction = "Deactivate",
                                EmployeeID = Guid.Parse(UserID)

                            };

                            _context.AuditTrails.Add(auditTrail);

                            _context.Vehicles.Update(Vehicle);
                        }
                        else
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Deactivate Vehicle with identifier " +
                                    authNotification.Identifier + " has been rejected",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;
                        }
                        break;// End of Deactivate Vehicle Case


                    //DeactivateMachine
                    case "1864db97-436e-4bdc-a4b5-de563435b49d":
                        if (authNotification.AuthResponse == true) //True means the auth request has been accepted
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Deactivate Machine with identifier " +
                                    authNotification.Identifier + " has been accepted",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;

                            var Machine = await _context.Machines.Where(zz => zz.MachineID == authNotification.IdentifierID).FirstOrDefaultAsync();

                            Machine.TableStatusID = Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1");

                            var claimsIdentity = User.Identity as ClaimsIdentity;
                            var UserID = claimsIdentity.FindFirst("UserId").Value;
                            AuditTrail auditTrail = new()
                            {
                                AuditTrailID = Guid.NewGuid(),
                                ActionDate = DateTime.Now,
                                ActionDescription = "Deactivate Machine",
                                ActionData = "Decativate Machine " + Machine.MachineID,
                                EmployeeAction = "Deactivate",
                                EmployeeID = Guid.Parse(UserID)

                            };

                            _context.AuditTrails.Add(auditTrail);

                            _context.Machines.Update(Machine);
                        }
                        else
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Deactivate Machine with identifier " +
                                    authNotification.Identifier + " has been rejected",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;
                        }
                        break;// End of Deactivate Machine Case

                    //DeactivateSupplier
                    case "56e8715b-dee9-4fc6-929d-648cc4dd18a1":
                        if (authNotification.AuthResponse == true) //True means the auth request has been accepted
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Deactivate Supplier with identifier " +
                                    authNotification.Identifier + " has been accepted",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;


                            var Supplier = await _context.Suppliers.Where(zz => zz.SupplierID == authNotification.IdentifierID).FirstOrDefaultAsync();

                            Supplier.TableStatusID = Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1");

                            var claimsIdentity = User.Identity as ClaimsIdentity;
                            var UserID = claimsIdentity.FindFirst("UserId").Value;
                            AuditTrail auditTrail = new()
                            {
                                AuditTrailID = Guid.NewGuid(),
                                ActionDate = DateTime.Now,
                                ActionDescription = "Deactivate Supplier",
                                ActionData = "Decativate Supplier " + Supplier.SupplierName,
                                EmployeeAction = "Deactivate",
                                EmployeeID = Guid.Parse(UserID)

                            };

                            _context.AuditTrails.Add(auditTrail);

                            _context.Suppliers.Update(Supplier);

                        }
                        else
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Deactivate Supplier with identifier " +
                                    authNotification.Identifier + " has been rejected",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;
                        }
                        break;// End of Deactivate Supplier Case


                    //DeactivateProduct
                    case "866d1081-598c-46a0-bcd0-fdd8dec76aca":
                        if (authNotification.AuthResponse == true) //True means the auth request has been accepted
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Deactivate Product with identifier " +
                                    authNotification.Identifier + " has been accepted",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;

                            var product = await _context.Products.Where(zz => zz.ProductID == authNotification.IdentifierID).FirstOrDefaultAsync();

                            
                            product.TableStatusID = Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1");

                            var claimsIdentity = User.Identity as ClaimsIdentity;
                            var UserID = claimsIdentity.FindFirst("UserId").Value;
                            AuditTrail auditTrail = new()
                            {
                                AuditTrailID = Guid.NewGuid(),
                                ActionDate = DateTime.Now,
                                ActionDescription = "Decativate Product",
                                ActionData = "Deacivate Product: " + product.ProductName,
                                EmployeeAction = "Deactivate",
                                EmployeeID = Guid.Parse(UserID)

                            };

                            _context.AuditTrails.Add(auditTrail);

                            _context.Products.Update(product);
                        }
                        else
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Deactivate Product with identifier " +
                                    authNotification.Identifier + " has been rejected",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;
                        }
                        break;// End of Deactivate Product Case


                    //Write Off
                    case "06ae874b-3e31-4845-92e9-7d83fc8db842":
                        if (authNotification.AuthResponse == true) //True means the auth request has been accepted
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Write-Off Stock with identifier " +
                                    authNotification.Identifier + " has been accepted",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;

                            var Auth = await _context.AuthWriteOffs.Where(zz => zz.AuthWriteOffID == authNotification.IdentifierID).FirstOrDefaultAsync();

                            var inventory = await _context.Inventories.AsNoTracking().Where(zz => zz.InventoryID == Auth.InventoryID).FirstOrDefaultAsync();

                            inventory.Quantity -= Auth.Quantity;

                            _context.Inventories.Update(inventory);

                            StockWrittenOff stock = new()
                            {
                                StockWrittenOffID = Guid.NewGuid(),
                                WriteOffDate = DateTime.Now,
                                InventoryID = Auth.InventoryID,
                                WrittenOffReasonID = Auth.WrittenOffReasonID,
                                Quantity = Auth.Quantity
                            };

                            _context.StockWrittenOff.Add(stock);

                            if (Auth.EmployeeID.ToString() != "00000000-0000-0000-0000-000000000000")
                            {
                                EmployeeError employeeError = new()
                                {
                                    EmployeeErrorID = Guid.NewGuid(),
                                    DateOfError = DateTime.Now,
                                    DescriptionOfError = "Employee Broke " + stock.Quantity + " " + inventory.ItemName,
                                    EmployeeID = Auth.EmployeeID,
                                    ErrorCode = "BRKBAG"
                                };

                                _context.EmployeeErrors.Add(employeeError);
                            }

                            var claimsIdentity = User.Identity as ClaimsIdentity;
                            var UserID = claimsIdentity.FindFirst("UserId").Value;
                            AuditTrail auditTrail = new()
                            {
                                AuditTrailID = Guid.NewGuid(),
                                ActionDate = DateTime.Now,
                                ActionDescription = "Write-off Stock",
                                ActionData = "Write-off Stock for " + inventory.ItemName,
                                EmployeeAction = "Write-off",
                                EmployeeID = Guid.Parse(UserID)

                            };

                            _context.AuditTrails.Add(auditTrail);


                        }
                        else
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Wrie-Off Stock with identifier " +
                                    authNotification.Identifier + " has been rejected",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;
                        }
                        break;// End of Write Off Case

                    //Deactivate Inventory
                    case "4ae95884-8692-43c6-8829-c0d787518dff":
                        if (authNotification.AuthResponse == true) //True means the auth request has been accepted
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Deactivate Inventory with identifier " +
                                    authNotification.Identifier + " has been accepted",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;

         

                            var inventory = await _context.Inventories.Where(zz => zz.InventoryID == authNotification.IdentifierID).FirstOrDefaultAsync();

                            inventory.TableStatusID = Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1");


                            _context.Inventories.Update(inventory);

                      

            
                            

                            var claimsIdentity = User.Identity as ClaimsIdentity;
                            var UserID = claimsIdentity.FindFirst("UserId").Value;
                            AuditTrail auditTrail = new()
                            {
                                AuditTrailID = Guid.NewGuid(),
                                ActionDate = DateTime.Now,
                                ActionDescription = "Deactivate Inventory",
                                ActionData = "Deactivate Inventory " + inventory.ItemName,
                                EmployeeAction = "Deactivate",
                                EmployeeID = Guid.Parse(UserID)

                            };

                            _context.AuditTrails.Add(auditTrail);


                        }
                        else
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Deactivate Inventory with identifier " +
                                    authNotification.Identifier + " has been rejected",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;
                        }
                        break;// End of Deactivate Inventory Case

                    //Cancel Sale
                    case "d0721f9c-011b-43eb-b35f-6a0b5e8b2fbc":
                        if (authNotification.AuthResponse == true) //True means the auth request has been accepted
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Cancel Sale with identifier " +
                                    authNotification.Identifier + " has been accepted",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;



                            var Sale = await _context.Sales.Where(zz => zz.SaleID == authNotification.IdentifierID).Include(zz => zz.Invoice).FirstOrDefaultAsync();

                            Sale.SaleStatusID = Guid.Parse("5321a73d-ca18-4770-94e3-4cbf28adffc4");
                            Sale.Invoice.InvoiceStatusID = Guid.Parse("365b7848-2cb8-4c74-93d9-40ddb17e62f1");


                            _context.Sales.Update(Sale);
                            _context.Invoices.Update(Sale.Invoice);





                            var claimsIdentity = User.Identity as ClaimsIdentity;
                            var UserID = claimsIdentity.FindFirst("UserId").Value;
                            AuditTrail auditTrail = new()
                            {
                                AuditTrailID = Guid.NewGuid(),
                                ActionDate = DateTime.Now,
                                ActionDescription = "Cancel Sale",
                                ActionData = "Cancel Sale " + Sale.SaleID,
                                EmployeeAction = "Cancel",
                                EmployeeID = Guid.Parse(UserID)

                            };

                            _context.AuditTrails.Add(auditTrail);


                        }
                        else
                        {
                            Notification notification = new()
                            {
                                NotificationID = Guid.NewGuid(),
                                Created = DateTime.Now,
                                EmployeeID = authNotification.SenderID,
                                IsRead = false,
                                Show = true,
                                NotificationDescription = "Auth request to Cancel Sale with identifier " +
                                    authNotification.Identifier + " has been rejected",
                                NotificationTitle = "Auth Response",
                                NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5"),
                            };
                            newNotification = notification;
                        }
                        break;// End of Deactivate Sales Case
                    default:
                        break;
                }

                _context.Notifications.Add(newNotification);
                await _context.SaveChangesAsync();
                await _NotHub.SendNotification(GetNotificationDTO(newNotification));// Send the notification to the user in real time
                return Ok(authNotification);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> DismissNotification(Guid NotificationID)
        {
            try
            {
                //get Notification
                var notification = await _context.Notifications
                    .Where(n => n.NotificationID == NotificationID)
                    .FirstOrDefaultAsync();

                notification.IsRead = true;
                notification.Show = false;

                _context.Notifications.Update(notification);
                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> GetNotificationTimers()
        {
            try
            {
                var Timers = await _context.NotificationTimers.ToListAsync();

                return Ok(Timers);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;

            }
            
        }

        [HttpPut]
        [Route("[action]")]
        public async Task<IActionResult> ChangeTimePeriod(Guid ID, NotificationTimer notificationTimer)
        {

            try
            {
                var CurrentTimer = await _context.NotificationTimers.Where(zz => zz.NotificationTimerID == ID).FirstOrDefaultAsync();

                CurrentTimer.TimePeriod = notificationTimer.TimePeriod;

                _context.Update(CurrentTimer);

                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;

            }

        }




    }
}
