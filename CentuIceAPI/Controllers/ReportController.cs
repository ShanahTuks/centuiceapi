﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using CentuIceAPI.DTO;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly CentuIceDBContext _context;

        public ReportController(CentuIceDBContext context)
        {
            _context = context;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<EmployeeErrorVM>>> GetErrorReport(DateSelectorVM dates)
        {

            try
            {
                var Errors = await _context.EmployeeErrors.Where(zz => zz.DateOfError.Date >= dates.StartDate.Date && zz.DateOfError.Date <= dates.EndDate.Date).Include(zz => zz.Employee).ToListAsync();

                List<EmployeeErrorVM> employeeErrorVMs = new();

                if (Errors != null)
                {
                    foreach (var error in Errors)
                    {
                        EmployeeErrorVM employeeError = new();
                        employeeError.EmployeeErrorID = error.EmployeeErrorID;
                        employeeError.EmployeeFullName = error.Employee.FirstName + " " + error.Employee.Surname;
                        employeeError.ErrorCode = error.ErrorCode;
                        employeeError.DescriptionOfError = error.DescriptionOfError;
                        employeeError.DateOfError = error.DateOfError;

                        employeeErrorVMs.Add(employeeError);
                    }

                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var UserID = claimsIdentity.FindFirst("UserId").Value;
                    AuditTrail auditTrail = new()
                    {
                        AuditTrailID = Guid.NewGuid(),
                        ActionDate = DateTime.Now,
                        ActionDescription = "Generate Error Report",
                        ActionData = "Generate Error Report",
                        EmployeeAction = "Generate",
                        EmployeeID = Guid.Parse(UserID)

                    };

                    _context.AuditTrails.Add(auditTrail);

                   await _context.SaveChangesAsync();
                    return Ok(employeeErrorVMs);
                }
                else
                {
                    return NoContent();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<ServiceFrequencyVM>>> GetServiceFrequencyReport(DateSelectorVM dates)
        {
            try
            {
                var ServiceMachines = await _context.ServiceMachines.Include(zz => zz.Service).Include(zz => zz.Machine).Where(zz => zz.Service.ServiceDate.Date >= dates.StartDate.Date && zz.Service.ServiceDate.Date <= dates.EndDate.Date).ToListAsync();
                var ServiceVehicles = await _context.ServiceVehicles.Include(zz => zz.Service).Include(zz => zz.Vehicle).Where(zz => zz.Service.ServiceDate.Date >= dates.StartDate.Date && zz.Service.ServiceDate.Date <= dates.EndDate.Date).ToListAsync();

                List<ServiceFrequencyVM> serviceFrequencies = new();



                if (ServiceMachines != null)
                {
                    foreach (var machine in ServiceMachines)
                    {
                        ServiceFrequencyVM frequencyVM = new();
                        List<JobsFrequencyVM> jobsFrequencyVMs = new();
                        frequencyVM.MachineModel = machine.Machine.Model;

                        var Jobs = await _context.ServiceServiceJobs.Include(zz => zz.ServiceJob).Include(zz => zz.Service).Include(zz => zz.Service.Supplier).Where(zz => zz.ServiceID == machine.ServiceID).ToListAsync();

                        foreach (var job in Jobs)
                        {
                            JobsFrequencyVM jobVM = new();

                            jobVM.DateOfService = job.Service.ServiceDate;
                            jobVM.ServiceJobName = job.ServiceJob.ServiceJobName;
                            jobVM.SupplierName = job.Service.Supplier.SupplierName;

                            jobsFrequencyVMs.Add(jobVM);
                        }

                        frequencyVM.JobList = jobsFrequencyVMs;

                        serviceFrequencies.Add(frequencyVM);

                    }
                }

                if (ServiceVehicles != null)
                {
                    foreach (var vehicle in ServiceVehicles)
                    {
                        ServiceFrequencyVM frequencyVM = new();
                        List<JobsFrequencyVM> jobsFrequencyVMs = new();
                        frequencyVM.VehicleReg = vehicle.Vehicle.Reg_Number;

                        var Jobs = await _context.ServiceServiceJobs.Include(zz => zz.ServiceJob).Include(zz => zz.Service).Include(zz => zz.Service.Supplier).Where(zz => zz.ServiceID == vehicle.ServiceID).ToListAsync();

                        foreach (var job in Jobs)
                        {
                            JobsFrequencyVM jobVM = new();

                            jobVM.DateOfService = job.Service.ServiceDate;
                            jobVM.ServiceJobName = job.ServiceJob.ServiceJobName;
                            jobVM.SupplierName = job.Service.Supplier.SupplierName;

                            jobsFrequencyVMs.Add(jobVM);
                        }

                        frequencyVM.JobList = jobsFrequencyVMs;

                        serviceFrequencies.Add(frequencyVM);

                    }

                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Generate Service Frequency Report",
                    ActionData = "Generate Service Frequency Report",
                    EmployeeAction = "Generate",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return Ok(serviceFrequencies);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<ServiceReportVM>>> GetServiceCostReport(DateSelectorVM dates)
        {

            try
            {
                var ServiceMachines = await _context.ServiceMachines.Include(zz => zz.Service).Include(zz => zz.Machine).Where(zz => zz.Service.ServiceDate.Date >= dates.StartDate.Date && zz.Service.ServiceDate.Date <= dates.EndDate.Date).ToListAsync();
                var ServiceVehicles = await _context.ServiceVehicles.Include(zz => zz.Service).Include(zz => zz.Vehicle).Where(zz => zz.Service.ServiceDate.Date >= dates.StartDate.Date && zz.Service.ServiceDate.Date <= dates.EndDate.Date).ToListAsync();

                List<ServiceReportVM> serviceReports = new();



                if (ServiceMachines != null)
                {
                    foreach (var machine in ServiceMachines)
                    {
                        ServiceReportVM reportVM = new();
                        List<JobsServiceReportVM> jobsReportVMs = new();
                        reportVM.MachineModel = machine.Machine.Model;

                        var Jobs = await _context.ServiceServiceJobs.Include(zz => zz.ServiceJob).Include(zz => zz.Service).Include(zz => zz.Service.Supplier).Where(zz => zz.ServiceID == machine.ServiceID).ToListAsync();

                        foreach (var job in Jobs)
                        {
                            JobsServiceReportVM jobVM = new();

                            jobVM.DateOfService = job.Service.ServiceDate;
                            jobVM.ServiceJobName = job.ServiceJob.ServiceJobName;
                            jobVM.SupplierName = job.Service.Supplier.SupplierName;
                            jobVM.ServiceJobCost = job.ServiceJob.CostOfJob;

                            jobsReportVMs.Add(jobVM);
                        }

                        reportVM.JobList = jobsReportVMs;

                        serviceReports.Add(reportVM);

                    }
                }

                if (ServiceVehicles != null)
                {
                    foreach (var vehicle in ServiceVehicles)
                    {
                        ServiceReportVM reportVM = new();
                        List<JobsServiceReportVM> jobsReportVMs = new();
                        reportVM.VehicleReg = vehicle.Vehicle.Reg_Number;

                        var Jobs = await _context.ServiceServiceJobs.Include(zz => zz.ServiceJob).Include(zz => zz.Service).Include(zz => zz.Service.Supplier).Where(zz => zz.ServiceID == vehicle.ServiceID).ToListAsync();

                        foreach (var job in Jobs)
                        {
                            JobsServiceReportVM jobVM = new();

                            jobVM.DateOfService = job.Service.ServiceDate;
                            jobVM.ServiceJobName = job.ServiceJob.ServiceJobName;
                            jobVM.SupplierName = job.Service.Supplier.SupplierName;
                            jobVM.ServiceJobCost = job.ServiceJob.CostOfJob;

                            jobsReportVMs.Add(jobVM);
                        }

                        reportVM.JobList = jobsReportVMs;

                        serviceReports.Add(reportVM);

                    }

                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Generate Service Cost Report",
                    ActionData = "Generate Service Cost Report",
                    EmployeeAction = "Generate",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();


                return Ok(serviceReports);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<ClientTrendReportVM>>> GetClientTrendReport(DateSelectorVM dates)
        {

            try
            {
                var OrderList = await _context.Orders.Where(zz => zz.OrderDate.Date >= dates.StartDate.Date && zz.OrderDate.Date <= dates.EndDate.Date && zz.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).ToListAsync();

                ClientTrendReportVM mon = new();
                ClientTrendReportVM tue = new();
                ClientTrendReportVM wed = new();
                ClientTrendReportVM thu = new();
                ClientTrendReportVM fri = new();
                ClientTrendReportVM sat = new();


                int Monday = 0;
                int Tuesday = 0;
                int Wednesday = 0;
                int Thursday = 0;
                int Friday = 0;
                int Saturday = 0;

                foreach (var order in OrderList)
                {

                    switch (order.OrderDate.DayOfWeek)
                    {
                        case DayOfWeek.Monday:
                            Monday++;

                            break;

                        case DayOfWeek.Tuesday:
                            Tuesday++;

                            break;

                        case DayOfWeek.Wednesday:
                            Wednesday++;

                            break;

                        case DayOfWeek.Thursday:
                            Thursday++;

                            break;

                        case DayOfWeek.Friday:
                            Friday++;

                            break;

                        case DayOfWeek.Saturday:
                            Saturday++;

                            break;
                    }
                }

                List<ClientTrendReportVM> clientTrendReportVMs = new List<ClientTrendReportVM>();


                mon.Day = "Monday";
                mon.OrderCount = Monday;
                tue.Day = "Tuesday";
                tue.OrderCount = Tuesday;
                wed.Day = "Wednesday";
                wed.OrderCount = Wednesday;
                thu.Day = "Thursday";
                thu.OrderCount = Thursday;
                fri.Day = "Friday";
                fri.OrderCount = Friday;
                sat.Day = "Saturday";
                sat.OrderCount = Saturday;


                clientTrendReportVMs.Add(mon);

                clientTrendReportVMs.Add(tue);

                clientTrendReportVMs.Add(wed);



                clientTrendReportVMs.Add(thu);


                clientTrendReportVMs.Add(fri);



                clientTrendReportVMs.Add(sat);

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Generate Client Trend Report",
                    ActionData = "Generate Client Trend Report",
                    EmployeeAction = "Generate",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return Ok(clientTrendReportVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }
        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<SaleProductReportVM>>> GetSaleProductReport(DateSelectorVM dates)
        {

            try
            {
                var ProductList = await _context.Products.ToListAsync();
                decimal TotalCost = 0;


                List<SaleProductReportVM> saleProductsVMs = new();

                foreach (var product in ProductList)
                {
                    TotalCost = 0;

                    var SaleList = await _context.SaleLines.Include(zz => zz.Sale).Include(zz => zz.Product).Where(zz => zz.Sale.SaleDate.Date >= dates.StartDate.Date && zz.Sale.SaleDate.Date <= dates.EndDate.Date && zz.ProductID == product.ProductID && zz.Sale.SaleStatusID == Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")).ToListAsync();
                    var OrderList = await _context.OrderLines.Include(zz => zz.Order).Include(zz => zz.Product).Where(zz => zz.Order.OrderDate.Date >= dates.StartDate.Date && zz.Order.OrderDate.Date <= dates.EndDate.Date && zz.ProductID == product.ProductID && zz.Order.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).ToListAsync();

                    foreach (var sale in SaleList)
                    {
                        var ProductPrice = await _context.ProductPrices.Where(zz => zz.ProductPriceID == sale.ProductPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();
                        TotalCost += sale.Quantity * ProductPrice;
                    }

                    foreach (var order in OrderList)
                    {
                        var ProductPrice = await _context.ProductPrices.Where(zz => zz.ProductPriceID == order.ProductPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();
                        TotalCost += order.Quantity * ProductPrice;
                    }

                    SaleProductReportVM report = new();

                    report.ProductName = product.ProductName;
                    report.TotalSalesPerProduct = TotalCost;

                    saleProductsVMs.Add(report);
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Generate Sale Product Report",
                    ActionData = "Generate Sale Product Report",
                    EmployeeAction = "Generate",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return Ok(saleProductsVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }




        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<SalesTotalVM>>> GetSalesTotal(DateSelectorVM dates)
        {

            try
            {
                var SaleList = await _context.Sales.Where(zz => zz.SaleDate.Date >= dates.StartDate.Date && zz.SaleDate.Date <= dates.EndDate.Date && zz.SaleStatusID == Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959")).Include(zz => zz.VatConfiguration).ToListAsync();




                List<SalesTotalVM> salesTotalVMs = new();

                foreach (var sale in SaleList)
                {
                    var SaleLine = await _context.SaleLines.Include(zz => zz.Sale).Include(zz => zz.Product).Where(zz => zz.SaleID == sale.SaleID).ToListAsync();

                   

                    var CurrentVat = sale.VatConfiguration.VatPercentage;
                    SalesTotalVM sales = new();
                    foreach (var line in SaleLine)
                    {
                        var ProductPrice = await _context.ProductPrices.Where(zz => zz.ProductPriceID == line.ProductPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();


                        if (line.Sale.VatActivated == true)
                        {
                            if (line.Product.isVatApplicable == true)
                            {
                                var total = ProductPrice * line.Quantity;
                                sales.SalesDate = line.Sale.SaleDate;
                                sales.TotalSales += total * ((100 + CurrentVat * 100) / 100);
                            }
                            else
                            {
                                sales.SalesDate = line.Sale.SaleDate;
                                sales.TotalSales += ProductPrice * line.Quantity;
                            }
                        }
                        else
                        {
                            sales.SalesDate = line.Sale.SaleDate;
                            sales.TotalSales += ProductPrice * line.Quantity;
                        }

                        
                      
                    }

                    salesTotalVMs.Add(sales);


                }

                

                return Ok(salesTotalVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<OrderTotalVM>>> GetOrdersTotal(DateSelectorVM dates)
        {
            try
            {
                var OrderList = await _context.Orders.Where(zz => zz.OrderDate.Date >= dates.StartDate.Date && zz.OrderDate.Date <= dates.EndDate.Date && zz.OrderStatusID == Guid.Parse("37196611-3ac3-4ca5-8982-3fdcdf726b85")).Include(zz => zz.VatConfiguration).ToListAsync();




                List<OrderTotalVM> orderTotalVMs = new();

                foreach (var order in OrderList)
                {
                    var OrderLine = await _context.OrderLines.Include(zz => zz.Order).Include(zz => zz.Product).Where(zz => zz.OrderID == order.OrderID).ToListAsync();
                 

                    var CurrentVat = order.VatConfiguration.VatPercentage;

                    OrderTotalVM orders = new();
                    foreach (var line in OrderLine)
                    {
                        var ProductPrice = await _context.ProductPrices.Where(zz => zz.ProductPriceID == line.ProductPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();

                        if (line.Order.VatActivated == true)
                        {
                            if (line.Product.isVatApplicable == true)
                            {
                                var total = ProductPrice * line.Quantity;
                                orders.OrderDate = line.Order.OrderDate;
                                orders.TotalOrders += total * ((100 + CurrentVat * 100) / 100);
                            }
                            else
                            {
                                orders.OrderDate = line.Order.OrderDate;
                                orders.TotalOrders += ProductPrice * line.Quantity;
                            }
                        }
                        else
                        {
                            orders.OrderDate = line.Order.OrderDate;
                            orders.TotalOrders += ProductPrice * line.Quantity;
                        }
                     
                    }

                    orderTotalVMs.Add(orders);


                }

                return Ok(orderTotalVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<InventoryExpensesVM>>> GetProductExpenseReport(DateSelectorVM dates)
        {

            try
            {
                var InventoryList = await _context.Inventories.ToListAsync();
                decimal TotalCost = 0;

                List<InventoryExpensesVM> inventoryExpensesVMs = new();

                foreach (var inventory in InventoryList)
                {
                    TotalCost = 0;

                    var SupplierOrderList = await _context.SupplierOrderLines.Include(zz => zz.SupplierOrder).Include(zz => zz.Inventory).Where(zz => zz.SupplierOrder.DatePlaced.Date >= dates.StartDate.Date && zz.SupplierOrder.DatePlaced.Date <= dates.EndDate.Date && zz.InventoryID == inventory.InventoryID && zz.SupplierOrder.SupplierOrderStatusID == Guid.Parse("20fc8be8-ad9b-4ccd-a300-02d5ef12d8a0")).ToListAsync();

                    if (SupplierOrderList != null)
                    {


                        foreach (var order in SupplierOrderList)
                        {
                            var ItemPrice = await _context.InventoryPrices.Where(zz => zz.InventoryPriceID == order.InventoryPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();
                            TotalCost += order.Quantity * ItemPrice;
                        }
                    }
                    InventoryExpensesVM inventoryExpenses = new();

                    inventoryExpenses.InventoryName = inventory.ItemName;
                    inventoryExpenses.TotalExpense = TotalCost;

                    inventoryExpensesVMs.Add(inventoryExpenses);

                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Generate Product Expense Report",
                    ActionData = "Generate Product Expense Report",
                    EmployeeAction = "Generate",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return Ok(inventoryExpensesVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<PurchaseReportVM>>> GetPurchaseExpenseReport(DateSelectorVM dates)
        {

            try
            {
                var SupplierOrders = await _context.SupplierOrders.Where(zz => zz.DatePlaced.Date >= dates.StartDate.Date && zz.DatePlaced.Date <= dates.EndDate.Date && zz.SupplierOrderStatusID == Guid.Parse("20fc8be8-ad9b-4ccd-a300-02d5ef12d8a0")).ToListAsync();

                List<PurchaseReportVM> purchaseReportVMs = new();

                foreach (var order in SupplierOrders)
                {
                    var OrderLine = await _context.SupplierOrderLines.Include(zz => zz.SupplierOrder).Include(zz => zz.Inventory).Include(zz => zz.SupplierOrder.Supplier).Where(zz => zz.SupplierOrderID == order.SupplierOrderID).ToListAsync();
                    PurchaseReportVM orders = new();
                    foreach (var line in OrderLine)
                    {
                        var ItemPrice = await _context.InventoryPrices.Where(zz => zz.InventoryPriceID == line.InventoryPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();

                        orders.DateOrdered = line.SupplierOrder.DatePlaced;
                        orders.DateReceived = line.SupplierOrder.DateReceived;
                        orders.SupplierName = line.SupplierOrder.Supplier.SupplierName;
                        orders.TotalCost += line.Quantity * ItemPrice;
                    }

                    purchaseReportVMs.Add(orders);
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Generate Purchase Expense Report",
                    ActionData = "Generate Purchase Expense Report",
                    EmployeeAction = "Generate",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return Ok(purchaseReportVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<InventoryReportVM>>> GetInventoryReport(SingleDateVM date)
        {

            try
            {
                var StockTakeList = await _context.StockTakes.Where(zz => zz.DateOfStockTake.Date == date.ChosenDate.Date).ToListAsync();

                List<InventoryReportVM> inventoryReportVMs = new();

                foreach (var stock in StockTakeList)
                {
                    var StockTakeLine = await _context.StockTakeLines.Include(zz => zz.Inventory).Where(zz => zz.StockTakeID == stock.StockTakeID).ToListAsync();

                    InventoryReportVM reportVM = new();
                    foreach (var line in StockTakeLine)
                    {
                        reportVM.StockTakeDate = stock.DateOfStockTake;
                        reportVM.InventoryName = line.Inventory.ItemName;
                        reportVM.Quantity = line.StockTakeQuantity;
                        reportVM.DiscrepancyAmount = line.DiscrepancyAmount;

                    }

                    inventoryReportVMs.Add(reportVM);

                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Generate Inventory Report",
                    ActionData = "Generate Inventory Report",
                    EmployeeAction = "Generate",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return Ok(inventoryReportVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<AttendanceReportVM>>> GetAttendanceReport(AttendanceReportDate date)
        {
            try
            {
                var EmployeeList = await _context.Employees.ToListAsync();


                int year = Convert.ToInt32(date.Date.Substring(0, 4));
                int month = Convert.ToInt32(date.Date.Substring(5, 2));
                DateTime ChosenDate = new DateTime(year, month, 1);
                List<AttendanceReportVM> attendanceReportVMs = new();
                foreach (var employee in EmployeeList)
                {

                    var AttendanceList = await _context.DailyAttendances.Include(zz => zz.Employee).Where(zz => zz.Date.Month == ChosenDate.Month && zz.Date.Year == ChosenDate.Year && zz.EmployeeID == employee.EmployeeID).ToListAsync();




                    if (AttendanceList.Count != 0)
                    {
                        AttendanceReportVM reportVM = new();
                        TimeSpan Hours = TimeSpan.Zero;
                        foreach (var attendance in AttendanceList)
                        {


                            reportVM.EmployeeName = employee.FirstName + " " + employee.Surname;
                            Hours += attendance.CheckOutTime.TimeOfDay - attendance.CheckInTime.TimeOfDay;

                        }
                        reportVM.HoursWorked = Convert.ToInt32(Hours.TotalHours);
                        attendanceReportVMs.Add(reportVM);
                    }
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Generate Attendance Report",
                    ActionData = "Generate Attendance Report",
                    EmployeeAction = "Generate",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return Ok(attendanceReportVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<ProductPopularityReportVM>>> GetProductPopularityReport(AttendanceReportDate date)
        {
            try
            {
                var ProductList = await _context.Products.ToListAsync();
                int count = 0;

                int year = Convert.ToInt32(date.Date.Substring(0, 4));
                int month = Convert.ToInt32(date.Date.Substring(5, 2));
                DateTime ChosenDate = new DateTime(year, month, 1);

                List<ProductPopularityReportVM> ProductsVMs = new();

                foreach (var product in ProductList)
                {
                    count = 0;

                    var SaleList = await _context.SaleLines.Include(zz => zz.Sale).Include(zz => zz.Product).Where(zz => zz.Sale.SaleDate.Month == ChosenDate.Month && zz.Sale.SaleDate.Year == ChosenDate.Year && zz.ProductID == product.ProductID).ToListAsync();
                    var OrderList = await _context.OrderLines.Include(zz => zz.Order).Include(zz => zz.Product).Where(zz => zz.Order.OrderDate.Month == ChosenDate.Month && zz.Order.OrderDate.Year == ChosenDate.Year && zz.ProductID == product.ProductID).ToListAsync();

                    foreach (var sale in SaleList)
                    {
                        count += sale.Quantity;
                    }

                    foreach (var order in OrderList)
                    {
                        count += order.Quantity;
                    }

                    ProductPopularityReportVM report = new();

                    report.ProductName = product.ProductName;
                    report.BagsSold = count;

                    ProductsVMs.Add(report);
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Generate Product Popularity Report",
                    ActionData = "Generate Product Popularity Report",
                    EmployeeAction = "Generate",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return Ok(ProductsVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }

       
    }

}
