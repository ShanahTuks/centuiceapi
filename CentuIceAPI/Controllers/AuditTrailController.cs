using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AuditTrailController : ControllerBase
    {
        private readonly CentuIceDBContext _context;

        public AuditTrailController(CentuIceDBContext context)
        {
            _context = context;
        }

        // GET: api/AuditTrail
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AuditTrailVM>>> GetAuditTrails()
        {

            try
            {
                var AuditTrailList = await _context.AuditTrails.Include(at => at.Employee).OrderByDescending(at => at.ActionDate).ToListAsync();

                List<AuditTrailVM> AuditTrailVMList = new List<AuditTrailVM>();

                AuditTrailList.ForEach((trail) =>
                {
                    AuditTrailVM trailTemp = new()
                    {
                        AuditTrailID = trail.AuditTrailID,
                        ActionDate = trail.ActionDate,
                        EmployeeName = trail.Employee.FirstName + ' ' + trail.Employee.Surname,
                        ActionDescription = trail.ActionDescription,
                        EmployeeAction = trail.EmployeeAction,
                        ActionData = trail.ActionData
                        
                    };
                    AuditTrailVMList.Add(trailTemp);
                });

                return AuditTrailVMList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        // GET: api/AuditTrail/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AuditTrail>> GetAuditTrail(Guid id)
        {
            var auditTrail = await _context.AuditTrails.FindAsync(id);

            if (auditTrail == null)
            {
                return NotFound();
            }

            return auditTrail;
        }

        // PUT: api/AuditTrail/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAuditTrail(Guid id, AuditTrail auditTrail)
        {
            if (id != auditTrail.AuditTrailID)
            {
                return BadRequest();
            }

            _context.Entry(auditTrail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AuditTrailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AuditTrail
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<AuditTrail>> PostAuditTrail(AuditTrail auditTrail)
        {
            auditTrail.AuditTrailID = Guid.NewGuid();

            _context.AuditTrails.Add(auditTrail);

            try
            {
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
                throw;
            }

        }

        // DELETE: api/AuditTrail/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAuditTrail(Guid id)
        {
            var auditTrail = await _context.AuditTrails.FindAsync(id);
            if (auditTrail == null)
            {
                return NotFound();
            }

            _context.AuditTrails.Remove(auditTrail);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool AuditTrailExists(Guid id)
        {
            return _context.AuditTrails.Any(e => e.AuditTrailID == id);
        }
    }
}
