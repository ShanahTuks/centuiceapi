using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using CentuIceAPI.Services;
using Microsoft.AspNetCore.Authorization;
using CentuIceAPI.Services.ServiceBase;
using CentuIceAPI.DTO;
using UglyToad.PdfPig;
using UglyToad.PdfPig.Content;
using Tabula;
using Tabula.Extractors;
using System.IO;
using System.Text.Json;
using System.Security.Claims;
using System.Net;
using Twilio;
using Twilio.Rest.Api.V2010;
using Microsoft.Extensions.Configuration;
using Twilio.Rest.Api.V2010.Account;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly CentuIceDBContext _context;
        private readonly IAuthService _authService;
        private readonly IMailer _mailer;

        public EmployeeController(CentuIceDBContext context, IAuthService authService, IMailer mailer, IConfiguration configuration)
        {
            _context = context;
            _authService = authService;
            _mailer = mailer;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // GET: api/Employee
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Employee>>> GetEmployees()
        {
            try
            {
                return await _context.Employees.ToListAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        // GET: api/Employee/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FullEmployeeDTOVM>> GetEmployee(Guid id)
        {
            try
            {
                var employee = await _context.Employees.FindAsync(id);

                EmployeeDTO employeeDTO = new()
                {
                    EmployeeID = employee.EmployeeID,
                    AccountNumber = employee.AccountNumber,
                    AccountTypeID = employee.AccountTypeID,
                    BankID = employee.BankID,
                    BranchCode = employee.BranchCode,
                    CellNumber = employee.CellNumber,
                    DateOfBirth = employee.DateOfBirth,
                    Email = employee.Email,
                    EmployeeQualificationID = employee.EmployeeQualificationID,
                    EmployeeStatusID = employee.EmployeeStatusID,
                    EmployeeTitleID = employee.EmployeeTitleID,
                    EmployeeTypeID = employee.EmployeeTypeID,
                    FirstName = employee.FirstName,
                    HomeNumber = employee.HomeNumber,
                    IDNumber = employee.IDNumber,
                    Initials = employee.Initials,
                    isLoggedIn = employee.isLoggedIn,
                    InstitutionName = employee.InstitutionName,
                    KnownAsName = employee.KnownAsName,
                    PassportCountry = employee.PassportCountry,
                    PassportNumber = employee.PassportNumber,
                    Surname = employee.Surname,
                    SystemPassword = employee.SystemPassword,
                    SystemUsername = employee.SystemUsername,
                    TableStatusID = employee.TableStatusID,
                    TaxNumber = employee.TaxNumber,
                    WorkNumber = employee.WorkNumber,
                    YearCompleted = employee.YearCompleted,
                    FingerprintAccountNumber = employee.FingerprintAccountNumber,

                };

                if (employee == null)
                {
                    return NotFound();
                }

                var EmployeeAddresses = await _context.EmployeeAddresses
                    .Include(address => address.Address)
                    .Where(address => address.EmployeeID == id && address.Address.isActive == true)
                    .ToListAsync();
                var Contacts = await _context.EmergencyContacts
                    .Where(contact => contact.EmployeeID == id)
                    .ToListAsync();

                List<EmergencyContactDTO> ContactList = new();

                Contacts.ForEach(contact =>
                {
                    EmergencyContactDTO contactDTO = new()
                    {
                        ContactPerson = contact.ContactPerson,
                        ContactTelephoneNumber = contact.ContactTelephoneNumber,
                        EmergencyContactID = contact.EmergencyContactID,
                        EmployeeID = contact.EmployeeID,
                        RelationshipToEmployee = contact.RelationshipToEmployee
                    };
                    ContactList.Add(contactDTO);
                });

                List<AddressDTO> AddressList = new();

                EmployeeAddresses.ForEach(ad =>
                {

                    AddressDTO address = new()
                    {
                        AddressID = ad.Address.AddressID,
                        AddressTypeID = ad.Address.AddressTypeID,
                        CityName = ad.Address.CityName,
                        ComplexName = ad.Address.ComplexName,
                        CountryName = ad.Address.CountryName,
                        PostalCode = ad.Address.PostalCode,
                        StreetName = ad.Address.StreetName,
                        StreetNumber = ad.Address.StreetNumber,
                        SuburbName = ad.Address.SuburbName,
                        UnitNumber = ad.Address.UnitNumber,
                        Province = ad.Address.Province,
                        isActive = ad.Address.isActive
                    };

                    AddressList.Add(address);
                });

                FullEmployeeDTOVM FullEmployee = new()
                {
                    Employee = employeeDTO,
                    EmergencyContacts = ContactList,
                    Addresses = AddressList
                };

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "View Employees",
                    ActionData = "N/A",
                    EmployeeAction = "View",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();

                return FullEmployee;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }
        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<EmployeeDetailsVM>> GetEmployeeDetails(Guid id)
        {
            try
            {
                var employee = await _context.Employees
                .Include(em => em.AccountType)
                .Include(em => em.Bank)
                .Include(em => em.EmployeeQualification)
                .Include(em => em.EmployeeStatus)
                .Include(em => em.EmployeeTitle)
                .Include(em => em.EmployeeType)
                .Include(em => em.TableStatus)
                .Include(em => em.EmployeeAddresses)
                .Where(em => em.EmployeeID == id)
                .FirstOrDefaultAsync();

                if (employee == null)
                {
                    return NotFound();
                }

                List<AddressVM> AddressVMList = new List<AddressVM>();
                List<EmergencyContactVM> EmergencyContactVMList = new List<EmergencyContactVM>();

                foreach (var employeeAddress in employee.EmployeeAddresses)
                {
                    var address = _context.Addresses
                        .Include(ad => ad.AddressType)
                        .Where(ad => ad.AddressID == employeeAddress.AddressID && ad.isActive == true)
                        .FirstOrDefault();

                    AddressVM addressVM = new()
                    {
                        AddressType = address.AddressType.Description,
                        Suburb = address.SuburbName,
                        City = address.CityName,
                        ComplexName = address.ComplexName,
                        Country = address.CountryName,
                        PostalCode = address.PostalCode,
                        StreetName = address.StreetName,
                        StreetNumber = address.StreetNumber,
                        UnitNumber = address.UnitNumber,
                        Province = address.Province,
                        isActive = address.isActive
                    };

                    AddressVMList.Add(addressVM);
                }

                var emergencyContactList = await _context.EmergencyContacts
                    .Where(ec => ec.EmployeeID == id)
                    .ToListAsync();

                foreach (var contact in emergencyContactList)
                {
                    EmergencyContactVM emergencyContactVM = new()
                    {
                        ContactPerson = contact.ContactPerson,
                        ContactTelephoneNumber = contact.ContactTelephoneNumber,
                        RelationshipToEmployee = contact.RelationshipToEmployee
                    };

                    EmergencyContactVMList.Add(emergencyContactVM);
                }

                EmployeeDetailsVM employeeDetailsVM = new()
                {
                    AccountNumber = employee.AccountNumber,
                    AccountType = employee.AccountType.Description,
                    Bank = employee.Bank.Name,
                    BranchCode = employee.BranchCode,
                    CellNumber = employee.CellNumber,
                    DateOfBirth = employee.DateOfBirth,
                    Email = employee.Email,
                    EmployeeQualification = employee.EmployeeQualification.Description,
                    EmployeeStatus = employee.EmployeeStatus.Description,
                    EmployeeTitle = employee.EmployeeTitle.Description,
                    EmployeeType = employee.EmployeeType.Description,
                    FirstName = employee.FirstName,
                    HomeNumber = employee.HomeNumber,
                    IDNumber = employee.IDNumber,
                    Initials = employee.Initials,
                    isLoggedIn = employee.isLoggedIn,
                    InstitutionName = employee.InstitutionName,
                    KnownAsName = employee.KnownAsName,
                    PassportCountry = employee.PassportCountry,
                    PassportNumber = employee.PassportNumber,
                    Surname = employee.Surname,
                    SystemPassword = employee.SystemPassword,
                    SystemUsername = employee.SystemUsername,
                    TableStatus = employee.TableStatus.Description,
                    TaxNumber = employee.TaxNumber,
                    WorkNumber = employee.WorkNumber,
                    YearCompleted = employee.YearCompleted,

                    Addresses = AddressVMList,
                    EmergencyContacts = EmergencyContactVMList
                };

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "View Employee Details",
                    ActionData = "View Employee Details for : " + employee.FirstName + " " + employee.Surname,
                    EmployeeAction = "View",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();

                return employeeDetailsVM;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<EmployeeVM>> GetEmployeeVM(Guid id)
        {
            try
            {
                var employee = await _context.Employees
                    .Include(e => e.EmployeeType)
                    .Include(e => e.EmployeeStatus)
                    .Where(e => e.EmployeeID == id)
                    .FirstOrDefaultAsync();

                EmployeeVM employeeVM = new()
                {
                    EmployeeID = employee.EmployeeID,
                    FirstName = employee.FirstName,
                    Surname = employee.Surname,
                    Initials = employee.Initials,
                    CellNumber = employee.CellNumber,
                    WorkNumber = employee.WorkNumber,
                    EmployeeStatus = employee.EmployeeStatus.Description,
                    EmployeeType = employee.EmployeeType.Description
                };

                return employeeVM;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<EmployeeVM>>> GetEmployeesVM()
        {
            try
            {
                var EmployeeList = await _context.Employees
                    .Include(el => el.EmployeeType)
                    .Include(el => el.EmployeeStatus).Where(el => el.TableStatusID != Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1")).OrderBy(el => el.FirstName)
                    .ToListAsync();

                List<EmployeeVM> EmployeeVMList = new();


                foreach (var employee in EmployeeList)
                {
                    EmployeeVM employeeVM = new();
                    employeeVM.EmployeeID = employee.EmployeeID;
                    employeeVM.FirstName = employee.FirstName;
                    employeeVM.Surname = employee.Surname;
                    employeeVM.Initials = employee.Initials;
                    employeeVM.CellNumber = employee.CellNumber;
                    employeeVM.WorkNumber = employee.WorkNumber;
                    employeeVM.EmployeeStatus = employee.EmployeeStatus.Description;
                    employeeVM.EmployeeType = employee.EmployeeType.Description;
                    EmployeeVMList.Add(employeeVM);
                }

                return Ok(EmployeeVMList);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<Employee>>> GetOwners()
        {
            try
            {
                var owners = await _context.Employees
                    .Include(employee => employee.EmployeeType)
                    .Where(employee => employee.EmployeeType.Description == "Owner")
                    .ToListAsync();

                if (owners == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(owners);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<EmployeeDropdownVM>>> GetEmployeeDropdown(string type, bool active)
        {
            try
            {
                List<Employee> employees = new();

                if (type == "All")
                {
                    employees = await _context.Employees
                        .AsNoTracking()
                        .Include(em => em.EmployeeType)
                        //Active
                        .Where(em => em.TableStatusID == Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e")).OrderBy(em => em.FirstName)
                        .ToListAsync();
                }
                else
                {
                    //Get all Employees if Active is False
                    //False is the default value
                    if (active == false)
                    {
                        employees = await _context.Employees
                            .AsNoTracking()
                            .Include(em => em.EmployeeType)
                            .Where(em => em.EmployeeType.Description == type)
                            //Available
                            //.Where(em => em.EmployeeStatusID == Guid.Parse("9664ce89-cfac-4904-b5eb-7dda3a038b21"))
                            //Active
                            .Where(em => em.TableStatusID == Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"))
                            .ToListAsync();
                    }
                    else
                    {
                        employees = await _context.Employees
                            .AsNoTracking()
                            .Include(em => em.EmployeeType)
                            .Where(em => em.EmployeeType.Description == type)
                            //Available
                            .Where(em => em.EmployeeStatusID == Guid.Parse("9664ce89-cfac-4904-b5eb-7dda3a038b21"))
                            //Active
                            .Where(em => em.TableStatusID == Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e"))
                            .ToListAsync();
                    }

                }


                if (employees == null)
                {
                    employees = await _context.Employees
                        .AsNoTracking()
                        .ToListAsync();
                }

                List<EmployeeDropdownVM> EmployeeDropdownList = new();

                employees.ForEach((employee) => {

                    EmployeeDropdownVM employeeDropdown = new()
                    {
                        EmployeeID = employee.EmployeeID,
                        FullName = employee.FirstName + ' ' + employee.Surname
                    };

                    EmployeeDropdownList.Add(employeeDropdown);

                });

                return EmployeeDropdownList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }


        // PUT: api/Employee/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmployee(Guid id, Employee employee)
        {
            if (id != employee.EmployeeID)
            {
                return BadRequest();
            }

            _context.Entry(employee).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Employee
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<FullEmployeeVM>> PostEmployee(FullEmployeeVM FullEmployee)
        {

            try
            {
                FullEmployee.Employee.EmployeeID = Guid.NewGuid();
                FullEmployee.Employee.SystemPassword = _authService.BuildHash(FullEmployee.Employee.SystemPassword);
                FullEmployee.Employee.TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e");
                FullEmployee.Employee.EmployeeStatusID = Guid.Parse("9664ce89-cfac-4904-b5eb-7dda3a038b21");

                var FingerFound = await _context.Employees.Where(zz => zz.FingerprintAccountNumber == FullEmployee.Employee.FingerprintAccountNumber).FirstOrDefaultAsync();
                var UserNameFound = await _context.Employees.Where(zz => zz.SystemUsername == FullEmployee.Employee.SystemUsername).FirstOrDefaultAsync();


                if (FingerFound == null && UserNameFound == null)
                {



                    Employee employee = new()
                    {
                        EmployeeID = FullEmployee.Employee.EmployeeID,
                        AccountNumber = FullEmployee.Employee.AccountNumber,
                        AccountTypeID = FullEmployee.Employee.AccountTypeID,
                        BankID = FullEmployee.Employee.BankID,
                        BranchCode = FullEmployee.Employee.BranchCode,
                        DateOfBirth = FullEmployee.Employee.DateOfBirth,
                        CellNumber = FullEmployee.Employee.CellNumber,
                        EmployeeQualificationID = FullEmployee.Employee.EmployeeQualificationID,
                        EmployeeStatusID = FullEmployee.Employee.EmployeeStatusID,
                        EmployeeTitleID = FullEmployee.Employee.EmployeeTitleID,
                        EmployeeTypeID = FullEmployee.Employee.EmployeeTypeID,
                        FingerprintAccountNumber = FullEmployee.Employee.FingerprintAccountNumber,
                        FirstName = FullEmployee.Employee.FirstName,
                        HomeNumber = FullEmployee.Employee.HomeNumber,
                        IDNumber = FullEmployee.Employee.IDNumber,
                        InstitutionName = FullEmployee.Employee.InstitutionName,
                        Initials = FullEmployee.Employee.Initials,
                        isLoggedIn = false,
                        KnownAsName = FullEmployee.Employee.KnownAsName,
                        OTP = null,
                        PassportCountry = FullEmployee.Employee.PassportCountry,
                        PassportNumber = FullEmployee.Employee.PassportNumber,
                        Surname = FullEmployee.Employee.Surname,
                        SystemPassword = FullEmployee.Employee.SystemPassword,
                        SystemUsername = FullEmployee.Employee.SystemUsername,
                        TableStatusID = FullEmployee.Employee.TableStatusID,
                        TaxNumber = FullEmployee.Employee.TaxNumber,
                        YearCompleted = FullEmployee.Employee.YearCompleted,
                        WorkNumber = FullEmployee.Employee.WorkNumber,
                        Email = FullEmployee.Employee.Email
                    };




                    _context.Employees.Add(employee);

                    FullEmployee.Addresses.ForEach((addressDTO) =>
                    {

                        Address address = new()
                        {
                            AddressID = Guid.NewGuid(),
                            AddressTypeID = addressDTO.AddressTypeID,
                            CityName = addressDTO.CityName,
                            ComplexName = addressDTO.ComplexName,
                            CountryName = addressDTO.CountryName,
                            StreetName = addressDTO.StreetName,
                            Province = addressDTO.Province,
                            PostalCode = addressDTO.PostalCode,
                            StreetNumber = addressDTO.StreetNumber,
                            SuburbName = addressDTO.SuburbName,
                            UnitNumber = addressDTO.UnitNumber,
                            isActive = true
                        };

                        EmployeeAddress employeeAddress = new()
                        {
                            AddressID = address.AddressID,
                            EmployeeID = FullEmployee.Employee.EmployeeID
                        };
                        _context.Addresses.Add(address);
                        _context.EmployeeAddresses.Add(employeeAddress);

                    });

                    FullEmployee.EmergencyContacts.ForEach((contactDTO) =>
                    {

                        EmergencyContact emergencyContact = new()
                        {
                            ContactPerson = contactDTO.ContactPerson,
                            ContactTelephoneNumber = contactDTO.ContactTelephoneNumber,
                            EmergencyContactID = Guid.NewGuid(),
                            EmployeeID = FullEmployee.Employee.EmployeeID,
                            RelationshipToEmployee = contactDTO.RelationshipToEmployee
                        };
                        _context.EmergencyContacts.Add(emergencyContact);
                    });

                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var UserID = claimsIdentity.FindFirst("UserId").Value;
                    AuditTrail auditTrail = new()
                    {
                        AuditTrailID = Guid.NewGuid(),
                        ActionDate = DateTime.Now,
                        ActionDescription = "Create Employee",
                        ActionData = "Create Employee :" + employee.FirstName + " " + employee.Surname,
                        EmployeeAction = "Create",
                        EmployeeID = Guid.Parse(UserID)

                    };

                    _context.AuditTrails.Add(auditTrail);


                    await _context.SaveChangesAsync();

                    return CreatedAtAction("GetEmployee", new { id = FullEmployee.Employee.EmployeeID }, FullEmployee.Employee);
                }

                else if (FingerFound != null)
                {
                    return Conflict(new { message = "FingerPrint Exists!" });
                }
                else if (UserNameFound != null)
                {
                    return Conflict(new { message = "UserName Exists!" });
                }
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> UpdateEmployee(FullEmployeeVM FullEmployee)
        {
            try
            {
                var currentEmployee = _context.Employees
                    .Where(e => e.EmployeeID == FullEmployee.Employee.EmployeeID)
                    .FirstOrDefault();

                var FingerFound = await _context.Employees.Where(zz => zz.FingerprintAccountNumber == FullEmployee.Employee.FingerprintAccountNumber && zz.EmployeeID != FullEmployee.Employee.EmployeeID).FirstOrDefaultAsync();
                var UserNameFound = await _context.Employees.Where(zz => zz.SystemUsername == FullEmployee.Employee.SystemUsername && zz.EmployeeID != FullEmployee.Employee.EmployeeID).FirstOrDefaultAsync();


                if (FingerFound == null && UserNameFound == null)
                {
                    currentEmployee.EmployeeID = FullEmployee.Employee.EmployeeID;
                    currentEmployee.AccountNumber = FullEmployee.Employee.AccountNumber;
                    currentEmployee.AccountTypeID = FullEmployee.Employee.AccountTypeID;
                    currentEmployee.BankID = FullEmployee.Employee.BankID;
                    currentEmployee.BranchCode = FullEmployee.Employee.BranchCode;
                    currentEmployee.DateOfBirth = FullEmployee.Employee.DateOfBirth;
                    currentEmployee.CellNumber = FullEmployee.Employee.CellNumber;
                    currentEmployee.EmployeeQualificationID = FullEmployee.Employee.EmployeeQualificationID;
                    currentEmployee.EmployeeStatusID = FullEmployee.Employee.EmployeeStatusID;
                    currentEmployee.EmployeeTitleID = FullEmployee.Employee.EmployeeTitleID;
                    currentEmployee.EmployeeTypeID = FullEmployee.Employee.EmployeeTypeID;
                    currentEmployee.FingerprintAccountNumber = FullEmployee.Employee.FingerprintAccountNumber;
                    currentEmployee.FirstName = FullEmployee.Employee.FirstName;
                    currentEmployee.HomeNumber = FullEmployee.Employee.HomeNumber;
                    currentEmployee.IDNumber = FullEmployee.Employee.IDNumber;
                    currentEmployee.InstitutionName = FullEmployee.Employee.InstitutionName;
                    currentEmployee.Initials = FullEmployee.Employee.Initials;
                    currentEmployee.isLoggedIn = false;
                    currentEmployee.KnownAsName = FullEmployee.Employee.KnownAsName;
                    currentEmployee.OTP = null;
                    currentEmployee.PassportCountry = FullEmployee.Employee.PassportCountry;
                    currentEmployee.PassportNumber = FullEmployee.Employee.PassportNumber;
                    currentEmployee.Surname = FullEmployee.Employee.Surname;
                    currentEmployee.SystemUsername = FullEmployee.Employee.SystemUsername;
                    currentEmployee.TableStatusID = FullEmployee.Employee.TableStatusID;
                    currentEmployee.TaxNumber = FullEmployee.Employee.TaxNumber;
                    currentEmployee.YearCompleted = FullEmployee.Employee.YearCompleted;
                    currentEmployee.WorkNumber = FullEmployee.Employee.WorkNumber;
                    currentEmployee.Email = FullEmployee.Employee.Email;

                    _context.Employees.Update(currentEmployee);

                    //Update Employee Address
                    FullEmployee.Addresses.ForEach((addressDTO) =>
                    {
                        var currentAddress = _context.Addresses
                            .Where(ca => ca.AddressID == addressDTO.AddressID)
                            .FirstOrDefault();

                        if (currentAddress == null)
                        {
                            Address address = new()
                            {
                                AddressID = Guid.NewGuid(),
                                AddressTypeID = addressDTO.AddressTypeID,
                                CityName = addressDTO.CityName,
                                ComplexName = addressDTO.ComplexName,
                                CountryName = addressDTO.CountryName,
                                StreetName = addressDTO.StreetName,
                                Province = addressDTO.Province,
                                PostalCode = addressDTO.PostalCode,
                                StreetNumber = addressDTO.StreetNumber,
                                SuburbName = addressDTO.SuburbName,
                                UnitNumber = addressDTO.UnitNumber,
                                isActive = true
                            };
                            EmployeeAddress newEmployeeAdress = new()
                            {
                                AddressID = address.AddressID,
                                EmployeeID = FullEmployee.Employee.EmployeeID
                            };
                            _context.Addresses.Add(address);
                            _context.EmployeeAddresses.Add(newEmployeeAdress);

                        }
                        else
                        {
                            currentAddress.AddressTypeID = addressDTO.AddressTypeID;
                            currentAddress.CityName = addressDTO.CityName;
                            currentAddress.ComplexName = addressDTO.ComplexName;
                            currentAddress.CountryName = addressDTO.CountryName;
                            currentAddress.StreetName = addressDTO.StreetName;
                            currentAddress.Province = addressDTO.Province;
                            currentAddress.PostalCode = addressDTO.PostalCode;
                            currentAddress.StreetNumber = addressDTO.StreetNumber;
                            currentAddress.SuburbName = addressDTO.SuburbName;
                            currentAddress.UnitNumber = addressDTO.UnitNumber;

                            _context.Addresses.Update(currentAddress);
                        }

                    });

                    //Update Emergency Contacts
                    FullEmployee.EmergencyContacts.ForEach((contactDTO) =>
                    {
                        var currentContact = _context.EmergencyContacts
                            .Where(cc => cc.EmergencyContactID == contactDTO.EmergencyContactID)
                            .FirstOrDefault();

                        if (currentContact == null)
                        {
                            EmergencyContact emergencyContact = new()
                            {
                                ContactPerson = contactDTO.ContactPerson,
                                ContactTelephoneNumber = contactDTO.ContactTelephoneNumber,
                                EmergencyContactID = Guid.NewGuid(),
                                EmployeeID = FullEmployee.Employee.EmployeeID,
                                RelationshipToEmployee = contactDTO.RelationshipToEmployee
                            };

                            emergencyContact.EmployeeID = FullEmployee.Employee.EmployeeID;
                            _context.EmergencyContacts.Add(emergencyContact);
                        }
                        else
                        {
                            currentContact.ContactPerson = contactDTO.ContactPerson;
                            currentContact.ContactTelephoneNumber = contactDTO.ContactTelephoneNumber;
                            currentContact.EmergencyContactID = contactDTO.EmergencyContactID;
                            currentContact.EmployeeID = FullEmployee.Employee.EmployeeID;
                            currentContact.RelationshipToEmployee = contactDTO.RelationshipToEmployee;
                            _context.EmergencyContacts.Update(currentContact);
                        }
                    });

                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var UserID = claimsIdentity.FindFirst("UserId").Value;
                    AuditTrail auditTrail = new()
                    {
                        AuditTrailID = Guid.NewGuid(),
                        ActionDate = DateTime.Now,
                        ActionDescription = "Update Employee",
                        ActionData = "Update Employee " + FullEmployee.Employee.FirstName + " " + FullEmployee.Employee.Surname,
                        EmployeeAction = "Update",
                        EmployeeID = Guid.Parse(UserID)

                    };

                    _context.AuditTrails.Add(auditTrail);


                    await _context.SaveChangesAsync();
                    return Ok();
                }
                else if (FingerFound != null)
                {

                    return Conflict(new { message = "FingerPrint Exists!" });
                }
                else if (UserNameFound != null)
                {
                    return Conflict(new { message = "UserName Exists!" });
                }
                return Ok();



            }// End of try
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        // DELETE: api/Employee/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee(Guid id)
        {
            try
            {
                var employee = await _context.Employees.FindAsync(id);
                var Contacts = await _context.EmergencyContacts.Where(zz => zz.EmployeeID == id).ToListAsync();
                var Addresses = await _context.EmployeeAddresses.Include(zz => zz.Address).Where(zz => zz.EmployeeID == id).ToListAsync();
                if (employee == null)
                {
                    return NotFound();
                }

                foreach (var person in Contacts)
                {
                    _context.EmergencyContacts.Remove(person);
                }

                foreach (var address in Addresses)
                {
                    _context.EmployeeAddresses.Remove(address);
                    _context.Addresses.Remove(address.Address);
                }


                _context.Employees.Remove(employee);

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Delete Employee",
                    ActionData = "Delete Employee " + employee.FirstName + " " + employee.Surname,
                    EmployeeAction = "Delete",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpDelete]
        [Route("[action]")]
        public async Task<IActionResult> DeleteEmergencyContact(Guid id)
        {
            try
            {
                var contact = await _context.EmergencyContacts.FindAsync(id);

                if (contact != null)
                {
                    _context.EmergencyContacts.Remove(contact);

                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var UserID = claimsIdentity.FindFirst("UserId").Value;
                    AuditTrail auditTrail = new()
                    {
                        AuditTrailID = Guid.NewGuid(),
                        ActionDate = DateTime.Now,
                        ActionDescription = "Delete Contact",
                        ActionData = "Delete Contact From Employee",
                        EmployeeAction = "Delete",
                        EmployeeID = Guid.Parse(UserID)

                    };

                    _context.AuditTrails.Add(auditTrail);

                    await _context.SaveChangesAsync();
                }

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpDelete]
        [Route("[action]")]
        public async Task<IActionResult> DeleteAddress(Guid id)
        {
            try
            {
                var address = await _context.Addresses
                    .AsNoTracking()
                    .Where(a => a.AddressID == id)
                    .FirstOrDefaultAsync();

                var EmployeeAddress = await _context.EmployeeAddresses
                    .AsNoTracking()
                    .Where(ea => ea.AddressID == id)
                    .FirstOrDefaultAsync();




                if (address != null)
                {
                    _context.EmployeeAddresses.Remove(EmployeeAddress);
                    _context.Addresses.Remove(address);

                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var UserID = claimsIdentity.FindFirst("UserId").Value;
                    AuditTrail auditTrail = new()
                    {
                        AuditTrailID = Guid.NewGuid(),
                        ActionDate = DateTime.Now,
                        ActionDescription = "Delete Address",
                        ActionData = "Delete Address From Employee",
                        EmployeeAction = "Delete",
                        EmployeeID = Guid.Parse(UserID)

                    };

                    _context.AuditTrails.Add(auditTrail);

                    await _context.SaveChangesAsync();
                }

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }



        private bool EmployeeExists(Guid id)
        {
            return _context.Employees.Any(e => e.EmployeeID == id);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("[action]")]
        public IActionResult Login(LoginViewModel login)
        {
            try
            {
                var employee = _authService.AuthenticateEmployee(login);

                if (employee != null)
                {
                    return Ok(new
                    {
                        UserToken = _authService.BuildToken(employee),
                        UserName = employee.FirstName,
                        EmployeeType = employee.EmployeeType.Description,
                        EmployeeID = employee.EmployeeID,
                        CellNumber = employee.CellNumber
                    });
                }

                return NotFound();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [AllowAnonymous]
        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> GetOTP(string email)
        {

            try
            {
                var employee = await GetEmployeeWithEmail(email);

                if (employee == null)
                {
                    return NotFound();
                }

                employee.OTP = GetRandomNumber(7);
                _context.Employees.Update(employee);

                await _context.SaveChangesAsync();
                await _mailer.SendOtpAsync(email, "Request for OTP", employee, employee.OTP);
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [AllowAnonymous]
        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> SubmitOTP(string email, string OTP)
        {
            try
            {
                var employee = await GetEmployeeWithEmail(email);

                if (employee.OTP == OTP)
                {
                    return Ok(new
                    {
                        token = _authService.BuildToken(employee)
                    });
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        //[EnableCors("TheOrigin")]
        [AllowAnonymous]
        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> SubmitPassword(string email, string password, string token)
        {
            try
            {
                var isTokenValid = _authService.AuthenticateToken(token);

                if (isTokenValid)
                {
                    var employee = await GetEmployeeWithEmail(email);
                    employee.SystemPassword = _authService.BuildHash(password);
                    _context.Employees.Update(employee);
                    await _context.SaveChangesAsync();
                    return Ok();
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        private async Task<Employee> GetEmployeeWithEmail(string email)
        {
            try
            {
                var employee = await _context.Employees
                    .AsNoTracking()
                    .Include(e => e.EmployeeType)
                    .Where(em => em.Email.ToLower() == email.ToLower())
                    .FirstOrDefaultAsync();

                return employee;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
                throw;
            }
        }

        public string GetRandomNumber(int count)
        {
            string RandomNumber = "";
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                RandomNumber = RandomNumber + rnd.Next(0, 9);
            }
            return RandomNumber;
        }

        //--------------------- Employee Lookups -----------------------------------
        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<Bank>>> GetBanks()
        {
            try
            {
                return await _context.Banks.ToListAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<AccountType>>> GetAccountTypes()
        {
            try
            {
                return await _context.AccountTypes.ToListAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<EmployeeQualification>>> GetEmployeeQualifications()
        {
            try
            {
                return await _context.EmployeeQualifications.ToListAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<EmployeeType>>> GetEmployeeTypes()
        {
            try
            {
                return await _context.EmployeeTypes.ToListAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }
        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<EmployeeTitle>>> GetEmployeeTitles()
        {
            try
            {
                return await _context.EmployeeTitles.ToListAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }


        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<Table>>> UploadAttendance(IFormCollection data)
        {
            try
            {
                var file = data.Files[0];

                var bytePDF = await GetBytes(file);

                using PdfDocument document = PdfDocument.Open(bytePDF, new ParsingOptions() { ClipPaths = true });
                ObjectExtractor oe = new ObjectExtractor(document);

                List<Table> tables = new();

                var NumberOfPages = document.NumberOfPages;
                for (int x = 1; x <= NumberOfPages; x++)
                {

                    PageArea currentPage = oe.Extract(x);
                    IExtractionAlgorithm ea = new SpreadsheetExtractionAlgorithm();
                    List<Table> currenttable = ea.Extract(currentPage);

                    var newtable = currenttable[0];

                    tables.Add(newtable);

                }

                List<string> Column0 = new();
                List<string> Column2 = new();
                List<string> Column3 = new();
                List<string> Column4 = new();
                List<string> Column5 = new();
                List<string> Column6 = new();

                for (int k = 0; k < tables.Count; k++)
                {

                    int rowCount = tables[k].RowCount;

                    //Ac-No Column
                    for (int i = 2; i < rowCount; i++)
                    {

                        var row = tables[k].Rows[i][0].GetText();

                        Column0.Add(row);

                    }

                    //Name Column
                    for (int i = 2; i < rowCount; i++)
                    {
                        var row = tables[k].Rows[i][2].GetText();
                        Column2.Add(row);

                    }

                    //Time Column
                    for (int i = 2; i < rowCount; i++)
                    {
                        var row = tables[k].Rows[i][3].GetText();
                        Column3.Add(row);

                    }

                    //State Column
                    for (int i = 2; i < rowCount; i++)
                    {
                        var row = tables[k].Rows[i][4].GetText();
                        Column4.Add(row);

                    }

                    //New State Column
                    for (int i = 2; i < rowCount; i++)
                    {
                        var row = tables[k].Rows[i][5].GetText();
                        Column5.Add(row);

                    }

                    //Exception Column
                    for (int i = 2; i < rowCount; i++)
                    {
                        var row = tables[k].Rows[i][6].GetText();

                        Column6.Add(row);
                    }

                }

                List<AttendanceUploadDTO> attendances = new();

                for (int x = 0; x < Column0.Count; x++)
                {
                    AttendanceUploadDTO attendance = new();

                    attendance.AcNo = Column0[x];
                    attendance.Name = Column2[x];
                    attendance.Time = Column3[x];
                    attendance.State = Column4[x];
                    attendance.NewState = Column5[x];
                    attendance.Exception = Column6[x];

                    attendances.Add(attendance);
                }



                //Adding it into context
                for (int x = 0; x < attendances.Count - 1; x++)
                {
                    if ((attendances[x].Time.Substring(0, 9) == attendances[x + 1].Time.Substring(0, 9)) && (attendances[x].Exception == "FOT") && (attendances[x + 1].Exception == "FOT") && (attendances[x].AcNo == attendances[x + 1].AcNo))
                    {
                        DailyAttendance dailyAttendance = new();
                        var EmployeeList = await _context.Employees.ToListAsync();

                        var EmployeeFound = EmployeeList.Where(zz => zz.FingerprintAccountNumber == attendances[x].AcNo).FirstOrDefault();

                        if (EmployeeFound != null)
                        {
                            dailyAttendance.EmployeeID = EmployeeFound.EmployeeID;

                            int year = Convert.ToInt32(attendances[x].Time.Substring(6, 4));
                            int month = Convert.ToInt32(attendances[x].Time.Substring(3, 2));
                            int day = Convert.ToInt32(attendances[x].Time.Substring(0, 2));
                            int hourIn = Convert.ToInt32(attendances[x].Time.Substring(11, 2));
                            int minuteIn = Convert.ToInt32(attendances[x].Time.Substring(14, 2));

                            int hourOut = Convert.ToInt32(attendances[x + 1].Time.Substring(11, 2)); ;
                            int minuteOut = Convert.ToInt32(attendances[x + 1].Time.Substring(14, 2)); ;

                            dailyAttendance.DailyAttendanceID = Guid.NewGuid();
                            dailyAttendance.Date = new DateTime(year, month, day, hourIn, minuteIn, 00);
                            dailyAttendance.CheckInTime = new DateTime(year, month, day, hourIn, minuteIn, 00);
                            dailyAttendance.CheckOutTime = new DateTime(year, month, day, hourOut, minuteOut, 00);

                            var AttendanceFound = await _context.DailyAttendances.Where(zz => zz.Date.Date == dailyAttendance.Date.Date).FirstOrDefaultAsync();

                            if (AttendanceFound == null)
                            {
                                _context.Add(dailyAttendance);

                            }


                        }

                    }
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Upload Attendance",
                    ActionData = "Upload Attendance For Employees",
                    EmployeeAction = "Create",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);


                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }


        private async Task<byte[]> GetBytes(IFormFile formFile)
        {
            using (var memoryStream = new MemoryStream())
            {
                await formFile.CopyToAsync(memoryStream);
                return memoryStream.ToArray();
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> CheckEmployeeDeleteStatus(Guid id)
        {
            try
            {
                //Audit Trail For Employee
                var EmployeeAuditTrail = await _context.AuditTrails.Where(zz => zz.EmployeeID == id).ToListAsync();
                //Tripsheet in Progress
                var InProgressTripsheets = await _context.DeliveryTripSheets.Where(zz => (zz.DriverID == id || zz.AssistantID == id || zz.SupervisorID == id) && zz.TripSheetStatusID != Guid.Parse("7f559fbe-0c0b-4985-9369-b1055769a35a") && zz.TripSheetStatusID != Guid.Parse("4c53fe83-fb7d-49f1-92ae-6ff72aaec10c")).ToListAsync();
                //All Tripsheet
                var AllTripsheets = await _context.DeliveryTripSheets.Where(zz => zz.DriverID == id || zz.AssistantID == id || zz.SupervisorID == id).ToListAsync();
                //Attendance
                var Attendance = await _context.DailyAttendances.Where(zz => zz.EmployeeID == id).ToListAsync();

                if (AllTripsheets.Count == 0 && EmployeeAuditTrail.Count == 0 && Attendance.Count == 0)
                {
                    //Delete
                    return Ok();
                }
                else if (InProgressTripsheets.Count == 0)
                {
                    //Deactivate
                    return Conflict(new { message = "Deactivate" });
                }
                else if (InProgressTripsheets.Count > 0)
                {
                    //Do Nothing
                    return Conflict(new { message = "In Progress" });
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
                throw;
            }
        }

        public static string ConfirmCode;
        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> Send2FA(string Number)
        {

            try
            {

                ConfirmCode = GetRandomNumber(7);
                /* Environment.GetEnvironmentVariable("TWILIO_ACCOUNT_SID");*/
                /*Environment.GetEnvironmentVariable("TWILIO_AUTH_TOKEN");*/
                string accountSid = Configuration["TWILIO:TWILIO_ACCOUNT_SID"];
                string authToken = Configuration["TWILIO:TWILIO_AUTH_TOKEN"];

                TwilioClient.Init(accountSid, authToken);

                var message = MessageResource.Create(
                    body: "Your 2FA Code is the following : " + ConfirmCode,
                    from: new Twilio.Types.PhoneNumber("+18787787556"),
                    to: new Twilio.Types.PhoneNumber("+27" + Number.Substring(1, 9))
                );

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);

            }



        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> Submit2FA(string Code)
        {
            try
            {
                if (Code == ConfirmCode)
                {
                    return Ok();
                }
                else
                {
                    return Forbid();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
            }
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<EmployeeProfileVM>> GetEmployeeProfile(Guid EmployeeID)
        {
            try
            {
                var currentEmployee = await _context.Employees
                    .Where(em => em.EmployeeID == EmployeeID)
                    .FirstOrDefaultAsync();

                if (currentEmployee != null)
                {
                    EmployeeProfileVM employeeProfileVM = new()
                    {
                        EmployeeID = EmployeeID,
                        CellNumber = currentEmployee.CellNumber,
                        Email = currentEmployee.Email,
                        FirstName = currentEmployee.FirstName,
                        KnownAsName = currentEmployee.KnownAsName,
                        Surname = currentEmployee.Surname,
                        SystemUsername = currentEmployee.SystemUsername,

                    };

                    return employeeProfileVM;
                }

                return NotFound();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }

    }



}
