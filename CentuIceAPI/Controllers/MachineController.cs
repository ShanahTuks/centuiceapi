using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using CsvHelper.Configuration;
using System.Globalization;
using CsvHelper;
using System.IO;
using System.Data;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MachineController : ControllerBase
    {
        private readonly CentuIceDBContext _context;

        public MachineController(CentuIceDBContext context)
        {
            _context = context;
        }

        // GET: api/Machine
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Machine>>> GetMachines()
        {
            return await _context.Machines.ToListAsync();
        }

        // GET: api/Machine/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Machine>> GetMachine(Guid id)
        {
            var machine = await _context.Machines.FindAsync(id);

            if (machine == null)
            {
                return NotFound();
            }

            return machine;
        }

        // PUT: api/Machine/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMachine(Guid id, Machine machine)
        {
            if (id != machine.MachineID)
            {
                return BadRequest();
            }

            _context.Entry(machine).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MachineExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Machine
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Machine>> PostMachine(Machine machine)
        {
            machine.MachineID = Guid.NewGuid();
            machine.TableStatusID = Guid.Parse("c70f248b-1eb9-4f0a-a5be-f95ed556789e");
            _context.Machines.Add(machine);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMachine", new { id = machine.MachineID }, machine);
        }

        // DELETE: api/Machine/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMachine(Guid id)
        {
            var machine = await _context.Machines.FindAsync(id);
            if (machine == null)
            {
                return NotFound();
            }

            var claimsIdentity = User.Identity as ClaimsIdentity;
            var UserID = claimsIdentity.FindFirst("UserId").Value;
            AuditTrail auditTrail = new()
            {
                AuditTrailID = Guid.NewGuid(),
                ActionDate = DateTime.Now,
                ActionDescription = "Delete Machine",
                ActionData = "Delete Machine " + machine.Model,
                EmployeeAction = "Delete",
                EmployeeID = Guid.Parse(UserID)

            };

            _context.AuditTrails.Add(auditTrail);

            _context.Machines.Remove(machine);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool MachineExists(Guid id)
        {
            return _context.Machines.Any(e => e.MachineID == id);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MachineVM>>> GetMachinesVM()
        {
            try
            {
                var MachineList = await _context.Machines.Include(zz => zz.TableStatus).Include(zz => zz.MachineType).Where(zz => zz.TableStatusID != Guid.Parse("049d36ce-4dd3-468d-9631-4faecd8977d1")).ToListAsync();

                List<MachineVM> machineVMs = new();

                foreach (var machine in MachineList)
                {
                    MachineVM newMachine = new();

                    newMachine.MachineID = machine.MachineID;
                    newMachine.MachineTypeID = machine.MachineTypeID;
                    newMachine.TableStatusID = machine.TableStatusID;
                    newMachine.Make = machine.Make;
                    newMachine.Model = machine.Model;
                    newMachine.Last_Service_Date = machine.Last_Service_Date;
                    newMachine.Year_Made = machine.Year_Made;
                    newMachine.MachineType = machine.MachineType.Description;
                    newMachine.MachineStatus = machine.TableStatus.Description;


                    machineVMs.Add(newMachine);
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "View Machines",
                    ActionData = "N/A",
                    EmployeeAction = "View",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();


                return Ok(machineVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
         
        }


        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<MachineDetailsVM>> GetMachineDetailsVM(Guid id)
        {
            try
            {
                var Machine = await _context.Machines.Include(zz => zz.TableStatus).Include(zz => zz.MachineType).Where(zz => zz.MachineID == id).FirstOrDefaultAsync();


                MachineDetailsVM machineDetailsVM = new();

                MachineVM RetrievedMachine = new();

                RetrievedMachine.MachineID = Machine.MachineID;
                RetrievedMachine.MachineTypeID = Machine.MachineTypeID;
                RetrievedMachine.TableStatusID = Machine.TableStatusID;
                RetrievedMachine.Make = Machine.Make;
                RetrievedMachine.Model = Machine.Model;
                RetrievedMachine.Last_Service_Date = Machine.Last_Service_Date;
                RetrievedMachine.Year_Made = Machine.Year_Made;
                RetrievedMachine.MachineType = Machine.MachineType.Description;
                RetrievedMachine.MachineStatus = Machine.TableStatus.Description;


                var MachineData = await _context.MachineMachineData.Include(zz => zz.MachineData).Where(zz => zz.MachineID == id).ToListAsync();

                List<MachineData> machineDatas = new();
                foreach (var machine in MachineData)
                {
                    MachineData machineData = new();

                    machineData.MachineDataID = machine.MachineData.MachineDataID;
                    machineData.Date = machine.MachineData.Date;
                    machineData.Cycles = machine.MachineData.Cycles;
                    machineData.Weight = machine.MachineData.Weight;
                    machineData.WaterUse = machine.MachineData.WaterUse;
                    machineData.FullBin = machine.MachineData.FullBin;
                    machineData.RunTime = machine.MachineData.RunTime;
                    machineData.AVGCycleMin = machine.MachineData.AVGCycleMin;

                    machineDatas.Add(machineData);
                }

                machineDetailsVM.Machine = RetrievedMachine;
                machineDetailsVM.MachineData = machineDatas;

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "View Machine Details",
                    ActionData = "View Machine Details for Machine : " + RetrievedMachine.MachineID,
                    EmployeeAction = "View",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();

                return Ok(machineDetailsVM);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MachineType>>> GetMachineTypes()
        {
            var Types = await _context.MachineTypes.ToListAsync();

            return Ok(Types);
        }


       
        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> UploadMachineData(Guid ID, IFormCollection data)
        {

            try
            {
                var config = new CsvConfiguration(CultureInfo.InvariantCulture)
                {
                    NewLine = Environment.NewLine,
                };

                var file = data.Files[0];

                string fileName = Path.GetTempPath() + Guid.NewGuid().ToString() + ".csv";


                List<ReadMachineDataVM> readMachineDataVMs = new();

                using (var StreamFile = System.IO.File.Create(fileName))
                {
                    await file.CopyToAsync(StreamFile);

                    StreamFile.Close();

                    using (var reader = new StreamReader(fileName))

                    using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                    {

                        var records = csv.GetRecords<ReadMachineDataVM>().ToList();

                        readMachineDataVMs = records;

                    }
                }


                foreach (var machine in readMachineDataVMs)
                {

                    MachineData machineData = new();
                    if (machine.Cycles.Length <= 3)
                    {
                        machineData.Cycles = machine.Cycles;
                        machineData.AVGCycleMin = machine.AVGCycleMin;
                        machineData.FullBin = machine.FullBin;
                        machineData.WaterUse = machine.WaterUse;
                        machineData.Weight = machine.Weight;
                        machineData.MachineDataID = Guid.NewGuid();
                        machineData.RunTime = machine.RunTime;
                        var year = Convert.ToInt32(machine.Date.Substring(1, 4));
                        var month = Convert.ToInt32(machine.Date.Substring(5, 2));
                        var day = Convert.ToInt32(machine.Date.Substring(7, 2));
                        var hour = Convert.ToInt32(machine.Date.Substring(10, 2));
                        var minute = Convert.ToInt32(machine.Date.Substring(13, 2));
                        var second = Convert.ToInt32(machine.Date.Substring(16, 2));
                        DateTime date = new DateTime(year, month, day, hour, minute, second);
                        machineData.Date = date;

                        MachineMachineData machineMachineData = new()
                        {
                            MachineDataID = machineData.MachineDataID,
                            MachineID = ID
                        };

                        _context.MachineData.Add(machineData);
                        _context.MachineMachineData.Add(machineMachineData);
                    }

                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Upload Machine Data",
                    ActionData = "Upload Machine Data for Machine",
                    EmployeeAction = "Create",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();







                return Ok(readMachineDataVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
            



            
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> CheckMachineDeleteStatus(Guid id)
        {
            try
            {
                //Services With Machine
                var MachineService = await _context.ServiceMachines.Where(zz => zz.MachineID == id).ToListAsync();
               
                //Machine Data
                var MachineData = await _context.MachineMachineData.Where(zz => zz.MachineID == id).ToListAsync();
 

                if (MachineService.Count == 0 && MachineData.Count == 0)
                {
                    //Delete
                    return Ok();
                }
                else
                {
                    //Deactivate
                    return Conflict(new { message = "Deactivate" });
                }
               
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
                throw;
            }
        }

    }
}
