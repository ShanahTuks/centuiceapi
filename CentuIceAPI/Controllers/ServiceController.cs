using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using CentuIceAPI.DTO;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private readonly CentuIceDBContext _context;

        public ServiceController(CentuIceDBContext context)
        {
            _context = context;
        }

        // GET: api/Service
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Service>>> GetServices()
        {
            return await _context.Services.ToListAsync();
        }

        // GET: api/Service/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Service>> GetService(Guid id)
        {
            var service = await _context.Services.FindAsync(id);

            if (service == null)
            {
                return NotFound();
            }

            return service;
        }

        // PUT: api/Service/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutService(Guid id, Service service)
        {
            if (id != service.ServiceID)
            {
                return BadRequest();
            }

            _context.Entry(service).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Service
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Service>> PostService(Service service)
        {
            _context.Services.Add(service);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetService", new { id = service.ServiceID }, service);
        }

        // DELETE: api/Service/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteService(Guid id)
        {
            var service = await _context.Services.FindAsync(id);
            if (service == null)
            {
                return NotFound();
            }

            _context.Services.Remove(service);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ServiceExists(Guid id)
        {
            return _context.Services.Any(e => e.ServiceID == id);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ServiceVM>>> GetServiceVM()
        {
            try
            {
                var ServiceList = await _context.Services.Include(zz => zz.Supplier).ToListAsync();

                List<ServiceVM> serviceVMs = new();

                foreach (var service in ServiceList)
                {
                    ServiceVM newService = new();
                    var Vehicle = await _context.ServiceVehicles.Where(zz => zz.ServiceID == service.ServiceID).FirstOrDefaultAsync();



                    if (Vehicle == null)
                    {
                        var Machine = await _context.ServiceMachines.Where(zz => zz.ServiceID == service.ServiceID).FirstOrDefaultAsync();
                        newService.AssetID = Machine.MachineID;
                        newService.AssetType = "Machine";
                    }
                    else
                    {
                        newService.AssetID = Vehicle.VehicleID;
                        newService.AssetType = "Vehicle";
                    }

                    newService.ServiceID = service.ServiceID;
                    newService.SupplierName = service.Supplier.SupplierName;
                    newService.ServiceDate = service.ServiceDate;

                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var UserID = claimsIdentity.FindFirst("UserId").Value;
                    AuditTrail auditTrail = new()
                    {
                        AuditTrailID = Guid.NewGuid(),
                        ActionDate = DateTime.Now,
                        ActionDescription = "View Services",
                        ActionData = "N/A",
                        EmployeeAction = "View",
                        EmployeeID = Guid.Parse(UserID)

                    };

                    _context.AuditTrails.Add(auditTrail);

                    await _context.SaveChangesAsync();

                    serviceVMs.Add(newService);
                }
                return Ok(serviceVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }

        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult<ServiceDTO>> CreateService(ServiceDTO service)
        {
            try
            {
                Service newService = new();

                service.Service.ServiceID = Guid.NewGuid();
                newService = service.Service;

                _context.Services.Add(newService);
                foreach (var job in service.ServiceJobs)
                {
                    job.ServiceJobID = Guid.NewGuid();

                    _context.ServiceJobs.Add(job);

                    ServiceServiceJob serviceServiceJob = new()
                    {
                        ServiceID = service.Service.ServiceID,
                        ServiceJobID = job.ServiceJobID
                    };

                    _context.ServiceServiceJobs.Add(serviceServiceJob);


                }

                if (service.AssetType == "Vehicle")
                {
                    var Vehicle = await _context.Vehicles.Where(zz => zz.VehicleID == Guid.Parse(service.AssetID)).FirstOrDefaultAsync();

                    Vehicle.Last_Service_Date = service.Service.ServiceDate;
                    Vehicle.VehicleStatusID = Guid.Parse("65563356-8382-4c0d-8e2c-f94c98cb786b");
                    Vehicle.LastServicedMileage = Vehicle.Mileage;

                    _context.Vehicles.Update(Vehicle);
                    ServiceVehicle serviceVehicle = new()
                    {
                        ServiceID = service.Service.ServiceID,
                        VehicleID = Guid.Parse(service.AssetID)
                    };

                    _context.ServiceVehicles.Add(serviceVehicle);
                }
                else if (service.AssetType == "Machine")
                {
                    var Machine = await _context.Machines.Where(zz => zz.MachineID == Guid.Parse(service.AssetID)).FirstOrDefaultAsync();

                    Machine.Last_Service_Date = service.Service.ServiceDate;

                    _context.Machines.Update(Machine);
                    ServiceMachine serviceMachine = new()
                    {
                        ServiceID = service.Service.ServiceID,
                        MachineID = Guid.Parse(service.AssetID)
                    };

                    _context.ServiceMachines.Add(serviceMachine);
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Create Service",
                    ActionData = "Create Service :" + service.Service.ServiceID,
                    EmployeeAction = "Crete",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

             
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }



        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<ServiceDTO>> GetCurrentService(Guid id)
        {
            try
            {
                ServiceDTO CurrentService = new();
                List<ServiceJob> serviceJobs = new();
                var Service = await _context.Services.Where(zz => zz.ServiceID == id).FirstOrDefaultAsync();

                Service service = new();

                service.ServiceID = Service.ServiceID;
                service.SupplierID = Service.SupplierID;
                service.ServiceDate = Service.ServiceDate;

                CurrentService.Service = service;
                var Jobs = await _context.ServiceServiceJobs.Include(zz => zz.ServiceJob).Where(zz => zz.ServiceID == id).ToListAsync();

                foreach (var job in Jobs)
                {
                    ServiceJob serviceJob = new();

                    serviceJob.CostOfJob = job.ServiceJob.CostOfJob;
                    serviceJob.ServiceJobID = job.ServiceJob.ServiceJobID;
                    serviceJob.ServiceJobName = job.ServiceJob.ServiceJobName;


                    serviceJobs.Add(serviceJob);
                }

                CurrentService.ServiceJobs = serviceJobs;

                var Vehicle = await _context.ServiceVehicles.Where(zz => zz.ServiceID == id).FirstOrDefaultAsync();

                if (Vehicle == null)
                {
                    var Machine = await _context.ServiceMachines.Where(zz => zz.ServiceID == id).FirstOrDefaultAsync();
                    CurrentService.AssetID = Convert.ToString(Machine.MachineID);
                    CurrentService.AssetType = "Machine";

                }
                else
                {
                    CurrentService.AssetID = Convert.ToString(Vehicle.VehicleID);
                    CurrentService.AssetType = "Vehicle";
                }

                return Ok(CurrentService);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }


        [Route("[action]")]
        [HttpPut]
        public async Task<IActionResult> UpdateService(ServiceDTO serviceDTO, Guid id)
        {

            try
            {
                var Service = await _context.Services.AsNoTracking().Where(zz => zz.ServiceID == id).FirstOrDefaultAsync();

                serviceDTO.ServiceJobs.ForEach(job =>
                {
                    //Checking if the ServiceJob already exists
                    var currentJob = _context.ServiceJobs.AsNoTracking().Where(zz => zz.ServiceJobID == job.ServiceJobID).FirstOrDefault();

                    if (currentJob == null)
                    {
                        job.ServiceJobID = Guid.NewGuid();
                        ServiceServiceJob serviceServiceJob = new()
                        {
                            ServiceID = serviceDTO.Service.ServiceID,
                            ServiceJobID = job.ServiceJobID
                        };

                        _context.ServiceJobs.Add(job);
                        _context.ServiceServiceJobs.Add(serviceServiceJob);
                    }
                    else
                    {
                        _context.ServiceJobs.Update(job);
                    }
                });

                if (Service != null)
                {
                    _context.Services.Update(serviceDTO.Service);
                }

                if (serviceDTO.AssetType == "Vehicle")
                {
                    var Vehicle = _context.ServiceVehicles.AsNoTracking().Where(zz => zz.ServiceID == id).FirstOrDefault();

                    if (Vehicle != null)
                    {

                        _context.ServiceVehicles.Remove(Vehicle);
                        ServiceVehicle serviceVehicle = new()
                        {
                            ServiceID = serviceDTO.Service.ServiceID,
                            VehicleID = Guid.Parse(serviceDTO.AssetID)
                        };

                        _context.ServiceVehicles.Add(serviceVehicle);


                    }
                }
                else
                {
                    var Machine = _context.ServiceMachines.AsNoTracking().Where(zz => zz.ServiceID == id).FirstOrDefault();

                    if (Machine != null)
                    {

                        _context.ServiceMachines.Remove(Machine);
                        ServiceMachine serviceMachine = new()
                        {
                            ServiceID = serviceDTO.Service.ServiceID,
                            MachineID = Guid.Parse(serviceDTO.AssetID)
                        };

                        _context.ServiceMachines.Add(serviceMachine);

                    }
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Update Service",
                    ActionData = "Update Service : " + serviceDTO.Service.ServiceID,
                    EmployeeAction = "Update",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

        

                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<ServiceDetailsVM>> GetServiceDetails(Guid id)
        {

            try
            {
                ServiceDetailsVM CurrentService = new();
                List<ServiceJob> serviceJobs = new();
                var Service = await _context.Services.Where(zz => zz.ServiceID == id).Include(zz => zz.Supplier).FirstOrDefaultAsync();

                Service service = new();

                service.ServiceID = Service.ServiceID;
                service.SupplierID = Service.SupplierID;
                service.ServiceDate = Service.ServiceDate;

                CurrentService.Service = service;
                CurrentService.SupplierName = Service.Supplier.SupplierName;
                var Jobs = await _context.ServiceServiceJobs.Include(zz => zz.ServiceJob).Where(zz => zz.ServiceID == id).ToListAsync();

                foreach (var job in Jobs)
                {
                    ServiceJob serviceJob = new();

                    serviceJob.CostOfJob = job.ServiceJob.CostOfJob;
                    serviceJob.ServiceJobID = job.ServiceJob.ServiceJobID;
                    serviceJob.ServiceJobName = job.ServiceJob.ServiceJobName;


                    serviceJobs.Add(serviceJob);
                }

                CurrentService.ServiceJobs = serviceJobs;

                var Vehicle = await _context.ServiceVehicles.Include(zz => zz.Vehicle).Where(zz => zz.ServiceID == id).FirstOrDefaultAsync();

                if (Vehicle == null)
                {
                    var Machine = await _context.ServiceMachines.Include(zz => zz.Machine).Where(zz => zz.ServiceID == id).FirstOrDefaultAsync();
                    CurrentService.AssetID = Convert.ToString(Machine.MachineID);
                    CurrentService.AssetType = "Machine";
                    CurrentService.MachineModel = Machine.Machine.Model;

                }
                else
                {
                    CurrentService.AssetID = Convert.ToString(Vehicle.VehicleID);
                    CurrentService.AssetType = "Vehicle";
                    CurrentService.VehicleReg = Vehicle.Vehicle.Reg_Number;
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "View Service Details",
                    ActionData = "View Service Details for Service " + service.ServiceID,
                    EmployeeAction = "View",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

                await _context.SaveChangesAsync();

                return Ok(CurrentService);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
            }
           


        [Route("[action]")]
        [HttpDelete]
        public async Task<ActionResult<ServiceDetailsVM>> RemoveJob(Guid id)
        {
            try
            {
                var Job = await _context.ServiceJobs.Where(zz => zz.ServiceJobID == id).FirstOrDefaultAsync();

                var Service = await _context.ServiceServiceJobs.Where(zz => zz.ServiceJobID == id).FirstOrDefaultAsync();

                

                var ServiceJob = await _context.ServiceServiceJobs.Where(zz => zz.ServiceJobID == id).FirstOrDefaultAsync();

                _context.ServiceServiceJobs.Remove(ServiceJob);
                _context.ServiceJobs.Remove(Job);

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Remove Service Job",
                    ActionData = "Remove Service Job From Service : " + Service.ServiceID,
                    EmployeeAction = "Delete",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

            

                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;

            }



        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<ServiceDetailsVM>> GetChosenAssetDate(Guid id)
        {
           try
            {
                var Machine = await _context.Machines.Where(zz => zz.MachineID == id).FirstOrDefaultAsync();
                var Vehicle = await _context.Vehicles.Where(zz => zz.VehicleID == id).FirstOrDefaultAsync();   

                if (Machine == null && Vehicle != null)
                {
                    return Ok(Vehicle.Last_Service_Date); 
                }
                else if (Vehicle == null && Machine != null)
                {
                    return Ok(Machine.Last_Service_Date);
                }

                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
                throw;
            }
        }
    }

}