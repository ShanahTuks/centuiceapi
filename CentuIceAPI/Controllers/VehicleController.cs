using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using CentuIceAPI.DTO;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly CentuIceDBContext _context;

        public VehicleController(CentuIceDBContext context)
        {
            _context = context;
        }

        // GET: api/Vehicle
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Vehicle>>> GetVehicles()
        {
            return await _context.Vehicles.ToListAsync();
        }

        // GET: api/Vehicle/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Vehicle>> GetVehicle(Guid id)
        {
            var vehicle = await _context.Vehicles.FindAsync(id);

            if (vehicle == null)
            {
                return NotFound();
            }

            return vehicle;
        }

        // PUT: api/Vehicle/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVehicle(Guid id, Vehicle vehicle)
        {
            if (id != vehicle.VehicleID)
            {
                return BadRequest();
            }

            _context.Entry(vehicle).State = EntityState.Modified;

            try
            {

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Update Vehicle",
                    ActionData = "Update Vehicle  " + vehicle.VehicleID,
                    EmployeeAction = "Update",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);




              
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VehicleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Vehicle
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Vehicle>> PostVehicle(Vehicle vehicle)
        {
            vehicle.VehicleID = Guid.NewGuid();
            vehicle.VehicleStatusID = Guid.Parse("65563356-8382-4c0d-8e2c-f94c98cb786b");
            _context.Vehicles.Add(vehicle);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVehicle", new { id = vehicle.VehicleID }, vehicle);
        }

        // DELETE: api/Vehicle/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVehicle(Guid id)
        {
            var vehicle = await _context.Vehicles.FindAsync(id);
            if (vehicle == null)
            {
                return NotFound();
            }

            var claimsIdentity = User.Identity as ClaimsIdentity;
            var UserID = claimsIdentity.FindFirst("UserId").Value;
            AuditTrail auditTrail = new()
            {
                AuditTrailID = Guid.NewGuid(),
                ActionDate = DateTime.Now,
                ActionDescription = "Delete Vehicle",
                ActionData = "Delete Vehicle " + vehicle.Reg_Number,
                EmployeeAction = "Delete",
                EmployeeID = Guid.Parse(UserID)

            };

            _context.AuditTrails.Add(auditTrail);

            _context.Vehicles.Remove(vehicle);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool VehicleExists(Guid id)
        {
            return _context.Vehicles.Any(e => e.VehicleID == id);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VehicleVM>>> GetVehiclesVM(string Service)
        {
            try
            {
                List<Vehicle> VehicleList = new List<Vehicle>();
                if (Service == "no")
                {
                    VehicleList = await _context.Vehicles.Include(zz => zz.VehicleStatus).Where(zz => zz.VehicleStatusID != Guid.Parse("6ffce810-cbb1-4192-87d6-378487d19913")).ToListAsync();
                }
                else if (Service == "yes")
                {
                     VehicleList = await _context.Vehicles.Include(zz => zz.VehicleStatus).Where(zz => zz.VehicleStatusID == Guid.Parse("e7b83455-818c-409a-9742-bf66b34ed341")).ToListAsync();
                }

                List<VehicleVM> vehicleVMs = new();

                foreach (var vehicle in VehicleList)
                {
                    VehicleVM newVehicle = new();

                    newVehicle.VehicleID = vehicle.VehicleID;
                    newVehicle.VehicleStatusID = vehicle.VehicleStatusID;
                    newVehicle.Make = vehicle.Make;
                    newVehicle.Model = vehicle.Model;
                    newVehicle.Reg_Number = vehicle.Reg_Number;
                    newVehicle.Mileage = vehicle.Mileage;
                    newVehicle.Tracker_Number = vehicle.Tracker_Number;
                    newVehicle.Year_Made = vehicle.Year_Made;
                    newVehicle.Capacity = vehicle.Capacity;
                    newVehicle.Last_Service_Date = vehicle.Last_Service_Date;
                    newVehicle.VehicleStatus = vehicle.VehicleStatus.Description;

                    vehicleVMs.Add(newVehicle);
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "View Vehicles",
                    ActionData = "N/A",
                    EmployeeAction = "View",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);




                await _context.SaveChangesAsync();

                return Ok(vehicleVMs);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
         
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<VehicleVM>> GetVehicleDetailsVM(Guid id)
        {

            try
            {
                var Vehicle = await _context.Vehicles.Include(zz => zz.VehicleStatus).Where(zz => zz.VehicleID == id).FirstOrDefaultAsync();



                VehicleVM RetrievedVehicle = new();

                RetrievedVehicle.VehicleID = Vehicle.VehicleID;
                RetrievedVehicle.VehicleStatusID = Vehicle.VehicleStatusID;
                RetrievedVehicle.Make = Vehicle.Make;
                RetrievedVehicle.Model = Vehicle.Model;
                RetrievedVehicle.Reg_Number = Vehicle.Reg_Number;
                RetrievedVehicle.Mileage = Vehicle.Mileage;
                RetrievedVehicle.Tracker_Number = Vehicle.Tracker_Number;
                RetrievedVehicle.Last_Service_Date = Vehicle.Last_Service_Date;
                RetrievedVehicle.Year_Made = Vehicle.Year_Made;
                RetrievedVehicle.Capacity = Vehicle.Capacity;
                RetrievedVehicle.VehicleStatus = Vehicle.VehicleStatus.Description;
                RetrievedVehicle.License_Expiry_Date = Vehicle.License_Expiry_Date;


                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "View Vehicle Details",
                    ActionData = "View Vehicle Details " + RetrievedVehicle.VehicleID,
                    EmployeeAction = "View",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);




                await _context.SaveChangesAsync();


                return Ok(RetrievedVehicle);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }

        [Route("[action]")]
        [HttpPut]
        public async Task<ActionResult> UpdateStatus(Guid ID, StatusDTO status)
        {
            try
            {
                var Vehicle = await _context.Vehicles.Where(zz => zz.VehicleID == ID).FirstOrDefaultAsync();

                if (Vehicle == null)
                {
                    return NoContent();
                }
                else
                {
                    Vehicle.VehicleStatusID = status.OrderStatusID;
                    _context.Update(Vehicle);

                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var UserID = claimsIdentity.FindFirst("UserId").Value;
                    AuditTrail auditTrail = new()
                    {
                        AuditTrailID = Guid.NewGuid(),
                        ActionDate = DateTime.Now,
                        ActionDescription = "Update Vehicle Status",
                        ActionData = "Update Vehicle Status " + Vehicle.VehicleID,
                        EmployeeAction = "Update",
                        EmployeeID = Guid.Parse(UserID)

                    };

                    _context.AuditTrails.Add(auditTrail);




                    await _context.SaveChangesAsync();
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VehicleStatus>>> GetVehicleStatuses()
        {
            var Statuses = await _context.VehicleStatuses.ToListAsync();

            return Ok(Statuses);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VehicleDropdownVM>>> GetVehiclesDropdown(bool active)
        {
            try
            {
                List<VehicleDropdownVM> vehicleDropdowns = new();

                if (active == false)
                {
                    var VehicleList = await _context.Vehicles
                        .Include(zz => zz.VehicleStatus)
                        .Where(zz => zz.VehicleStatus.Description == "Available")
                        .ToListAsync();

                    foreach (var vehicle in VehicleList)
                    {
                        VehicleDropdownVM vehicleDropdown = new()
                        {
                            VehicleID = vehicle.VehicleID,
                            Reg_Number = vehicle.Reg_Number,
                        };
                        vehicleDropdowns.Add(vehicleDropdown);
                    }
                }
                else
                {
                    var VehicleList = await _context.Vehicles
                        .Include(zz => zz.VehicleStatus)
                        .Where(zz => zz.VehicleStatus.Description != "Written-Off")
                        .Where(zz => zz.VehicleStatus.Description != "Deactivated")
                        .Where(zz => zz.VehicleStatus.Description != "Pending")
                        .ToListAsync();

                    foreach (var vehicle in VehicleList)
                    {
                        VehicleDropdownVM vehicleDropdown = new()
                        {
                            VehicleID = vehicle.VehicleID,
                            Reg_Number = vehicle.Reg_Number,
                        };
                        vehicleDropdowns.Add(vehicleDropdown);
                    }
                }

                return vehicleDropdowns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> CheckVehicleDeleteStatus(Guid id)
        {
            try
            {
                //Services With Vehicle
                var VehicleService = await _context.ServiceVehicles.Where(zz => zz.VehicleID == id).ToListAsync();
                //All Tripsheets
                var AllTripsheets = await _context.DeliveryTripSheets.Where(zz => zz.VehicleID == id).ToListAsync();
                //Tripsheets in Progress
                var TripsheetsInProgress = await _context.DeliveryTripSheets.Where(zz => zz.VehicleID == id && zz.TripSheetStatusID != Guid.Parse("7f559fbe-0c0b-4985-9369-b1055769a35a") && zz.TripSheetStatusID != Guid.Parse("4c53fe83-fb7d-49f1-92ae-6ff72aaec10c")).ToListAsync();


                if (VehicleService.Count == 0 && AllTripsheets.Count == 0)
                {
                    //Delete
                    return Ok();
                }
                else if (TripsheetsInProgress.Count == 0)
                {
                    //Deactivate
                    return Conflict(new { message = "Deactivate" });
                }
                else if (TripsheetsInProgress.Count > 0)
                {
                    //Do Nothing
                    return Conflict(new { message = "In Progress" });
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }


        [Route("[action]")]
        [HttpDelete]
        public async Task<ActionResult<IEnumerable<VehicleVM>>> ChangeToServiced(Guid ID)
        {
            try
            {
                var Vehicle = await _context.Vehicles.Where(zz => zz.VehicleID == ID).FirstOrDefaultAsync();

                if (Vehicle != null)
                {
                    Vehicle.VehicleStatusID = Guid.Parse("e7b83455-818c-409a-9742-bf66b34ed341");

                    _context.Vehicles.Update(Vehicle);

                    await _context.SaveChangesAsync();

                    return Ok();
                }

                return StatusCode(500);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }

    }
}
