using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using CentuIceAPI.DTO;
using CentuIceAPI.Services.ServiceBase;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly CentuIceDBContext _context;
        private readonly IMailer _mailer;

        public InvoiceController(CentuIceDBContext context, IMailer mailer)
        {
            _context = context;
            _mailer = mailer;
        }



        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult<IEnumerable<InvoiceVM>>> GetInvoices()
        {

            try
            {
                var Invoices = await _context.Invoices.Include(zz => zz.InvoiceStatus).Include(zz => zz.PaymentMethod).OrderByDescending(zz => zz.InvoiceDate).ToListAsync();

                List<InvoiceVM> invoiceVMs = new();

                foreach (var invoice in Invoices)
                {
                    InvoiceVM invoiceVM = new();
                    var Sale = await _context.Sales.Where(zz => zz.InvoiceID == invoice.InvoiceID).FirstOrDefaultAsync();
                    invoiceVM.InvoiceID = invoice.InvoiceID;
                    invoiceVM.InvoiceDate = invoice.InvoiceDate;
                    invoiceVM.InvoiceNumber = invoice.InvoiceNumber;
                    invoiceVM.InvoiceStatus = invoice.InvoiceStatus.Description;

                    if (invoice.PaymentMethod == null)
                    {
                        invoiceVM.InvoicePaymentMethod = "";
                    }
                    else
                    {
                        invoiceVM.InvoicePaymentMethod = invoice.PaymentMethod.Description;
                    }
    


                    if (Sale != null)
                    {
                        invoiceVM.ClientName = "Walk-In";
                        invoiceVM.InvoiceType = "Sale";
                    }
                    else
                    {
                        var Order = await _context.Orders.Include(zz => zz.Client).Where(zz => zz.InvoiceID == invoice.InvoiceID).FirstOrDefaultAsync();

                        invoiceVM.ClientName = Order.Client.ClientName;
                        invoiceVM.InvoiceType = "Order";
                    }



                    invoiceVMs.Add(invoiceVM);
                }


                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "View Invoices",
                    ActionData = "N/A",
                    EmployeeAction = "View",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();

                return Ok(invoiceVMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SaleInvoiceVM>>> GetSaleLines(Guid ID)
        {

            try
            {
                var Invoice = await _context.Invoices.Where(zz => zz.InvoiceID == ID).Include(zz => zz.InvoiceStatus).Include(zz => zz.PaymentMethod).FirstOrDefaultAsync();

                SaleInvoiceVM saleInvoiceVM = new();

                InvoiceVM invoiceVM = new();

                invoiceVM.InvoiceID = Invoice.InvoiceID;
                invoiceVM.InvoiceDate = Invoice.InvoiceDate;
                invoiceVM.ClientName = "Walk-In";
                invoiceVM.InvoiceNumber = Invoice.InvoiceNumber;
                invoiceVM.InvoiceStatus = Invoice.InvoiceStatus.Description;
                invoiceVM.InvoicePaymentMethod = Invoice.PaymentMethod.Description;
                invoiceVM.InvoiceType = "Sale";



                var Sale = await _context.Sales.Where(zz => zz.InvoiceID == ID).FirstOrDefaultAsync();

                var SaleLines = await _context.SaleLines.Include(zz => zz.Product).Where(zz => zz.SaleID == Sale.SaleID).ToListAsync();

                List<SaleLineDTO> saleLineDTOs = new();

                foreach (var sale in SaleLines)
                {
                    var Price = await _context.ProductPrices.Where(zz => zz.ProductPriceID == sale.ProductPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();
                    SaleLineDTO line = new();
                    line.SaleID = sale.SaleID;
                    line.ProductID = sale.ProductID;
                    line.ProductName = sale.Product.ProductName;
                    line.ProductPrice = Price;
                    line.Quantity = sale.Quantity;

                    if (sale.Product.isVatApplicable == true)
                    {
                        line.ProductVatActivated = true;
                    }
                    else
                    {
                        line.ProductVatActivated = false;
                    }

                    if (sale.Sale.VatActivated == true)
                    {
                        line.SaleVatActivated = true;
                    }
                    else
                    {
                        line.SaleVatActivated = false;
                    }


                    if (line.SaleVatActivated == true && line.ProductVatActivated == true)
                    {
                        var CurrentVat = await _context.VatConfigurations.OrderByDescending(zz => zz.DateSet).Select(zz => zz.VatPercentage).FirstOrDefaultAsync();

                        line.VatPercentage = CurrentVat;
                    }


                    saleLineDTOs.Add(line);
                }
                saleInvoiceVM.Invoice = invoiceVM;
                saleInvoiceVM.SaleLines = saleLineDTOs;

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Generate Invoice",
                    ActionData = "Generate Invoice " + invoiceVM.InvoiceNumber,
                    EmployeeAction = "Generate",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();

                return Ok(saleInvoiceVM);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }


        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderInvoiceVM>>> GetOrderLines(Guid ID)
        {
            try
            {
                var Invoice = await _context.Invoices.Where(zz => zz.InvoiceID == ID).Include(zz => zz.InvoiceStatus).Include(zz => zz.PaymentMethod).FirstOrDefaultAsync();
                var Order = await _context.Orders.Where(zz => zz.InvoiceID == ID).Include(zz => zz.Client).FirstOrDefaultAsync();

                OrderInvoiceVM orderInvoiceVM = new();

                InvoiceVM invoiceVM = new();

                invoiceVM.InvoiceID = Invoice.InvoiceID;
                invoiceVM.InvoiceDate = Invoice.InvoiceDate;
                invoiceVM.ClientName = Order.Client.ClientName;
                invoiceVM.InvoiceNumber = Invoice.InvoiceNumber;
                invoiceVM.InvoiceStatus = Invoice.InvoiceStatus.Description;
                if (Invoice.PaymentMethod != null)
                {
                    invoiceVM.InvoicePaymentMethod = Invoice.PaymentMethod.Description;
                }
                else
                {
                    invoiceVM.InvoicePaymentMethod = "N/A";
                }

                invoiceVM.InvoiceType = "Order";

              

                




                var Contact = await _context.ClientContactPeople.Where(zz => zz.ClientID == Order.Client.ClientID).FirstOrDefaultAsync();

                orderInvoiceVM.EmailAddress = Contact.EmailAddress;
                orderInvoiceVM.PhoneNumber = Contact.PhoneNumber;

                var Address = await _context.ClientAddresses.Include(zz => zz.Address).Where(zz => zz.ClientID == Order.Client.ClientID && zz.Address.AddressTypeID == Guid.Parse("42cb8c8b-3098-4f21-b797-a15166d0964b")).FirstOrDefaultAsync();

                orderInvoiceVM.SuburbName = Address.Address.SuburbName;
                orderInvoiceVM.CityName = Address.Address.CityName;
                orderInvoiceVM.StreetName = Address.Address.StreetName;
                orderInvoiceVM.StreetNumber = Address.Address.StreetNumber;


                var OrderLines = await _context.OrderLines.Include(zz => zz.Product).Where(zz => zz.OrderID == Order.OrderID).ToListAsync();

                List<OrderLineDTO> orderLineDTOs = new();

                foreach (var order in OrderLines)
                {
                    var Price = await _context.ProductPrices.Where(zz => zz.ProductPriceID == order.ProductPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();
                    OrderLineDTO line = new();
                    line.OrderID = order.OrderID;
                    line.ProductID = order.ProductID;
                    line.ProductName = order.Product.ProductName;
                    line.ProductPrice = Price;
                    line.Quantity = order.Quantity;

                    if (order.Product.isVatApplicable == true)
                    {
                        line.ProductVatActivated = true;
                    }
                    else
                    {
                        line.ProductVatActivated = false;
                    }

                    if (order.Order.VatActivated == true)
                    {
                        line.OrderVatActivated = true;
                    }
                    else
                    {
                        line.OrderVatActivated = false;
                    }


                    if (line.OrderVatActivated == true && line.ProductVatActivated == true)
                    {
                        var CurrentVat = await _context.VatConfigurations.OrderByDescending(zz => zz.DateSet).Select(zz => zz.VatPercentage).FirstOrDefaultAsync();

                        line.VatPercentage = CurrentVat;
                    }

                    orderLineDTOs.Add(line);
                }
                orderInvoiceVM.Invoice = invoiceVM;
                orderInvoiceVM.OrderLines = orderLineDTOs;

                var Extras = await _context.OrderExtraProductsTaken.Where(zz => zz.OrderID == Order.OrderID).Include(zz => zz.ExtraProductTaken.Product).Include(zz => zz.ExtraProductTaken).ToListAsync();


                List<OrderLineDTO> extras = new();
                if (Extras != null)
                {
                    foreach (var order in Extras)
                    {
                        var Price = await _context.ProductPrices.Where(zz => zz.ProductPriceID == order.ExtraProductTaken.ProductPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();
                        OrderLineDTO line = new();
                        line.OrderID = order.OrderID;
                        line.ProductID = order.ExtraProductTaken.ProductID;
                        line.ProductName = order.ExtraProductTaken.Product.ProductName;
                        line.ProductPrice = Price;
                        line.Quantity = order.Quantity;

                        if (order.ExtraProductTaken.Product.isVatApplicable == true)
                        {
                            line.ProductVatActivated = true;
                        }
                        else
                        {
                            line.ProductVatActivated = false;
                        }

                        if (order.Order.VatActivated == true)
                        {
                            line.OrderVatActivated = true;
                        }
                        else
                        {
                            line.OrderVatActivated = false;
                        }


                        if (line.OrderVatActivated == true && line.ProductVatActivated == true)
                        {
                            var CurrentVat = await _context.VatConfigurations.OrderByDescending(zz => zz.DateSet).Select(zz => zz.VatPercentage).FirstOrDefaultAsync();

                            line.VatPercentage = CurrentVat;
                        }

                        extras.Add(line);
                    }
                }

                orderInvoiceVM.Extras = extras;

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Generate Invoice",
                    ActionData = "Generate Invoice " + invoiceVM.InvoiceNumber,
                    EmployeeAction = "Generate",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();


                return Ok(orderInvoiceVM);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }

        }



        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> GetPaymentTypes()
        {
            var PaymentTypes = await _context.PaymentMethods.ToListAsync();

            return Ok(PaymentTypes);
        }


        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult> SetToPaid(Guid ID, PaymentMethod PaymentType)
        {
            try
            {
                var Invoice = await _context.Invoices.Where(zz => zz.InvoiceID == ID).FirstOrDefaultAsync();


                
                    Invoice.InvoiceStatusID = Guid.Parse("7ad6de50-3811-462b-8118-2257149154be");
                    Invoice.PaymentMethodID = PaymentType.PaymentMethodID;

                    _context.Invoices.Update(Invoice);

                    await _context.SaveChangesAsync();
                    return Ok();
                

                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }


        }


        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult> SendInvoice(Guid ID, IFormCollection file)
        {

            try
            {
                var InvoiceOrder = await _context.Orders.Include(zz => zz.Client).Include(zz => zz.Invoice).Where(zz => zz.InvoiceID == ID).FirstOrDefaultAsync();
                var ContactPerson = await _context.ClientContactPeople.Where(zz => zz.ClientID == InvoiceOrder.Client.ClientID).ToListAsync();
                var Client = await _context.Clients.Where(zz => zz.ClientID == InvoiceOrder.Client.ClientID).FirstOrDefaultAsync();

                List<string> EmailList = new List<string>();


                foreach (var contact in ContactPerson)
                {
                    EmailList.Add(contact.EmailAddress);
                }
                
                var subject = "Centurion Ice Invoice";


                await _mailer.SendInvoice(EmailList, subject, Client, file);

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Send Invoice",
                    ActionData = "Send Invoice " + InvoiceOrder.Invoice.InvoiceNumber,
                    EmployeeAction = "Sending",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);
                await _context.SaveChangesAsync();


                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }



        }



    }
}
