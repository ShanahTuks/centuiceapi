using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CentuIceAPI.Models;
using CentuIceAPI.DTO;
using CentuIceAPI.ViewModels;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CentuIceAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SaleController : ControllerBase
    {
        private readonly CentuIceDBContext _context;

        public SaleController(CentuIceDBContext context)
        {
            _context = context;
        }

        // GET: api/Sale
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Sale>>> GetSales()
        {
            return await _context.Sales.ToListAsync();
        }

        // GET: api/Sale/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Sale>> GetSale(Guid id)
        {
            var sale = await _context.Sales.FindAsync(id);

            if (sale == null)
            {
                return NotFound();
            }

            return sale;
        }

        // PUT: api/Sale/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSale(Guid id, Sale sale)
        {
            if (id != sale.SaleID)
            {
                return BadRequest();
            }

            _context.Entry(sale).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SaleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Sale
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Sale>> PostSale(Sale sale)
        {
            _context.Sales.Add(sale);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSale", new { id = sale.SaleID }, sale);
        }

        // DELETE: api/Sale/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSale(Guid id)
        {
            var sale = await _context.Sales.FindAsync(id);
            if (sale == null)
            {
                return NotFound();
            }

            _context.Sales.Remove(sale);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool SaleExists(Guid id)
        {
            return _context.Sales.Any(e => e.SaleID == id);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SaleVM>>> GetSalesList()
        {
            var Sales = await _context.Sales.Include(zz => zz.Invoice).Include(zz => zz.SaleStatus).OrderByDescending(zz => zz.SaleDate).ToListAsync();

            List<SaleVM> saleList = new();

            foreach (var CurrentSale in Sales)
            {
                SaleVM Sale = new();

                Sale.SaleID = CurrentSale.SaleID;
                Sale.SaleDate = CurrentSale.SaleDate;
                Sale.InvoiceID = CurrentSale.InvoiceID;
                Sale.InvoiceNumber = CurrentSale.Invoice.InvoiceNumber;
                Sale.SaleStatus = CurrentSale.SaleStatus.Description;

                saleList.Add(Sale);
            }

            var claimsIdentity = User.Identity as ClaimsIdentity;
            var UserID = claimsIdentity.FindFirst("UserId").Value;
            AuditTrail auditTrail = new()
            {
                AuditTrailID = Guid.NewGuid(),
                ActionDate = DateTime.Now,
                ActionDescription = "View Sales",
                ActionData = "N/A",
                EmployeeAction = "View",
                EmployeeID = Guid.Parse(UserID)

            };

            _context.AuditTrails.Add(auditTrail);

            await _context.SaveChangesAsync();

            return Ok(saleList);
        }



        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SaleLineDTO>>> GetSaleLines(Guid ID)
        {
            var SaleLines = await _context.SaleLines.Include(zz => zz.Product).Where(zz => zz.SaleID == ID).Include(zz => zz.Sale).ToListAsync();
            var Sale = await _context.Sales.Include(zz => zz.VatConfiguration).Where(zz => zz.SaleID != ID).FirstOrDefaultAsync();
            List<SaleLineDTO> saleLineDTOs = new();

            foreach (var sale in SaleLines)
            {
                var ProductPrice = await _context.ProductPrices.Where(zz => zz.ProductPriceID == sale.ProductPriceID).Include(zz => zz.Price).Select(zz => zz.Price.CurrentPrice).FirstOrDefaultAsync();
                SaleLineDTO line = new();
                line.SaleID = sale.SaleID;
                line.ProductID = sale.ProductID;
                line.ProductName = sale.Product.ProductName;
                line.ProductPrice = ProductPrice;
                line.Quantity = sale.Quantity;

                if (sale.Product.isVatApplicable == true)
                {
                    line.ProductVatActivated = true;
                }
                else
                {
                    line.ProductVatActivated = false;
                }

                if (sale.Sale.VatActivated == true)
                {
                    line.SaleVatActivated = true;
                }
                else
                {
                    line.SaleVatActivated = false;
                }


                if (line.SaleVatActivated == true && line.ProductVatActivated == true)
                {


                    line.VatPercentage = Sale.VatConfiguration.VatPercentage; 
                }
                else
                {
                    line.VatPercentage = 0;
                }

                

                saleLineDTOs.Add(line);
            }

            var claimsIdentity = User.Identity as ClaimsIdentity;
            var UserID = claimsIdentity.FindFirst("UserId").Value;
            AuditTrail auditTrail = new()
            {
                AuditTrailID = Guid.NewGuid(),
                ActionDate = DateTime.Now,
                ActionDescription = "View Sale Details",
                ActionData = "View Sale Details for Sale: " + ID,
                EmployeeAction = "View",
                EmployeeID = Guid.Parse(UserID)

            };

            _context.AuditTrails.Add(auditTrail);

            await _context.SaveChangesAsync();

            return Ok(saleLineDTOs);
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult> CreateSale(List<SaleLineDTO> NewSale)
        {

            try
            {
                Sale newSale = new();

                newSale.SaleID = Guid.NewGuid();
                newSale.SaleDate = DateTime.Now;
                newSale.InvoiceID = Guid.NewGuid();
                newSale.SaleStatusID = Guid.Parse("77cd235f-e443-414d-b2fb-8b374a0f7959");

                var VatConfig = await _context.VatConfigurations.OrderByDescending(zz => zz.DateSet).FirstOrDefaultAsync();
                newSale.VatConfigurationID = VatConfig.VatConfigurationID;
                if (VatConfig.Activated == true)
                {
                    newSale.VatActivated = true;
                }
                else
                {
                    newSale.VatActivated = false;
                }

                _context.Sales.Add(newSale);

                Invoice newInvoice = new();

                var Number = await _context.InvoiceNumbers.FirstOrDefaultAsync();
                newInvoice.InvoiceID = newSale.InvoiceID;
                newInvoice.InvoiceDate = newSale.SaleDate;
                newInvoice.InvoiceStatusID = Guid.Parse("7ad6de50-3811-462b-8118-2257149154be");
                newInvoice.PaymentMethodID = Guid.Parse("d074225e-723f-4846-b385-32cff5a0adda");
                newInvoice.InvoiceNumber = "INV" + Number.Number.ToString("D8");

                Number.Number++;

                _context.InvoiceNumbers.Update(Number);

                _context.Invoices.Add(newInvoice);




                foreach (var sale in NewSale)
                {
                    var ProductPriceID = await _context.ProductPrices.Where(zz => zz.ProductID == sale.ProductID).OrderByDescending(zz => zz.Date).Select(zz => zz.ProductPriceID).FirstOrDefaultAsync();
                    var InventoryStock = await _context.Products.Where(zz => zz.ProductID == sale.ProductID).Include(zz => zz.Inventory).FirstOrDefaultAsync();

                    if (InventoryStock.Inventory.Quantity < sale.Quantity)
                    {
                        return Conflict(new { message = "Inventory Quantity To Little!" });
                    }

                    SaleLine newLine = new();

                    newLine.SaleID = newSale.SaleID;
                    newLine.ProductID = sale.ProductID;
                    newLine.Quantity = sale.Quantity;
                    newLine.ProductPriceID = ProductPriceID;

                    var product = await _context.Products.Include(zz => zz.Inventory).Where(zz => zz.ProductID == sale.ProductID).FirstOrDefaultAsync();

                    Inventory newInventory = new();

                    newInventory = product.Inventory;


                    newInventory.Quantity = newInventory.Quantity - newLine.Quantity;

                    _context.Inventories.Update(newInventory);


                    _context.SaleLines.Add(newLine);
                }

                var claimsIdentity = User.Identity as ClaimsIdentity;
                var UserID = claimsIdentity.FindFirst("UserId").Value;
                AuditTrail auditTrail = new()
                {
                    AuditTrailID = Guid.NewGuid(),
                    ActionDate = DateTime.Now,
                    ActionDescription = "Create Sales",
                    ActionData = "Create Sale : " + newSale.SaleID,
                    EmployeeAction = "Create",
                    EmployeeID = Guid.Parse(UserID)

                };

                _context.AuditTrails.Add(auditTrail);

               

                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }


        [Route("[action]")]
        [HttpDelete]
        public async Task<ActionResult> CancelSale(Guid ID)
        {
            try
            {
                var Sale = await _context.Sales.Where(zz => zz.SaleID == ID).Include(zz => zz.Invoice).FirstOrDefaultAsync();

                Sale.SaleStatusID = Guid.Parse("5321a73d-ca18-4770-94e3-4cbf28adffc4");
                Sale.Invoice.InvoiceStatusID = Guid.Parse("365b7848-2cb8-4c74-93d9-40ddb17e62f1");

                _context.Sales.Update(Sale);
                _context.Invoices.Update(Sale.Invoice);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500);
                throw;
            }
        }



    }
}
