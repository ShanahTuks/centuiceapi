﻿using System;
using System.Collections.Generic;
using CentuIceAPI.Models;

namespace CentuIceAPI.DTO
{
    public class ServiceDTO
    {
        public Service Service { get; set; }
        public List<ServiceJob> ServiceJobs { get; set; }
        public string AssetID { get; set; }
        public string AssetType { get; set; }
    }
}
