﻿using System;
using System.Collections.Generic;

namespace CentuIceAPI.DTO
{
    public class SaleDTO
    {
        public List<SaleLineDTO> SaleLine { get; set; }
        public SaleInvoiceDTO Invoice { get; set; }

    }
}
