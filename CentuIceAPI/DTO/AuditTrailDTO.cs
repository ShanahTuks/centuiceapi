﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.DTO
{
    public class AuditTrailDTO
    {
        public Guid AuditTrailID { get; set; }
        public Guid EmployeeID { get; set; }
        public DateTime ActionDate { get; set; }
        public string EmployeeAction { get; set; }
        public string ActionDescription { get; set; }
    }
}
