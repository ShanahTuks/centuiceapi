﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.DTO
{
    public class FullSupplierOrderDTO
    {
        public SupplierOrderDTO SupplierOrders { get; set; }
        public List<SupplierOrderLineDTO> SupplierOrderLines { get; set; }
    }
}
