﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.DTO
{
    public class SupplierOrderDTO
    {
        public Guid SupplierOrderID { get; set; }
        public Guid SupplierID { get; set; }
        public Guid SupplierOrderStatusID { get; set; }

        public string Details { get; set; }
        public DateTime? DateReceived { get; set; }
        public DateTime DatePlaced { get; set; }
    }
}
