﻿using System;
using System.Collections.Generic;
using CentuIceAPI.Models;
namespace CentuIceAPI.DTO
{
    public class OrderDTO
    {
        public Guid ClientID { get; set; }
        public Guid AddressID { get; set; }
        public List<OrderLine> OrderLines { get; set; }
        public DateTime DeliveryDate { get; set; }
    }
}
