﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.DTO
{
    public class EmployeeDTO
    {

        public Guid EmployeeID { get; set; }
        public Guid BankID { get; set; }
        public Guid AccountTypeID { get; set; }
        public Guid EmployeeQualificationID { get; set; }
        public Guid EmployeeTypeID { get; set; }
        public Guid EmployeeStatusID { get; set; }
        public Guid EmployeeTitleID { get; set; }
        public Guid TableStatusID { get; set; }

        public string KnownAsName { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string IDNumber { get; set; }
        public string HomeNumber { get; set; }
        public string PassportNumber { get; set; }
        public string CellNumber { get; set; }
        public string WorkNumber { get; set; }
        public string FingerprintAccountNumber { get; set; }
        public string Initials { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string SystemUsername { get; set; }
        public string SystemPassword { get; set; }
        public string BranchCode { get; set; }
        public string AccountNumber { get; set; }
        public string TaxNumber { get; set; }
        public string PassportCountry { get; set; }
        public DateTime YearCompleted { get; set; }
        public string InstitutionName { get; set; }
        public bool isLoggedIn { get; set; }
        public string OTP { get; set; }
    }
}
