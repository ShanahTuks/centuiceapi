﻿using System;
using System.Collections.Generic;

namespace CentuIceAPI.DTO
{
    public class AttendanceUploadDTO
    {
        public string AcNo {get;set;}
        public string Name { get; set; }
        public string Time { get; set; }
        public string State { get; set; }
        public string NewState { get; set; }
        public string Exception { get; set; }


    }
}
