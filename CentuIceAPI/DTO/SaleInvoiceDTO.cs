﻿using System;
namespace CentuIceAPI.DTO
{
    public class SaleInvoiceDTO
    {
        public Guid InvoiceBookID { get; set; }
        public string InvoiceNumber { get; set; }
    }
}
