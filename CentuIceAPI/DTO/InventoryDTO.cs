﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.DTO
{
    public class InventoryDTO
    {
        public Guid InventoryID { get; set; }
        public Guid SupplierID { get; set; }
        public Guid InventoryTypeID { get; set; }
        public string ItemName { get; set; }
        public decimal ItemPrice { get; set; }
        public int Quantity { get; set; }
    }
}
