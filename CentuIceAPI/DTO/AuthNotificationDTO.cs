﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.DTO
{
    public class AuthNotificationDTO
    {
        public Guid AuthNotificationID { get; set; }
        public Guid NotificationID { get; set; }
        public Guid SenderID { get; set; }
        public string SenderName { get; set; }
        public Guid OwnerID { get; set; }
        public Guid AuthNotificationTypeID { get; set; }
        public DateTime NotificationDateTime { get; set; }
        public string Identifier { get; set; }
        public Guid IdentifierID { get; set; }

        //Records whether the auth response has been handled yet
        public bool isHandled { get; set; }

        //Records the actual reponse of the Auth Request
        public bool AuthResponse { get; set; }
    }
}
