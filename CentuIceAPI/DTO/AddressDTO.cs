﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.DTO
{
    public class AddressDTO
    {
        public Guid AddressID { get; set; }
        public Guid AddressTypeID { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string SuburbName { get; set; }
        public int UnitNumber { get; set; }
        public string Province { get; set; }
        public string ComplexName { get; set; }
        public int StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string PostalCode { get; set; }
        public bool isActive { get; set; }
        
    }
}
