﻿using System;
namespace CentuIceAPI.DTO
{
    public class SaleLineDTO
    {
        public Guid SaleID { get; set; }
        public Guid ProductID { get; set; }
        public string ProductName { get; set; }
        public bool SaleVatActivated { get; set; }
        public bool ProductVatActivated { get; set; }
        public int Quantity { get; set; }
        public Decimal VatPercentage { get; set; }
        public decimal ProductPrice { get; set; }
    }
}
