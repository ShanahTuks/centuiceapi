﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.DTO
{
    public class StockWrittenOffDTO
    {
        public Guid StockWrittenOffID { get; set; }
        public Guid InventoryID { get; set; }
        public int Quantity { get; set; }
        public string Reason { get; set; }
        public DateTime WriteOffDate { get; set; }
    }
}
