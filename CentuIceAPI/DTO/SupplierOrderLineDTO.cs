﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.DTO
{
    public class SupplierOrderLineDTO
    {
        public Guid SupplierOrderLineID { get; set; }
        public Guid InventoryID { get; set; }
        public Guid SupplierOrderID { get; set; }
        public int Quantity { get; set; }
    }
}
