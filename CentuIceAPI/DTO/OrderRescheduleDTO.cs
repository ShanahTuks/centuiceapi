﻿using System;
namespace CentuIceAPI.DTO
{
    public class OrderRescheduleDTO
    {
  
        public Guid OrderID { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime NewDeliveryDate { get; set; }
    }
}
