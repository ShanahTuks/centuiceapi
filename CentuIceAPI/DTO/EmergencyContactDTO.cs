﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.DTO
{
    public class EmergencyContactDTO
    {
        public Guid EmergencyContactID { get; set; }
        public Guid EmployeeID { get; set; }
        public string ContactPerson { get; set; }
        public string ContactTelephoneNumber { get; set; }
        public string RelationshipToEmployee { get; set; }
    }
}
