﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.DTO
{
    public class ProductDTO
    {
        public Guid ProductID { get; set; }
        public Guid InventoryID { get; set; }
        public string ProductName { get; set; }
        public bool isVatApplicable { get; set; }
        public decimal ProductPrice { get; set; }
    }
}
