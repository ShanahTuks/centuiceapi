﻿using System;
namespace CentuIceAPI.DTO
{
    public class OrderLineDTO
    {
        public Guid OrderID { get; set; }
        public Guid ProductID { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public decimal ProductPrice { get; set; }
        public bool OrderVatActivated { get; set; }
        public bool ProductVatActivated { get; set; }
        public Decimal VatPercentage { get; set; }
    }
}
