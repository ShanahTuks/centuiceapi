﻿using CentuIceAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Services
{
    //db should be the context with the entity so we can get straight to the where
    public static class CheckGuid<T> where T:class
    {
        public static bool GuidEists(DbSet<T> context, Guid id)
        {
            if (context.Find(id) != null)
            {
                return true;
            }
            return false;
        }

    }
}
