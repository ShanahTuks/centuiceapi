﻿using CentuIceAPI.Entities;
using CentuIceAPI.Models;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Services.ServiceBase
{
    public class Mailer : IMailer
    {

        private readonly SmtpSettings smtpSettings;
        private readonly IWebHostEnvironment env;

        public Mailer(IOptions<SmtpSettings> _smtpSettings, IWebHostEnvironment _env)
        {
            smtpSettings = _smtpSettings.Value;
            env = _env;
        }

        public async Task SendInvoice(List<string> email, string subject, Client client, IFormCollection attachment)
        {
            

            try
            {

                
                var invoice = attachment.Files[0];

                

                var invoicepdf = await GetBytes(invoice);

                var message = new MimeMessage();

                var builder = new BodyBuilder()
                {
                    HtmlBody = GetInvoiceEmailBody(client)
                };





                
                builder.Attachments.Add("Invoice.pdf", invoicepdf, new ContentType("application", "pdf"));


                message.From.Add(new MailboxAddress(smtpSettings.SenderName, smtpSettings.SenderEmail));
                InternetAddressList Emails = new InternetAddressList();
                
                foreach (var mail in  email)
                {
                    Emails.Add(MailboxAddress.Parse(mail));
                }
                message.To.AddRange(Emails);
                message.Subject = subject;


                message.Body = builder.ToMessageBody();


                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    if (env.IsDevelopment())
                    {
                        await smtpClient.ConnectAsync(smtpSettings.Server, smtpSettings.Port, true);
                    }
                    else
                    {
                        await smtpClient.ConnectAsync(smtpSettings.Server);
                    }

                    await smtpClient.AuthenticateAsync(smtpSettings.Username, smtpSettings.Password);
                    await smtpClient.SendAsync(message);
                    await smtpClient.DisconnectAsync(true);
                }
            }
            catch (Exception ex)
            {
               
                Console.WriteLine(ex);
                throw;
            }



        }

        private async Task<byte[]> GetBytes(IFormFile formFile)
        {
            using (var memoryStream = new MemoryStream())
            {
                await formFile.CopyToAsync(memoryStream);
                return memoryStream.ToArray();
            }
        }

        public async Task SendOtpAsync(string email, string subject, Employee employee, string OTP)
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress(smtpSettings.SenderName, smtpSettings.SenderEmail));
                message.To.Add(MailboxAddress.Parse(email));
                message.Subject = subject;

                //var builder = new BodyBuilder();
                //builder.HtmlBody = GetForgotPasswordBody(employee, 5624558);

                message.Body = new TextPart("html")
                {
                    Text = GetForgotPasswordBody(employee, OTP)
                };

                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    if (env.IsDevelopment())
                    {
                        await client.ConnectAsync(smtpSettings.Server, smtpSettings.Port, true);
                    }
                    else
                    {
                        await client.ConnectAsync(smtpSettings.Server);
                    }

                    await client.AuthenticateAsync(smtpSettings.Username, smtpSettings.Password);
                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public string GetForgotPasswordBody(Employee employee, string OTP)
        {
            
            string Otp = OTP.ToString();

            return string.Format(@"<div style='text-align:center;'>
                                    <h1>Hi {1}, We heard through the grapevine that you have forgotten your password,</h1>
                                    <h1> we had to send you a email first to confirm that it's really you. </h1>
                                    <h3>Use the below OTP to reset your password on the website</h3>
                                      <p  style=' display: block;
                                                                    text-align: center;
                                                                    font-weight: bold;
                                                                    background-color: #008CBA;
                                                                    font-size: 16px;
                                                                    border-radius: 10px;
                                                                    color:#ffffff;
                                                                    cursor:pointer;
                                                                    width:100%;
                                                                    padding:10px;'>
                                        {0}
                                      </p>
                                    </form>
                                </div>", Otp, employee.FirstName);
        }

        public string GetInvoiceEmailBody(Client client)
        {
            return string.Format(@"<div style='text-align:center;'>
                                    <h3>Hi {0}, We are happy that you have chosen our business for your Ice Supply,</h1>
                                    <h3> Please find the attached invoice for your latest order. </h1>
                                    </form>
                                </div>", client.ClientName);
        }
    }
}
