﻿using CentuIceAPI.Models;
using CentuIceAPI.Services.ServiceBase;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.Security.Cryptography;
using CentuIceAPI.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace CentuIceAPI.Services
{
    public class AuthService : IAuthService
    {
        private readonly IConfiguration _config;
        private readonly CentuIceDBContext _context;

        public AuthService(IConfiguration config, CentuIceDBContext context)
        {
            _config = config;
            _context = context;
        }

        public Employee AuthenticateEmployee(LoginViewModel login)
        {
            var employee = _context.Employees
                .Include(em => em.EmployeeType)
                .Where(em => em.SystemUsername.ToLower() == login.SystemUsername.ToLower()
                    && em.SystemPassword == BuildHash(login.SystemPassword))
                .FirstOrDefault();

            return employee;
        }

        public bool AuthenticateToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = _config["Jwt:Issuer"],
                    ValidAudience = _config["Jwt:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]))
                }, out SecurityToken validatedToken);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public string BuildHash(string value)
        {
            // Create a SHA256   
            using SHA256 sha256Hash = SHA256.Create();
            // ComputeHash - returns byte array  
            byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(value));

            // Convert byte array to a string   
            StringBuilder builder = new StringBuilder();
            foreach (var t in bytes)
            {
                builder.Append(t.ToString("x2"));
            }
            return builder.ToString();
        }

        public string BuildToken(Employee employee)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim("Username", employee.SystemUsername),
                new Claim("Role", employee.EmployeeType.Description),
                new Claim("UserId", employee.EmployeeID.ToString())
            };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Audience"],
                claims,
                expires: DateTime.Now.AddHours(9),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
