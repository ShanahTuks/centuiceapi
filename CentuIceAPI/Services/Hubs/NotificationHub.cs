﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CentuIceAPI.DTO;
using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using Microsoft.AspNetCore.SignalR;

namespace CentuIceAPI.Services.Hubs
{
    public class NotificationHub : Hub
    {
        //public async Task SendNotification()
        //{
        //    await Clients.All.SendAsync("New Notification");
        //}

        public async Task RequestAuth(OwnerAuthRequestVM authNotification)
        {
            try
            {
                await Clients.All.SendAsync("Phone" + authNotification.AuthNotification.OwnerID + "AuthRequest", authNotification);
            }
            catch (Exception)
            {
                throw;
            }
            
        }

        public async Task AuthResponse(AuthNotification authNotification)
        {
            try
            {
                await Clients.All.SendAsync("PC" + authNotification.SenderID + "AuthResponse", authNotification);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task SendNotification(NotificationDTO notification)
        {
            try
            {
                await Clients.All.SendAsync("PC" + notification.EmployeeID + "NewNotification", notification);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
