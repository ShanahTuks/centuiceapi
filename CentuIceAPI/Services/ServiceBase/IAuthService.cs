﻿using CentuIceAPI.Models;
using CentuIceAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Services.ServiceBase
{
    public interface IAuthService
    {
        string BuildToken(Employee employee);
        string BuildHash(string value);
        Employee AuthenticateEmployee(LoginViewModel login);
        bool AuthenticateToken(string token);
    }
}
