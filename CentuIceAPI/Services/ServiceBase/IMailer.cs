﻿using CentuIceAPI.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI.Services.ServiceBase
{
    public interface IMailer
    {
        Task SendOtpAsync(string email, string subject, Employee employee, string OTP);
        Task SendInvoice(List<string> email, string subject, Client client, IFormCollection attachment);
    }
}
