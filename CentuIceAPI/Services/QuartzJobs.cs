﻿using System;
using Quartz;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using CentuIceAPI.Models;
using System.Linq;
using CentuIceAPI.Services.Hubs;
using CentuIceAPI.DTO;
using Microsoft.EntityFrameworkCore;

namespace CentuIceAPI.Services
{
    [DisallowConcurrentExecution]
    public class QuartzJobs : IJob
    {
       
        private readonly ILogger<QuartzJobs> _logger;
        private readonly NotificationHub _NotHub;
        private readonly CentuIceDBContext _context;


        public QuartzJobs(ILogger<QuartzJobs> logger, CentuIceDBContext context, NotificationHub _hubContext)
        {
            _NotHub = _hubContext;
            _context = context;
            _logger = logger;
        }

        public async Task Execute(IJobExecutionContext context)
        {

            

            try
            {
                var CurrentDate = DateTime.Now;

                var VehicleTimers = _context.NotificationTimers.AsEnumerable().Where(x => x.NotificationType == "Vehicle" && x.NotificationReason == "License Expiry").OrderByDescending(zz => zz.TimePeriod).ToList();

                //Vehicle 1st Check
                var Vehicles31 = _context.Vehicles.AsEnumerable().Where(x => Math.Floor((x.License_Expiry_Date - CurrentDate).TotalDays) == VehicleTimers[0].TimePeriod).ToList();

                if (Vehicles31 != null)
                {

                    foreach (var vehicle in Vehicles31)
                    {
                        NotificationDTO notificationDTO = new();

                        notificationDTO.NotificationID = Guid.NewGuid();
                        notificationDTO.NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5");
                        notificationDTO.EmployeeID = Guid.Parse("f11765a0-58d2-42e5-9c5c-34022016e8c6");
                        notificationDTO.NotificationTitle = "Vehicle" + " " + vehicle.Reg_Number + " License";
                        notificationDTO.NotificationDescription = "Vehicle" + " " + vehicle.Reg_Number + " License Expires in " + VehicleTimers[0].TimePeriod + " Days!";
                        notificationDTO.IsRead = false;
                        notificationDTO.Created = CurrentDate;

                        
                        Notification notification = new();

                        notification.NotificationID = notificationDTO.NotificationID;
                        notification.NotificationTypeID = notificationDTO.NotificationTypeID;
                        notification.EmployeeID = notificationDTO.EmployeeID;
                        notification.NotificationTitle = notificationDTO.NotificationTitle;
                        notification.NotificationDescription = notificationDTO.NotificationDescription;
                        notification.IsRead = notificationDTO.IsRead;
                        notification.Created = notificationDTO.Created;


                        _context.Notifications.Add(notification);
                        await _context.SaveChangesAsync();
                        await _NotHub.SendNotification(notificationDTO);
                    }



                }


                //Vehicle 2nd Check
                var Vehicles20 = _context.Vehicles.AsEnumerable().Where(x => Math.Floor((x.License_Expiry_Date - CurrentDate).TotalDays) == VehicleTimers[1].TimePeriod).ToList();

                if (Vehicles20 != null)
                {

                    foreach (var vehicle in Vehicles20)
                    {
                        NotificationDTO notificationDTO = new();

                        notificationDTO.NotificationID = Guid.NewGuid();
                        notificationDTO.NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5");
                        notificationDTO.EmployeeID = Guid.Parse("f11765a0-58d2-42e5-9c5c-34022016e8c6");
                        notificationDTO.NotificationTitle = "Vehicle" + " " + vehicle.Reg_Number + " License";
                        notificationDTO.NotificationDescription = "Vehicle" + " " + vehicle.Reg_Number + " License Expires " + VehicleTimers[1].TimePeriod + " Days!";
                        notificationDTO.IsRead = false;
                        notificationDTO.Created = CurrentDate;

                      
                        Notification notification = new();

                        notification.NotificationID = notificationDTO.NotificationID;
                        notification.NotificationTypeID = notificationDTO.NotificationTypeID;
                        notification.EmployeeID = notificationDTO.EmployeeID;
                        notification.NotificationTitle = notificationDTO.NotificationTitle;
                        notification.NotificationDescription = notificationDTO.NotificationDescription;
                        notification.IsRead = notificationDTO.IsRead;
                        notification.Created = notificationDTO.Created;


                        _context.Notifications.Add(notification);
                        await _context.SaveChangesAsync();
                        await _NotHub.SendNotification(notificationDTO);
                    }


                }




                //Vehicle 3rd Check
                var Vehicles5 = _context.Vehicles.AsEnumerable().Where(x => Math.Floor((x.License_Expiry_Date - CurrentDate).TotalDays) == VehicleTimers[2].TimePeriod).ToList();

                if (Vehicles5 != null)
                {


                    foreach (var vehicle in Vehicles5)
                    {


                        NotificationDTO notificationDTO = new();

                        notificationDTO.NotificationID = Guid.NewGuid();
                        notificationDTO.NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5");
                        notificationDTO.EmployeeID = Guid.Parse("f11765a0-58d2-42e5-9c5c-34022016e8c6");
                        notificationDTO.NotificationTitle = "URGENT: Vehicle" + " " + vehicle.Reg_Number + " License";
                        notificationDTO.NotificationDescription = "Vehicle" + " " + vehicle.Reg_Number + " License Expires " + VehicleTimers[2].TimePeriod + " Days!";
                        notificationDTO.IsRead = false;
                        notificationDTO.Created = CurrentDate;

                        
                        Notification notification = new();

                        notification.NotificationID = notificationDTO.NotificationID;
                        notification.NotificationTypeID = notificationDTO.NotificationTypeID;
                        notification.EmployeeID = notificationDTO.EmployeeID;
                        notification.NotificationTitle = notificationDTO.NotificationTitle;
                        notification.NotificationDescription = notificationDTO.NotificationDescription;
                        notification.IsRead = notificationDTO.IsRead;
                        notification.Created = notificationDTO.Created;


                        _context.Notifications.Add(notification);
                        await _context.SaveChangesAsync();
                        await _NotHub.SendNotification(notificationDTO);
                    }


                }

                var FilterTimer = _context.NotificationTimers.Where(x => x.NotificationType == "Filter").FirstOrDefault();


                //Water Filter Check 5 days

                var Filters = _context.Machines.AsEnumerable().Where(x => Math.Floor((CurrentDate - x.Last_Service_Date).TotalDays) == 30 - FilterTimer.TimePeriod).ToList();

                if (Filters != null)
                {


                    foreach (var filter in Filters)
                    {


                        NotificationDTO notificationDTO = new();

                        notificationDTO.NotificationID = Guid.NewGuid();
                        notificationDTO.NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5");
                        notificationDTO.EmployeeID = Guid.Parse("f11765a0-58d2-42e5-9c5c-34022016e8c6");
                        notificationDTO.NotificationTitle = "Water Filter" + " " + filter.Model + " Replacement";
                        notificationDTO.NotificationDescription = "Water Filter" + " " + filter.Model + " Replacement in 5 Days!";
                        notificationDTO.IsRead = false;
                        notificationDTO.Created = CurrentDate;

                        
                        Notification notification = new();

                        notification.NotificationID = notificationDTO.NotificationID;
                        notification.NotificationTypeID = notificationDTO.NotificationTypeID;
                        notification.EmployeeID = notificationDTO.EmployeeID;
                        notification.NotificationTitle = notificationDTO.NotificationTitle;
                        notification.NotificationDescription = notificationDTO.NotificationDescription;
                        notification.IsRead = notificationDTO.IsRead;
                        notification.Created = notificationDTO.Created;


                        _context.Notifications.Add(notification);
                        await _context.SaveChangesAsync();
                        await _NotHub.SendNotification(notificationDTO);
                    }


                }

                //Vehicle Mileage Check

                var VehiclesMileages = _context.Vehicles.AsEnumerable().ToList();

                if (VehiclesMileages != null)
                {
                    foreach (var vehicle in VehiclesMileages)
                    {
                        if (vehicle.Mileage - vehicle.LastServicedMileage == 30000)
                        {

                            NotificationDTO notificationDTO = new();

                            notificationDTO.NotificationID = Guid.NewGuid();
                            notificationDTO.NotificationTypeID = Guid.Parse("f351c16a-13cb-4572-81a7-cc45a486cee5");
                            notificationDTO.EmployeeID = Guid.Parse("f11765a0-58d2-42e5-9c5c-34022016e8c6");
                            notificationDTO.NotificationTitle = "Vehcile " + " " + vehicle.Reg_Number + " Service";
                            notificationDTO.NotificationDescription = "Vehicle" + " " + vehicle.Reg_Number + " is due for a Service";
                            notificationDTO.IsRead = false;
                            notificationDTO.Created = CurrentDate;

                            
                            Notification notification = new();

                            notification.NotificationID = notificationDTO.NotificationID;
                            notification.NotificationTypeID = notificationDTO.NotificationTypeID;
                            notification.EmployeeID = notificationDTO.EmployeeID;
                            notification.NotificationTitle = notificationDTO.NotificationTitle;
                            notification.NotificationDescription = notificationDTO.NotificationDescription;
                            notification.IsRead = notificationDTO.IsRead;
                            notification.Created = notificationDTO.Created;


                            _context.Notifications.Add(notification);
                            await _context.SaveChangesAsync();
                            await _NotHub.SendNotification(notificationDTO);
                        }
                    }
                }
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

           
        }
            


           
        }

        
    }

