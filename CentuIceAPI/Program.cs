using CentuIceAPI.Models;
using CentuIceAPI.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentuIceAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        private readonly CentuIceDBContext _context;

        public Program(CentuIceDBContext context)
        {
            _context = context;
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>

            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                }).ConfigureServices((hostContext, services) =>
                {
                    // Add the required Quartz.NET services
                    services.AddQuartz(q =>
                    {
                        
                        q.UseMicrosoftDependencyInjectionJobFactory();

                        var jobkey = new JobKey("QuartzJob");
                        q.AddJob<QuartzJobs>(opts => opts.WithIdentity(jobkey));
                        

                        q.AddTrigger(opts => opts.ForJob(jobkey).WithIdentity("QuartzJob-trigger").WithDailyTimeIntervalSchedule(s => s.WithIntervalInHours(24).OnMondayThroughFriday().StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(10, 00)).Build()));
                    });

                    // Add the Quartz.NET hosted service

                    services.AddQuartzHostedService(
                        q => q.WaitForJobsToComplete = true);

                    // other config
                });
         
    }
}
